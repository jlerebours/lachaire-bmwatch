/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50142
Source Host           : localhost:3306
Source Database       : apps_db

Target Server Type    : MYSQL
Target Server Version : 50142
File Encoding         : 65001

Date: 2014-08-01 10:17:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_android_categorie`
-- ----------------------------
DROP TABLE IF EXISTS `t_android_categorie`;
CREATE TABLE `t_android_categorie` (
  `categorie_android_id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_android_name` varchar(50) DEFAULT NULL,
  `categorie_android_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`categorie_android_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_android_categorie
-- ----------------------------
INSERT INTO `t_android_categorie` VALUES ('1', 'Books & Reference', 'https://play.google.com/store/apps/category/BOOKS_AND_REFERENCE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('2', 'Books & Reference', 'https://play.google.com/store/apps/category/BOOKS_AND_REFERENCE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('3', 'Business', 'https://play.google.com/store/apps/category/BUSINESS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('4', 'Business', 'https://play.google.com/store/apps/category/BUSINESS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('5', 'Comics', 'https://play.google.com/store/apps/category/COMICS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('6', 'Comics', 'https://play.google.com/store/apps/category/COMICS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('7', 'Communication', 'https://play.google.com/store/apps/category/COMMUNICATION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('8', 'Communication', 'https://play.google.com/store/apps/category/COMMUNICATION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('9', 'Education', 'https://play.google.com/store/apps/category/EDUCATION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('10', 'Education', 'https://play.google.com/store/apps/category/EDUCATION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('11', 'Entertainment', 'https://play.google.com/store/apps/category/ENTERTAINMENT/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('12', 'Entertainment', 'https://play.google.com/store/apps/category/ENTERTAINMENT/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('13', 'Finance', 'https://play.google.com/store/apps/category/FINANCE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('14', 'Finance', 'https://play.google.com/store/apps/category/FINANCE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('15', 'Health & Fitness', 'https://play.google.com/store/apps/category/HEALTH_AND_FITNESS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('16', 'Health & Fitness', 'https://play.google.com/store/apps/category/HEALTH_AND_FITNESS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('17', 'Libraries & Demo', 'https://play.google.com/store/apps/category/LIBRARIES_AND_DEMO/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('18', 'Libraries & Demo', 'https://play.google.com/store/apps/category/LIBRARIES_AND_DEMO/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('19', 'Lifestyle', 'https://play.google.com/store/apps/category/LIFESTYLE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('20', 'Lifestyle', 'https://play.google.com/store/apps/category/LIFESTYLE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('21', 'Live Wallpaper', 'https://play.google.com/store/apps/category/APP_WALLPAPER/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('22', 'Live Wallpaper', 'https://play.google.com/store/apps/category/APP_WALLPAPER/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('23', 'Media & Video', 'https://play.google.com/store/apps/category/MEDIA_AND_VIDEO/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('24', 'Media & Video', 'https://play.google.com/store/apps/category/MEDIA_AND_VIDEO/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('25', 'Medical', 'https://play.google.com/store/apps/category/MEDICAL/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('26', 'Medical', 'https://play.google.com/store/apps/category/MEDICAL/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('27', 'Music & Audio', 'https://play.google.com/store/apps/category/MUSIC_AND_AUDIO/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('28', 'Music & Audio', 'https://play.google.com/store/apps/category/MUSIC_AND_AUDIO/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('29', 'News & Magazines', 'https://play.google.com/store/apps/category/NEWS_AND_MAGAZINES/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('30', 'News & Magazines', 'https://play.google.com/store/apps/category/NEWS_AND_MAGAZINES/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('31', 'Personalization', 'https://play.google.com/store/apps/category/PERSONALIZATION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('32', 'Personalization', 'https://play.google.com/store/apps/category/PERSONALIZATION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('33', 'Photography', 'https://play.google.com/store/apps/category/PHOTOGRAPHY/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('34', 'Photography', 'https://play.google.com/store/apps/category/PHOTOGRAPHY/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('35', 'Productivity', 'https://play.google.com/store/apps/category/PRODUCTIVITY/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('36', 'Productivity', 'https://play.google.com/store/apps/category/PRODUCTIVITY/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('37', 'Shopping', 'https://play.google.com/store/apps/category/SHOPPING/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('38', 'Shopping', 'https://play.google.com/store/apps/category/SHOPPING/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('39', 'Social', 'https://play.google.com/store/apps/category/SOCIAL/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('40', 'Social', 'https://play.google.com/store/apps/category/SOCIAL/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('41', 'Sports', 'https://play.google.com/store/apps/category/SPORTS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('42', 'Sports', 'https://play.google.com/store/apps/category/SPORTS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('43', 'Tools', 'https://play.google.com/store/apps/category/TOOLS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('44', 'Tools', 'https://play.google.com/store/apps/category/TOOLS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('45', 'Transportation', 'https://play.google.com/store/apps/category/TRANSPORTATION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('46', 'Transportation', 'https://play.google.com/store/apps/category/TRANSPORTATION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('47', 'Travel & Local', 'https://play.google.com/store/apps/category/TRAVEL_AND_LOCAL/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('48', 'Travel & Local', 'https://play.google.com/store/apps/category/TRAVEL_AND_LOCAL/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('49', 'Weather', 'https://play.google.com/store/apps/category/WEATHER/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('50', 'Weather', 'https://play.google.com/store/apps/category/WEATHER/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('51', 'Widgets', 'https://play.google.com/store/apps/category/APP_WIDGETS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('52', 'Widgets', 'https://play.google.com/store/apps/category/APP_WIDGETS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('53', 'Action', 'https://play.google.com/store/apps/category/GAME_ACTION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('54', 'Action', 'https://play.google.com/store/apps/category/GAME_ACTION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('55', 'Adventure', 'https://play.google.com/store/apps/category/GAME_ADVENTURE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('56', 'Adventure', 'https://play.google.com/store/apps/category/GAME_ADVENTURE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('57', 'Arcade', 'https://play.google.com/store/apps/category/GAME_ARCADE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('58', 'Arcade', 'https://play.google.com/store/apps/category/GAME_ARCADE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('59', 'Board', 'https://play.google.com/store/apps/category/GAME_BOARD/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('60', 'Board', 'https://play.google.com/store/apps/category/GAME_BOARD/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('61', 'Card', 'https://play.google.com/store/apps/category/GAME_CARD/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('62', 'Card', 'https://play.google.com/store/apps/category/GAME_CARD/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('63', 'Casino', 'https://play.google.com/store/apps/category/GAME_CASINO/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('64', 'Casino', 'https://play.google.com/store/apps/category/GAME_CASINO/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('65', 'Casual', 'https://play.google.com/store/apps/category/GAME_CASUAL/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('66', 'Casual', 'https://play.google.com/store/apps/category/GAME_CASUAL/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('67', 'Educational', 'https://play.google.com/store/apps/category/GAME_EDUCATIONAL/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('68', 'Educational', 'https://play.google.com/store/apps/category/GAME_EDUCATIONAL/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('69', 'Family', 'https://play.google.com/store/apps/category/GAME_FAMILY/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('70', 'Family', 'https://play.google.com/store/apps/category/GAME_FAMILY/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('71', 'Music', 'https://play.google.com/store/apps/category/GAME_MUSIC/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('72', 'Music', 'https://play.google.com/store/apps/category/GAME_MUSIC/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('73', 'Puzzle', 'https://play.google.com/store/apps/category/GAME_PUZZLE/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('74', 'Puzzle', 'https://play.google.com/store/apps/category/GAME_PUZZLE/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('75', 'Racing', 'https://play.google.com/store/apps/category/GAME_RACING/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('76', 'Racing', 'https://play.google.com/store/apps/category/GAME_RACING/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('77', 'Role Playing', 'https://play.google.com/store/apps/category/GAME_ROLE_PLAYING/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('78', 'Role Playing', 'https://play.google.com/store/apps/category/GAME_ROLE_PLAYING/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('79', 'Simulation', 'https://play.google.com/store/apps/category/GAME_SIMULATION/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('80', 'Simulation', 'https://play.google.com/store/apps/category/GAME_SIMULATION/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('81', 'Sports', 'https://play.google.com/store/apps/category/GAME_SPORTS/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('82', 'Sports', 'https://play.google.com/store/apps/category/GAME_SPORTS/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('83', 'Strategy', 'https://play.google.com/store/apps/category/GAME_STRATEGY/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('84', 'Strategy', 'https://play.google.com/store/apps/category/GAME_STRATEGY/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('85', 'Trivia', 'https://play.google.com/store/apps/category/GAME_TRIVIA/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('86', 'Trivia', 'https://play.google.com/store/apps/category/GAME_TRIVIA/collection/topselling_free');
INSERT INTO `t_android_categorie` VALUES ('87', 'Word', 'https://play.google.com/store/apps/category/GAME_WORD/collection/topselling_paid');
INSERT INTO `t_android_categorie` VALUES ('88', 'Word', 'https://play.google.com/store/apps/category/GAME_WORD/collection/topselling_free');
