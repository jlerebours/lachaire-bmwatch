/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50142
Source Host           : localhost:3306
Source Database       : apps_db

Target Server Type    : MYSQL
Target Server Version : 50142
File Encoding         : 65001

Date: 2014-08-01 10:18:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_apple_categorie`
-- ----------------------------
DROP TABLE IF EXISTS `t_apple_categorie`;
CREATE TABLE `t_apple_categorie` (
  `apple_id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_apple_Name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `categorie_apple_Link` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`apple_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_apple_categorie
-- ----------------------------
INSERT INTO `t_apple_categorie` VALUES ('1', 'Books', 'https://itunes.apple.com/us/genre/ios-books/id6018?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('2', 'Business', 'https://itunes.apple.com/us/genre/ios-business/id6000?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('3', 'Catalogs', 'https://itunes.apple.com/us/genre/ios-catalogs/id6022?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('4', 'Education', 'https://itunes.apple.com/us/genre/ios-education/id6017?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('5', 'Entertainment', 'https://itunes.apple.com/us/genre/ios-entertainment/id6016?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('6', 'Finance', 'https://itunes.apple.com/us/genre/ios-finance/id6015?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('7', 'Food & Drink', 'https://itunes.apple.com/us/genre/ios-food-drink/id6023?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('8', 'Games', 'https://itunes.apple.com/us/genre/ios-games/id6014?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('9', 'Action', 'https://itunes.apple.com/us/genre/ios-games-action/id7001?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('10', 'Adventure', 'https://itunes.apple.com/us/genre/ios-games-adventure/id7002?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('11', 'Arcade', 'https://itunes.apple.com/us/genre/ios-games-arcade/id7003?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('12', 'Board', 'https://itunes.apple.com/us/genre/ios-games-board/id7004?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('13', 'Card', 'https://itunes.apple.com/us/genre/ios-games-card/id7005?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('14', 'Casino', 'https://itunes.apple.com/us/genre/ios-games-casino/id7006?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('15', 'Dice', 'https://itunes.apple.com/us/genre/ios-games-dice/id7007?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('16', 'Educational', 'https://itunes.apple.com/us/genre/ios-games-educational/id7008?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('17', 'Family', 'https://itunes.apple.com/us/genre/ios-games-family/id7009?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('18', 'Music', 'https://itunes.apple.com/us/genre/ios-games-music/id7011?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('19', 'Puzzle', 'https://itunes.apple.com/us/genre/ios-games-puzzle/id7012?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('20', 'Racing', 'https://itunes.apple.com/us/genre/ios-games-racing/id7013?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('21', 'Role Playing', 'https://itunes.apple.com/us/genre/ios-games-role-playing/id7014?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('22', 'Simulation', 'https://itunes.apple.com/us/genre/ios-games-simulation/id7015?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('23', 'Sports', 'https://itunes.apple.com/us/genre/ios-games-sports/id7016?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('24', 'Strategy', 'https://itunes.apple.com/us/genre/ios-games-strategy/id7017?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('25', 'Trivia', 'https://itunes.apple.com/us/genre/ios-games-trivia/id7018?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('26', 'Word', 'https://itunes.apple.com/us/genre/ios-games-word/id7019?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('27', 'Health & Fitness', 'https://itunes.apple.com/us/genre/ios-health-fitness/id6013?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('28', 'Lifestyle', 'https://itunes.apple.com/us/genre/ios-lifestyle/id6012?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('29', 'Medical', 'https://itunes.apple.com/us/genre/ios-medical/id6020?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('30', 'Music', 'https://itunes.apple.com/us/genre/ios-music/id6011?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('31', 'Navigation', 'https://itunes.apple.com/us/genre/ios-navigation/id6010?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('32', 'News', 'https://itunes.apple.com/us/genre/ios-news/id6009?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('33', 'Newsstand', 'https://itunes.apple.com/us/genre/ios-newsstand/id6021?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('34', 'Arts & Photography', 'https://itunes.apple.com/us/genre/ios-newsstand-arts-photography/id13007?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('35', 'Automotive', 'https://itunes.apple.com/us/genre/ios-newsstand-automotive/id13006?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('36', 'Brides & Weddings', 'https://itunes.apple.com/us/genre/ios-newsstand-brides-weddings/id13008?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('37', 'Business & Investing', 'https://itunes.apple.com/us/genre/ios-newsstand-business-investing/id13009?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('38', 'Children\'s Magazines', 'https://itunes.apple.com/us/genre/ios-newsstand-childrens-magazines/id13010?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('39', 'Computers & Internet', 'https://itunes.apple.com/us/genre/ios-newsstand-computers-internet/id13011?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('40', 'Cooking, Food & Drink', 'https://itunes.apple.com/us/genre/ios-newsstand-cooking-food/id13012?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('41', 'Crafts & Hobbies', 'https://itunes.apple.com/us/genre/ios-newsstand-crafts-hobbies/id13013?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('42', 'Electronics & Audio', 'https://itunes.apple.com/us/genre/ios-newsstand-electronics/id13014?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('43', 'Entertainment', 'https://itunes.apple.com/us/genre/ios-newsstand-entertainment/id13015?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('44', 'Fashion & Style', 'https://itunes.apple.com/us/genre/ios-newsstand-fashion-style/id13002?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('45', 'Health, Mind & Body', 'https://itunes.apple.com/us/genre/ios-newsstand-health-mind/id13017?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('46', 'History', 'https://itunes.apple.com/us/genre/ios-newsstand-history/id13018?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('47', 'Home & Garden', 'https://itunes.apple.com/us/genre/ios-newsstand-home-garden/id13003?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('48', 'Literary Magazines & Journals', 'https://itunes.apple.com/us/genre/ios-newsstand-literary-magazines/id13019?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('49', 'Men\'s Interest', 'https://itunes.apple.com/us/genre/ios-newsstand-mens-interest/id13020?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('50', 'Movies & Music', 'https://itunes.apple.com/us/genre/ios-newsstand-movies-music/id13021?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('51', 'News & Politics', 'https://itunes.apple.com/us/genre/ios-newsstand-news-politics/id13001?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('52', 'Outdoors & Nature', 'https://itunes.apple.com/us/genre/ios-newsstand-outdoors-nature/id13004?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('53', 'Parenting & Family', 'https://itunes.apple.com/us/genre/ios-newsstand-parenting-family/id13023?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('54', 'Pets', 'https://itunes.apple.com/us/genre/ios-newsstand-pets/id13024?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('55', 'Professional & Trade', 'https://itunes.apple.com/us/genre/ios-newsstand-professional/id13025?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('56', 'Regional News', 'https://itunes.apple.com/us/genre/ios-newsstand-regional-news/id13026?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('57', 'Science', 'https://itunes.apple.com/us/genre/ios-newsstand-science/id13027?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('58', 'Sports & Leisure', 'https://itunes.apple.com/us/genre/ios-newsstand-sports-leisure/id13005?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('59', 'Teens', 'https://itunes.apple.com/us/genre/ios-newsstand-teens/id13028?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('60', 'Travel & Regional', 'https://itunes.apple.com/us/genre/ios-newsstand-travel-regional/id13029?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('61', 'Women\'s Interest', 'https://itunes.apple.com/us/genre/ios-newsstand-womens-interest/id13030?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('62', 'Photo & Video', 'https://itunes.apple.com/us/genre/ios-photo-video/id6008?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('63', 'Productivity', 'https://itunes.apple.com/us/genre/ios-productivity/id6007?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('64', 'Reference', 'https://itunes.apple.com/us/genre/ios-reference/id6006?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('65', 'Social Networking', 'https://itunes.apple.com/us/genre/ios-social-networking/id6005?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('66', 'Sports', 'https://itunes.apple.com/us/genre/ios-sports/id6004?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('67', 'Travel', 'https://itunes.apple.com/us/genre/ios-travel/id6003?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('68', 'Utilities', 'https://itunes.apple.com/us/genre/ios-utilities/id6002?mt=8');
INSERT INTO `t_apple_categorie` VALUES ('69', 'Weather', 'https://itunes.apple.com/us/genre/ios-weather/id6001?mt=8');
