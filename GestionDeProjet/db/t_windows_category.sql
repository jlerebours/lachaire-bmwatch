/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50142
Source Host           : localhost:3306
Source Database       : apps_db

Target Server Type    : MYSQL
Target Server Version : 50142
File Encoding         : 65001

Date: 2014-08-01 10:18:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_windows_categorie`
-- ----------------------------
DROP TABLE IF EXISTS `t_windows_categorie`;
CREATE TABLE `t_windows_categorie` (
  `categorie_windows_id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_windows_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `categorie_windows_link` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`categorie_windows_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_windows_categorie
-- ----------------------------
INSERT INTO `t_windows_categorie` VALUES ('1', 'entertainment', 'http://www.windowsphone.com/en-us/store/top-free-apps/entertainment/entertainment');
INSERT INTO `t_windows_categorie` VALUES ('2', 'music + vcategorie_windows_ideo', 'http://www.windowsphone.com/en-us/store/top-free-apps/music-vcategorie_windows_ideo/musicandvcategorie_windows_ideo');
INSERT INTO `t_windows_categorie` VALUES ('3', 'tools + productivity', 'http://www.windowsphone.com/en-us/store/top-free-apps/tools-productivity/toolsandproductivity');
INSERT INTO `t_windows_categorie` VALUES ('4', 'lifestyle', 'http://www.windowsphone.com/en-us/store/top-free-apps/lifestyle/lifestyle');
INSERT INTO `t_windows_categorie` VALUES ('5', 'kcategorie_windows_ids + family', 'http://www.windowsphone.com/en-us/store/top-free-apps/kcategorie_windows_ids-family/kcategorie_windows_idsandfamily');
INSERT INTO `t_windows_categorie` VALUES ('6', 'news + weather', 'http://www.windowsphone.com/en-us/store/top-free-apps/news-weather/newsandweather');
INSERT INTO `t_windows_categorie` VALUES ('7', 'travel + navigation', 'http://www.windowsphone.com/en-us/store/top-free-apps/travel-navigation/travel');
INSERT INTO `t_windows_categorie` VALUES ('8', 'health + fitness', 'http://www.windowsphone.com/en-us/store/top-free-apps/health-fitness/healthandfitness');
INSERT INTO `t_windows_categorie` VALUES ('9', 'photo', 'http://www.windowsphone.com/en-us/store/top-free-apps/photo/photo');
INSERT INTO `t_windows_categorie` VALUES ('10', 'social', 'http://www.windowsphone.com/en-us/store/top-free-apps/social/social');
INSERT INTO `t_windows_categorie` VALUES ('11', 'sports', 'http://www.windowsphone.com/en-us/store/top-free-apps/sports/sports');
INSERT INTO `t_windows_categorie` VALUES ('12', 'personal finance', 'http://www.windowsphone.com/en-us/store/top-free-apps/personal-finance/finance');
INSERT INTO `t_windows_categorie` VALUES ('13', 'business', 'http://www.windowsphone.com/en-us/store/top-free-apps/business/business');
INSERT INTO `t_windows_categorie` VALUES ('14', 'books + reference', 'http://www.windowsphone.com/en-us/store/top-free-apps/books-reference/booksandreference');
INSERT INTO `t_windows_categorie` VALUES ('15', 'education', 'http://www.windowsphone.com/en-us/store/top-free-apps/education/education');
INSERT INTO `t_windows_categorie` VALUES ('16', 'government + politics', 'http://www.windowsphone.com/en-us/store/top-free-apps/government-politics/governmentpolitics');
INSERT INTO `t_windows_categorie` VALUES ('17', 'art + entertainment', 'http://www.windowsphone.com/en-us/store/top-free-apps/art-entertainment/artandentertainment');
INSERT INTO `t_windows_categorie` VALUES ('18', 'automotive', 'http://www.windowsphone.com/en-us/store/top-free-apps/automotive/automotive');
INSERT INTO `t_windows_categorie` VALUES ('19', 'community', 'http://www.windowsphone.com/en-us/store/top-free-apps/community/community');
INSERT INTO `t_windows_categorie` VALUES ('20', 'food + dining', 'http://www.windowsphone.com/en-us/store/top-free-apps/food-dining/foodanddining');
INSERT INTO `t_windows_categorie` VALUES ('21', 'out + about', 'http://www.windowsphone.com/en-us/store/top-free-apps/out-about/outandabout');
INSERT INTO `t_windows_categorie` VALUES ('22', 'shopping', 'http://www.windowsphone.com/en-us/store/top-free-apps/shopping/shopping');
INSERT INTO `t_windows_categorie` VALUES ('23', 'style + fashion', 'http://www.windowsphone.com/en-us/store/top-free-apps/style-fashion/styleandfashion');
INSERT INTO `t_windows_categorie` VALUES ('24', 'local + national', 'http://www.windowsphone.com/en-us/store/top-free-apps/local-national/localandnational');
INSERT INTO `t_windows_categorie` VALUES ('25', 'international', 'http://www.windowsphone.com/en-us/store/top-free-apps/international/international');
INSERT INTO `t_windows_categorie` VALUES ('26', 'city gucategorie_windows_ides', 'http://www.windowsphone.com/en-us/store/top-free-apps/city-gucategorie_windows_ides/citygucategorie_windows_ides');
INSERT INTO `t_windows_categorie` VALUES ('27', 'hotels', 'http://www.windowsphone.com/en-us/store/top-free-apps/hotels/hotels');
INSERT INTO `t_windows_categorie` VALUES ('28', 'language', 'http://www.windowsphone.com/en-us/store/top-free-apps/language/language');
INSERT INTO `t_windows_categorie` VALUES ('29', 'mapping', 'http://www.windowsphone.com/en-us/store/top-free-apps/mapping/mapping');
INSERT INTO `t_windows_categorie` VALUES ('30', 'navigation', 'http://www.windowsphone.com/en-us/store/top-free-apps/navigation/navigation');
INSERT INTO `t_windows_categorie` VALUES ('31', 'planning', 'http://www.windowsphone.com/en-us/store/top-free-apps/planning/planning');
INSERT INTO `t_windows_categorie` VALUES ('32', 'travel tools', 'http://www.windowsphone.com/en-us/store/top-free-apps/travel-tools/traveltools');
INSERT INTO `t_windows_categorie` VALUES ('33', 'traveling with kcategorie_windows_ids', 'http://www.windowsphone.com/en-us/store/top-free-apps/traveling-with-kcategorie_windows_ids/travelwithkcategorie_windows_ids');
INSERT INTO `t_windows_categorie` VALUES ('34', 'diet + nutrition', 'http://www.windowsphone.com/en-us/store/top-free-apps/diet-nutrition/dietandnutrition');
INSERT INTO `t_windows_categorie` VALUES ('35', 'fitness', 'http://www.windowsphone.com/en-us/store/top-free-apps/fitness/fitness');
INSERT INTO `t_windows_categorie` VALUES ('36', 'health', 'http://www.windowsphone.com/en-us/store/top-free-apps/health/health');
INSERT INTO `t_windows_categorie` VALUES ('37', 'ereader', 'http://www.windowsphone.com/en-us/store/top-free-apps/ereader/ereader');
INSERT INTO `t_windows_categorie` VALUES ('38', 'fiction', 'http://www.windowsphone.com/en-us/store/top-free-apps/fiction/fiction');
INSERT INTO `t_windows_categorie` VALUES ('39', 'non-fiction', 'http://www.windowsphone.com/en-us/store/top-free-apps/non-fiction/nonfiction');
INSERT INTO `t_windows_categorie` VALUES ('40', 'reference', 'http://www.windowsphone.com/en-us/store/top-free-apps/reference/reference');
INSERT INTO `t_windows_categorie` VALUES ('41', 'commentary', 'http://www.windowsphone.com/en-us/store/top-free-apps/commentary/commentary');
INSERT INTO `t_windows_categorie` VALUES ('42', 'government resources', 'http://www.windowsphone.com/en-us/store/top-free-apps/government-resources/govresources');
INSERT INTO `t_windows_categorie` VALUES ('43', 'legal issues', 'http://www.windowsphone.com/en-us/store/top-free-apps/legal-issues/legalissues');
INSERT INTO `t_windows_categorie` VALUES ('44', 'politics', 'http://www.windowsphone.com/en-us/store/top-free-apps/politics/politics');
