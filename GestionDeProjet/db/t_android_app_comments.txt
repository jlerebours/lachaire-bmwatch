CREATE TABLE `t_android_app_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentUserName` varchar(100) DEFAULT NULL,
  `commentUserGender` varchar(8) DEFAULT NULL,
  `commentTime` varchar(255) DEFAULT NULL,
  `commentRating` int(11) DEFAULT NULL,
  `commentContent` text,
  `commentUserGplus` varchar(255) DEFAULT NULL,
  `commentAppId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_app_reviews` (`commentAppId`),
  CONSTRAINT `fk_app_reviews` FOREIGN KEY (`commentAppId`) REFERENCES `t_android_app` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11939 DEFAULT CHARSET=utf8;

