/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  01/08/2014 15:19:22                      */
/*==============================================================*/


drop table if exists R_ENTITE_CARACTERISEE_PAR_BMS;

drop table if exists R_ENTITE_POSSEDE_CRITERES;

drop table if exists T_ANDROID_APP;

drop table if exists T_ANDROID_APP_COMMENTS;

drop table if exists T_ANDROID_CATEGORIE;

drop table if exists T_APPLE_APP;

drop table if exists T_APPLE_CATEGORIE;

drop table if exists T_BM;

drop table if exists T_CATEGORIE_GENERIQUE;

drop table if exists T_CRITERE;

drop table if exists T_CRITERE_VALEUR;

drop table if exists T_DIMENSION;

drop table if exists T_ENTITE_CARACTERISEE;

drop table if exists T_ENTREPRISE;

drop table if exists T_ENTREPRISE_MODIFIE;

drop table if exists T_ETAT_ENTITE;

drop table if exists T_FAQ;

drop table if exists T_NON_PORTEUR_ASSIGNE_MODERATEUR;

drop table if exists T_OCCURENCE;

drop table if exists T_OC_BM;

drop table if exists T_PRODUIT_SERVICE;

drop table if exists T_PRODUIT_SERVICE_MODIFIE;

drop table if exists T_SOCIAL;

drop table if exists T_STATUT;

drop table if exists T_STATUT_UTILISATEUR;

drop table if exists T_UTILISATEUR;

drop table if exists T_WINDOWS_APP;

drop table if exists T_WINDOWS_CATEGORIE;

/*==============================================================*/
/* Table : R_ENTITE_CARACTERISEE_PAR_BMS                        */
/*==============================================================*/
create table R_ENTITE_CARACTERISEE_PAR_BMS
(
   BM_ID                int not null,
   ENTITE_ID            int not null,
   STATUT               text,
   DATE                 date,
   COMMENTAIRE          text,
   primary key (BM_ID, ENTITE_ID)
);

/*==============================================================*/
/* Table : R_ENTITE_POSSEDE_CRITERES                            */
/*==============================================================*/
create table R_ENTITE_POSSEDE_CRITERES
(
   ENTITE_ID            int not null,
   CV_ID                int not null,
   CRITERE_VALEUR       text not null,
   primary key (ENTITE_ID, CV_ID)
);

/*==============================================================*/
/* Table : T_ANDROID_APP                                        */
/*==============================================================*/
create table T_ANDROID_APP
(
   APP_ID               int not null auto_increment,
   CATEGORIE_ANDROID_ID int,
   COMMENTE_ID          int not null,
   APP_NAME             varchar(255),
   APP_FIRM             varchar(255),
   APP_UPDATED          varchar(250),
   APP_PRICE            varchar(255),
   APP_SCORE            double,
   APP_COMMENTSNUM      varchar(255),
   APP_GPLUS_RECOMMENDNUM varchar(255),
   APP_DESC             text,
   APP_NEWFEATURES      text,
   APP_SIZE             varchar(255),
   APP_INTALLS          varchar(255),
   APP_CURRENTVERSION   varchar(255),
   APP_RIQUIREDOS       varchar(50),
   APP_CONTENTRATING    varchar(200),
   APP_WEBSITE          varchar(255),
   APP_DEVELOPEREMAIL   varchar(255),
   APP_LINK             varchar(255),
   APP_INDEXTIME        varchar(255),
   APP_SCREENSHOTPATH   varchar(255),
   APP_FACTIVAPATH      varchar(255),
   APP_CAPITALLQPATH    varchar(255),
   primary key (APP_ID)
);

/*==============================================================*/
/* Table : T_ANDROID_APP_COMMENTS                               */
/*==============================================================*/
create table T_ANDROID_APP_COMMENTS
(
   COMMENTE_ID          int not null auto_increment,
   COMMENT_USERNAME     varchar(255),
   COMMENT_USERGENDER   varchar(255),
   COMMENT_TIME         varchar(255),
   COMMENT_RATING       int,
   COMMENT_CONTENT      text,
   COMMENT_USERGPLUS    varchar(255),
   primary key (COMMENTE_ID)
);

/*==============================================================*/
/* Table : T_ANDROID_CATEGORIE                                  */
/*==============================================================*/
create table T_ANDROID_CATEGORIE
(
   CATEGORIE_ANDROID_ID int not null auto_increment,
   CATEGORIE_ANDROID_NAME varchar(255),
   CATEGORIE_ANDROID_LINK varchar(255),
   primary key (CATEGORIE_ANDROID_ID)
);

/*==============================================================*/
/* Table : T_APPLE_APP                                          */
/*==============================================================*/
create table T_APPLE_APP
(
   APPLE_ID             int not null auto_increment,
   CATEGORIE_APPLE_ID   int,
   APPLE_NAME           varchar(255),
   APPLE_FIRM           varchar(255),
   APPLE_UPDATED        varchar(255),
   APPLE_PRICE          varchar(255),
   APPLE_DESC           text,
   APPLE_NEWFEATURES    text,
   APPLE_SIZE           varchar(255),
   APPLE_CURRENTVERSION text,
   APPLE_CURRENTVERSIONSCORE bigint,
   APPLE_CURRENTVERSIONRATINGS int,
   APPLE_ALLVERSIONSSCORE bigint,
   APPLE_ALLVERSIONSRATINGS int,
   APPLE_REQUIREDOS     text,
   APPLE_WEBSITE        varchar(255),
   APPLE_LINK           varchar(255),
   APPLE_INDEXTIME      varchar(255),
   APPLE_SCREENSHOTPATH varchar(255),
   APPLE_FACTIVAPATH    varchar(255),
   APPLE_CAPITALLQPATH  varchar(255),
   primary key (APPLE_ID)
);

/*==============================================================*/
/* Table : T_APPLE_CATEGORIE                                    */
/*==============================================================*/
create table T_APPLE_CATEGORIE
(
   CATEGORIE_APPLE_ID   int not null auto_increment,
   CATEGORIE_APPLE_NAME varchar(255),
   CATEGORIE_APPLE_LINK varchar(255),
   primary key (CATEGORIE_APPLE_ID)
);

/*==============================================================*/
/* Table : T_BM                                                 */
/*==============================================================*/
create table T_BM
(
   BM_ID                int not null auto_increment,
   BM_NOM               text not null,
   BM_PROFIL            int not null,
   BM_PRINCIPE          text,
   BM_DECLINAISONS      text,
   BM_VARIANTES         text,
   primary key (BM_ID)
);

/*==============================================================*/
/* Table : T_CATEGORIE_GENERIQUE                                */
/*==============================================================*/
create table T_CATEGORIE_GENERIQUE
(
   CATEGORIE_ID         int not null auto_increment,
   T_C_CATEGORIE_ID     int,
   CATEGORIE_NOM        text not null,
   CATEGORIE_DESCRIPTION text,
   CATEGORIE_TYPE       varchar(255),
   primary key (CATEGORIE_ID)
);

/*==============================================================*/
/* Table : T_CRITERE                                            */
/*==============================================================*/
create table T_CRITERE
(
   CRITERE_ID           int not null auto_increment,
   CRITERE_NOM          text,
   CRITERE_TYPE         varchar(255),
   primary key (CRITERE_ID)
);

/*==============================================================*/
/* Table : T_CRITERE_VALEUR                                     */
/*==============================================================*/
create table T_CRITERE_VALEUR
(
   CV_ID                int not null auto_increment,
   CRITERE_ID           int,
   CV_NOM               varchar(50) not null,
   primary key (CV_ID)
);

/*==============================================================*/
/* Table : T_DIMENSION                                          */
/*==============================================================*/
create table T_DIMENSION
(
   DIM_ID               int not null auto_increment,
   DIM_NOM              text not null,
   DIM_DESCRIPTION      text,
   primary key (DIM_ID)
);

/*==============================================================*/
/* Table : T_ENTITE_CARACTERISEE                                */
/*==============================================================*/
create table T_ENTITE_CARACTERISEE
(
   ENTITE_ID            int not null auto_increment,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ENTREPRISE                                         */
/*==============================================================*/
create table T_ENTREPRISE
(
   ENTITE_ID            int not null,
   UTILISATEUR_ID       int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   ENTREPRISE_FORME_SOCIALE text,
   ENTREPRISE_SECTEUR_ACTIVITE text,
   ENTREPRISE_TECHNOLOGIES_UTILISEES text,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ENTREPRISE_MODIFIE                                 */
/*==============================================================*/
create table T_ENTREPRISE_MODIFIE
(
   ENTITE_ID            int not null,
   ID_MODIFICATEUR_ENTREPRISE int,
   UTILISATEUR_ID       int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   ENTREPRISE_FORME_SOCIALE text,
   ENTREPRISE_SECTEUR_ACTIVITE text,
   ENTREPRISE_TECHNOLOGIES_UTILISEES text,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ETAT_ENTITE                                        */
/*==============================================================*/
create table T_ETAT_ENTITE
(
   ETAT_ID              int not null auto_increment,
   ETAT_VALEUR          text not null,
   primary key (ETAT_ID)
);

/*==============================================================*/
/* Table : T_FAQ                                                */
/*==============================================================*/
create table T_FAQ
(
   FAQ_ID               int not null auto_increment,
   FAQ_CATEGORIE        varchar(255),
   FAQ_QUESTION         varchar(255),
   FAQ_REPONSE          text,
   primary key (FAQ_ID)
);

/*==============================================================*/
/* Table : T_NON_PORTEUR_ASSIGNE_MODERATEUR                     */
/*==============================================================*/
create table T_NON_PORTEUR_ASSIGNE_MODERATEUR
(
   ASSIGNATION_ID       int not null auto_increment,
   ENTITE_ID            int,
   NON_PORTEUR_ID       int not null,
   MODERATEUR_ID        int not null,
   primary key (ASSIGNATION_ID)
);

/*==============================================================*/
/* Table : T_OCCURENCE                                          */
/*==============================================================*/
create table T_OCCURENCE
(
   OC_ID                int not null auto_increment,
   DIM_ID               int not null,
   OC_NOM               varchar(50),
   OC_STATUT            bool,
   OC_DESCRIPTION       longtext,
   primary key (OC_ID)
);

/*==============================================================*/
/* Table : T_OC_BM                                              */
/*==============================================================*/
create table T_OC_BM
(
   BM_ID                int not null,
   OC_ID                int not null,
   OC_COMMENTAIRE       varchar(256),
   primary key (BM_ID, OC_ID)
);

/*==============================================================*/
/* Table : T_PRODUIT_SERVICE                                    */
/*==============================================================*/
create table T_PRODUIT_SERVICE
(
   ENTITE_ID            int not null,
   T_E_ENTITE_ID        int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   PRODUIT_SERVICE_RANKING decimal,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_PRODUIT_SERVICE_MODIFIE                            */
/*==============================================================*/
create table T_PRODUIT_SERVICE_MODIFIE
(
   ENTITE_ID            int not null,
   ID_MODIFICATEUR_PRODUIT int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   PRODUIT_SERVICE_RANKING decimal,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_SOCIAL                                             */
/*==============================================================*/
create table T_SOCIAL
(
   SOCIAL_ID            int not null auto_increment,
   UTILISATEUR_ID       int,
   SOCIAL_IDENTIFIER    text not null,
   primary key (SOCIAL_ID)
);

/*==============================================================*/
/* Table : T_STATUT                                             */
/*==============================================================*/
create table T_STATUT
(
   STATUT_ID            int not null auto_increment,
   STATUT_TYPE          text not null,
   primary key (STATUT_ID)
);

/*==============================================================*/
/* Table : T_STATUT_UTILISATEUR                                 */
/*==============================================================*/
create table T_STATUT_UTILISATEUR
(
   UTILISATEUR_ID       int not null,
   STATUT_ID            int not null,
   primary key (UTILISATEUR_ID, STATUT_ID)
);

/*==============================================================*/
/* Table : T_UTILISATEUR                                        */
/*==============================================================*/
create table T_UTILISATEUR
(
   UTILISATEUR_ID       int not null auto_increment,
   UTILISATEUR_EMAIL    text not null,
   UTILISATEUR_MDP      varchar(256) not null,
   UTILISATEUR_NOM      text,
   UTILISATEUR_PRENOM   text,
   UTILISATEUR_GROUP    varchar(256),
   UTILISATEUR_INACTIF  bool,
   primary key (UTILISATEUR_ID)
);

/*==============================================================*/
/* Table : T_WINDOWS_APP                                        */
/*==============================================================*/
create table T_WINDOWS_APP
(
   WINDOWS_APP_ID       int not null auto_increment,
   CATEGORIE_WINDOWS_ID int,
   WINDOWS_APP_NAME     varchar(255),
   WINDOWS_APP_FIRM     varchar(255),
   WINDOWS_APP_UPDATED  varchar(255),
   WINDOWS_APP_PRICE    varchar(255),
   WINDOWS_APP_SCORE    bigint,
   WINDOWS_APP_COMMENTSNUM varchar(255),
   WINDOWS_APP_DESC     text,
   WINDOWS_APP_SIZE     varchar(255),
   WINDOWS_APP_CURRENTVERSION varchar(255),
   WINDOWS_APP_REQUIREDOS text,
   WINDOWS_APP_FBLIKE   int,
   WINDOWS_APP_TWITTERLIKE int,
   WINDOWS_APP_LINK     varchar(255),
   WINDOWS_APP_INDEXTIME varchar(255),
   WINDOWS_APP_SCREENSHOTPATH varchar(255),
   WINDOWS_APP_FACTIVAPATH varchar(255),
   WINDOWS_APP_CAPITALQPATH varchar(255),
   primary key (WINDOWS_APP_ID)
);

/*==============================================================*/
/* Table : T_WINDOWS_CATEGORIE                                  */
/*==============================================================*/
create table T_WINDOWS_CATEGORIE
(
   CATEGORIE_WINDOWS_ID int not null auto_increment,
   CATEGORIE_WINDOWS_NAME varchar(255),
   CATEGORIE_WINDOWS_LINK varchar(255),
   primary key (CATEGORIE_WINDOWS_ID)
);

alter table R_ENTITE_CARACTERISEE_PAR_BMS add constraint FK_R_ENTITE_CARACTERISEE_PAR_BMS foreign key (BM_ID)
      references T_BM (BM_ID) on delete restrict on update restrict;

alter table R_ENTITE_CARACTERISEE_PAR_BMS add constraint FK_R_ENTITE_CARACTERISEE_PAR_BMS2 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table R_ENTITE_POSSEDE_CRITERES add constraint FK_R_ENTITE_POSSEDE_CRITERES foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table R_ENTITE_POSSEDE_CRITERES add constraint FK_R_ENTITE_POSSEDE_CRITERES2 foreign key (CV_ID)
      references T_CRITERE_VALEUR (CV_ID) on delete restrict on update restrict;

alter table T_ANDROID_APP add constraint FK_RELATION_14 foreign key (COMMENTE_ID)
      references T_ANDROID_APP_COMMENTS (COMMENTE_ID) on delete restrict on update restrict;

alter table T_ANDROID_APP add constraint FK_RELATION_15 foreign key (CATEGORIE_ANDROID_ID)
      references T_ANDROID_CATEGORIE (CATEGORIE_ANDROID_ID) on delete restrict on update restrict;

alter table T_APPLE_APP add constraint FK_RELATION_16 foreign key (CATEGORIE_APPLE_ID)
      references T_APPLE_CATEGORIE (CATEGORIE_APPLE_ID) on delete restrict on update restrict;

alter table T_CATEGORIE_GENERIQUE add constraint FK_R_CAT_POSSEDE_CAT_PARENTE foreign key (T_C_CATEGORIE_ID)
      references T_CATEGORIE_GENERIQUE (CATEGORIE_ID) on delete restrict on update restrict;

alter table T_CRITERE_VALEUR add constraint FK_RELATION_11 foreign key (CRITERE_ID)
      references T_CRITERE (CRITERE_ID) on delete restrict on update restrict;

alter table T_ENTITE_CARACTERISEE add constraint FK_R_ENTITE_POSSEDE_CAT foreign key (CATEGORIE_ID)
      references T_CATEGORIE_GENERIQUE (CATEGORIE_ID) on delete restrict on update restrict;

alter table T_ENTITE_CARACTERISEE add constraint FK_R_ENTITE_POSSEDE_ETAT foreign key (ETAT_ID)
      references T_ETAT_ENTITE (ETAT_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE add constraint FK_HERITAGE_3 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE add constraint FK_RELATION_9 foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE_MODIFIE add constraint FK_H_ENTRMODIF_HERITE_ENTR foreign key (ENTITE_ID)
      references T_ENTREPRISE (ENTITE_ID) on delete restrict on update restrict;

alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add constraint FK_RELATION_12 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_OCCURENCE add constraint FK_RELATION_8 foreign key (DIM_ID)
      references T_DIMENSION (DIM_ID) on delete restrict on update restrict;

alter table T_OC_BM add constraint FK_T_OC_BM foreign key (BM_ID)
      references T_BM (BM_ID) on delete restrict on update restrict;

alter table T_OC_BM add constraint FK_T_OC_BM2 foreign key (OC_ID)
      references T_OCCURENCE (OC_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE add constraint FK_HERITAGE_4 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE add constraint FK_R_BM_POSSEDE_PS foreign key (T_E_ENTITE_ID)
      references T_ENTREPRISE (ENTITE_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE_MODIFIE add constraint FK_H_PSM_HERITE_PS foreign key (ENTITE_ID)
      references T_PRODUIT_SERVICE (ENTITE_ID) on delete restrict on update restrict;

alter table T_SOCIAL add constraint FK_RELATION_13 foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_STATUT_UTILISATEUR add constraint FK_T_STATUT_UTILISATEUR foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_STATUT_UTILISATEUR add constraint FK_T_STATUT_UTILISATEUR2 foreign key (STATUT_ID)
      references T_STATUT (STATUT_ID) on delete restrict on update restrict;

alter table T_WINDOWS_APP add constraint FK_RELATION_17 foreign key (CATEGORIE_WINDOWS_ID)
      references T_WINDOWS_CATEGORIE (CATEGORIE_WINDOWS_ID) on delete restrict on update restrict;


-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 24 Juin 2014 à 08:56
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bmwatch_test`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_dimension`
--

CREATE TABLE IF NOT EXISTS `T_DIMENSION` (
  `DIM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIM_NOM` text NOT NULL,
  `DIM_DESCRIPTION` text,
  PRIMARY KEY (`DIM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `T_DIMENSION`
--

INSERT INTO `T_DIMENSION` (`DIM_ID`, `DIM_NOM`, `DIM_DESCRIPTION`) VALUES
(1, 'Segments de clientèle', 'Une organisation cible un ou plusieurs segments de clients'),
(2, 'Propositions de valeur', 'L’organisation cherche à résoudre les problèmes des clients et à satisfaire leurs besoins avec des propositions de valeur.'),
(3, 'Canaux', 'Les propositions de valeur sont apportées aux clients via les canaux de communication, de distribution et de vente.'),
(4, 'Relations avec le client', 'L’organisation met en place et entretient des relations avec chaque segment de clientèle.'),
(5, 'Flux de revenus', 'Lorsqu''une proposition de valeur trouve le succès auprès de sa cible, elle génère des flux de revenus.'),
(6, 'Ressources clés', 'Les ressources clés sont les actifs requis pour proposer et délivrer les éléments décrits précédemment.'),
(7, 'Activités clés', '... En accomplissant un certain nombre d''activités clés.'),
(8, 'Partenaires clés', 'Certaines activités sont externalisées et certaines ressources sont acquises à l''extérieur de l''entreprise.'),
(9, 'Structure de coûts', 'Les éléments du modèle économique engendrent la structure des coûts.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


/* phpMyAdmin SQL Dum */
INSERT INTO `T_OCCURENCE` (`OC_ID`, `DIM_ID`,`OC_STATUT`, `OC_NOM`, `OC_DESCRIPTION`) VALUES
(1, 1, 1, 'Marché de masse ', 'Les modèles économiques centres sur les marches de masse ne distinguent pas plusieurs segments de clients. Les propositions de valeur, les canaux de distribution et les relations clients ciblent tous une masse de clients aux besoins et problèmes globalement similaires. On trouve souvent ce type de modèle dans le secteur de I ‘électronique grand public. '),
(2, 1, 1, 'Marché de niche ', 'Les modèles économiques ciblant les marches de niche desservent des segments de clientèle spécifiques, spécialisés. Les propositions de valeur, les canaux de distribution et les relations clients sont définis en fonction des besoins spécifiques d''un marché de niche. Ce type de modèles économiques s''observe souvent dans les relations fournisseur- acheteur. Par exemple, de nombreux fabricants de pièces détachées automobiles sont très dépendants des achats de grands constructeurs.\r\n'),
(3, 1, 1, 'Marché segmenté ', 'Certains modèles économiques distinguent des segments de marché aux besoins et problèmes légèrement différents. L''activité de détail d''une banque comme le Crédit Suisse, par exempte, distinguera les clients dont les actifs n''excèdent pas 100 000 dollars de ceux possédant une fortune de plus de 500 000 dollars. Les deux segments présentent des besoins et des problèmes distincts - ce qui a des implications au niveau des autres éléments du modèle économique du Crédit Suisse, notamment les propositions de valeur, les canaux de distribution, les relations clients et les flux de revenus. Autre exemple, celui de Micro Précision Systems, spécialiste de solutions externalisées de conception et de fabrication micromécaniques. L''entreprise sert trois segments de clients - l''industrie horlogère, l’industrie médicale et le secteur de l''automatisation industrielle - et apporte à chacun des propositions de valeur légèrement différentes.'),
(4, 1, 1, 'Marché diversifié ', 'Une organisation au modèle économique ciblant des clients diversifiés sert deux segments de clientèle totalement distincts, aux besoins et problèmes très différents. En 2006, Amazon.com a par exemple décidé de se diversifier en vendant des services de cloud Computing : utilisation de serveurs à la demande et espace de stockage en ligne. L''entreprise s''est ainsi positionnée auprès d''un segment de clients totalement différent - les sociétés Internet - avec une proposition de valeur complètement différente elle aussi. La logique stratégique qui sous-tend cette diversification réside dans la puissante infrastructure informatique d''Amazon, qui peut être partagée par ses activités de détail et la nouvelle unité de services de cloud Computing. Plates-formes multilatérales (ou marchés multilatéraux) Certaines organisations servent plusieurs segments de clientèle interdépendants. Une société de cartes de crédit, par exemple, a besoin à la fois d''une large base de détenteurs de cartes et d''une large base de commerçants qui les acceptent. De la même manière, un groupe de presse éditant un journal gratuit a besoin d''un nombre élevé de lecteurs pour attirer des annonceurs, mais aussi d''annonceurs pour financer la production et la distribution. Les deux segments sont requis pour que le modèle économique fonctionne (sur les plates-formes multilatérales, voir p. 76).'),
(5, 1, 1, 'Plates-formes multilatérales ', 'Certaines organisations servent plusieurs segments de clientèle interdépendants. Une société de cartes de crédit, par exemple, a besoin à la fois d''une large base de détenteurs de cartes et d''une large base de commerçants qui les acceptent. De la même manière, un groupe de presse éditant un journal gratuit a besoin d''un nombre élevé de lecteurs pour attirer des annonceurs, mais aussi d''annonceurs pour financer la production et la distribution. Les deux segments sont requis pour que le modèle économique fonctionne.'),
(7, 2, 1, 'Nouveauté', 'Certaines propositions de valeur apportent une réponse a un ensemble totalement inédit de besoins que les clients n''avaient pas perçus parce qu''il n''existait pas d''offre similaire. La technologie y tient souvent, mais pas toujours, une place importante. Les téléphones cellulaires, par exemple, ont donné naissance à toute une industrie autour des télécommunications mobiles.'),
(8, 2, 1, 'Performance', 'Améliorer les performances d''un produit ou d''un service est une manière banale de créer de la valeur. Le secteur des ordinateurs personnels, par exemple, y a longtemps eu recours en mettant régulièrement sur le marché des machines plus puissantes. Mais la recette a ses limites. Ainsi, ces dernières années, le lancement de PC plus rapides, dotes de capacités de stockage plus importantes et de meilleures performances graphiques, n''a pas entrain^ de progression correspondante de la demande pour autant.\r\n'),
(9, 2, 1, 'Personnalisation', 'Adapter les produits ou les services aux besoins particuliers de clients ou de segments de clients crée de la valeur. C''est ainsi que les concepts de personnalisation de masse ou de co-création avec le client, par exemple, ne cessent de gagner en importance. Cette approche permet la personnalisation tout en conservant le bénéfice des économies d''échelle.'),
(10, 2, 1, 'Accompagner', 'II est possible de créer de la valeur en aidant simplement le client à accomplir certaines choses. C''est une démarche que Rolls-Royce à parfaitement comprise: ses clients de l’aéronautique s''en remettent totalement a Rolls-Royce pour fabriquer les moteurs de tiers avions et en assurer la maintenance. Les clients peuvent are se concentrer sur la gestion de leurs compagnes aériennes. En échange, celles-ci payent à Rolls-Royce des honoraires pour chaque heure de fonctionnement des moteurs.'),
(11, 2, 1, 'Design', 'Le design est un élément important mais difficile à mesurer. Un produit peut se différencier par son design. Dans tes secteurs de la mode et de I ‘électronique grand public, il n''est pas rare que le design sort une composante majeure de la proposition de valeur.'),
(12, 2, 1, 'Marque/Statut', 'Les consommateurs peuvent trouver de la valeur dans le simple fait d''utiliser et d’arborer une marque donnée. Porter une Rolex, par exemple, est signe de richesse. A l’autre extrémité du spectre, les skate-boarders porteront les dernières marques « underground » pour montrer qu''ils sont « branchés ».'),
(13, 2, 1, 'Prix', 'Proposer une valeur similaire à un prix inférieur est un moyen classique de satisfaire les besoins des segments de clientèle sensibles à cet aspect. Mais les propositions de valeur fondées sur des prix bas ont des répercussions importantes sur les autres composantes du modèle économique. Les compagnies aériennes low-cost comme Southwest, easyJet ou Ryanair ont conçu de A à Z des modèles économiques spécifiques. Un autre exemple de proposition de valeur basée sur le prix est la Nano, la voiture la moins chère du monde, conçue et fabriquée par I’indien Tata. Les offres gratuites pénètrent de plus en plus de secteurs des journaux aux messageries Internet, en par les services de téléphonie mobile.'),
(14, 2, 1, 'Réduction des coûts', 'Aider les clients à réduire leurs couts est un vecteur important de création de valeur. Salesforce.com, par exemple, vend une application de CRM (Customer Relationship Management) hébergée. Les entreprises s''épargnent ainsi la dépense et le tracas de devoir acheter, installer et gérer elles-mêmes des logiciels de CRM.'),
(15, 2, 1, 'Réduction des risques ', 'La réduction des risques associes a I''achat de produits ou de services est une dimension à laquelle les clients sont sensibles. Pour I''acheteur d''une voiture d''occasion, par exemple, une garantie de service d''un an réduit le risque de pannes et de réparations. De même, une garantie de niveau de \r\nservice limite le risque encouru par I''acheteur de services informatiques externalises.\r\n'),
(16, 2, 1, 'Accessibilité', 'Rendre des produits et des services accessibles a des clients qui en étaient prives est une autre manière de créer de la valeur. Ce peut être le fruit d''un nouveau modèle économique, d''une innovation technologique ou d''une combinaison des deux. NetJets, par exemple, a adapté aux \r\njets prives le concept de multipropriété. Grace à un business model innovant, NetJets permet aux particuliers et aux entreprises de se déplacer en avion prive, service jusque-là inabordable pour la plupart. Les fonds mutualises sont un autre exemple de création de valeur par une accessibilité accrue. Ce produit financier innovant a donné la possibilité a des individus aux moyens financiers limites de se constituer des portefeuilles d''investissement diversifies.\r\n'),
(17, 2, 1, 'Commodité/Ergonomie ', 'Rendre les choses plus pratiques ou plus faciles à utiliser peut créer une valeur substantielle. Avec iPod et iTunes, Apple a offert aux consommateurs une facilite sans précèdent de recherche, d''achat, de téléchargement et d''écoute de musique numérique. La marque à la pomme domine désormais le marché.'),
(20, 3, 1, 'Force de vente', 'Force de vente'),
(21, 3, 1, 'Ventes en ligne', 'Ventes en ligne'),
(22, 3, 1, 'Magasin en propre', 'Magasin en propre'),
(23, 3, 1, 'Magasins des partenaires', 'Magasins des partenaires'),
(25, 3, 1, 'Grossiste', 'Grossiste'),
(26, 4, 1, 'Assistance personnelle', 'Cette relation repose sur l’interaction humaine. Le client a la possibilité de communiquer avec un conseiller de clientèle pour obtenir de I''aide au cours du processus de vente ou S Tissue de l’achat Cette interaction peut se dérouler sur te point de vente. via des centres d’appels ou par courriels entre autres.'),
(27, 4, 1, 'Assistance personnelle dédiée ', 'Un conseiller de clientèle est dédié à un client donné. C''est le type de relations le plus personnalise, qui se construit généralement sur le long terme. Dans le secteur de la banque privée, par exemple, des banquiers dédiés sont au service de clients présentant une valeur nette élevée. Ce type de relations s''observe également dans d''autres secteurs d''activité, c''est notamment le cas des gestionnaires de grands comptes qui entretiennent des relations individualisées avec des clients importants.'),
(28, 4, 1, 'Self-service ', 'Dans ce cas de figure, une entreprise n''entretient pas de relations directes avec les clients. Elle met à leur disposition tous les moyens nécessaires pour que ceux-ci se débrouillent par eux-mêmes.	\r\n'),
(29, 4, 1, 'Services automatisés', 'Ce type de relations combine une forme plus sophistiquée de self-service a des processus automatises. Par exemple, les profils personnels en ligne permettent aux clients d''accéder à des services personnalises. Les services automatisés sont capables de reconnaître les clients et leurs caractéristiques, et de leur fournir des informations relatives à des commandes ou des transactions. Les services automatisés les plus sophistiqués peuvent simuler une relation personnelle (par exemple, proposer des recommandations de livres ou de films).'),
(30, 4, 1, 'Communautés', 'Les entreprises ont de plus en plus recours aux communautés d''utilisateurs pour s''impliquer davantage auprès des clients/prospects et faciliter les connexions entre les membres de la communauté. De nombreuses entreprises entretiennent des communautés en ligne qui permettent aux utilisateurs d''échanger des connaissances et de résoudre leurs problèmes. Les communautés peuvent également aider les entreprises à mieux comprendre leurs clients. Le laboratoire GlaxoSmithKline a ainsi créé une communauté privée en ligne lors du lancement d''Alli, un nouveau produit amaigrissant délivré sans ordonnance. GlaxoSmithKline souhaitait mieux comprendre les difficultés rencontrées par les adultes en surpoids, et, partant, apprendre à mieux gérer les attentes des clients. '),
(31, 4, 1, 'Co-création', 'Les entreprises sont de plus en plus nombreuses à aller au-delà de la relation traditionnelle acheteur-vendeur pour co-créer de la valeur avec leurs clients. Amazon.com encourage ses clients à écrire des \r\ncritiques de livres et crée ainsi de la valeur pour les autres lecteurs. Certaines entreprises impliquent des clients dans la conception de produits nouveaux et innovants. D''autres, comme YouTube.com, sollicitent leurs clients pour créer du contenu qui sera disponible pour tous.\r\n\r\n'),
(32, 5, 1, 'Vente de biens', 'Le flux de revenus le plus courant découle de la vente de droits de propriété sur un produit physique. Amazon.com vend en ligne des livres, des CD, des produits électroniques, et bien d''autres choses. Fiat vend des voitures que les acheteurs sont libres de conduire, revendre ou même détruire. '),
(33, 5, 1, 'Droit d''usage', 'Ce flux de revenus est généré par I''utilisation d''un service donne. Plus le service est utilisé, plus le client paye. Un opérateur de télécommunications facturera ainsi à ses abonnes le nombre de minutes passées au téléphone. Un hôtel facture le nombre de nuits ou les chambres sont utilisées. Un service de livraison fait payer aux clients l''acheminement d''un colis d''un lieu à un autre.'),
(34, 5, 1, 'Abonnements', 'Ce flux de revenus découle de la vente d''un accès en continu à un service. Une salle de sport vend à ses membres des abonnements mensuels ou annuels en échange de l’accès à ses installations. Le jeu World of Warcraft Online permet aux utilisateurs de jouer en ligne moyennant le paiement d''un abonnement mensuel. Le service Comes with Music de Nokia donne accès à une bibliothèque musicale en ligne en contrepartie d''une cotisation. '),
(35, 5, 1, 'Location/Prêt ', 'Ce flux de revenus est créé en accordant de manière temporaire à quelqu''un le droit exclusif d''utiliser un actif donné pour un temps donné, en échange du versement d''une rémunération. Pour le loueur ou le prêteur, I''avantage est de bénéficier de revenus récurrents. La personne qui loue, quant à elle, ne consent la dépense que pour une période donnée au lieu d''assumer les coûts de propriété. Le concept de Zipcar en offre une excellente illustration. L’entreprise permet aux clients de louer des voitures à l’heure dans des villes d’Amérique du Nord, un service innovant qui a conduit de nombreux citadins à renoncer à l’achat d’une voiture.'),
(36, 5, 1, 'Licensing', 'Est accordée aux clients I''autorisation d''utiliser de la propriété intellectuelle protégée en échange d''un droit de licence. Ce système permet aux détenteurs de droits de tirer des revenus de leur propriété intellectuelle sans avoir à fabriquer un produit ou commercialiser un service. Cette démarche est courante dans I''industrie des médias, ou les propriétaires de contenu conservent leur copyright tout en vendant des licences d''utilisation à des tiers. De même, dans les secteurs technologiques, les détenteurs de brevets accordent à d''autres entreprises le droit d''utiliser leur technologie en contrepartie d''un droit de licence.'),
(37, 6, 1, 'Physiques', 'II s''agit des actifs physiques: sites de fabrication, immeubles, véhicules, machines, systèmes informatiques et réseaux de distribution. Les distributeurs comme Wal-Mart et Amazon.com sont lourdement dépendants de ressources physiques, souvent gourmandes en capital. Wal-Mart dispose d''un gigantesque réseau mondial de magasins et d''une infrastructure logistique à l''avenant L''activité d''Amazon repose également sur de solides infrastructures : informatique, entrepôts et logistiques.'),
(38, 6, 1, 'Intellectuelles ', 'Les ressources intellectuelles comme les marques, les savoirs propriétaires, les brevets et droits d’auteurs partenariats et les fichiers clients sont des éléments de plus en plus importants du modèle économiques. Ces ressources sont difficiles à développer une fois créées, elles sont source d''une valeur substantielle. Les marques constituent une ressource clé particulièrement importante pour des fabricants de produits de consommation comme Nike et Sony. Dans le cas de sociétés comme Microsoft et SAP, il s''agira de logiciels et autres contenus brevetés développés au cours de nombreuses années. Qualcomm, concepteur et fournisseur de microprocesseurs pour les appareils mobiles broadband, a construit son business model autour de puces brevetées dont I''entreprise retire des revenus de licence importants. '),
(39, 6, 1, 'Humaines', 'Toute entreprise a besoin de ressources humaines, mais elles sont plus proéminentes dans certains \r\nmodèles économiques que dans d''autres. Elles sont essentielles dans les secteurs ou la matière grise et la créativité sont au premier plan. Le modèle économique d''un laboratoire pharmaceutique comme Novartis repose ainsi sur une armée de chercheurs expérimentés et une force de vente étendue et spécialisée.\r\n'),
(40, 6, 1, 'Financières', 'Certains modèles économiques exigent des ressources financières et/ou des garanties financières - trésorerie, lignes de crédit ou stock-options pour engager des collaborateurs clés. Ericsson, le fabricant de produits de télécommunications, a fait des ressources financières une composante centrale de son business model. L''entreprise peut ainsi décider d''emprunter de l’argent auprès des banques et des marches financiers, pour ensuite utiliser une partie de ces fonds afin de proposer des solutions de financement à ses clients, s''assurant ainsi que des commandes soient passées auprès d''Ericsson plutôt que de ses concurrents.'),
(41, 7, 1, 'Production', 'Ces activités concernent la conception, la fabrication et la livraison d''un produit dans des quantités importantes et/ou de qualité supérieure. L’activité de production domine les modèles économiques des entreprises de fabrication. '),
(42, 7, 1, 'Résolution des problèmes', 'II s''agit ici de concevoir et de proposer de nouvelles solutions aux problèmes de clients donnes. La résolution de problèmes est au cœur des activités des sociétés de conseil, des établissements hospitaliers et des organisations de services en général. Leurs business models exigent des activités comme la gestion des connaissances et la formation continue.'),
(43, 7, 1, 'Plateforme/réseau Plateforme/réseau ', 'Dans les modèles économiques qui ont une plateforme comme ressource clé, les activités les plus importantes sont, en toute logique, liées à la plateforme ou au réseau. Réseaux, plateformes de mise en relation de l''offre et de la demande, logiciels et jusqu''aux marques peuvent avoir un rôle de plateforme. Le modèle économique d''eBay nécessite que l''entreprise développe et entretienne son site Internet eBay.com. Le business model de visa exige des activités liées à sa plate-forme de transactions pour les commerçants, les clients et les banques. Celui de Microsoft exige de gérer l''interface entre les logiciels d''autres marques et la plate-forme de son système d''exploitation Windows. Plus largement, les activités clés de ce type concernent la gestion et la promotion de la plateforme.'),
(44, 8, 1, 'Optimisation et économies d''échelle ', 'La forme la plus élémentaire de partenariat ou de relation acheteur-vendeur vise à optimiser l''allocation des ressources et des activités. II est absurde pour une entreprise de posséder toutes les ressources ou de réaliser elle-même toutes les activités. On noue généralement ce type de partenariat en vue de réduire les couts et il s''accompagne souvent de l''externalisation ou du partage des infrastructures. '),
(45, 8, 1, 'Réduction du risque et de l''incertitude ', 'Les partenariats peuvent contribuer a réduire le risque dans un environnement concurrentiel caractérisé par l''incertitude. II n''est pas rare que des concurrents forment une alliance stratégique dans un domaine tout en étant en compétition par ailleurs. Le format de disque optique Blu-ray, par exemple, a été développé par un groupe réunissant les plus grands fabricants mondiaux de produits électroniques, d''ordinateurs et de medias. Les membres du groupe ont coopéré pour lancer cette technologie, ce qui ne les empêche pas d''être en concurrence pour vendre leurs propres produits Blu-ray. \r\n'),
(46, 8, 1, 'Acquisition de certaines ressources et activités ', 'Peu d''entreprises possèdent toutes les ressources ou réalisent elles-mêmes toutes les activités décrites dans leur modèle économique. Elles prolongent leurs capacités en se tournant vers d''autres entreprises, qui leur fournissent des ressources ou prennent en charge certaines activités. Ce type de partenariat peut être motive par le besoin d''acquérir des connaissances, des licences ou d''accéder aux clients. Un fabricant de téléphones mobiles, par exemple, achètera sous licence le système d''exploitation qui équipe ses appareils au lieu d''en développer un en interne. Un assureur pourra choisir de s''appuyer sur des courtiers indépendants pour vendre ses polices au lieu de créer sa propre force de vente.\r\n'),
(47, 9, 1, 'Logique de coûts ', 'Les modèles économiques reposant sur cette logique visent à minimiser les coûts partout où cela est possible. II s''agit de créer et de préserver la structure de coûts la plus légère possible, en recourant à des propositions de valeur fondées sur les prix bas, une automatisation maximale et une externalisation a grande échelle. Les compagnies aériennes dites low-cost, comme Southwest, easyJet et Ryanair, en sont l''illustration par excellence.\r\n'),
(48, 9, 1, 'Logique de valeur ', 'Lorsqu''elles conçoivent leur business model, certaines entreprises se préoccupent davantage de création de valeur que duplications en termes de coûts. Propositions de valeur premium et haut degré de personnalisation du service caractérisent en général les modèles économiques value-driven. Ainsi en va-t-il par exemple des hôtels de luxe avec leurs somptueuses installations et les services exclusifs proposés à la clientèle. \r\n'),
(49, 9, 1, 'Coûts fixes ', 'Les coûts sont les mêmes quel que soit le volume de biens ou de services produits. C''est le cas des salaires, des loyers et des installations de fabrication. Certaines entreprises, ainsi des sociétés de fabrication, sont caractérisées par une proportion élevée de coûts fixes.\r\n'),
(50, 9, 1, 'Coûts variables ', 'Les coûts varient en proportion du volume de biens ou de services produits. Certaines activités, les festivals de musique par exemple, se caractérisent par une part élevée de coûts variables. \r\n'),
(51, 9, 1, 'Economies d''échelle ', 'II s''agit des avantages de coûts dont bénéficie une entreprise à mesure que sa production augmente. Les grandes entreprises, par exemple, bénéficient de prix en volume plus intéressants que les petites structures. Le coût moyen par unité diminue avec l''augmentation de la production. \r\n'),
(52, 9, 1, 'Economies d''envergure ', 'II s''agit des avantages de coûts dont bénéficie une entreprise du fait d''une activité plus diversifiée. Dans une grande entreprise, les mêmes activités marketing ou les mêmes canaux de distribution soutiendront plusieurs produits.\r\n');


INSERT INTO `T_STATUT` (`STATUT_ID`, `STATUT_TYPE`) VALUES
(1, 'ADMINISTRATEUR'),
(2, 'MODERATEUR'),
(3, 'PORTEUR'),
(4, 'NONPORTEUR'),
(5, 'CHERCHEUR');

INSERT INTO `T_CATEGORIE_GENERIQUE` (`CATEGORIE_ID`, `T_C_CATEGORIE_ID`, `CATEGORIE_NOM`, `CATEGORIE_DESCRIPTION`, `CATEGORIE_TYPE`) VALUES
('1', NULL, 'categorie1', 'description1', '0'),
('2', NULL, 'categorie2', 'description2', '0'),
('3', NULL, 'categorie3', 'description3', '0'),
('4', NULL, 'categorie4', 'description4', '0'),
('5', '1', 'categorie5', 'description5', '0'),
('6', '1', 'categorie6', 'description6', '0'),
('7', '2', 'categorie7', 'description7', '0'),
('8', '2', 'categorie8', 'description8', '0'),
('9', '3', 'categorie9', 'description9', '0'),
('10', '3', 'categorie10', 'description10', '0'),
('11', '3', 'categorie11', 'description11', '0'),
('12', '6', 'categorie12', 'description12', '0'),
('13', '8', 'categorie13', 'description13', '0'),
('14', '10', 'categorie14', 'description14', '0'),
('15', NULL, 'categorie15', 'description15', '0'),
('16', '3', 'categorie16', 'description16', '0');


INSERT INTO `T_ETAT_ENTITE` (`ETAT_ID`, `ETAT_VALEUR`) VALUES
('1', 'VALIDE'),
('2', 'NOUVEAU'),
('3', 'ASSIGNER'),
('4', 'TRAITER'),
('5', 'MODIFIER'),
('6', 'AFAIRE'),
('7', 'INACTIF');

INSERT INTO `T_CRITERE` (`CRITERE_ID`, `CRITERE_NOM`, `CRITERE_TYPE`) VALUES
('1', 'critere1', '0'),
('2', 'critere2', '0'),
('3', 'critere3', '0'),
('4', 'critere4', '0'),
('5', 'critere5', '0'),
('6', 'critere6', '0');

INSERT INTO `T_CRITERE_VALEUR` (`CV_ID`, `CRITERE_ID`, `CV_NOM` ) VALUES
('1', '1', 'NUMERIQUE'),
('2', '2', 'CLEF'),
('3', '3','Valeur 1 critere3'),
('4', '3','Valeur 2 critere3'),
('5', '3','Valeur 3 critere3'),
('6', '4','Valeur 1 critere4'),
('7', '4','Valeur 2 critere4'),
('8', '4','Valeur 3 critere4'),
('9', '5','Valeur 1 critere5'),
('10', '5','Valeur 2 critere5'),
('11', '5','Valeur 3 critere5'),
('12', '6','Valeur 1 critere6'),
('13', '6','Valeur 2 critere6'),
('14', '6','Valeur 3 critere6');

alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add foreign key (MODERATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;
      
alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add foreign key (NON_PORTEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;
      
/*==============================================================*/
/* Admin                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('1', 'root@root.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'root', 'root', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '1');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '3');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '5');

/*==============================================================*/
/* modo                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('2', 'moderateur1@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo1', 'modo1', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('2', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('2', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('3', 'moderateur2@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo2', 'modo2', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('3', '2');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('4', 'moderateur3@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo2', 'modo2', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('4', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('4', '5');

/*==============================================================*/
/* porteur projet                      */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('5', 'porteur1@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur1', 'porteur1', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('5', '3');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('5', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('6', 'porteur2@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur2', 'porteur2', 'Porteurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('6', '3');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('7', 'porteur3@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur3', 'porteur3', 'Porteurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('7', '3');

/*==============================================================*/
/* non porteur                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('8', 'nonporteur1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1', 'nonporteur1', 'Groupe 12 - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('8', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('9', 'nonporteur2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2', 'nonporteur2', 'Etudiants - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('9', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('9', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('10', 'nonporteur3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3', 'nonporteur3', 'Etudiants - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('10', '4');

/*==============================================================*/
/* chercheur                      */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('11', 'chercheur1@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur1', 'chercheur1', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('11', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('12', 'chercheur2@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur2', 'chercheur2', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('12', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('13', 'chercheur3@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur3', 'chercheur3', 'Chercheurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('13', '5');

/*==============================================================*/
/* non porteur   avec groupe                    */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('14', 'nonporteur1Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe1', 'nonporteur1Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('14', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('15', 'nonporteur2Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe1', 'nonporteur2Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('15', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('15', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('16', 'nonporteur3Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe1', 'nonporteur3Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('16', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('17', 'nonporteur1Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe2', 'nonporteur1Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('17', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('18', 'nonporteur2Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe2', 'nonporteur2Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('18', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('19', 'nonporteur3Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe2', 'nonporteur3Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('19', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('20', 'nonporteur1Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe3', 'nonporteur1Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '5');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '3');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('21', 'nonporteur2Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe3', 'nonporteur2Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('21', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('22', 'nonporteur3Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe3', 'nonporteur3Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('22', '4');

/*==============================================================*/
/* BM                     */
/*==============================================================*/

/*==============================================================*/
/* profil     1                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (1, 'profile1', '1', 'pricnipe1', 'declinaisons1', 'variantes1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('1', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('7', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('20', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('26', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('32', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('37', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('41', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('44', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('47', '1');

/*==============================================================*/
/* profil     2                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (2, 'profile2', '1', 'pricnipe2', 'declinaisons2', 'variantes2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('2', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('8', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('21', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('27', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('33', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('38', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('42', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('45', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('48', '2');

/*==============================================================*/
/* profil     3                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (3, 'profile3', '1', 'pricnipe3', 'declinaisons3', 'variantes3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('3', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('9', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('22', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('34', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('39', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('43', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('46', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('49', '3');

/*==============================================================*/
/* BM perso       1           */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (4, 'bmperso1', '0', 'pricnipeperso1', 'declinaisonsperso1', 'variantesperso1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('9', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('34', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('49', '4');

/*==============================================================*/
/* BM perso       2           */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (5, 'bmperso2', '0', 'pricnipeperso2', 'declinaisonsperso2', 'variantesperso2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('3', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('7', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('37', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('44', '5');

/*==============================================================*/
/* entreprise nouvelle                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('1', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise1','https://www.google.fr/webhp#q=nouvelleEntreprise');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('2', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise2','https://www.google.fr/webhp#q=nouvelleEntreprise');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('3', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise3','https://www.google.fr/webhp#q=nouvelleEntreprise');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('1', '5', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise1','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('2', '6', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise2','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('3', '7', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise3','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '1', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '2', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '3', 'ACTUEL', '2014-06-01', 'premier ajout');

/*==============================================================*/
/* critere entreprise nouvelle 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '1', '100');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '2', 'info');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '6', 'Valeur 1 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* critere entreprise nouvelle 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '1', '10');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '2', 'web; info');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere entreprise nouvelle 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '1', '1000');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '2', 'coiffure');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '5', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '11', 'Valeur 3 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* produit nouveau                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('4', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit1','https://www.google.fr/webhp#q=nouveauxProduit');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('5', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit2','https://www.google.fr/webhp#q=nouveauxProduit');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('6', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit3','https://www.google.fr/webhp#q=nouveauxProduit');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('4', '1', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit1','https://www.google.fr/webhp#q=nouveauxProduit', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('5', '2', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit2','https://www.google.fr/webhp#q=nouveauxProduit', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('6', '3', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit3','https://www.google.fr/webhp#q=nouveauxProduit', '5');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '4', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '5', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '6', 'ACTUEL', '2014-06-01', 'premier ajout');

/*==============================================================*/
/* critere produit nouveaux 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '1', '42');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '2', 'JAVA');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '11', 'Valeur 3 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* critere produit nouveaux 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '1', '69');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '2', 'PHP; JS; JSON; JQUERY');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere produit nouveaux 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '1', '1664');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '2', 'J2EE');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '5', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '6', 'Valeur 1 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* entreprise valide et modifier                     */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide1','https://www.google.fr/webhp#q=EntrepriseValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('8', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide2','https://www.google.fr/webhp#q=EntrepriseValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('9', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide3','https://www.google.fr/webhp#q=EntrepriseValide');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('7', '5', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide1','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('8', '6', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide2','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('9', '7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide3','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');

INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('7', '5', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier1','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');
INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('8', '6', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier2','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');
INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('9', '7', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier3','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '7', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '8', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '9', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '7', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('4', '8', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('5', '9', 'ACTUEL', '2014-06-02', 'maj');

/*==============================================================*/
/* critere entreprise valide 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '1', '123');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '2', 'web');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '3', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* critere entreprise valide 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '1', '432');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '2', 'PHP; JAVA');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere entreprise valide 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '1', '3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '2', 'PHP');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* produit valide et modifier                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('10', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide1','https://www.google.fr/webhp#q=ProduitValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('11', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide2','https://www.google.fr/webhp#q=ProduitValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('12', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide3','https://www.google.fr/webhp#q=ProduitValide');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('10', '7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide1','https://www.google.fr/webhp#q=ProduitValide', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('11', '8', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide2','https://www.google.fr/webhp#q=ProduitValide', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('12', '9', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide3','https://www.google.fr/webhp#q=ProduitValide', '5');

INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('10', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier1','https://www.google.fr/webhp#q=produitModifier1', '10');
INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('11', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier2','https://www.google.fr/webhp#q=produitModifier2', '10');
INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('12', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier3','https://www.google.fr/webhp#q=produitModifier3', '10');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '10', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '11', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '12', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '10', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('4', '11', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('5', '12', 'ACTUEL', '2014-06-02', 'maj');

/*==============================================================*/
/* entreprise a faire                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('13',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire1','https://www.google.fr/webhp#q=EntrepriseAFaire1');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('14',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire2','https://www.google.fr/webhp#q=EntrepriseAFaire2');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('15',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire3','https://www.google.fr/webhp#q=EntrepriseAFaire3');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('13',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire1','https://www.google.fr/webhp#q=EntrepriseAFaire1',  null,  null,  null);
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('14',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire2','https://www.google.fr/webhp#q=EntrepriseAFaire2',  null,  null,  null);
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('15',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire3','https://www.google.fr/webhp#q=EntrepriseAFaire3',  null,  null,  null);

/*==============================================================*/
/* produit a faire                     */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('16',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire1','https://www.google.fr/webhp#q=ProduitAFaire1');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('17',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire2','https://www.google.fr/webhp#q=ProduitAFaire2');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('18',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire3','https://www.google.fr/webhp#q=ProduitAFaire3');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('16', '13', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire1','https://www.google.fr/webhp#q=ProduitAFaire1', null);
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('17', '14', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire2','https://www.google.fr/webhp#q=ProduitAFaire2', null);
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('18', '15', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire3','https://www.google.fr/webhp#q=ProduitAFaire3', null);


/*==============================================================*/
/* FAQ                    */
/*==============================================================*/

INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","Where can I change an entity ?","You need to click on the button 'My assignation' and check what entity you need to change.");
INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","Where can I change informations about my conpanie or my product ?","You need to go to 'My companies & Products'.");
INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","Where can I add a new criteria ?","You need to click on 'Administration' then 'DataBase Administration' and select 'Add a new criterion'.");
INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","How can I make an assignment ?","You need to click on 'Administration' then 'Assignation'. After you choose non project holder and you assign them to an entity.");
INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","How can I change the BM of an entity ?","You need to go to the record of the entity, then you click on 'Modify' and you change all informations you want.");
INSERT INTO `T_FAQ` VALUES (NULL,"Navigation","How can I look the record of an entity ?","You need to go to your assignation and you must to click on the little button magnifying glass");
INSERT INTO `T_FAQ` VALUES (NULL,"Account","How to create an account ?","You must go to the login page and click the button 'Not subscribe yet'.");
INSERT INTO `T_FAQ` VALUES (NULL,"Account","How can I change my personal information ?","You must click on your name at the top right of the page and click on 'Edit profile'.");
INSERT INTO `T_FAQ` VALUES (NULL,"Account","How can I link my account with a social network ?","You need to go on the page 'Edit profile' and click on 'Link to a social network account'.");
INSERT INTO `T_FAQ` VALUES (NULL,"Account","Where can I reset my forgotten password ?","You can find the button 'Password forgotten' on the login page under 'Not subscribe yet'.");
