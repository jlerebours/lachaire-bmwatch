/*==============================================================*/
/* Admin                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('1', 'root@root.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'root', 'root', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '1');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '3');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('1', '5');

/*==============================================================*/
/* modo                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('2', 'moderateur1@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo1', 'modo1', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('2', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('2', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('3', 'moderateur2@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo2', 'modo2', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('3', '2');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('4', 'moderateur3@modo.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'modo2', 'modo2', 'Administration', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('4', '2');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('4', '5');

/*==============================================================*/
/* porteur projet                      */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('5', 'porteur1@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur1', 'porteur1', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('5', '3');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('5', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('6', 'porteur2@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur2', 'porteur2', 'Porteurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('6', '3');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('7', 'porteur3@porteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'porteur3', 'porteur3', 'Porteurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('7', '3');

/*==============================================================*/
/* non porteur                       */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('8', 'nonporteur1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1', 'nonporteur1', 'Groupe 12 - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('8', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('9', 'nonporteur2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2', 'nonporteur2', 'Etudiants - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('9', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('9', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('10', 'nonporteur3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3', 'nonporteur3', 'Etudiants - cours 1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('10', '4');

/*==============================================================*/
/* chercheur                      */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('11', 'chercheur1@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur1', 'chercheur1', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('11', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('12', 'chercheur2@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur2', 'chercheur2', 'Groupe 12', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('12', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('13', 'chercheur3@chercheur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'chercheur3', 'chercheur3', 'Chercheurs', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('13', '5');

/*==============================================================*/
/* non porteur   avec groupe                    */
/*==============================================================*/

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('14', 'nonporteur1Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe1', 'nonporteur1Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('14', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('15', 'nonporteur2Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe1', 'nonporteur2Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('15', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('15', '5');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('16', 'nonporteur3Groupe1@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe1', 'nonporteur3Groupe1', '1', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('16', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('17', 'nonporteur1Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe2', 'nonporteur1Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('17', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('18', 'nonporteur2Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe2', 'nonporteur2Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('18', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('19', 'nonporteur3Groupe2@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe2', 'nonporteur3Groupe2', '2', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('19', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('20', 'nonporteur1Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur1Groupe3', 'nonporteur1Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '4');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '5');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('20', '3');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('21', 'nonporteur2Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur2Groupe3', 'nonporteur2Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('21', '4');

INSERT INTO `T_UTILISATEUR` (`UTILISATEUR_ID`,  `UTILISATEUR_EMAIL`, `UTILISATEUR_MDP`, `UTILISATEUR_NOM`, `UTILISATEUR_PRENOM`, `UTILISATEUR_GROUP`, `UTILISATEUR_INACTIF` ) VALUES ('22', 'nonporteur3Groupe3@nonporteur.fr', 'aaba966314c52b061e5704b2b4643030e5097b20', 'nonporteur3Groupe3', 'nonporteur3Groupe3', '3', '0');
INSERT INTO `T_STATUT_UTILISATEUR` (`UTILISATEUR_ID`,`STATUT_ID`) VALUES ('22', '4');

/*==============================================================*/
/* BM                     */
/*==============================================================*/

/*==============================================================*/
/* profil     1                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (1, 'profile1', '1', 'pricnipe1', 'declinaisons1', 'variantes1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('1', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('7', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('20', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('26', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('32', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('37', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('41', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('44', '1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('47', '1');

/*==============================================================*/
/* profil     2                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (2, 'profile2', '1', 'pricnipe2', 'declinaisons2', 'variantes2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('2', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('8', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('21', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('27', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('33', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('38', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('42', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('45', '2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('48', '2');

/*==============================================================*/
/* profil     3                  */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (3, 'profile3', '1', 'pricnipe3', 'declinaisons3', 'variantes3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('3', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('9', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('22', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('34', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('39', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('43', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('46', '3');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('49', '3');

/*==============================================================*/
/* BM perso       1           */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (4, 'bmperso1', '0', 'pricnipeperso1', 'declinaisonsperso1', 'variantesperso1');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('9', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('34', '4');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('49', '4');

/*==============================================================*/
/* BM perso       2           */
/*==============================================================*/

INSERT INTO `T_BM` (`BM_ID`, `BM_NOM`, `BM_PROFIL`, `BM_PRINCIPE`, `BM_DECLINAISONS`, `BM_VARIANTES`) VALUES (5, 'bmperso2', '0', 'pricnipeperso2', 'declinaisonsperso2', 'variantesperso2');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('3', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('7', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('28', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('37', '5');
INSERT INTO `T_OC_BM` (`OC_ID`, `BM_ID`) VALUES ('44', '5');

/*==============================================================*/
/* entreprise nouvelle                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('1', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise1','https://www.google.fr/webhp#q=nouvelleEntreprise');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('2', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise2','https://www.google.fr/webhp#q=nouvelleEntreprise');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('3', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise3','https://www.google.fr/webhp#q=nouvelleEntreprise');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('1', '5', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise1','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('2', '6', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise2','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('3', '7', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouvelleEntreprise3','https://www.google.fr/webhp#q=nouvelleEntreprise', 'forme Sociale', 'secteur activité', 'techno utilisées');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '1', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '2', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '3', 'ACTUEL', '2014-06-01', 'premier ajout');

/*==============================================================*/
/* critere entreprise nouvelle 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '1', '100');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '2', 'info');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '6', 'Valeur 1 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('1', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* critere entreprise nouvelle 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '1', '10');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '2', 'web; info');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('2', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere entreprise nouvelle 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '1', '1000');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '2', 'coiffure');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '5', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '11', 'Valeur 3 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('3', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* produit nouveau                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('4', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit1','https://www.google.fr/webhp#q=nouveauxProduit');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('5', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit2','https://www.google.fr/webhp#q=nouveauxProduit');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('6', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit3','https://www.google.fr/webhp#q=nouveauxProduit');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('4', '1', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit1','https://www.google.fr/webhp#q=nouveauxProduit', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('5', '2', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit2','https://www.google.fr/webhp#q=nouveauxProduit', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('6', '3', '9', '2', 'description', '2014-07-01', '2014-07-01', 'nouveauxProduit3','https://www.google.fr/webhp#q=nouveauxProduit', '5');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '4', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '5', 'ACTUEL', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '6', 'ACTUEL', '2014-06-01', 'premier ajout');

/*==============================================================*/
/* critere produit nouveaux 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '1', '42');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '2', 'JAVA');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '11', 'Valeur 3 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('4', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* critere produit nouveaux 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '1', '69');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '2', 'PHP; JS; JSON; JQUERY');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('5', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere produit nouveaux 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '1', '1664');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '2', 'J2EE');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '5', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '6', 'Valeur 1 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('6', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* entreprise valide et modifier                     */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide1','https://www.google.fr/webhp#q=EntrepriseValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('8', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide2','https://www.google.fr/webhp#q=EntrepriseValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('9', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide3','https://www.google.fr/webhp#q=EntrepriseValide');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('7', '5', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide1','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('8', '6', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide2','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('9', '7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'EntrepriseValide3','https://www.google.fr/webhp#q=EntrepriseValide', 'forme Sociale', 'secteur activité', 'techno utilisées');

INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('7', '5', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier1','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');
INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('8', '6', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier2','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');
INSERT INTO `T_ENTREPRISE_MODIFIE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('9', '7', '9', '5', 'descriptionModifier', '2014-07-01', '2014-07-01', 'entrepriseModifier3','https://www.google.fr/webhp#q=entrepriseModifier', 'forme sociale modifier', 'secteur activite modifier', 'techno utilisées modiier');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '7', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '8', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '9', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '7', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('4', '8', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('5', '9', 'ACTUEL', '2014-06-02', 'maj');

/*==============================================================*/
/* critere entreprise valide 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '1', '123');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '2', 'web');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '3', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('7', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* critere entreprise valide 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '1', '432');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '2', 'PHP; JAVA');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('8', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere entreprise valide 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '1', '3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '2', 'PHP');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('9', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* produit valide et modifier                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('10', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide1','https://www.google.fr/webhp#q=ProduitValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('11', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide2','https://www.google.fr/webhp#q=ProduitValide');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('12', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide3','https://www.google.fr/webhp#q=ProduitValide');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('10', '7', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide1','https://www.google.fr/webhp#q=ProduitValide', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('11', '8', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide2','https://www.google.fr/webhp#q=ProduitValide', '5');
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('12', '9', '9', '1', 'description', '2014-07-01', '2014-07-01', 'ProduitValide3','https://www.google.fr/webhp#q=ProduitValide', '5');

INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('10', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier1','https://www.google.fr/webhp#q=produitModifier1', '10');
INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('11', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier2','https://www.google.fr/webhp#q=produitModifier2', '10');
INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('12', '9', '5', 'description modifier', '2014-07-01', '2014-07-01', 'produitModifier3','https://www.google.fr/webhp#q=produitModifier3', '10');

INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('1', '10', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('2', '11', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '12', 'ANCIEN', '2014-06-01', 'premier ajout');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('3', '10', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('4', '11', 'ACTUEL', '2014-06-02', 'maj');
INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` (`BM_ID`, `ENTITE_ID`, `STATUT`, `DATE`, `COMMENTAIRE`) VALUES ('5', '12', 'ACTUEL', '2014-06-02', 'maj');

/*==============================================================*/
/* critere produit valide 1                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '1', '123');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '2', 'web');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '3', 'Valeur 3 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '7', 'Valeur 2 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('10', '14', 'Valeur 3 critere6');

/*==============================================================*/
/* critere produit valide 2                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '1', '432');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '2', 'PHP; JAVA');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '4', 'Valeur 2 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '9', 'Valeur 1 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('11', '13', 'Valeur 2 critere6');

/*==============================================================*/
/* critere produit valide 3                      */
/*==============================================================*/

INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '1', '3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '2', 'PHP');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '3', 'Valeur 1 critere3');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '8', 'Valeur 3 critere4');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '10', 'Valeur 2 critere5');
INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, CV_ID, `CRITERE_VALEUR`) VALUES ('12', '12', 'Valeur 1 critere6');

/*==============================================================*/
/* entreprise a faire                      */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('13',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire1','https://www.google.fr/webhp#q=EntrepriseAFaire1');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('14',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire2','https://www.google.fr/webhp#q=EntrepriseAFaire2');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('15',  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire3','https://www.google.fr/webhp#q=EntrepriseAFaire3');

INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('13',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire1','https://www.google.fr/webhp#q=EntrepriseAFaire1',  null,  null,  null);
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('14',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire2','https://www.google.fr/webhp#q=EntrepriseAFaire2',  null,  null,  null);
INSERT INTO `T_ENTREPRISE` (`ENTITE_ID`, `UTILISATEUR_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `ENTREPRISE_FORME_SOCIALE`, `ENTREPRISE_SECTEUR_ACTIVITE`, `ENTREPRISE_TECHNOLOGIES_UTILISEES`) VALUES ('15',  null,  null, '6',  null, '2014-07-01', '2014-07-01', 'EntrepriseAFaire3','https://www.google.fr/webhp#q=EntrepriseAFaire3',  null,  null,  null);

/*==============================================================*/
/* produit a faire                     */
/*==============================================================*/

INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('16',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire1','https://www.google.fr/webhp#q=ProduitAFaire1');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('17',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire2','https://www.google.fr/webhp#q=ProduitAFaire2');
INSERT INTO `T_ENTITE_CARACTERISEE` (`ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`) VALUES ('18',  null, '6',  null, '2014-07-01', '2014-07-01', 'ProduitAFaire3','https://www.google.fr/webhp#q=ProduitAFaire3');

INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('16', '13', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire1','https://www.google.fr/webhp#q=ProduitAFaire1', null);
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('17', '14', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire2','https://www.google.fr/webhp#q=ProduitAFaire2', null);
INSERT INTO `T_PRODUIT_SERVICE` (`ENTITE_ID`, `T_E_ENTITE_ID`, `CATEGORIE_ID`, `ETAT_ID`, `ENTITE_DESCRIPTION`, `ENTITE_DATE_CREATION`, `ENTITE_DATE_DERNIERE_MODIF`, `ENTITE_NOM`, `ENTITE_SOURCE`, `PRODUIT_SERVICE_RANKING`) VALUES ('18', '15', null, '6', null, '2014-07-01', '2014-07-01', 'ProduitAFaire3','https://www.google.fr/webhp#q=ProduitAFaire3', null);

/*==============================================================*/
/* FAQ                    */
/*==============================================================*/

INSERT INTO `T_FAQ` VALUES ("Where can I change an entity ?","You need to click on the button 'My assignation' and check what entity you need to change.");
INSERT INTO `T_FAQ` VALUES ("Where can I change informations about my conpanie or my product ?","You need to go to 'My companies & Products'.");
INSERT INTO `T_FAQ` VALUES ("Where can I add a new criteria ?","You need to click on 'Administration' then 'DataBase Administration' and select 'Add a new criterion'.");
INSERT INTO `T_FAQ` VALUES ("How can I make an assignment ?","You need to click on 'Administration' then 'Assignation'. After you choose non project holder and you assign them to an entity.");
INSERT INTO `T_FAQ` VALUES ("How can I change the BM of an entity ?","You need to go to the record of the entity, then you click on 'Modify' and you change all informations you want.");
INSERT INTO `T_FAQ` VALUES ("How can I look the record of an entity ?","You need to go to your assignation and you must to click on the little button magnifying glass");
INSERT INTO `T_FAQ` VALUES ("How to create an account ?","You must go to the login page and click the button 'Not subscribe yet'.");
INSERT INTO `T_FAQ` VALUES ("How can I change my personal information ?","You must click on your name at the top right of the page and click on 'Edit profile'.");
INSERT INTO `T_FAQ` VALUES ("How can I link my account with a social network ?","You need to go on the page 'Edit profile' and click on 'Link to a social network account'.");
INSERT INTO `T_FAQ` VALUES ("Where can I reset my forgotten password ?","You can find the button 'Password forgotten' on the login page under 'Not subscribe yet'.");




