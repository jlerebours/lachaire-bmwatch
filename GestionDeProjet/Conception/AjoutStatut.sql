INSERT INTO `T_STATUT` (`STATUT_ID`, `STATUT_TYPE`) VALUES
(1, 'ADMINISTRATEUR'),
(2, 'MODERATEUR'),
(3, 'PORTEUR'),
(4, 'NONPORTEUR'),
(5, 'CHERCHEUR');

INSERT INTO `T_CATEGORIE_GENERIQUE` (`CATEGORIE_ID`, `T_C_CATEGORIE_ID`, `CATEGORIE_NOM`, `CATEGORIE_DESCRIPTION`) VALUES
('1', NULL, 'categorie1', 'description1'),
('2', NULL, 'categorie2', 'description2'),
('3', NULL, 'categorie3', 'description3'),
('4', NULL, 'categorie4', 'description4'),
('5', '1', 'categorie5', 'description5'),
('6', '1', 'categorie6', 'description6'),
('7', '2', 'categorie7', 'description7'),
('8', '2', 'categorie8', 'description8'),
('9', '3', 'categorie9', 'description9'),
('10', '3', 'categorie10', 'description10'),
('11', '3', 'categorie11', 'description11'),
('12', '6', 'categorie12', 'description12'),
('13', '8', 'categorie13', 'description13'),
('14', '10', 'categorie14', 'description14'),
('15', NULL, 'categorie15', 'description15'),
('16', '3', 'categorie16', 'description16');


INSERT INTO `T_ETAT_ENTITE` (`ETAT_ID`, `ETAT_VALEUR`) VALUES
('1', 'VALIDE'),
('2', 'NOUVEAU'),
('3', 'ASSIGNER'),
('4', 'TRAITER'),
('5', 'MODIFIER'),
('6', 'AFAIRE'),
('7', 'INACTIF');

INSERT INTO `T_CRITERE` (`CRITERE_ID`, `CRITERE_NOM`) VALUES
('1', 'critere1'),
('2', 'critere2'),
('3', 'critere3'),
('4', 'critere4'),
('5', 'critere5'),
('6', 'critere6');

INSERT INTO `T_CRITERE_VALEUR` (`CV_ID`, `CRITERE_ID`, `CV_NOM` ) VALUES
('1', '1', 'NUMERIQUE'),
('2', '2', 'CLEF'),
('3', '3','Valeur 1 critere3'),
('4', '3','Valeur 2 critere3'),
('5', '3','Valeur 3 critere3'),
('6', '4','Valeur 1 critere4'),
('7', '4','Valeur 2 critere4'),
('8', '4','Valeur 3 critere4'),
('9', '5','Valeur 1 critere5'),
('10', '5','Valeur 2 critere5'),
('11', '5','Valeur 3 critere5'),
('12', '6','Valeur 1 critere6'),
('13', '6','Valeur 2 critere6'),
('14', '6','Valeur 3 critere6');

alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add foreign key (MODERATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;
      
alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add foreign key (NON_PORTEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;