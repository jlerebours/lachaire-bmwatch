/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de cr�ation :  29/07/2014 10:59:47                      */
/*==============================================================*/


drop table if exists R_ENTITE_CARACTERISEE_PAR_BMS;

drop table if exists R_ENTITE_POSSEDE_CRITERES;

drop table if exists T_BM;

drop table if exists T_CATEGORIE_GENERIQUE;

drop table if exists T_CRITERE;

drop table if exists T_CRITERE_VALEUR;

drop table if exists T_DIMENSION;

drop table if exists T_ENTITE_CARACTERISEE;

drop table if exists T_ENTREPRISE;

drop table if exists T_ENTREPRISE_MODIFIE;

drop table if exists T_ETAT_ENTITE;

drop table if exists T_NON_PORTEUR_ASSIGNE_MODERATEUR;

drop table if exists T_OCCURENCE;

drop table if exists T_OC_BM;

drop table if exists T_PRODUIT_SERVICE;

drop table if exists T_PRODUIT_SERVICE_MODIFIE;

drop table if exists T_SOCIAL;

drop table if exists T_STATUT;

drop table if exists T_STATUT_UTILISATEUR;

drop table if exists T_UTILISATEUR;

/*==============================================================*/
/* Table : R_ENTITE_CARACTERISEE_PAR_BMS                        */
/*==============================================================*/
create table R_ENTITE_CARACTERISEE_PAR_BMS
(
   BM_ID                int not null,
   ENTITE_ID            int not null,
   STATUT               text,
   DATE                 date,
   COMMENTAIRE          text,
   primary key (BM_ID, ENTITE_ID)
);

/*==============================================================*/
/* Table : R_ENTITE_POSSEDE_CRITERES                            */
/*==============================================================*/
create table R_ENTITE_POSSEDE_CRITERES
(
   ENTITE_ID            int not null,
   CV_ID                int not null,
   CRITERE_VALEUR       text not null,
   primary key (ENTITE_ID, CV_ID)
);

/*==============================================================*/
/* Table : T_BM                                                 */
/*==============================================================*/
create table T_BM
(
   BM_ID                int not null auto_increment,
   BM_NOM               text not null,
   BM_PROFIL            int not null,
   BM_PRINCIPE          text,
   BM_DECLINAISONS      text,
   BM_VARIANTES         text,
   primary key (BM_ID)
);

/*==============================================================*/
/* Table : T_CATEGORIE_GENERIQUE                                */
/*==============================================================*/
create table T_CATEGORIE_GENERIQUE
(
   CATEGORIE_ID         int not null auto_increment,
   T_C_CATEGORIE_ID     int,
   CATEGORIE_NOM        text not null,
   CATEGORIE_DESCRIPTION text,
   CATEGORIE_TYPE       varchar(255),
   primary key (CATEGORIE_ID)
);

/*==============================================================*/
/* Table : T_CRITERE                                            */
/*==============================================================*/
create table T_CRITERE
(
   CRITERE_ID           int not null auto_increment,
   CRITERE_NOM          text,
   CRITERE_TYPE         varchar(255),
   primary key (CRITERE_ID)
);

/*==============================================================*/
/* Table : T_CRITERE_VALEUR                                     */
/*==============================================================*/
create table T_CRITERE_VALEUR
(
   CV_ID                int not null auto_increment,
   CRITERE_ID           int,
   CV_NOM               varchar(50) not null,
   primary key (CV_ID)
);

/*==============================================================*/
/* Table : T_DIMENSION                                          */
/*==============================================================*/
create table T_DIMENSION
(
   DIM_ID               int not null auto_increment,
   DIM_NOM              text not null,
   DIM_DESCRIPTION      text,
   primary key (DIM_ID)
);

/*==============================================================*/
/* Table : T_ENTITE_CARACTERISEE                                */
/*==============================================================*/
create table T_ENTITE_CARACTERISEE
(
   ENTITE_ID            int not null auto_increment,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ENTREPRISE                                         */
/*==============================================================*/
create table T_ENTREPRISE
(
   ENTITE_ID            int not null,
   UTILISATEUR_ID       int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   ENTREPRISE_FORME_SOCIALE text,
   ENTREPRISE_SECTEUR_ACTIVITE text,
   ENTREPRISE_TECHNOLOGIES_UTILISEES text,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ENTREPRISE_MODIFIE                                 */
/*==============================================================*/
create table T_ENTREPRISE_MODIFIE
(
   ENTITE_ID            int not null,
   UTILISATEUR_ID       int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   ENTREPRISE_FORME_SOCIALE text,
   ENTREPRISE_SECTEUR_ACTIVITE text,
   ENTREPRISE_TECHNOLOGIES_UTILISEES text,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_ETAT_ENTITE                                        */
/*==============================================================*/
create table T_ETAT_ENTITE
(
   ETAT_ID              int not null auto_increment,
   ETAT_VALEUR          text not null,
   primary key (ETAT_ID)
);

/*==============================================================*/
/* Table : T_NON_PORTEUR_ASSIGNE_MODERATEUR                     */
/*==============================================================*/
create table T_NON_PORTEUR_ASSIGNE_MODERATEUR
(
   ASSIGNATION_ID       int not null auto_increment,
   ENTITE_ID            int,
   NON_PORTEUR_ID       int not null,
   MODERATEUR_ID        int not null,
   primary key (ASSIGNATION_ID)
);

/*==============================================================*/
/* Table : T_OCCURENCE                                          */
/*==============================================================*/
create table T_OCCURENCE
(
   OC_ID                int not null auto_increment,
   DIM_ID               int not null,
   OC_NOM               varchar(50),
   OC_STATUT            bool,
   OC_DESCRIPTION       longtext,
   primary key (OC_ID)
);

/*==============================================================*/
/* Table : T_OC_BM                                              */
/*==============================================================*/
create table T_OC_BM
(
   BM_ID                int not null,
   OC_ID                int not null,
   OC_COMMENTAIRE       varchar(256),
   primary key (BM_ID, OC_ID)
);

/*==============================================================*/
/* Table : T_PRODUIT_SERVICE                                    */
/*==============================================================*/
create table T_PRODUIT_SERVICE
(
   ENTITE_ID            int not null,
   T_E_ENTITE_ID        int,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   PRODUIT_SERVICE_RANKING decimal,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_PRODUIT_SERVICE_MODIFIE                            */
/*==============================================================*/
create table T_PRODUIT_SERVICE_MODIFIE
(
   ENTITE_ID            int not null,
   CATEGORIE_ID         int,
   ETAT_ID              int,
   ENTITE_DESCRIPTION   text,
   ENTITE_DATE_CREATION date not null,
   ENTITE_DATE_DERNIERE_MODIF date not null,
   ENTITE_NOM           text not null,
   ENTITE_SOURCE        varchar(256),
   PRODUIT_SERVICE_RANKING decimal,
   primary key (ENTITE_ID)
);

/*==============================================================*/
/* Table : T_SOCIAL                                             */
/*==============================================================*/
create table T_SOCIAL
(
   SOCIAL_ID            int not null auto_increment,
   UTILISATEUR_ID       int,
   SOCIAL_IDENTIFIER    text not null,
   primary key (SOCIAL_ID)
);

/*==============================================================*/
/* Table : T_STATUT                                             */
/*==============================================================*/
create table T_STATUT
(
   STATUT_ID            int not null auto_increment,
   STATUT_TYPE          text not null,
   primary key (STATUT_ID)
);

/*==============================================================*/
/* Table : T_STATUT_UTILISATEUR                                 */
/*==============================================================*/
create table T_STATUT_UTILISATEUR
(
   UTILISATEUR_ID       int not null,
   STATUT_ID            int not null,
   primary key (UTILISATEUR_ID, STATUT_ID)
);

/*==============================================================*/
/* Table : T_UTILISATEUR                                        */
/*==============================================================*/
create table T_UTILISATEUR
(
   UTILISATEUR_ID       int not null auto_increment,
   UTILISATEUR_EMAIL    text not null,
   UTILISATEUR_MDP      varchar(256) not null,
   UTILISATEUR_NOM      text,
   UTILISATEUR_PRENOM   text,
   UTILISATEUR_GROUP    varchar(256),
   UTILISATEUR_INACTIF  bool,
   primary key (UTILISATEUR_ID)
);

alter table R_ENTITE_CARACTERISEE_PAR_BMS add constraint FK_R_ENTITE_CARACTERISEE_PAR_BMS foreign key (BM_ID)
      references T_BM (BM_ID) on delete restrict on update restrict;

alter table R_ENTITE_CARACTERISEE_PAR_BMS add constraint FK_R_ENTITE_CARACTERISEE_PAR_BMS2 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table R_ENTITE_POSSEDE_CRITERES add constraint FK_R_ENTITE_POSSEDE_CRITERES foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table R_ENTITE_POSSEDE_CRITERES add constraint FK_R_ENTITE_POSSEDE_CRITERES2 foreign key (CV_ID)
      references T_CRITERE_VALEUR (CV_ID) on delete restrict on update restrict;

alter table T_CATEGORIE_GENERIQUE add constraint FK_R_CAT_POSSEDE_CAT_PARENTE foreign key (T_C_CATEGORIE_ID)
      references T_CATEGORIE_GENERIQUE (CATEGORIE_ID) on delete restrict on update restrict;

alter table T_CRITERE_VALEUR add constraint FK_RELATION_11 foreign key (CRITERE_ID)
      references T_CRITERE (CRITERE_ID) on delete restrict on update restrict;

alter table T_ENTITE_CARACTERISEE add constraint FK_R_ENTITE_POSSEDE_CAT foreign key (CATEGORIE_ID)
      references T_CATEGORIE_GENERIQUE (CATEGORIE_ID) on delete restrict on update restrict;

alter table T_ENTITE_CARACTERISEE add constraint FK_R_ENTITE_POSSEDE_ETAT foreign key (ETAT_ID)
      references T_ETAT_ENTITE (ETAT_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE add constraint FK_HERITAGE_3 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE add constraint FK_RELATION_9 foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_ENTREPRISE_MODIFIE add constraint FK_H_ENTRMODIF_HERITE_ENTR foreign key (ENTITE_ID)
      references T_ENTREPRISE (ENTITE_ID) on delete restrict on update restrict;

alter table T_NON_PORTEUR_ASSIGNE_MODERATEUR add constraint FK_RELATION_12 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_OCCURENCE add constraint FK_RELATION_8 foreign key (DIM_ID)
      references T_DIMENSION (DIM_ID) on delete restrict on update restrict;

alter table T_OC_BM add constraint FK_T_OC_BM foreign key (BM_ID)
      references T_BM (BM_ID) on delete restrict on update restrict;

alter table T_OC_BM add constraint FK_T_OC_BM2 foreign key (OC_ID)
      references T_OCCURENCE (OC_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE add constraint FK_HERITAGE_4 foreign key (ENTITE_ID)
      references T_ENTITE_CARACTERISEE (ENTITE_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE add constraint FK_R_BM_POSSEDE_PS foreign key (T_E_ENTITE_ID)
      references T_ENTREPRISE (ENTITE_ID) on delete restrict on update restrict;

alter table T_PRODUIT_SERVICE_MODIFIE add constraint FK_H_PSM_HERITE_PS foreign key (ENTITE_ID)
      references T_PRODUIT_SERVICE (ENTITE_ID) on delete restrict on update restrict;

alter table T_SOCIAL add constraint FK_RELATION_13 foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_STATUT_UTILISATEUR add constraint FK_T_STATUT_UTILISATEUR foreign key (UTILISATEUR_ID)
      references T_UTILISATEUR (UTILISATEUR_ID) on delete restrict on update restrict;

alter table T_STATUT_UTILISATEUR add constraint FK_T_STATUT_UTILISATEUR2 foreign key (STATUT_ID)
      references T_STATUT (STATUT_ID) on delete restrict on update restrict;

