<?php
    require_once 'OccurenceClass.php';
    require_once 'AccesBDClass.php';

class Critere{
    private $id;
    private $nomCritere;
    private $type;
    private $typeCritere;
    
    public function __construct($id, $nomCritere='',$type=array(),$typeCritere=""){      
        $this->id = $id;
        $this->nomCritere = $nomCritere;
        $this->type = $type;
        $this->typeCritere = $typeCritere;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getType() {
        return $this->type;
    }

    public function getNomCritere() {
        return $this->nomCritere;
    }
 
    public function getTypeCritere() {
        return $this->typeCritere;
    }    
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setType($type) {
        $this->type = $type;
    }
    
    public function setNomCritere($nomCritere) {
        $this->nomCritere = $nomCritere;
    }
    
    public function setTypeCritere($typeCritere) {
        $this->typeCritere = $typeCritere;
    }    
    
    public function getInfos(){
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "SELECT * "
                    . " FROM `T_CRITERE` C, `T_CRITERE_VALEUR` CV"
                    . " WHERE C.`CRITERE_ID` = :id "
                    . " AND C.`CRITERE_ID` = CV.`CRITERE_ID` ";

            $reponse = $bdd->prepare($requete);

            $id = $this->getId();
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            
            $rows = $reponse->fetchAll();
            $tabValeursListe = array();
            foreach ($rows as $row) {
                $this->setNomCritere($row['CRITERE_NOM']);
                array_push($tabValeursListe,$row['CV_NOM']);
            }
            $this->setType($tabValeursListe);
            
        }
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
            . " FROM `T_CRITERE` "
            . " WHERE `CRITERE_ID` = :id " ;
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
    public function ajouterChampVide($type,$idCritere){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `R_ENTITE_POSSEDE_CRITERES` WHERE 1" ;
        $reponse = $bdd->prepare($requete);
        $reponse->execute(); 
        
        $rows = $reponse->fetchAll();
        $tabListeEntite = array();
        
        foreach ($rows as $row) {
            array_push($tabListeEntite,$row['ENTITE_ID']);
        }
        
        $tabListeEntite = array_unique($tabListeEntite);
        $valeur ="";
        
        $requete = "SELECT `CV_ID` FROM `T_CRITERE_VALEUR` WHERE `CRITERE_ID` = :idCritere" ;
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idCritere', $idCritere, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetch();
        
        $idCv = $rows[0];
        
        
        foreach ($tabListeEntite as $entite){
            $lentite = new Entite($entite);
            
            if($lentite->getType()==$type || $type ==""){
                $requete = "INSERT INTO `R_ENTITE_POSSEDE_CRITERES` (`ENTITE_ID`, `CV_ID`,`CRITERE_VALEUR`) "
                    . " VALUES (:idEntite,:idCV,:valeur)";
                $reponse = $bdd->prepare($requete);
                $reponse->bindParam(':idEntite', $entite, PDO::PARAM_INT);
                $reponse->bindParam(':idCV', $idCv, PDO::PARAM_INT);            
                $reponse->bindParam(':valeur', $valeur, PDO::PARAM_STR); 
                $reponse->execute();
            }
        }
        
    }
    
    public function ajouterALaBD(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Insertion dans la table T_CRITERE
        $requete = "INSERT INTO `T_CRITERE` (CRITERE_ID, `CRITERE_NOM`,`CRITERE_TYPE`) "
                . " VALUES (NULL,:nom,:type)";

        $reponse = $bdd->prepare($requete);
        $nom = $this->getNomCritere();
        $type = $this->getTypeCritere();
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR);
        $reponse->bindParam(':type', $type, PDO::PARAM_STR);
        $reponse->execute();
        
        $idInsere = $bdd->lastInsertId();
        // Insertion dans la table T_CRITERE_VALEUR du type ou de la liste d'éléments
        foreach ($this->getType() as $element) {
            $requete = "INSERT INTO `T_CRITERE_VALEUR`(`CV_ID`, `CRITERE_ID`, `CV_NOM`) VALUES (NULL,:idCritere,:element)";
            $reponse = $bdd->prepare($requete);
            
            $reponse->bindParam(':idCritere', $idInsere, PDO::PARAM_INT);
            $reponse->bindParam(':element', $element, PDO::PARAM_STR);
            
            $reponse->execute();
        }
        $this->ajouterChampVide($type,$idInsere);
        
    }
    
    public function supprimer(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Suppression des valeurs du critere dans la table T_CRITERE_VALEUR
        $requete = "DELETE FROM `T_CRITERE_VALEUR` "
                . " WHERE `CRITERE_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        
        
        $requete = "DELETE FROM `T_CRITERE` "
                . " WHERE `CRITERE_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute();
    }
        
    public static function getTousLesCriteres(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_CRITERE`" ;

        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        return $reponse->fetchAll();
    }
}