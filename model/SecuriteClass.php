<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SecuriteClass
 * Le but de la classe est d'automatiser certaines actions de sécurité
 * @author julien
 */
class SecuriteClass {
    // Sécurité données entrantes
    public static function securite_bdd($string)
    {
        $string= htmlentites(htmlspecialchars( $string));
        // On regarde si le type de string est un nombre entier (int)
        if(ctype_digit($string))
        {
                $string = intval($string);
                print "test";
        }
        // Pour tous les autres types
        else
        {
            
            // On échappe tous les caractères servant à l'injection
            $string = mysql_real_escape_string($string);
           
            // On échappe le caractère joker % 
            $string = addcslashes($string, '%_');
        }

        return $string;
    }
    
    // Sécurité données sortantes
    public static function htmlEscape($string)
    {
            return htmlentities($string);
    }
    
    //Si on recoi un message d erreur, c'est que ce n'est pas bon
    public static function securite_string($string, $min, $max){
        $message = null;
        if(!preg_match('/^[0-9a-zA-Zé-é\s\'-,]{'.$min.','.$max.'}$/', $string)) {
                $message =  "Caractére incorrect dans : ".$string;            
        }
        return $message;
    }
    
    public static function securite_vide($string){
        $message = null;
        if(empty($string)) {
            $message = "Champ manquant.";
        }
        return $message;
    }
    
    public static function securite_mdp($mdp, $min){
        $message = null;
        $uppercase = preg_match('@[A-Z]@', $mdp);
        $lowercase = preg_match('@[a-z]@', $mdp);
        $number    = preg_match('@[0-9]@', $mdp);        
        if(!$uppercase || !$lowercase || !$number || strlen($mdp) < $min) {
            $message = "Mot de passe invalide.";            
        }
        return $message;
    }
    
    public static function securite_email($email){
       $message = null;
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message = "Adresse mail invalide.";
        }
        return $message; 
    }
    
    
    
}
?>
