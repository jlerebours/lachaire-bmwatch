<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//session_start();
require_once dirname(__FILE__).'/AccesBDClass.php';
require_once dirname(__FILE__).'/UtilisateurClass.php';
include_once dirname(__FILE__).'/../controller/verificationLangueController.php';
/**
 * Description of ToolBox
 *
 * @author julien
 */
class ToolBox {
    // Petite fonction de redirection ou rafraichissement après un certain temps
    static function redirige($url, $time)
    {
       die('<meta http-equiv="refresh" content='.$time.';URL='.$url.'>');
    }
    
    // Génération d'une chaine aléatoire
    static function chaine_aleatoire($nb_car, $chaine = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN0123456789')
    {
        $nb_lettres = strlen($chaine) - 1;
        $generation = '';
        for($i=0; $i < $nb_car; $i++)
        {
            $pos = mt_rand(0, $nb_lettres);
            $car = $chaine[$pos];
            $generation .= $car;
        }
        return $generation;
    }
    
    public static function envoiMail($email, $message, $mdp){
        $message = "Bonjour, un moderateur vous a ajouté sur notre site, voici votre mot de passe pour te connecter : ".$mdp.". Connectez-vous rapidemment pour le changer.";
        $message = wordwrap($message, 70, "\r\n");
//        mail($email, 'Création de compte', $message);
    }
    public static function envoiMailAttenteNonPorteur($utilisateur){
            global $voc_mail_phraseIntroInscription;
            global $voc_mail_inscriptionNP;
            global $voc_mail_inscriptionNPPhrase;
            
            $mail = $utilisateur->getEmail();
            $mailExp ="lachairebmwatch@gmail.com";
            
            $code = sha1(sha1($mail)."chaine de verification inscription");
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
            {
                $passage_ligne = "\r\n";
            }
            else
            {
                $passage_ligne = "\n";
            }
            //===== Lecture du html de template.
            // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
            $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-general.html");
            // Modifications
            $doc->getElementById('idPhraseIntro')->nodeValue = $voc_mail_phraseIntroInscription;
            $doc->getElementById('idTitre1')->nodeValue = $voc_mail_inscriptionNP." : ";
            $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_inscriptionNPPhrase;
            $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-general.html");

            $fichier = fopen(dirname(__FILE__)."/../template/email-general.html", "r");
            $message_html="";
            while (!feof($fichier))
            {
                    $message_html .= fgets($fichier, 4096);
            }

            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========

            //=====Définition du sujet.
            $sujet = "BMWatch : ".$voc_mail_inscriptionNP;
            //=========

            //=====Création du header de l'e-mail.
            $header = "From:".$mailExp.$passage_ligne;
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========

            //=====Création du message.
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_html.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========

            //=====Envoi de l'e-mail.
            mail($mail,$sujet,$message,$header);
            //==========
    }
    
    public static function envoiMailValidationPorteur($utilisateur){
            global $voc_mail_phraseIntroInscrit;
            global $voc_mail_compteValide;
            global $voc_mail_compteValidePhrase;
            
            $mail = $utilisateur->getEmail();
            $mailExp ="lachairebmwatch@gmail.com";
            
            $code = sha1(sha1($mail)."chaine de verification inscription");
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
            {
                $passage_ligne = "\r\n";
            }
            else
            {
                $passage_ligne = "\n";
            }
            //===== Lecture du html de template.
            $pathTemplate = dirname(__FILE__)."/../template/email-general.html";
            // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
            $doc = DOMDocument::loadHTMLFile($pathTemplate);
            // Modifications
            $doc->getElementById('idPhraseIntro')->nodeValue = $voc_mail_phraseIntroInscrit;
            $doc->getElementById('idTitre1')->nodeValue = $voc_mail_compteValide." : ";
            $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_compteValidePhrase;
            $doc->saveHTMLFile($pathTemplate);

            $fichier = fopen($pathTemplate, "r");
            $message_html="";
            while (!feof($fichier))
            {
                    $message_html .= fgets($fichier, 4096);
            }

            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========

            //=====Définition du sujet.
            $sujet = "BMWatch : ".$voc_mail_compteValide;
            //=========

            //=====Création du header de l'e-mail.
            $header = "From:".$mailExp.$passage_ligne;
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========

            //=====Création du message.
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_html.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========

            //=====Envoi de l'e-mail.
            mail($mail,$sujet,$message,$header);
            //==========
    }
     public static function envoiMailVerification($utilisateur){
            $mail = $utilisateur->getEmail();
            $mailExp ="lachairebmwatch@gmail.com";
            
            $code = sha1(sha1($mail)."chaine de verification inscription");
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
            {
                $passage_ligne = "\r\n";
            }
            else
            {
                $passage_ligne = "\n";
            }
            //===== Lecture du html de template.
            // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
            $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-verification.html");
            // Modifications
//            $doc->getElementById('idTitreParagraphe1')->nodeValue = "Account verification";
//            $doc->getElementById('idParagraphe1')->nodeValue = $voc_paragrapheVerifCompte;
//            $doc->getElementById('idLienVerif')->nodeValue=$voc_lienVerifCompte;
            $doc->getElementById('idLienVerif')->setAttribute('href',"http://incubateurstartingbloc.com/BMwatch/controller/connexionController.php?idUser=".$utilisateur->getId()."&codeVerification=".$code);

//            $doc->getElementById('idTitreParagraphe2')->nodeValue = $voc_jamaisInscrit;
//            $doc->getElementById('idParagraphe2')->nodeValue = $voc_paragrapheSupprCompte;
//            $doc->getElementById('idLienSupprimer')->nodeValue=$voc_lienSupprCompte;
            $doc->getElementById('idLienSupprimer')->setAttribute('href',"http://incubateurstartingbloc.com/BMwatch/controller/connexionController.php?idUser=".$utilisateur->getId()."&codeSuppression=".$code);
            $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-verification.html");

            $fichier = fopen(dirname(__FILE__)."/../template/email-verification.html", "r");
            $message_html="";
            while (!feof($fichier))
            {
                    $message_html .= fgets($fichier, 4096);
            }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
            //==========

            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========

            //=====Définition du sujet.
            $sujet = "BMWatch : Account verification";
            //=========

            //=====Création du header de l'e-mail.
            $header = "From:".$mailExp.$passage_ligne;
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========

            //=====Création du message.
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_html.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========

            //=====Envoi de l'e-mail.
            mail($mail,$sujet,$message,$header);
            //==========
    }
    
    
    public static function envoiMailInscription($utilisateur,$mdpNonChiffre){
            $mail = $utilisateur->getEmail();
            $mailExp ="lachairebmwatch@gmail.com";
            
            $code = sha1(sha1($mail)."chaine de verification modification NP");
            if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
            {
                $passage_ligne = "\r\n";
            }
            else
            {
                $passage_ligne = "\n";
            }
            //===== Lecture du html de template.
            // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
            $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-inscription.html");
            $doc->getElementById('idLienFirstConnexion')->setAttribute('href',"http://incubateurstartingbloc.com/BMwatch/controller/connexionController.php?premiereConnexionNP=".$utilisateur->getId()."&codeModification=".$code);
            $doc->getElementById('idEmail')->nodeValue = $mail; 
            $doc->getElementById('idMdp')->nodeValue = $mdpNonChiffre; 
            $listeDroits = Utilisateur::getListeDroits();
            $stringDroits="";
            foreach ($utilisateur->getDroits() as $droit) {
                $stringDroits.=$listeDroits[$droit]." ";
            }
            $doc->getElementById('listeDroits')->nodeValue = $stringDroits;
            $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-inscription.html");

            $fichier = fopen(dirname(__FILE__)."/../template/email-inscription.html", "r");
            $message_html="";
            while (!feof($fichier))
            {
                    $message_html .= fgets($fichier, 4096);
            }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
            //==========

            //=====Création de la boundary
            $boundary = "-----=".md5(rand());
            //==========

            //=====Définition du sujet.
            $sujet = "BMWatch : Registration by moderator";
            //=========

            //=====Création du header de l'e-mail.
            $header = "From:".$mailExp.$passage_ligne;
            $header.= "MIME-Version: 1.0".$passage_ligne;
            $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
            //==========

            //=====Création du message.
            $message = $passage_ligne."--".$boundary.$passage_ligne;
            //=====Ajout du message au format HTML
            $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
            $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
            $message.= $passage_ligne.$message_html.$passage_ligne;
            //==========
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
            //==========

            //=====Envoi de l'e-mail.
            mail($mail,$sujet,$message,$header);
            //==========
    }
    
    public static function envoiModifDroits($utilisateur){
        
        global $voc_modificationDroits;
        global $voc_mail_modifDroits;
        global $voc_mail_modifDroitsPhrase;
        
        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        $code = sha1(sha1($mail)."chaine de verification modification NP");
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        $pathTemplate = dirname(__FILE__)."/../template/email-droits.html";
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile($pathTemplate);
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_modifDroits." : ";
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_modifDroitsPhrase;
        $listeDroits = Utilisateur::getListeDroits();
        $stringDroits="";
        foreach ($utilisateur->getDroits() as $droit) {
            $stringDroits.=$listeDroits[$droit].", ";
        }
        $stringDroits = substr($stringDroits,0,strlen($stringDroits)-2);
        $doc->getElementById('listeDroits')->nodeValue = $stringDroits;
        $doc->saveHTMLFile($pathTemplate);

        $fichier = fopen($pathTemplate, "r");
        $message_html="";
        while (!feof($fichier))
        {
                $message_html .= fgets($fichier, 4096);
        }

    //            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : ".$voc_mail_modifDroits;
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function envoiMailCompteDesactive($utilisateur){
        // Définition des variables de voc 
        global $voc_mail_compteDesactive;
        global $voc_mail_compteDesactivePhrase;
        
        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-general.html");
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_compteDesactive." : ";
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_compteDesactivePhrase;
        $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-general.html");

        $fichier = fopen(dirname(__FILE__)."/../template/email-general.html", "r");
        $message_html="";
        while (!feof($fichier))
        {
                $message_html .= fgets($fichier, 4096);
        }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : ".$voc_mail_compteDesactive;
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function envoiMailCompteReactive($utilisateur){
        // Définition des variables de voc 
        global $voc_mail_compteReactive;
        global $voc_mail_compteReactivePhrase;
        
        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        $pathTemplate=dirname(__FILE__)."/../template/email-general.html";
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile($pathTemplate);
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_compteReactive." : ";
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_compteReactivePhrase;
        $doc->saveHTMLFile($pathTemplate);

        $fichier = fopen($pathTemplate, "r");
        $message_html="";
        while (!feof($fichier))
        {
                $message_html .= fgets($fichier, 4096);
        }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : ".$voc_mail_compteReactive;
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function envoiNewAssignation($idUtilisateur,$entite){
        // Définition des variables de voc 
        global $voc_mail_newAssignation;
        global $voc_mail_newAssignationPhrase;
        global $voc_mail_newAssignationLien;
        
        $utilisateur = new Utilisateur($idUtilisateur);
        $utilisateur->getInfos();
        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-assignation.html");
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_newAssignation." : ".$entite->getNom();
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_newAssignationPhrase;
        $doc->getElementById('idParagraphe2')->nodeValue = "";
        $doc->getElementById('idCommentaire')->nodeValue = "";
        $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-assignation.html");

        $fichier = fopen(dirname(__FILE__)."/../template/email-assignation.html", "r");
        $message_html="";
        while (!feof($fichier))
        {
                $message_html .= fgets($fichier, 4096);
        }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : ".$voc_mail_newAssignation;
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function envoiModifAssignation($utilisateur, $entite,$commentaire){
        // Définition des variables de voc 
        global $voc_mail_modifAssignation;
        global $voc_mail_modifAssignationPhrase;
        global $voc_mail_modifAssignationLien;
        global $voc_mail_modifAssignationCom;
        
        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-assignation.html");
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_modifAssignation." : ".$entite->getNom();
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_modifAssignationPhrase;
        $doc->getElementById('idParagraphe2')->nodeValue = $voc_mail_modifAssignationCom." : ";
        $doc->getElementById('idCommentaire')->nodeValue = $commentaire;
        $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-assignation.html");

        $fichier = fopen(dirname(__FILE__)."/../template/email-assignation.html", "r");
        $message_html="";
        while (!feof($fichier))
        {
            $message_html .= fgets($fichier, 4096);
        }

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : ".$voc_mail_modifAssignation;
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function envoiNouveauMdp($utilisateur,$mdpNonChiffre){
        global $voc_mail_newMdp;
        global $voc_mail_newMdpPhrase;
        global $voc_mail_newMdpChgt;
        global $voc_mail_phraseIntroMdp;

        $mail = $utilisateur->getEmail();
        $mailExp ="lachairebmwatch@gmail.com";

        $code = sha1(sha1($mail)."chaine de verification modification NP");
        if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        }
        else
        {
            $passage_ligne = "\n";
        }
        //===== Lecture du html de template.
        // On ouvre le DOM du template pour le modifier avant de récupérer le contenu
        $doc = DOMDocument::loadHTMLFile(dirname(__FILE__)."/../template/email-newMdp.html");
        $doc->getElementById('idPhraseIntro')->nodeValue = $voc_mail_phraseIntroMdp." : ";
        $doc->getElementById('idTitre1')->nodeValue = $voc_mail_newMdp." : ";
        $doc->getElementById('idParagraphe1')->nodeValue = $voc_mail_newMdpPhrase." : ";
        $doc->getElementById('idParagraphe1Mdp')->nodeValue = $mdpNonChiffre;
        $doc->getElementById('idParagraphe2')->nodeValue = $voc_mail_newMdpChgt;
        $doc->saveHTMLFile(dirname(__FILE__)."/../template/email-newMdp.html");

        $fichier = fopen(dirname(__FILE__)."/../template/email-newMdp.html", "r");
        $message_html="";
        while (!feof($fichier))
        {
                $message_html .= fgets($fichier, 4096);
        }

//            $message_html = "<html><head></head><body><b>Salut à tous</b>, voici un e-mail envoyé par un <i>script PHP</i>.</body></html>";
        //==========

        //=====Création de la boundary
        $boundary = "-----=".md5(rand());
        //==========

        //=====Définition du sujet.
        $sujet = "BMWatch : Registration by moderator";
        //=========

        //=====Création du header de l'e-mail.
        $header = "From:".$mailExp.$passage_ligne;
        $header.= "MIME-Version: 1.0".$passage_ligne;
        $header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;
        //==========

        //=====Création du message.
        $message = $passage_ligne."--".$boundary.$passage_ligne;
        //=====Ajout du message au format HTML
        $message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
        $message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
        $message.= $passage_ligne.$message_html.$passage_ligne;
        //==========
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        $message.= $passage_ligne."--".$boundary."--".$passage_ligne;
        //==========

        //=====Envoi de l'e-mail.
        mail($mail,$sujet,$message,$header);
        //==========
    }
    
    public static function verifierDroits($valeurDroit) {
        $droitsOk=FALSE;
        if(!isset($_SESSION['donneesUtilisateur'])){
            $message = "Vous n'êtes pas connecté";
            echo "<p align=\"center\" style=\"color:red\"><strong>" . $message . "</strong></p>";
            ToolBox::redirige("../view/connexionView.php", 2);  
        }
        else{
            $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
            if($utilisateurCourant->possedeDroit($valeurDroit)){
                $droitsOk=TRUE;
            } 
        }      
        return $droitsOk;
    }
    
    public static function verifierDroitsEtRedirigerSiOk($valeurDroit,$url,$time){
        if(ToolBox::verifierDroits($valeurDroit)){
            ToolBox::redirige($url, $time); 
        }        
    }
    
    public static function verifierDroitsEtRedirigerSiPasOk($valeurDroit,$url,$time){
        if(!ToolBox::verifierDroits($valeurDroit)){
            $message="Vous n'avez pas les droits pour accéder à cette page !";
            echo "<p align=\"center\" style=\"color:red\"><strong>" . $message . "</strong></p>";
            ToolBox::redirige($url, $time); 
        }  
    }
    
    public static function getListeCouleurs(){
        return array(1 =>"btn-red",2 =>"btn-purple",3 =>"btn-orange", 4 =>"btn-satgreen",5 =>"btn-green",6 =>"btn-blue",7 =>"btn-grey");
    }
    
    public static function obtenirListeEntiteAvecCommentaire(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $resultat = array();
        
        $requete = "SELECT C.`comment_appid`, A.`app_name`FROM `t_android_app_comments` C,`t_android_app`A  WHERE C.`comment_appid`=A.`app_id` GROUP BY `comment_appid`";
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        
        $i=0;
        foreach($rows as $row){
            $resultat[$i][0] = $row['comment_appid'];
            $resultat[$i][1] = $row['app_name'];
            $i+=1;
        }
        return $resultat;
    }
    
        public static function obtenirListeCommentaires($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $resultat = array();
        
        $requete = "SELECT * FROM `t_android_app_comments` WHERE `comment_appid`= :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        
        $i=0;
        foreach($rows as $row){
            $resultat[$i][0] = $row['comment_usernmane'];
            $resultat[$i][1] = $row['comment_time'];
            $resultat[$i][2] = $row['comment_rating'];
            $resultat[$i][3] = $row['comment_content'];
            $i+=1;
        }
        return $resultat;
    }
}
