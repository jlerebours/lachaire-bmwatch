<?php

require_once dirname(__FILE__).'/EntiteClass.php';
require_once dirname(__FILE__).'/EntrepriseClass.php';
require_once dirname(__FILE__).'/ProduitClass.php';
require_once dirname(__FILE__).'/ToolBoxClass.php';


class Utilisateur {
    private $id;
    private $nom;
    private $prenom;
    private $email;
    private $mdp;
    private $droits;
    private $groupe;
    private $inactif;
    
    public function __construct($id, $nom='', $prenom='', $email='', $mdp='',$droits=array(), $groupe='',$inactif=0) {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->mdp = $mdp;
        $this->droits = $droits;
        $this->groupe = $groupe;
        $this->inactif = $inactif;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getDroits() {
        return $this->droits;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function getEmail() {
        return $this->email;
    }
    
    public function getMdp() {
        return $this->mdp;
    }
    
    public function getGroupe() {
        return $this->groupe;
    }
    
    public function estInactif() {
        return $this->inactif;
    }
    
    // Récupère les infos de l'utilisateur si celui-ci possède un id qui existe
    public function getInfos() {
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = " SELECT * "
                    . " FROM `T_UTILISATEUR` U, `T_STATUT_UTILISATEUR` SU "
                    . " WHERE SU.`UTILISATEUR_ID` = U.`UTILISATEUR_ID` "
                    . " AND U.`UTILISATEUR_ID` = :id ";

            $reponse = $bdd->prepare($requete);
            $id = $this->getId();
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            
            $rows = $reponse->fetchAll();
            $listeDroits=array();
            foreach($rows as $row){
                $this->setEmail($row['UTILISATEUR_EMAIL']);
                $this->setNom($row['UTILISATEUR_NOM']);
                $this->setPrenom($row['UTILISATEUR_PRENOM']);
                $this->setMdp($row['UTILISATEUR_MDP']);
                $this->setGroupe($row['UTILISATEUR_GROUP']);
                $this->setInactif($row['UTILISATEUR_INACTIF']);
                array_push($listeDroits, $row['STATUT_ID']);
            }
            $this->setDroits($listeDroits);
            
        }
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
    public function setMdp($mdp) {
        $this->mdp = $mdp;
    }
    
    public function setGroupe($groupe) {
        $this->groupe = $groupe;
    }
    
    public function setInactif($inactif) {
        $this->inactif = $inactif;
    }
    
    public function setDroits($droits) {
        $this->droits = $droits;
    }
    
    public function emailExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
            . "FROM `T_UTILISATEUR` "
            . "WHERE "
                . " `UTILISATEUR_EMAIL` = :email " ; 
        
        $reponse = $bdd->prepare($requete);
        $email = $this->getEmail();
        $reponse->bindParam(':email', $email, PDO::PARAM_STR,50);
        $reponse->execute(); 
        
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
            $row = $reponse->fetch();
            $this->setId($row['UTILISATEUR_ID']);
            $this->getInfos();
        }
        return $resultat;
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
            . "FROM `T_UTILISATEUR` "
            . "WHERE `UTILISATEUR_ID` = :id " ;
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
    public function mdpBon($mdp) {
        $resultat = FALSE;
        $mdpUtilisateur = $this->getMdp();
        $mdpCrypte = sha1(sha1($mdp).'chiffrement_bmwatch');
        if(!empty($mdpUtilisateur) and $mdpUtilisateur == $mdpCrypte){
            $resultat = TRUE;
        }
        return $resultat;   
    }
    
    public function possedeDroit($droitTest){
        $possedeDroit=FALSE;
        $listeDroits = $this->getDroits();
        foreach ($listeDroits as $droit){
            if($droit == $droitTest){
                $possedeDroit=TRUE;
            }
        }
        return $possedeDroit;
    }
    
    // Function to add a right to a user 
    // notification is a boolean to handle mail notification
    public function ajouterStatut($statut,$notification=false){
        
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "INSERT INTO `T_STATUT_UTILISATEUR` VALUES (:id, :statut)";
            $id = $this->getId();
            $reponse = $bdd->prepare($requete);
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->bindParam(':statut', $statut, PDO::PARAM_INT,1);
            
            $reponse->execute();
            
            if($notification){
                $this->getInfos();
                ToolBox::envoiModifDroits($this);
            }
        }
        
    }
    
    // Function to add a right to a user 
    // notification is a boolean to handle mail notification
    public function supprimerStatut($statut, $notification=false){
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "DELETE FROM `T_STATUT_UTILISATEUR` "
                    . " WHERE `UTILISATEUR_ID` = :id  "
                    . " AND `STATUT_ID` = :statut ";
            
            $id = $this->getId();
            
            $reponse = $bdd->prepare($requete);
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->bindParam(':statut', $statut, PDO::PARAM_INT,1);
            
            $reponse->execute();
            
            if($notification){
                $this->getInfos();
                ToolBox::envoiModifDroits($this);
            }
        }
        
    }
    
    public function changerInactif($statut){
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "UPDATE `T_UTILISATEUR` "
                    . " SET `UTILISATEUR_INACTIF` = :statut "
                    . " WHERE `UTILISATEUR_ID` = :id";
            
            $id = $this->getId();
            
            $reponse = $bdd->prepare($requete);
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->bindParam(':statut', $statut, PDO::PARAM_INT,1);
            
            $reponse->execute();
            
        }
    }
    
    // Fonction d'ajout à la BD de l'utilisateur instancié si l'email n'existe pas déjà en BD
    public function ajouterALaBD() {
        $ajoutEffectue=FALSE;
        if(!$this->emailExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "INSERT INTO `T_UTILISATEUR` VALUES (NULL, :email, :mdp , :nom, :prenom, :groupe, :inactif)";
            
            $nom = $this->getNom();
            $prenom = $this->getPrenom();
            $email = $this->getEmail();
            $mdp = $this->getMdp();
            $reponse = $bdd->prepare($requete);
            if($mdp != null){
                $mdpCrypte = sha1(sha1($mdp).'chiffrement_bmwatch');
                $this->setMdp($mdpCrypte);
                $reponse->bindParam(':mdp', $mdpCrypte, PDO::PARAM_STR,256);
            }
            else{
                $mdp="";
                $reponse->bindParam(':mdp', $mdp, PDO::PARAM_STR);
            }            
            $groupe = $this->getGroupe();
            $inactif = $this->estInactif();

           
            $reponse->bindParam(':email', $email, PDO::PARAM_STR,50);
            $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
            $reponse->bindParam(':prenom', $prenom, PDO::PARAM_STR,50);
            $reponse->bindParam(':groupe', $groupe, PDO::PARAM_STR,256);
            $reponse->bindParam(':inactif', $inactif, PDO::PARAM_INT,1);
            $ajoutEffectue = $reponse->execute();
            $this->setId($bdd->lastInsertId());
            
            $droits = $this->getDroits();
            // On ajoute les droits de l'utilisateur dans la table T_STATUT_UTILISATEUR
            foreach ($droits as $droit) {
                $this->ajouterStatut($droit);
            }
        }
        return $ajoutEffectue;
    }
    // Permet de supprimer un utilisateur de la BD en vérifiant les contraintes
    public function supprimerDeLaBDSansContraintes() {
        $suppressionEffectuee=FALSE;
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "DELETE FROM `T_UTILISATEUR` "
                    . "WHERE UTILISATEUR_ID = :id ";

            $id = $this->getId();

            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $suppressionEffectuee = $reponse->execute();  
        }
        
        return $suppressionEffectuee;
    }
    
    // Permet de supprimer un utilisateur de la BD en vérifiant les contraintes
    //On ne supprime plus, on met en incatif
    public function supprimerDeLaBD() {
        $suppressionEffectuee=FALSE;
        if($this->idExiste()){
//            switch ($this->getType()) {
//                case 2:
//                    // Suppression des assignations        
//                    $listeAssignations = $this->getEntitesAssigneesParModo();
//                    foreach ($listeAssignations[0] as $assignation) {                  
//                        $assignation->changerEtat(6);
//                    }
//                    $this->supprimerNonPorteursAssignes();
//                    break;
//                case 3:
//                    // Suppression du lien avec l'entreprise
//                    $this->majIdPorteurEntreprise();
//                    break;
//                case 4:
//                    // Suppression des assignations        
//                    $modosAssocies = $this->getModerateursAssocies();
//                    foreach($modosAssocies as $modo){
//                        $this->supprimerAssignations($modo->getId(), 6);
//                    }
//                    // Suppression des modérateurs associés
//                    $this->supprimerModosAssignes();
//                    break;
//                default:
//                    break;
//            }
//            
//            $bdd = new AccesBD();
//            $bdd = $bdd->getBdd();
//
//            $requete = "DELETE FROM `T_UTILISATEUR` "
//                    . "WHERE UTILISATEUR_ID = :id ";
//
//            $id = $this->getId();
//
//            $reponse = $bdd->prepare($requete);
//            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
//
//            $suppressionEffectuee = $reponse->execute();  
        }
        
        return $suppressionEffectuee;
    }
    
    public function rendreInactif(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_UTILISATEUR` SET `UTILISATEUR_INACTIF`= 1 "
                . " WHERE `UTILISATEUR_ID` = :id ";
        
        $id = $this->getId();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);

        $suppressionEffectuee = $reponse->execute();  
        
        return $suppressionEffectuee;
    }

    public function getUtilisateursParType($type){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . "FROM `T_STATUT_UTILISATEUR` SU "
                . "WHERE SU.`STATUT_ID` = :type ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':type',$type, PDO::PARAM_STR,50);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $i=0;
        $tabUtilisateurs=array();
        foreach($rows as $row){
            $utilisateur = new Utilisateur($row['UTILISATEUR_ID']);
            $utilisateur->getInfos();
            if($utilisateur->getId() != $this->getId()){
                array_push($tabUtilisateurs, $utilisateur);
            }
        }
        return $tabUtilisateurs;
    }
    
    // Permet de récupérer tous les utilisateurs  sauf l'utilisateur courant (instancié)
    public function getTousLesUtilisateurs(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . "FROM `T_UTILISATEUR` U ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $i=0;
        $tabUtilisateurs=array();
        foreach($rows as $row){
            $utilisateur = new Utilisateur($row['UTILISATEUR_ID']);
            $utilisateur->getInfos();
            if($utilisateur->getId() != $this->getId()){
                array_push($tabUtilisateurs, $utilisateur);
            }
        }
        return $tabUtilisateurs;
    }
    
    public function modifierLaBD($ancienEmail) {
        $modificationEffectue=FALSE;
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_UTILISATEUR` SET `UTILISATEUR_EMAIL` = :email, `UTILISATEUR_MDP` = :mdp, `UTILISATEUR_NOM` = :nom, `UTILISATEUR_PRENOM` = :prenom WHERE `UTILISATEUR_EMAIL` = :ancienEmail";

        $mdp = $this->getMdp();

        $mdpCrypte = sha1(sha1($mdp)+'chiffrement_bmwatch');
        $this->setMdp($mdpCrypte);
        $email = $this->getEmail();
        $nom = $this->getnom();
        $prenom = $this->getPrenom();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':email', $email, PDO::PARAM_STR,50);
        $reponse->bindParam(':mdp', $mdpCrypte, PDO::PARAM_STR,256);            
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->bindParam(':prenom', $prenom, PDO::PARAM_STR,50);
        $reponse->bindParam(':ancienEmail', $ancienEmail, PDO::PARAM_STR,50);

        $modificationEffectue = $reponse->execute(); 
        
        return $modificationEffectue;
    }
    
    public function modifierLaBDSansMDP($ancienEmail) {
        $modificationEffectue=FALSE;
        
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "UPDATE `T_UTILISATEUR` SET `UTILISATEUR_EMAIL` = :email, `UTILISATEUR_NOM` = :nom, `UTILISATEUR_PRENOM` = :prenom WHERE `UTILISATEUR_EMAIL` = :ancienEmail";
            $email = $this->getEmail();
            $nom = $this->getnom();
            $prenom = $this->getPrenom();

            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':email', $email, PDO::PARAM_STR,50);           
            $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
            $reponse->bindParam(':prenom', $prenom, PDO::PARAM_STR,50);
            $reponse->bindParam(':ancienEmail', $ancienEmail, PDO::PARAM_STR,50);
            
            $modificationEffectue = $reponse->execute(); 
        
        return $modificationEffectue;
    }    
    
    
/* ----------------------------------------------------------------------------------------- *
 * --------- Partie qui concerne un utilisateur qui est administrateur ou modérateur ------- *
 * ----------------------------------------------------------------------------------------- */
    
    
    public function possedeNonPorteur($idNonPorteur) {
        $resultat=FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        

        $requete = "SELECT * "
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM, `T_UTILISATEUR` U "
                . "WHERE `MODERATEUR_ID` = :idModerateur "
                . "AND NPAM.`NON_PORTEUR_ID` = :idNonPorteur "
                . "GROUP BY U.`UTILISATEUR_ID`";

        $idModerateur = $this->getId();
        
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);
        $reponse->bindParam(':idNonPorteur', $idNonPorteur, PDO::PARAM_INT);

        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        return $resultat;
        
    }
    
    // Fonction à utiliser pour un modérateur
    // Retourne un tableau d'utilisateurs qui sont les non porteurs associés au modérateur
    public function getNonPorteurs(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT * "
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM, `T_UTILISATEUR` U "
                . "WHERE `MODERATEUR_ID` = :idModerateur "
                . "AND NPAM.`NON_PORTEUR_ID` = U.`UTILISATEUR_ID` "
                . "GROUP BY U.`UTILISATEUR_ID`";

        $idModerateur = $this->getId();

        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);

        $reponse->execute();
        
        $i=0;
        $rows = $reponse->fetchAll();
        $tabNonPorteurs="";
        foreach($rows as $row){
            $nonPorteur = new Utilisateur($row['UTILISATEUR_ID'],$row['UTILISATEUR_NOM'],$row['UTILISATEUR_PRENOM'], $row['UTILISATEUR_EMAIL'], '',array(),$row['UTILISATEUR_GROUP'],$row['UTILISATEUR_INACTIF']);
            $tabNonPorteurs[$i]=$nonPorteur;
            $i+=1;
        }
        return $tabNonPorteurs;
    }
    
    // Fonction à utiliser pour un modérateur
    public function getNonPorteursNonAssignes(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_UTILISATEUR` U, `T_STATUT_UTILISATEUR` SU "
                . " WHERE SU.`STATUT_ID` = 4 "
                    . "AND SU.`UTILISATEUR_ID` = U.`UTILISATEUR_ID` "
                    ." AND NOT EXISTS ("
                        . " SELECT * FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM "
                        . " WHERE NPAM.`MODERATEUR_ID` = :idModerateur "
                        . " AND NPAM.`NON_PORTEUR_ID` = U.`UTILISATEUR_ID` "
                        . " ORDER BY U.`UTILISATEUR_ID` )";
        
        $idModerateur = $this->getId();

        
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);

        $reponse->execute();
        
        $i=0;
        $rows = $reponse->fetchAll();
        $tabNonPorteurs="";
        foreach($rows as $row){
            $nonPorteur = new Utilisateur($row['UTILISATEUR_ID'],$row['UTILISATEUR_NOM'],$row['UTILISATEUR_PRENOM'], $row['UTILISATEUR_EMAIL'], '',array(),$row['UTILISATEUR_GROUP'],$row['UTILISATEUR_INACTIF']);
            $tabNonPorteurs[$i]=$nonPorteur;
            $i+=1;
        }
        return $tabNonPorteurs;
    }

    public function assigneEntite($idNonPorteur, $entite,$notification=false) {
        // On commence par vérifier que l'utilisateur est un modérateur
        $assignationEffectuee=FALSE;
        $idEntite = $entite->getId();
        if(!$this->possedeAssignation($idEntite)){
            
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            
            $requete = "INSERT INTO `T_NON_PORTEUR_ASSIGNE_MODERATEUR`"
                    . "VALUES (NULL, :idEntite, :idNonPorteur, :idModerateur )";
            
            $idModerateur = $this->getId();
            
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':idModerateur',$idModerateur, PDO::PARAM_INT);
            $reponse->bindParam(':idNonPorteur',$idNonPorteur, PDO::PARAM_INT);
            $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
            
            $assignationEffectuee = $reponse->execute(); 
            if($notification){
                ToolBox::envoiNewAssignation($idNonPorteur,$entite);
            }
        }
        return $assignationEffectuee;
    }
    
    // Permet de vérifier que le modo ne possède pas déjà l'assignation
    // idEntite suffit car une seule assignation par entité
    public function possedeAssignation($idEntite){
        $resultat = FALSE;
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT * "
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` "
                . "WHERE `MODERATEUR_ID` = :idModerateur "
                . "AND `ENTITE_ID` = :idEntite ";

        $idModerateur = $this->getId();
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);
        $reponse->bindParam(':idEntite', $idEntite, PDO::PARAM_INT);

        $reponse->execute();
        
        if($reponse->rowCount()>0){
            $resultat=TRUE;
        }
       
        return $resultat;
    }
    
    
    
    public function assigneNonPorteur($idNonPorteur) {
        // On commence par vérifier que l'utilisateur est un modérateur
        $assignationEffectuee=FALSE;
        if(!$this->possedeNonPorteur($idNonPorteur)){
            
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "INSERT INTO `T_NON_PORTEUR_ASSIGNE_MODERATEUR`"
                    . "VALUES (NULL, NULL, :idNonPorteur, :idModerateur)";
            
            $idModerateur = $this->getId();

            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':idModerateur',$idModerateur, PDO::PARAM_INT);
            $reponse->bindParam(':idNonPorteur',$idNonPorteur, PDO::PARAM_INT);
            $assignationEffectuee = $reponse->execute(); 
        }
        return $assignationEffectuee;
    }
    
    public function getEntitesAssigneesParModo(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT * "
                . " FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM, `T_ENTITE_CARACTERISEE` E, `T_UTILISATEUR` U "
                . " WHERE `MODERATEUR_ID` = :idModerateur "
                . " AND NPAM.`ENTITE_ID` = E.`ENTITE_ID` "
                . " AND NPAM.`NON_PORTEUR_ID` = U.`UTILISATEUR_ID` ";

        $idModerateur = $this->getId();

        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);

        $reponse->execute();
        
        $i=0;
        $rows = $reponse->fetchAll();
        $tabEntitesAssignes="";
        $tabNonPorteurs="";
        foreach($rows as $row){
            $entite = new Entite($row['ENTITE_ID'], '', $row['ETAT_ID'], $row['ENTITE_DESCRIPTION'], '', $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM']);
            $tabEntitesAssignes[$i]=$entite;
            
            $nonPorteur = new Utilisateur($row['UTILISATEUR_ID'], '', '', $row['UTILISATEUR_NOM'], $row['UTILISATEUR_EMAIL'], '');
            $tabNonPorteurs[$i]=$nonPorteur;
            
            $i+=1;
        }
        $tab[0]=$tabEntitesAssignes;
        $tab[1]=$tabNonPorteurs;
        
        
        return $tab;
    }
    public function supprimerNonPorteursAssignes() {
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // On récupère toutes les entités associées au non porteur par le modérateur en paramètre (modérateur courant en fait)
        $requete = "DELETE FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` "
                . "WHERE `MODERATEUR_ID` = :idModerateur ";

        $idModerateur = $this->getId();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur',$idModerateur, PDO::PARAM_INT);
        
        $resultat = $reponse->execute();
        
        return $resultat;
    }
    
    
/* ----------------------------------------------------------------------------------------- *
 * ------------------- Partie qui concerne un utilisateur qui est porteur ------------------ *
 * ----------------------------------------------------------------------------------------- */
    
    public function getEntreprises(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_ENTREPRISE` "
                . "WHERE `UTILISATEUR_ID` = :idUtilisateur ";
        
        $idUtilisateur = $this->getId();
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idUtilisateur',$idUtilisateur, PDO::PARAM_INT);
        
        $reponse->execute();
        
        $rows= $reponse->fetchAll();
        $tabEntreprises = array();
        foreach($rows as $row){
            $entreprise = new Entreprise($row['ENTITE_ID'],$row['UTILISATEUR_ID'], $row['CATEGORIE_ID'], $row['ETAT_ID'], 
                    $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'], $row['ENTITE_DATE_DERNIERE_MODIF'], 
                    $row['ENTITE_NOM'], $row['ENTREPRISE_FORME_SOCIALE'], $row['ENTREPRISE_SECTEUR_ACTIVITE'], $row['ENTREPRISE_TECHNOLOGIES_UTILISEES']);
        
            array_push($tabEntreprises,$entreprise);
        }
        return $tabEntreprises;
    }
    
    public function majIdPorteurEntreprise(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_ENTREPRISE` "
                . "SET `UTILISATEUR_ID`=NULL "
                . "WHERE `UTILISATEUR_ID` = :idPorteur ";
        
        $idPorteur= $this->getId();
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idPorteur',$idPorteur, PDO::PARAM_INT);
        
        $reponse->execute();
        
    }
    
    
    public function getProduits($idEntreprise){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE` "
                . "WHERE `T_E_ENTITE_ID` = :idEntreprise ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntreprise',$idEntreprise, PDO::PARAM_INT);
        
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $tabProduits = array();
        foreach($rows as $row){
            
            $produit = new Produit($row['ENTITE_ID'], $idEntreprise, $row['CATEGORIE_ID'], $row['ETAT_ID'], 
                    $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'], $row['ENTITE_DATE_DERNIERE_MODIF'], 
                    $row['ENTITE_NOM'], $row['PRODUIT_SERVICE_RANKING']);
            array_push($tabProduits,$produit);
        }
        return $tabProduits;
    }
    
    
/* ----------------------------------------------------------------------------------------- *
 * --------------- Partie qui concerne un utilisateur qui est non porteur ------------------ *
 * ----------------------------------------------------------------------------------------- */
    
    public function getEntitesAssignesNPParModo($idModerateur){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT * "
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM, `T_ENTITE_CARACTERISEE` E "
                . "WHERE `MODERATEUR_ID` = :idModerateur "
                . "AND `NON_PORTEUR_ID` = :idNonPorteur "
                . "AND NPAM.`ENTITE_ID` = E.`ENTITE_ID` ";

        $idNonPorteur = $this->getId();

        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idModerateur', $idModerateur, PDO::PARAM_INT);
        $reponse->bindParam(':idNonPorteur', $idNonPorteur, PDO::PARAM_INT);

        $reponse->execute();
        
        $i=0;
        $rows = $reponse->fetchAll();
        $tabEntitesAssignes=array();
        foreach($rows as $row){
            $entite = new Entite($row['ENTITE_ID'], '',$row['ETAT_ID'], $row['ENTITE_DESCRIPTION'], '', $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM']);
            array_push($tabEntitesAssignes,$entite);
        }
        return $tabEntitesAssignes;
    }
    
     public function getModerateursAssocies(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM "
                . " WHERE NPAM.`NON_PORTEUR_ID` = :idNonPorteur "
                . " GROUP BY NPAM.`MODERATEUR_ID`";
        
        
        $idNonPorteur = $this->getId();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idNonPorteur',$idNonPorteur, PDO::PARAM_INT);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        
        $tabModos = array();
        foreach($rows as $row){
            array_push($tabModos,new Utilisateur($row['MODERATEUR_ID']));
        }
        
        return $tabModos; 
    }
    
    public function supprimerAssignations($idModerateur, $etat) {
        // On récupère les entités assignées au non porteur courant par le modérateur pour changer l'état avant suppression
        $tabEntites = $this->getEntitesAssignesNPParModo($idModerateur);
        foreach ($tabEntites as $entite){
            $entite = new Entite($entite->getId());
            $entite->changerEtat($etat);
        }    
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $idNonPorteur = $this->getId();
        
        // Il faut encore supprimer l'assignation du non porteur au modérateur
        $requete = "DELETE FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` "
                . "WHERE `NON_PORTEUR_ID` = :idNonPorteur "
                . "AND `MODERATEUR_ID` = :idModerateur";

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idNonPorteur',$idNonPorteur, PDO::PARAM_INT);
        $reponse->bindParam(':idModerateur',$idModerateur, PDO::PARAM_INT);
        
        $resultat = $reponse->execute();
        
        
        return $resultat;
    }
    
    public function supprimerModosAssignes() {
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // On récupère toutes les entités associées au non porteur par le modérateur en paramètre (modérateur courant en fait)
        $requete = "DELETE FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` "
                . " WHERE `NON_PORTEUR_ID` = :idNonPorteur " ;

        $idNonPorteur = $this->getId();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idNonPorteur',$idNonPorteur, PDO::PARAM_INT);
        
        $reponse->execute();
    }
    
    public function getEntitesAssignes(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT E.`ENTITE_ID`, E.`ENTITE_NOM`, E.`ENTITE_DESCRIPTION` ,E.`ENTITE_DATE_DERNIERE_MODIF`"
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM, `T_ENTITE_CARACTERISEE` E "
                . "WHERE `NON_PORTEUR_ID` = :idNonPorteur "
                . "AND NPAM.`ENTITE_ID` = E.`ENTITE_ID` ";

        $idNonPorteur = $this->getId();

        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idNonPorteur', $idNonPorteur, PDO::PARAM_INT);

        $reponse->execute();
        
        $i=0;
        $rows = $reponse->fetchAll();
        $tabEntitesAssignes=array();
        foreach($rows as $row){
            $entite = new Entite($row['ENTITE_ID'], '', '', $row['ENTITE_DESCRIPTION'], '', $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM']);
            array_push($tabEntitesAssignes,$entite);
        }
        return $tabEntitesAssignes;
    }
    
    public function possedeEntite($idEntite){
        $resultat = FALSE;
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        $requete = "SELECT * "
                . "FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` NPAM "
                . "WHERE `NON_PORTEUR_ID` = :idNonPorteur "
                . "AND NPAM.`ENTITE_ID` = :idEntite "
                . "AND NPAM.`ENTITE_ID` = E.`ENTITE_ID` ";

        $idNonPorteur = $this->getId();
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idNonPorteur', $idNonPorteur, PDO::PARAM_INT);
        $reponse->bindParam(':idEntite', $idEntite, PDO::PARAM_INT);

        $reponse->execute();
        
        if($reponse->rowCount()>0){
            $resultat=TRUE;
        }
       
        return $resultat;
    }
    
    
/* ----------------------------------------------------------------------------------------- *
 * -------------------------------- Fonctions statiques  ----------------------------------- *
 * ----------------------------------------------------------------------------------------- */
 
    public static function getGroupes() {
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . "FROM `T_UTILISATEUR` U "
                . "GROUP BY U.`UTILISATEUR_GROUP` ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $tabGroupes=array();
        foreach($rows as $row){
            array_push($tabGroupes, $row['UTILISATEUR_GROUP']);
        }
        return $tabGroupes;
    }
    
    public static function getUtilisateursParGroupe($groupe) {
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_UTILISATEUR` U "
                . " WHERE `UTILISATEUR_GROUP` = :groupe ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':groupe',$groupe,PDO::PARAM_STR);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $tabUtilisateurs=array();
        foreach($rows as $row){
            array_push($tabUtilisateurs, $row['UTILISATEUR_GROUP']);
        }
        return $tabUtilisateurs;
    }
    
    // Permet de rechercher un utilisateur selon son statut, tri auto par groupe
    public static function rechercherUtilisateurParEtat($etat) {
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_UTILISATEUR` U, `T_STATUT_UTILISATEUR` SU "
                . " WHERE `STATUT_ID` = :etat "
                . " AND U.`UTILISATEUR_ID` = SU.`UTILISATEUR_ID` "
                . " ORDER BY U.`UTILISATEUR_GROUP` ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':etat',$etat,PDO::PARAM_INT);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $tabUtilisateurs=array();
        foreach($rows as $row){
            $utilisateur = new Utilisateur($row['UTILISATEUR_ID']);
            $utilisateur->getInfos();
            array_push($tabUtilisateurs, $utilisateur);
        }
        return $tabUtilisateurs;
    }
    
    public function ajouterReseauSociale($identifiant){
        if(!Utilisateur::utilisateurSocialeExiste($identifiant)){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "INSERT INTO `T_SOCIAL` VALUES (NULL, :id, :identifiant)";
            $id = $this->getId();

            $reponse = $bdd->prepare($requete);

            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->bindParam(':identifiant', $identifiant, PDO::PARAM_STR);

            $reponse->execute();   
        }
        
        
    }

    public function obtenirUtilisateurSocial($identifiant) {
        $existe = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT *  FROM `T_SOCIAL` U  WHERE `SOCIAL_IDENTIFIER` = :identifiant ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':identifiant', $identifiant, PDO::PARAM_STR);
        
        $reponse->execute(); 
        
        if($reponse->rowCount()>0){
            $row= $reponse->fetch();
            $this->setId($row['UTILISATEUR_ID']);
            $this->getInfos();
            $existe = true;
        }
        
        return $existe;
                
    }
    
    public static function utilisateurSocialeExiste($identifiant){
        $existe = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT *  FROM `T_SOCIAL` U  WHERE `SOCIAL_IDENTIFIER` = :identifiant ";
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':identifiant', $identifiant, PDO::PARAM_STR);
        
        $reponse->execute(); 
        
        if($reponse->rowCount()>0){
            $existe = true;
        }
        
        return $existe;
    }
    
    public static function getListeDroits(){
        // PS : Ne pas définir le voc d'après la langue car tri 
        $voc_administrateur = "Administrator";
        $voc_moderateur="Moderator";
        $voc_nonPorteur="Non project holder";
        $voc_porteur="Project holder";
        $voc_chercheur="Researcher";
        return array(1 => $voc_administrateur, 2 => $voc_moderateur, 3 => $voc_nonPorteur, 4 => $voc_porteur, 5 => $voc_chercheur);
    }
    
    public static function getNonPorteursNonValides(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT *  FROM `T_UTILISATEUR`  WHERE `UTILISATEUR_INACTIF` = 2 ";
        
        $reponse = $bdd->prepare($requete);
        
        $reponse->execute(); 
        $tabUtilisateurs=array();
        foreach ($reponse->fetchAll() as $row) {
            $utilisateur = new Utilisateur($row['UTILISATEUR_ID']);
            $utilisateur->getInfos();
            array_push($tabUtilisateurs,$utilisateur);
        }
        return $tabUtilisateurs; 
        
    }

}
