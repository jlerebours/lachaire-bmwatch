<?php

//Herite d entite
require_once dirname(__FILE__).'/EntiteClass.php';
class Produit extends Entite{
    
    private $ranking;
    private $entreprise;
    public function __construct($id, $entreprise="", $categorie="", $etat="", $description="", $dateCreation="", $dateDerniereModif="", $nom="",  $ranking="", $source ='',$idModif='') {
        parent::__construct($id, $categorie, $etat, $description, $dateCreation, $dateDerniereModif, $nom,$source,$idModif);
        $this->entreprise = $entreprise;
        $this->ranking = $ranking;
    }
    
    public function getEntreprise() {
        return $this->entreprise;
    }
    public function getRanking() {
        return $this->ranking;
    }  
 
    public function setEntreprise($entreprise) {
        $this->entreprise = $entreprise;
    }    
 
    public function setRanking($ranking){
        $this->ranking = $ranking;
    }
    
    public function ajouterALaBDProduit() {
        $ajoutEffectue=FALSE;
        if($this->ajouterAlaBD()){
            if($this->nomExiste()){
                $bdd = new AccesBD();
                $bdd = $bdd->getBdd();
                $requete = "INSERT INTO `T_PRODUIT_SERVICE` VALUES (:entite, :entreprise, :categorie , :etat, :description , :date, :date,:nom,:source, :ranking)";
                $entite = $this->getId();
                $entreprise = $this->getEntreprise();
                $categorie = $this->getCategorie();
                $etat = $this->getEtat();
                $description = $this->getDescription();
                $date = date("Y-m-d");
                $this->setDateCreation($date);
                $nom = $this->getNom();
                $source = $this->getSource();
                $ranking = $this->getRanking();
                $reponse = $bdd->prepare($requete);
                $reponse->bindParam(':entite',$entite, PDO::PARAM_INT);
                $reponse->bindParam(':entreprise',$entreprise, PDO::PARAM_INT);
                $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT);
                $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
                $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
                $reponse->bindParam(':date', $date, PDO::PARAM_STR);
                $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
                $reponse->bindParam(':source', $source, PDO::PARAM_STR);
                $reponse->bindParam(':ranking', $ranking, PDO::PARAM_INT);
                $reponse->execute(); 
                $ajoutEffectue=TRUE;
            }
        }
        return $ajoutEffectue;
    }
    
    public static function rechercherProduitSelonEtat($etat){
        $tabProduits=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE` WHERE `ETAT_ID` = :etat";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        $i=0;
        foreach($rows as $row){
            $produit = new Produit($row['ENTITE_ID'], $row['T_E_ENTITE_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['PRODUIT_SERVICE_RANKING']);
            $tabProduits[$i]=$produit;
            $i+=1;
        }
        return $tabProduits;
    }
    
    public function produitExiste(){
        $resultat = FALSE;
        if($this->nomExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
        
            $requete = "SELECT `ENTITE_ID` FROM `T_PRODUIT_SERVICE` WHERE `ENTITE_NOM` = :nom";
        $reponse = $bdd->prepare($requete);
        $nom = $this->getNom();
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
            $donneesProduit = $reponse->fetch();
            $this->setId($donneesProduit['ENTITE_ID']);
        }
        }
        return $resultat;
    }
    
    public static function estUnProduit($id){
        $resultat = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_PRODUIT_SERVICE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        
        return $resultat;
        
    }
        
    public function supprimerProduit() {
        if($this->idExiste()){
            $this->supprimerBM();
            
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            
            $requete = "UPDATE `T_PRODUIT_SERVICE` SET `ETAT_ID` = '7' WHERE `ENTITE_ID` = :id ";    
            $id = $this->getId();    
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            
            $reponse->execute();
            
            $this->supprimer();
        }
    }
    
    public function validProduit(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_PRODUIT_SERVICE` SET `ETAT_ID` = '1' WHERE `ENTITE_ID` = :id ";
        $idProduit = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $idProduit, PDO::PARAM_INT);
        
        $reponse->execute();
        $this->valider();
    }
    
    public static function obtenirProduit($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT,1);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        foreach($rows as $row){
            $produit = new Produit($row['ENTITE_ID'], $row['T_E_ENTITE_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['PRODUIT_SERVICE_RANKING']);
        }
        return $produit;
    }
    
    public static function getProduitModifie(){
        $tabProduitsModifie=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE_MODIFIE` WHERE `ETAT_ID` = 5";
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        $i=0;
        foreach($rows as $row){
            $produitModif = new Produit($row['ENTITE_ID'], '',
                                        $row['CATEGORIE_ID'], $row['ETAT_ID'],    
                                        $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                        $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                        $row['PRODUIT_SERVICE_RANKING']);
            $tabProduitsModifie[$i]=$produitModif;
            $i+=1;
        }
        return $tabProduitsModifie;
    }
    
    public function supprimerProduitModif() {
        if($this->idExiste()){
            $this->supprimerBM();
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "DELETE FROM `T_PRODUIT_SERVICE_MODIFIE` WHERE `ENTITE_ID` = :id ";    
            $id = $this->getId();    
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->execute();
        }
    }

    public function updateProduit(){
        // On commence par maj les infos dans entité
        $this->updateProduitEntite();
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_PRODUIT_SERVICE` p INNER JOIN `T_PRODUIT_SERVICE_MODIFIE` pm ON p.`ENTITE_ID` = pm.`ENTITE_ID`
                    SET p.`CATEGORIE_ID` = pm.`CATEGORIE_ID`, p.`ENTITE_DESCRIPTION` = pm.`ENTITE_DESCRIPTION`, p.`ENTITE_DATE_DERNIERE_MODIF` = pm.`ENTITE_DATE_DERNIERE_MODIF`,
                    p.`ENTITE_NOM` = pm.`ENTITE_NOM`, p.`PRODUIT_SERVICE_RANKING` = pm.`PRODUIT_SERVICE_RANKING`
                    WHERE p.ENTITE_ID = :id";
        $idEntreprise = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $idEntreprise, PDO::PARAM_INT);
        $reponse->execute();
        $this->supprimerProduitModif();
    }
    
    public function modifierProduit(){
        $this->modifier();
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_PRODUIT_SERVICE` SET `CATEGORIE_ID` = :categorie , `ETAT_ID` = :etat , `ENTITE_DESCRIPTION` = :description ,
                    `ENTITE_DATE_DERNIERE_MODIF` = :dateModif , `ENTITE_NOM` = :nom  WHERE `ENTITE_ID` = :id";
        $entite = $this->getId();
        
        $id = $this->getId();
        $categorie = $this->getCategorie();
        $etat = $this->getEtat();
        $description = $this->getDescription();
        $date = date("Y-m-d");
        $nom = $this->getNom();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT,2);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
        $reponse->bindParam(':dateModif', $date, PDO::PARAM_STR);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute();
        
    }
    
    public static function obtenirProduitModifier($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE_MODIFIE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT,1);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        foreach($rows as $row){
            $produit = new Produit($row['ENTITE_ID'],"", 
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['PRODUIT_SERVICE_RANKING'],"",$row['ID_MODIFICATEUR_PRODUIT']);
        }
        return $produit;
    }
    
    public function ajouterProduitModifier(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "INSERT INTO `T_PRODUIT_SERVICE_MODIFIE` VALUES (:entite, :idModif,:categorie , :etat, :description , :date, :dateModif,:nom, :source, :ranking)";       
        $entite = $this->getId();
        $idModif = $this->getIdModif();
        $categorie = $this->getCategorie();
        $etat = $this->getEtat();
        $description = $this->getDescription();
        $date = $this->getDateCreation();
        $dateModif = date("Y-m-d");
        $this->setDateDerniereModif($dateModif);
        $nom = $this->getNom();
        $source = $this->getSource();
        $ranking = $this->getRanking();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':entite',$entite, PDO::PARAM_INT);
        $reponse->bindParam(':idModif',$idModif, PDO::PARAM_INT);
        $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
        $reponse->bindParam(':date', $date, PDO::PARAM_STR);
        $reponse->bindParam(':dateModif', $dateModif, PDO::PARAM_STR);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->bindParam(':source', $source, PDO::PARAM_STR);
        $reponse->bindParam(':ranking', $ranking, PDO::PARAM_INT);
        $reponse->execute();      
        
    }
    
    public function changerEtatProduit($nouvelEtat) {
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_PRODUIT_SERVICE` SET `ETAT_ID` = :idEtat WHERE `ENTITE_ID` = :idEntite "; 
        
        $idEntite=$this->getId();
        $reponse = $bdd->prepare($requete);
        
        $reponse->bindParam(':idEtat',$nouvelEtat, PDO::PARAM_INT,1);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        
        $reponse->execute();
        $this->changerEtat($nouvelEtat);
    }   
    
    public static function getApps($table, $rank){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        switch ($table) {
            case 'Android':
                $requete = "SELECT  `app_id`,`app_name`,`app_firm`,`app_Desc`,`app_Score`  FROM `t_android_app` WHERE `app_score` > :rank"; 
                break;
            case 'Apple':
                $requete = "SELECT `apple_id`,`apple_Name`,`apple_Firm`,`apple_Desc`,`apple_AllVersionsScore` FROM `t_apple_app` WHERE `apple_AllVersionsScore` > :rank"; 
                break;
            case 'Windows':
                $requete = "SELECT `windows_app_id`,`windows_app_Name`,`windows_app_Firm`,`windows_app_Desc`,`windows_app_Score` FROM `t_windows_app` WHERE `windows_app_Score` > :rank"; 
                break;
        }
        
        $reponse = $bdd->prepare($requete);
        
        $reponse->bindParam(':rank',$rank);
        
        $reponse->execute();
        
        return $reponse->fetchAll();
    }
    
    public static function getMinMaxRankApps($table){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        switch ($table) {
            case 'Android':
                $requete = "SELECT  MIN(`app_Score`), MAX(`app_Score`)  FROM `t_android_app`"; 
                break;
            case 'Apple':
                $requete = "SELECT MIN(`apple_AllVersionsScore`), MAX(`apple_AllVersionsScore`) FROM `t_apple_app`"; 
                break;
            case 'Windows':
                $requete = "SELECT MIN(`windows_app_Score`), MAX(`windows_app_Score`) FROM `t_windows_app` "; 
                break;
        }
        
        $reponse = $bdd->prepare($requete);
        
        $reponse->execute();
        
        return $reponse->fetch();
    }
    
    public static function transfererApps($table, $listeApps){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        foreach ($listeApps as $app) {
            
            switch ($table) {
                case 'Android':
                    $requete = "SELECT  `app_name`,`app_firm`,`app_Desc`,`app_Score`  FROM `t_android_app` WHERE `app_id` = :id "; 
                break;
                case 'Apple':
                    $requete = "SELECT `apple_Name`,`apple_Firm`,`apple_Desc`,`apple_AllVersionsScore` FROM `t_apple_app` WHERE `apple_id` = :id"; 
                    break;
                case 'Windows':
                    $requete = "SELECT `windows_app_Name`,`windows_app_Firm`,`windows_app_Desc`,`windows_app_Score` FROM `t_windows_app` WHERE `windows_app_id` = :id"; 
                    break;
            }
            
            $reponse = $bdd->prepare($requete);
            $idApp = intval($app);
            $reponse->bindParam(':id',$idApp);
            $reponse->execute();
            $row = $reponse->fetch();
            $produit = new Produit(NULL,NULL,NULL,6,$row[2],NULL,NULL,$row[0],$row[3]);
//            $produit = new Produit(NULL,NULL,NULL,6,'nouvelleDesc',NULL,NULL,'nouveauNom',6);
            $produit->ajouterALaBDProduit();
            
            // Recup lastInsertRow puis modif des autres infos
        }
        return $reponse->fetch();
    }
}
