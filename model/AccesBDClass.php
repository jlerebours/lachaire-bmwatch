<?php

/**
 * Description of accesBDClass
 *
 * @author julien
 */

require dirname(__FILE__).'/../config/configLocal.php';

class AccesBD {
    private $bdd;
    
    public function __construct(){
        $this->bdd = new PDO('mysql:host='.BDD_HOST.';dbname='.BDD_BASE, BDD_USER, BDD_PASS);
        $this->bdd->exec("SET CHARACTER SET utf8");
        
    }
    
    public function getBdd(){
        return $this->bdd;
    }
    
}
