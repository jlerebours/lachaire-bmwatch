<?php

require_once dirname(__FILE__).'/AccesBDClass.php';
require_once dirname(__FILE__).'/EntiteClass.php';
require_once dirname(__FILE__).'/ProduitClass.php';
require_once dirname(__FILE__).'/EntrepriseClass.php';
require_once dirname(__FILE__).'/BMClass.php';

/**
 * Description de la class entite
 *
 * @author olivier
 */

class Entite{
    private $id;
    private $categorie;
    private $etat;
    private $description;
    private $dateCreation;
    private $dateDerniereModif;
    private $nom;
    private $source;
    private $idModif;
    
    public function __construct($id, $categorie='', $etat='', $description='', $dateCreation='', $dateDerniereModif='', $nom='',$source='',$idModif='') {
        $this->id = $id;
        $this->categorie = $categorie;
        $this->etat = $etat;
        $this->description = $description;
        $this->dateCreation = $dateCreation;
        $this->dateDerniereModif = $dateDerniereModif;
        $this->nom = $nom;
        $this->source = $source;
        $this->idModif = $idModif;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getCategorie() {
        return $this->categorie;
    }

    public function getEtat() {
        return $this->etat;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getDateCreation() {
        return $this->dateCreation;
    }
    
    public function getDateDerniereModif() {
        return $this->dateDerniereModif;
    }
    
    public function getNom(){
        return $this->nom;
    }
    
    public function getSource(){
        return $this->source;
    }
    
    public function getIdModif(){
        return $this->idModif;
    }    
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    public function setEtat($etat) {
        $this->etat = $etat;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setDateCreation($dateCreation) {
        $this->dateCreation = $dateCreation;
    }
    public function setDateDerniereModif($dateDerniereModif) {
        $this->dateDerniereModif = $dateDerniereModif;
    }
    
    public function setNom($nom){
        $this->nom = $nom;
    }
    
    public function setSource($source){
        $this->source = $source;               
    }
    
    public function setIdModif($idModif){
        $this->idModif = $idModif;               
    }    
    
    public function getType(){
        $resultat=0;
        if(Entreprise::estUneEntreprise($this->getId())){
            $resultat = 1;
        }
        else if(Produit::estUnProduit($this->getId())){
            $resultat = 2;
        }
        else{
            $resultat=0;
        }
        return $resultat;
    }
    // Récupère les infos de l'utilisateur si celui-ci possède un id qui existe
    public function getInfos() {
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = " SELECT * "
                    . " FROM `T_ENTITE_CARACTERISEE` "
                    . " WHERE `ENTITE_ID` = :id ";

            $reponse = $bdd->prepare($requete);
            $id = $this->getId();
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            
            $row = $reponse->fetch();
            $this->setNom($row['ENTITE_NOM']);
            $this->setCategorie($row['CATEGORIE_ID']);
            $this->setEtat($row['ETAT_ID']);
            $this->setDescription($row['ENTITE_DESCRIPTION']);
            $this->setDateCreation($row['ENTITE_DATE_CREATION']);
            $this->setDateDerniereModif($row['ENTITE_DATE_DERNIERE_MODIF']);
            $this->setSource($row['ENTITE_SOURCE']);
        }
    }
    
    public function nomExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_ENTITE_CARACTERISEE` WHERE `ENTITE_NOM` = :nom";
        $reponse = $bdd->prepare($requete);
        $nom = $this->getNom();
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
            $donneesEntite = $reponse->fetch();
            $this->setId($donneesEntite['ENTITE_ID']);
        }
        return $resultat;
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_ENTITE_CARACTERISEE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        
        return $resultat;
    }

    
    public function ajouterALaBD() {
        $ajoutEffectue=FALSE;
        if(!$this->nomExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "INSERT INTO `T_ENTITE_CARACTERISEE` VALUES (NULL, :categorie , :etat, :description , :date, :date,:nom, :source)";
            
            $categorie = $this->getCategorie();
            $etat = $this->getEtat();
            $description = $this->getDescription();
            $date = date("Y-m-d");
            $this->setDateCreation($date);
            $source = $this->getSource();
            $nom = $this->getNom();
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT,1);
            $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
            $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
            $reponse->bindParam(':date', $date, PDO::PARAM_STR);
            $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
            $reponse->bindParam(':source', $source, PDO::PARAM_STR);
            $reponse->execute(); 
            $ajoutEffectue=TRUE;
        }
        return $ajoutEffectue;
    }
    
    public static function rechercherSelonEtat($etat){
        $tabEntites=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_ENTITE_CARACTERISEE` WHERE `ETAT_ID` = :etat";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        
        $i=0;
        foreach($rows as $row){
            $entite = new Entite($row['ENTITE_ID'], $row['CATEGORIE_ID'], $row['ETAT_ID'], $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'], $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM']);
            $tabEntites[$i]=$entite;
            $i+=1;
        }
        return $tabEntites;
    }
    
    public function changerEtat($nouvelEtat) {
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` SET `ETAT_ID` = :idEtat WHERE `ENTITE_ID` = :idEntite "; 
        
        $idEntite=$this->getId();
        $reponse = $bdd->prepare($requete);
        
        $reponse->bindParam(':idEtat',$nouvelEtat, PDO::PARAM_INT,1);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        
        $reponse->execute();
    }
    public function supprimerAssignation() {
        // On commence par vérifier que l'utilisateur est un modérateur
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();

        // Normalement une entité ne peut être attribuée qu'une fois
        $requete = "DELETE FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` "
                . "WHERE `ENTITE_ID` = :idEntite";

        $idEntite = $this->getId();

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);

        $assignationEffectuee = $reponse->execute();
        
        return $assignationEffectuee;
    }
    
    public static function obtenirCriteres(){
        $tabCriteres=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_CRITERE` ";
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        
        $i=0;
        
        foreach($rows as $row){
            $tabCriteres[$i][1]=$row['CRITERE_ID'];
            $tabCriteres[$i][2]=$row['CRITERE_NOM'];
            $tabCriteres[$i][3]=$row['CRITERE_TYPE'];
            $i+=1;
        }
        return $tabCriteres;
    }
    
    public static function obtenirValeursCritere($idCritere){
        $tabValeurs = array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_CRITERE_VALEUR` WHERE `CRITERE_ID` = :idCritere";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idCritere',$idCritere, PDO::PARAM_INT);
        $reponse->execute();        
        $rows = $reponse->fetchAll();
        
        $i=0;
        
        foreach($rows as $row){
            $tabValeurs[$i][1]=$row['CV_ID'];
            $tabValeurs[$i][2]=$row['CV_NOM'];
            $i+=1;
        }
        return $tabValeurs;        
        
    }
    
    
    public function obtenirCriteresFixees(){
        $tabCriteres=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `R_ENTITE_POSSEDE_CRITERES` R, `T_CRITERE` C, `T_CRITERE_VALEUR` CV WHERE R.`ENTITE_ID` = :id AND R.`CV_ID` = CV.`CV_ID` AND C.`CRITERE_ID`=CV.`CRITERE_ID` ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        
        $i=0;
        
        foreach($rows as $row){
            $tabCriteres[$i]=array("","","","","");               
            $tabCriteres[$i][1]=$row['CRITERE_ID'];            
            $tabCriteres[$i][2]=$row['CRITERE_NOM'];
            $tabCriteres[$i][3]=$row['CRITERE_TYPE'];
            $tabCriteres[$i][4]=$row['CV_ID'];
            $tabCriteres[$i][5]=$row['CRITERE_VALEUR'];
            $i+=1;
        }
        return $tabCriteres;
    }
    
    public function fixerCriteres($tabCriteres){
        var_dump($tabCriteres);
        for($i=0;$i<count($tabCriteres);$i++){
            if($tabCriteres[$i][0]!=NULL){
                $bdd = new AccesBD();
                $bdd = $bdd->getBdd();

                $requete = "INSERT INTO `R_ENTITE_POSSEDE_CRITERES` VALUES (:idEntite, :idCV, :valeurCritere)";

                $idEntite = $this->getId();
                var_dump($idEntite);
                $idCV = $tabCriteres[$i][0];
                var_dump($idCV);
                $valeurCritere = $tabCriteres[$i][1];
                var_dump($valeurCritere);
                $reponse = $bdd->prepare($requete);
                $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
                $reponse->bindParam(':idCV',$idCV, PDO::PARAM_INT);
                $reponse->bindParam(':valeurCritere', $valeurCritere, PDO::PARAM_STR,50);
                $reponse->execute(); 
            }
        }
    }
    
    public static function obtenirValeurCritere($idCV){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $cvNom = null;
        $requete = "SELECT * FROM `T_CRITERE_VALEUR` WHERE `CV_ID` = :idCV";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idCV',$idCV, PDO::PARAM_INT);
        $reponse->execute();       
               
        if($reponse->rowCount()> 0){
            $donnees = $reponse->fetch();
            $cvNom = $donnees['CV_NOM'];
            
        }
        return $cvNom;  

            
        
    }
        
    public static function obtenirCategories($type=0){
        $tabCategories=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        if($type==0){
            $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE` WHERE `T_C_CATEGORIE_ID` IS NULL";
            $reponse = $bdd->prepare($requete);
        }
        else{
            $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE` WHERE `T_C_CATEGORIE_ID` IS NULL AND ( `CATEGORIE_TYPE` IS NULL OR `CATEGORIE_TYPE` = :type)";
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':type', $type, PDO::PARAM_INT,1);
        }
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $i = 0;
        foreach($rows as $row){
            $tabCategories[$i][1]=$row['CATEGORIE_ID'];
            $tabCategories[$i][2]=$row['T_C_CATEGORIE_ID'];
            $tabCategories[$i][3]=$row['CATEGORIE_NOM'];
            $tabCategories[$i][4]=$row['CATEGORIE_DESCRIPTION'];
            $tabCategories[$i][5]=0;//On stock son niveau
            $tabCategories = Entite::obtenirEnfant($tabCategories,$i,0,$type);
            $i=count($tabCategories);
        }
        return $tabCategories;
    }
    
    public static function obtenirCategorie($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $nom = "";
        
        $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE` WHERE `CATEGORIE_ID` = :id ";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $donnees = $reponse->fetch();
            $nom = $donnees['CATEGORIE_NOM'];
        }
        return $nom;
    }
    
    //Permet de recuperer les categorie enfants
    public static function obtenirEnfant($tabCategories,$i,$niveau,$type=0){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        if($type==0)
            $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE` WHERE `T_C_CATEGORIE_ID`  = :idParent";
        else
            $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE` WHERE `T_C_CATEGORIE_ID`  = :idParent AND ( `CATEGORIE_TYPE` IS NULL OR `CATEGORIE_TYPE` = :type)";
        $idParent = $tabCategories[$i][1];
        $reponse = $bdd->prepare($requete);
        if($type!=0)
            $reponse->bindParam(':type', $type, PDO::PARAM_INT,1);
        $reponse->bindParam(':idParent',$idParent, PDO::PARAM_INT);
        $reponse->execute();
        if($reponse->rowCount()> 0){
            $i+=1;
            $niveau+=1;
            $rows = $reponse->fetchAll();
            foreach($rows as $row){
                $tabCategories[$i][1]=$row['CATEGORIE_ID'];                
                $tabCategories[$i][2]=$row['T_C_CATEGORIE_ID'];
                $tabCategories[$i][3]=$row['CATEGORIE_NOM'];
                $tabCategories[$i][4]=$row['CATEGORIE_DESCRIPTION'];
                $tabCategories[$i][5]=$niveau;
                $tabCategories = Entite::obtenirEnfant($tabCategories,$i,$niveau);
                $i = count($tabCategories);
            }
            
        }
        
        return $tabCategories;
        
    }
    
    public function miseAJour($idBM){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $statut = "ANCIEN";
        $idEntite = $this->getId();
        $actuel = "ACTUEL";
        $requete = "UPDATE `R_ENTITE_CARACTERISEE_PAR_BMS` SET `STATUT` = :statut WHERE `ENTITE_ID` = :idEntite AND `BM_ID` != :idBM AND `STATUT` LIKE :actuel ";

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT,1);
        $reponse->bindParam(':idBM',$idBM, PDO::PARAM_INT,1);
        $reponse->bindParam(':statut', $statut, PDO::PARAM_STR,50);
        $reponse->bindParam(':actuel', $actuel, PDO::PARAM_STR,50);
        $reponse->execute();
    
    }
    
    public function relationExiste($idBM){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `BM_ID` FROM `R_ENTITE_CARACTERISEE_PAR_BMS` WHERE `BM_ID` = :idBm AND `ENTITE_ID` = :idEntite";
        $reponse = $bdd->prepare($requete);
        $idEntite = $this->getId();
        $reponse->bindParam(':idBm', $idBM, PDO::PARAM_INT,1);
        $reponse->bindParam(':idEntite', $idEntite, PDO::PARAM_INT,1);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        return $resultat;
        
    }
    
    public function ajouterBM($idBM,$commentaire){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $ajoutEffectue = FALSE;
        if(BM::bmExisteId($idBM)){
            if(!$this->relationExiste($idBM)){
                $requete = "INSERT INTO `R_ENTITE_CARACTERISEE_PAR_BMS` VALUES (:idBM, :idEntite , :statut, :date , :commentaire)";
                $idEntite = $this->getId();
                $date = date("Y-m-d");
                $statut = "ACTUEL";
                $reponse = $bdd->prepare($requete);
                $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
                $reponse->bindParam(':idBM',$idBM, PDO::PARAM_INT);
                $reponse->bindParam(':statut', $statut, PDO::PARAM_STR,50);
                $reponse->bindParam(':date', $date, PDO::PARAM_STR);
                $reponse->bindParam(':commentaire', $commentaire, PDO::PARAM_STR,250);
                $reponse->execute();
                $this->miseAJour($idBM);
                $ajoutEffectue = TRUE;
            }
        }
        return $ajoutEffectue;
    }
    
    public function supprimerBM(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "DELETE FROM `R_ENTITE_CARACTERISEE_PAR_BMS` WHERE `ENTITE_ID` = :idEntite";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
    }
    
    public function enleverCriteres(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "DELETE FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `ENTITE_ID` = :idEntite";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
        
    }
    public function supprimer(){
        $this->enleverCriteres();
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` SET `ETAT_ID` = '7' WHERE`ENTITE_ID` = :idEntite";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
    }
    
    public function valider(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` SET `ETAT_ID` = '1' WHERE `ENTITE_ID` = :id ";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
    }
    
        
    public static function estUneEntite($id){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_ENTITE_CARACTERISEE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        
        return $resultat;
    }
    
    public function obtenirBM(){
        $bm = null;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_BM` T, `R_ENTITE_CARACTERISEE_PAR_BMS` R WHERE R.`ENTITE_ID` = :id AND R.`STATUT` LIKE :statut AND R.`BM_ID` = T.`BM_ID`";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $statut = "ACTUEL";
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->bindParam(':statut', $statut, PDO::PARAM_STR,30);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $donneesBM = $reponse->fetch();
            $bm = new BM($donneesBM['BM_ID'],$donneesBM['BM_NOM'],$donneesBM['BM_PROFIL'],$donneesBM['BM_PRINCIPE'],$donneesBM['BM_DECLINAISONS'],$donneesBM['BM_VARIANTES']);
            
        }
        return $bm;
    }
    public function possedeBM(){
        $resultat = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `BM_ID` FROM `R_ENTITE_CARACTERISEE_PAR_BMS` WHERE `ENTITE_ID` = :id ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
        $resultat = true;
        }
        return $resultat;
    }
    
    public function updateEntrepriseEntite(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` e INNER JOIN `T_ENTREPRISE_MODIFIE` m ON e.`ENTITE_ID` = m.`ENTITE_ID`
                    SET e.`CATEGORIE_ID` = m.`CATEGORIE_ID`, e.`ENTITE_DESCRIPTION` = m.`ENTITE_DESCRIPTION`, e.`ENTITE_DATE_DERNIERE_MODIF` = m.`ENTITE_DATE_DERNIERE_MODIF`, e.`ENTITE_NOM` = m.`ENTITE_NOM`
                    WHERE e.ENTITE_ID = :id";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
    }
    
    public function updateProduitEntite(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` e INNER JOIN `T_PRODUIT_SERVICE_MODIFIE` p ON e.`ENTITE_ID` = p.`ENTITE_ID`
                    SET e.`CATEGORIE_ID` = p.`CATEGORIE_ID`, e.`ENTITE_DESCRIPTION` = p.`ENTITE_DESCRIPTION`, e.`ENTITE_DATE_DERNIERE_MODIF` = p.`ENTITE_DATE_DERNIERE_MODIF`, e.`ENTITE_NOM` = p.`ENTITE_NOM`
                    WHERE e.ENTITE_ID = :id";
        $idEntite = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$idEntite, PDO::PARAM_INT);
        $reponse->execute();
    }
    
    public function modifier(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTITE_CARACTERISEE` SET `CATEGORIE_ID` = :categorie , `ETAT_ID` = :etat , `ENTITE_DESCRIPTION` = :description ,
                    `ENTITE_DATE_DERNIERE_MODIF` = :dateModif , `ENTITE_NOM` = :nom WHERE `ENTITE_ID` = :id";
        $entite = $this->getId();
        
        $id = $this->getId();
        $categorie = $this->getCategorie();
        $etat = $this->getEtat();
        $description = $this->getDescription();
        $date = date("Y-m-d");
        $nom = $this->getNom();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT,2);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
        $reponse->bindParam(':dateModif', $date, PDO::PARAM_STR);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute();
    }
    
    public function modifierCritere($tabCriteres){
        $this->enleverCriteres();
        $this->fixerCriteres($tabCriteres);
    }
    
    public function modifierBM($idBM){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $idEntite = $this->getId();
        $actuel = "ACTUEL";
        $requete = "UPDATE `R_ENTITE_CARACTERISEE_PAR_BMS` SET `BM_ID` = :idBm WHERE `ENTITE_ID` = :idEntite  AND `STATUT` LIKE :actuel ";

        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        $reponse->bindParam(':idBm',$idBM, PDO::PARAM_INT);
        $reponse->bindParam(':actuel', $actuel, PDO::PARAM_STR,50);
        $reponse->execute();
    }
    
    public function getHistoriqueBm(){
        $tabHistorique=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `R_ENTITE_CARACTERISEE_PAR_BMS` INNER JOIN `T_BM` WHERE `R_ENTITE_CARACTERISEE_PAR_BMS`.`BM_ID` = `T_BM`.`BM_ID` AND `ENTITE_ID` = :id ORDER BY `DATE`";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        if($reponse->rowCount()> 0){
            $i=0;
            $rows = $reponse->fetchAll();
            foreach($rows as $row){
                $tabHistorique[$i][1]=$row['BM_NOM'];
                $tabHistorique[$i][2]=$row['DATE'];
                $tabHistorique[$i][3]=$row['STATUT'];
                $tabHistorique[$i][4]=$row['COMMENTAIRE'];
                $tabHistorique[$i][5]=$row['BM_ID'];
                $i++;
            }
        }
        return $tabHistorique;
    }
    
    public function getSourceBM(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $source = "";
        
        $requete = "SELECT `ENTITE_SOURCE` FROM `T_ENTITE_CARACTERISEE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $donnees = $reponse->fetch();
            $source = $donnees['ENTITE_SOURCE'];
        }
        return $source;
    }
    
    public function getNonPorteurAssigne(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $idNonPorteur= "";
        
        $requete = "SELECT * FROM `T_NON_PORTEUR_ASSIGNE_MODERATEUR` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $donnees = $reponse->fetch();
            $idNonPorteur = $donnees['NON_PORTEUR_ID'];
        }
        return $idNonPorteur;        
    }
    
    //Fonction pour recuperer l'etat correspondant au numero
     public static function getListeEtats(){
         
        $voc_valide = "Valid";
        $voc_nouveau = "New";
        $voc_assigne = "Assigned";
        $voc_traite="Treated";
        $voc_modifie="Modified";
        $voc_aFaire="ToDo";
        $voc_inactif="Disabled";
        return array(1 => $voc_valide, 2 => $voc_nouveau, 3 => $voc_assigne, 4 => $voc_traite, 5 => $voc_modifie, 6 => $voc_aFaire, 7 => $voc_inactif);
    }
    
    //Fonction pour recuperer l'etat correspondant au numero
     public static function getListeTypes(){
         
        $voc_entreprise = "Company";
        $voc_produit = "Product";
        return array(1 => $voc_entreprise, 2 => $voc_produit);
    }
    
    public static function getEntitesChercheur(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $source = "";
        
        $requete = "SELECT `ENTITE_SOURCE` FROM `T_ENTITE_CARACTERISEE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $id = "5";
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
        if($reponse->rowCount()> 0){
            $donnees = $reponse->fetch();
            $source = $donnees['ENTITE_SOURCE'];
        }
        return $source;
    }

}