<?php
    require_once 'AccesBDClass.php';

class Categorie{
    private $id;
    private $idCatParente;
    private $nom;
    private $description;
    private $range;
    
    public function __construct($id, $idCatParente='', $nom='',$description='',$range=0){      
        $this->id = $id;
        $this->idCatParente = $idCatParente;
        $this->nom = $nom;
        $this->description = $description;
        $this->range = $range;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getIdCatParente() {
        return $this->idCatParente;
    }
    
    public function getNom() {
        return $this->nom;
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function getRange() {
        return $this->range;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setIdCatParente($idCatparente) {
        $this->idCatParente = $idCatparente;
    }
    
    public function setNom($nom) {
        $this->nom= $nom;
    }
    
    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function setRange($range) {
        $this->range = $range;
    }
    
    public function getInfos(){
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "SELECT * "
                    . " FROM `T_CATEGORIE_GENERIQUE` CG"
                    . " WHERE CG.`CATEGORIE_ID` = :id ";

            $reponse = $bdd->prepare($requete);

            $id = $this->getId();
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            
            if($reponse->rowCount()>1){
                error_log("Error: non unicite de l'identifiant");
            }
        }
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_CATEGORIE_GENERIQUE` CG"
                . " WHERE CG.`CATEGORIE_ID` = :id ";
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
    public function ajouterALaBD(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Insertion dans la table T_CRITERE
        $requete = "INSERT INTO `T_CATEGORIE_GENERIQUE`(`CATEGORIE_ID`, `T_C_CATEGORIE_ID`, `CATEGORIE_NOM`, `CATEGORIE_DESCRIPTION`, `CATEGORIE_TYPE`) "
                . " VALUES (NULL,:idCatParente,:nom,:description,:range)";

        $reponse = $bdd->prepare($requete);
        $nom = $this->getNom();
        $idCatParente = $this->getIdCatParente();
        $description = $this->getDescription();
        $range= $this->getRange();
        if($range==0)
            $range=NULL;
        $reponse->bindParam(':idCatParente', $idCatParente, PDO::PARAM_INT);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR);
        $reponse->bindParam(':range', $range, PDO::PARAM_INT,1);
        $reponse->execute();
        
    }
    
    public function supprimer(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Suppression des valeurs du critere dans la table T_CRITERE_VALEUR
        $requete = "DELETE FROM `T_CATEGORIE_GENERIQUE` "
                . " WHERE `CATEGORIE_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute();
    }
        
    public static function getTousLesCategories(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_CATEGORIE_GENERIQUE`" ;

        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        return $reponse->fetchAll();
    }
    
  
}