<?php

/**
 * Description of FaqClass
 *
 * @author julien
 */
class Faq {
    private $id;
    private $categorie;
    private $question;
    private $reponse;
    
    
    public function __construct($id, $categorie='', $question='', $reponse='') {
        $this->id = $id;
        $this->categorie = $categorie;
        $this->question = $question;
        $this->reponse = $reponse;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getCategorie() {
        return $this->categorie;
    }

    public function getQuestion() {
        return $this->question;
    }

    public function getReponse() {
        return $this->reponse;
    }
    
    public function setId($id) {
        $this->id = $id;
    }

    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    public function setQuestion($question) {
        $this->question = $question;
    }

    public function setReponse($reponse) {
        $this->reponse = $reponse;
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
                . " FROM `T_FAQ`"
                . " WHERE `FAQ_ID` = :id ";
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
    public function getInfos(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT *  FROM `T_FAQ` WHERE `FAQ_ID` = :id";
        
        $id = $this->getId();
        
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        
        $row = $reponse->fetch();
        $this->setCategorie($row['FAQ_CATEGORIE']);
        $this->setQuestion($row['FAQ_QUESTION']);
        $this->setReponse($row['FAQ_REPONSE']);
    }
    
    public function ajouterALaBD(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Insertion dans la table T_CRITERE
        $requete = "INSERT INTO `T_FAQ` VALUES (NULL,:categorie,:question,:reponse)";

        $reponseRequete = $bdd->prepare($requete);
        $categorie = $this->getCategorie();
        $question = $this->getQuestion();
        $reponse= $this->getReponse();
        
        $reponseRequete->bindParam(':categorie', $categorie, PDO::PARAM_STR);
        $reponseRequete->bindParam(':question', $question, PDO::PARAM_STR);
        $reponseRequete->bindParam(':reponse', $reponse, PDO::PARAM_STR);
        $reponseRequete->execute();
        
    }
    
     public function update(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Insertion dans la table T_CRITERE
        $requete = "UPDATE `T_FAQ` SET `FAQ_CATEGORIE`=:categorie,`FAQ_QUESTION`=:question,`FAQ_REPONSE`=:reponse WHERE `FAQ_ID` = :id";

        $reponseRequete = $bdd->prepare($requete);
        
        $id = $this->getId();
        $categorie = $this->getCategorie();
        $question = $this->getQuestion();
        $reponse= $this->getReponse();
        
        $reponseRequete->bindParam(':id', $id, PDO::PARAM_INT);
        $reponseRequete->bindParam(':categorie', $categorie, PDO::PARAM_STR);
        $reponseRequete->bindParam(':question', $question, PDO::PARAM_STR);
        $reponseRequete->bindParam(':reponse', $reponse, PDO::PARAM_STR);
        $reponseRequete->execute();
        
    }
    
    public function supprimer(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        // Suppression des valeurs du critere dans la table T_CRITERE_VALEUR
        $requete = "DELETE FROM `T_FAQ` "
                . " WHERE `FAQ_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute();
    }
    
    public static function obtenirListeFaq(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT *  FROM `T_FAQ` ";
        
        $reponse = $bdd->prepare($requete);
        
        $reponse->execute(); 
        $tabFaqs=array();
        foreach ($reponse->fetchAll() as $row) {
            $faq = new Faq($row['FAQ_ID'],$row['FAQ_CATEGORIE'],$row['FAQ_QUESTION'],$row['FAQ_REPONSE']);
            array_push($tabFaqs,$faq);
        }
        return $tabFaqs; 
        
    }
    
    public static function getListeCategories(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_FAQ` GROUP BY `FAQ_CATEGORIE`";
        
        $reponse = $bdd->prepare($requete);
        $reponse->execute(); 
        $tabCategories=array();
        foreach ($reponse->fetchAll() as $row) {
            array_push($tabCategories,$row['FAQ_CATEGORIE']);
        }
        return $tabCategories; 
    }
    
    public static function getListeParCategorie($categorie){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_FAQ` WHERE `FAQ_CATEGORIE` = :categorie";
        
        $reponse = $bdd->prepare($requete);
        
        $reponse->bindParam(':categorie', $categorie, PDO::PARAM_STR);
        $reponse->execute(); 
        $tabCategories=array();
        foreach ($reponse->fetchAll() as $row) {
            $faq = new Faq($row['FAQ_ID'],$row['FAQ_CATEGORIE'],$row['FAQ_QUESTION'],$row['FAQ_REPONSE']);
            array_push($tabCategories,$faq);
        }
        return $tabCategories; 
    }
}
