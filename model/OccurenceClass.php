<?php
    require_once 'OccurenceClass.php';
    require_once 'AccesBDClass.php';

class Occurence{
    private $id;
    private $idDimAsociee;
    private $nom;
    private $statut;
    private $description ;
    
    public function __construct($id, $idDimAsociee='', $nom='', $statut='', $description=''){      
        $this->id = $id;
        $this->idDimAsociee = $idDimAsociee;
        $this->nom = $nom;
        $this->statut = $statut;
        $this->description = $description;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getIdDim() {
        return $this->idDimAsociee;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getStatut() {
        return $this->statut;
    }

    public function getDescription() {
        return $this->description;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setIdDim($idDim) {
        $this->idDimAsociee = $idDim;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }
    
    public function setStatut($statut) {
        $this->statut = $statut;
    }
    
    public function setDescription($description) {
        $this->description = $description;
    }

    public function getInfos(){
        
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "SELECT * FROM `T_OCCURENCE` "
                    . " WHERE `OC_ID` = :id ";

            $reponse = $bdd->prepare($requete);

            $id = $this->getId();
            
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            if($reponse->rowCount()>1){
                error_log("Problem : plus d'une occurence avec le même id !");
            }
            $row = $reponse->fetch();
            $this->setIdDim($row['DIM_ID']);
            $this->setNom($row['OC_NOM']);
            $this->setStatut($row['OC_STATUT']);
            $this->setDescription($row['OC_DESCRIPTION']);
        }
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
            . "FROM `T_OCCURENCE` "
            . "WHERE `OC_ID` = :id " ;
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
    public static function getNouvellesOccurences(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_OCCURENCE` O, `T_DIMENSION` D "
                . " WHERE O.`OC_STATUT` = 0 "
                . " AND D.`DIM_ID` = O.`DIM_ID` ";

        $reponse = $bdd->prepare($requete);
        
        $reponse->execute();

        $rows = $reponse->fetchAll();
        
        $tabOccurences = array();
        foreach ($rows as $row){
            $occ=array($row['OC_ID'], $row['OC_NOM'], $row['DIM_NOM']);
            array_push($tabOccurences,$occ);
        }
        return $tabOccurences;
    }
    
    public function changerEtat($etat){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_OCCURENCE`"
                . " SET `OC_STATUT` = :etat "
                . " WHERE `OC_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->bindParam(':etat', $etat, PDO::PARAM_INT);
        
        $reponse->execute();
    }
    
    public function supprimer(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "DELETE FROM `T_OCCURENCE` "
                . " WHERE `OC_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute();
    }
        
    public function update(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_OCCURENCE` "
                . " SET `OC_NOM` = :nom, `DIM_ID` = :idDim, `OC_DESCRIPTION` = :description, `OC_STATUT` = :statut"
                . " WHERE `OC_ID` = :id ";

        $reponse = $bdd->prepare($requete);
        
        $id = $this->getId();
        $idDim = $this->getIdDim();
        $nom=$this->getNom();
        $statut = $this->getStatut();
        $description = $this->getDescription();
        
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->bindParam(':idDim', $idDim, PDO::PARAM_INT);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_INT);
        $reponse->bindParam(':statut', $statut, PDO::PARAM_INT);
        $reponse->bindParam(':description', $description, PDO::PARAM_INT);
        
        $reponse->execute();
    }
}