<?php

require_once 'EntiteClass.php';
 
//Herite d entite
class Entreprise extends Entite{
    
    private $formeSociale;
    private $secteurActivite;
    private $technologieUtilisees;
    private $porteur;
    public function __construct($id,$porteur='', $categorie='', $etat='', $description='', $dateCreation='', $dateDerniereModif='', $nom='', $formeSociale='', $secteurActivite='', $technologieUtilisees='', $source='',$idModif='') {
        parent::__construct($id, $categorie, $etat, $description, $dateCreation, $dateDerniereModif, $nom, $source,$idModif);
        $this->porteur = $porteur;
        $this->formeSociale = $formeSociale;
        $this->secteurActivite = $secteurActivite;
        $this->technologieUtilisees = $technologieUtilisees;
    }
    
    public function getFormesSociale() {
        return $this->formeSociale;
    }
    public function getPorteur() {
        return $this->porteur;
    }
    
    public function getSecteurActivite() {
        return $this->secteurActivite;
    }
    
    public function getTechnologieUtilisees() {
        return $this->technologieUtilisees;
    }
    
    public function setFormesSociale($formeSociale) {
        $this->formeSociale = $formeSociale;
    }    
 
    public function setPorteur($porteur){
        $this->porteur = $porteur;
    }
 
    public function setSecteurActivite($SecteurActivite) {
        $this->SecteurActivite = $SecteurActivite;
    }
    
    public function setTechnologieUtilisees($technologieUtilisees) {
        $this->technologieUtilisees = $technologieUtilisees;
    }
    
    public function ajouterALaBDEntreprise() {
        
        $ajoutEffectue=FALSE;
        if($this->ajouterAlaBD()){
            if($this->nomExiste()){
                $bdd = new AccesBD();
                $bdd = $bdd->getBdd();

                $requete = "INSERT INTO `T_ENTREPRISE` VALUES (:entite, :porteur, :categorie , :etat, :description , :date, :date,:nom, :source, :formeSociale, :secteurActivite, :technologieUtilisees)";
                $entite = $this->getId();
                $porteur = $this->getPorteur();
                $categorie = $this->getCategorie();
                $etat = $this->getEtat();
                $description = $this->getDescription();
                $date = date("Y-m-d");
                $this->setDateCreation($date);
                $nom = $this->getNom();
                $source = $this->getSource();
                $formeSociale = $this->getFormesSociale();
                $secteurActivite = $this->getSecteurActivite();
                $technologieUtilisees = $this->getTechnologieUtilisees();
                $reponse = $bdd->prepare($requete);
                $reponse->bindParam(':entite',$entite, PDO::PARAM_INT,1);
                $reponse->bindParam(':porteur',$porteur, PDO::PARAM_INT,1);
                $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT,1);
                $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
                $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
                $reponse->bindParam(':date', $date, PDO::PARAM_STR);
                $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
                $reponse->bindParam(':source', $source, PDO::PARAM_STR);
                $reponse->bindParam(':formeSociale', $formeSociale, PDO::PARAM_STR,50);
                $reponse->bindParam(':secteurActivite', $secteurActivite, PDO::PARAM_STR,50);
                $reponse->bindParam(':technologieUtilisees', $technologieUtilisees, PDO::PARAM_STR,250);
                $reponse->execute();
                
                $ajoutEffectue=TRUE;
            }
        }
        return $ajoutEffectue;
    }
    
    public static function rechercherEntrepriseSelonEtat($etat){
        $tabEntreprises=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_ENTREPRISE` WHERE `ETAT_ID` = :etat";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        $i=0;
        foreach($rows as $row){
            $entreprise = new Entreprise($row['ENTITE_ID'], $row['UTILISATEUR_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['ENTREPRISE_FORME_SOCIALE'],$row['ENTREPRISE_SECTEUR_ACTIVITE'],
                                     $row['ENTREPRISE_TECHNOLOGIES_UTILISEES'] );
            $tabEntreprises[$i]=$entreprise;
            $i+=1;
        }
        return $tabEntreprises;
    }
    
public function entrepriseExiste(){
        $resultat = FALSE;
        if($this->nomExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_ENTREPRISE` WHERE `ENTITE_NOM` = :nom";
        $reponse = $bdd->prepare($requete);
        $nom = $this->getNom();
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
            $donneesEntreprise = $reponse->fetch();
            $this->setId($donneesEntreprise['ENTITE_ID']);
        }
        }
        return $resultat;
    }

    public static function estUneEntreprise($id){
        $resultat = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `ENTITE_ID` FROM `T_ENTREPRISE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        
        return $resultat;
        
    }
    
    public function enleverProduit(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_PRODUIT_SERVICE` SET `T_E_ENTITE_ID` = NULL WHERE `T_E_ENTITE_ID` = :idEntreprise ";
        $idEntreprise = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntreprise', $idEntreprise, PDO::PARAM_INT);
        
        $reponse->execute(); 
    }
    
    public function supprimerEntreprise() {
        if($this->idExiste()){
            $this->supprimerBM();
            $this->enleverProduit();
            
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            
            $requete = "UPDATE `T_ENTREPRISE` SET `ETAT_ID` = '7' WHERE `ENTITE_ID` = :id ";    
            $id = $this->getId();    
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            
            $reponse->execute();
            $this->supprimer();
        }
    }
    
    public function validEntreprise(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_ENTREPRISE` SET `ETAT_ID` = '1' WHERE `ENTITE_ID` = :id ";
        $idEntreprise = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $idEntreprise, PDO::PARAM_INT);
        
        $reponse->execute();
        $this->valider();
    }
    
    public static function obtenirEntreprise($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_ENTREPRISE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        foreach($rows as $row){
            $entreprise = new Entreprise($row['ENTITE_ID'], $row['UTILISATEUR_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['ENTREPRISE_FORME_SOCIALE'],$row['ENTREPRISE_SECTEUR_ACTIVITE'],
                                     $row['ENTREPRISE_TECHNOLOGIES_UTILISEES'] );
        }
        return $entreprise;
    }
    
    public static function getEntrepriseModifie(){
        $tabEntreprisesModif=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_ENTREPRISE_MODIFIE` WHERE `ETAT_ID` = 5";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        $i=0;
        foreach($rows as $row){
            $entrepriseModif = new Entreprise($row['ENTITE_ID'], $row['UTILISATEUR_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['ENTREPRISE_FORME_SOCIALE'],$row['ENTREPRISE_SECTEUR_ACTIVITE'],
                                     $row['ENTREPRISE_TECHNOLOGIES_UTILISEES'] );
            $tabEntreprisesModif[$i]=$entrepriseModif;
            $i+=1;
        }
        return $tabEntreprisesModif;
    }
    
    public function supprimerEntrepriseModif() {
        if($this->idExiste()){
            // On commence par supprimer les liens de l'entreprise (BM et produits)
            $this->supprimerBM();
            $this->enleverProduit();
            
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            
            // Puis on supprime l'entrée dans la table T_ENTREPRISE_MODIFIE
            $requete = "DELETE FROM `T_ENTREPRISE_MODIFIE` WHERE `ENTITE_ID` = :id  ";    
            $id = $this->getId();    
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);
            $reponse->execute();
        }
    }
  
    public static function obtenirNomEntreprise($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $nomEntreprise = "";
        $requete = "SELECT * FROM `T_ENTREPRISE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        foreach($rows as $row){
            
            $nomEntreprise = $row['ENTITE_NOM'];
        }
        return $nomEntreprise;
    }
    
    public function updateEntreprise(){
        // On commence par maj les infos dans entité
        $this->updateEntrepriseEntite();
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTREPRISE` e INNER JOIN `T_ENTREPRISE_MODIFIE` m ON e.`ENTITE_ID` = m.`ENTITE_ID`
                    SET e.`CATEGORIE_ID` = m.`CATEGORIE_ID`, e.`ENTITE_DESCRIPTION` = m.`ENTITE_DESCRIPTION`, e.`ENTITE_DATE_DERNIERE_MODIF` = m.`ENTITE_DATE_DERNIERE_MODIF`,
                    e.`ENTITE_NOM` = m.`ENTITE_NOM`, e.`ENTREPRISE_FORME_SOCIALE` = m.`ENTREPRISE_FORME_SOCIALE`, e.`ENTREPRISE_SECTEUR_ACTIVITE` = m.`ENTREPRISE_SECTEUR_ACTIVITE`,
                    e.`ENTREPRISE_TECHNOLOGIES_UTILISEES` = m.`ENTREPRISE_TECHNOLOGIES_UTILISEES` WHERE e.ENTITE_ID = :id";
        $idEntreprise = $this->getId();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $idEntreprise, PDO::PARAM_INT);
        $reponse->execute();
        $this->supprimerEntrepriseModif();
    }
    
    public function modifierEntreprise(){
        $this->modifier();
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_ENTREPRISE` SET `CATEGORIE_ID` = :categorie , `ETAT_ID` = :etat , `ENTITE_DESCRIPTION` = :description ,
                    `ENTITE_DATE_DERNIERE_MODIF` = :dateModif , `ENTITE_NOM` = :nom, `ENTREPRISE_FORME_SOCIALE` = :formeSociale , `ENTREPRISE_SECTEUR_ACTIVITE` = :secteurActivite ,
                    `ENTREPRISE_TECHNOLOGIES_UTILISEES` = :technologieUtilisees WHERE `ENTITE_ID` = :id";
        $entite = $this->getId();
        
        $id = $this->getId();
        $categorie = $this->getCategorie();
        $etat = $this->getEtat();
        $description = $this->getDescription();
        $date = date("Y-m-d");
        $nom = $this->getNom();
        $formeSociale = $this->getFormesSociale();
        $secteurActivite = $this->getSecteurActivite();
        $technologieUtilisees = $this->getTechnologieUtilisees();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT,2);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
        $reponse->bindParam(':dateModif', $date, PDO::PARAM_STR);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->bindParam(':formeSociale', $formeSociale, PDO::PARAM_STR,50);
        $reponse->bindParam(':secteurActivite', $secteurActivite, PDO::PARAM_STR,50);
        $reponse->bindParam(':technologieUtilisees', $technologieUtilisees, PDO::PARAM_STR,250);
        $reponse->execute();
        
    }
    
    public static function obtenirEntrepriseModifier($id){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_ENTREPRISE_MODIFIE` WHERE `ENTITE_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id',$id, PDO::PARAM_INT);
        $reponse->execute();
        $rows = $reponse->fetchAll();
        foreach($rows as $row){
            $entreprise = new Entreprise($row['ENTITE_ID'], $row['UTILISATEUR_ID'],
                                     $row['CATEGORIE_ID'], $row['ETAT_ID'],
                                     $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                     $row['ENTITE_DATE_DERNIERE_MODIF'],  $row['ENTITE_NOM'],
                                     $row['ENTREPRISE_FORME_SOCIALE'],$row['ENTREPRISE_SECTEUR_ACTIVITE'],
                                     $row['ENTREPRISE_TECHNOLOGIES_UTILISEES'],"",$row['ID_MODIFICATEUR_ENTREPRISE'] );
        }
        return $entreprise;
    }
    
    public function getAllProductEntreprise(){
        $tabProduit = array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $idEntreprise = $this->getId();
        
        $requete = "SELECT * FROM `T_PRODUIT_SERVICE` WHERE `T_E_ENTITE_ID` = :idEntreprise";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':idEntreprise',$idEntreprise, PDO::PARAM_INT);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();
        $i=0;
        foreach ($rows as $row){
            $produit = new Produit($row['ENTITE_ID'], $row['T_E_ENTITE_ID'], $row['CATEGORIE_ID'],
                                   $row['ETAT_ID'], $row['ENTITE_DESCRIPTION'], $row['ENTITE_DATE_CREATION'],
                                   $row['ENTITE_DATE_DERNIERE_MODIF'], $row['ENTITE_NOM']);
                $tabProduit[$i]=$produit;
                $i+=1;
        }
        return $tabProduit;
    }
    
    public function ajouterEntrepriseModifier(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "INSERT INTO `T_ENTREPRISE_MODIFIE` VALUES (:entite,:idModif, :idUtilisateur, :categorie , :etat, :description , :date, :dateModif,:nom, :source, :formeSociale, :secteurActivite, :technologieUtilisees)";       
        $entite = $this->getId();
        $idModif=$this->getIdModif();
        $idUtilisateur = $this->getPorteur();
        $categorie = $this->getCategorie();
        $etat = $this->getEtat();
        $description = $this->getDescription();
        $date = $this->getDateCreation();
        $dateModif = date("Y-m-d");
        $this->setDateDerniereModif($dateModif);
        $nom = $this->getNom();
        $source = $this->getSource();
        $formeSociale = $this->getFormesSociale();
        $secteurActivite = $this->getSecteurActivite();
        $technologieUtilisees = $this->getTechnologieUtilisees();
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':entite',$entite, PDO::PARAM_INT);
        $reponse->bindParam(':idModif',$idModif, PDO::PARAM_INT);
        $reponse->bindParam(':idUtilisateur',$idUtilisateur, PDO::PARAM_INT);
        $reponse->bindParam(':categorie',$categorie, PDO::PARAM_INT);
        $reponse->bindParam(':etat',$etat, PDO::PARAM_INT,1);
        $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
        $reponse->bindParam(':date', $date, PDO::PARAM_STR);
        $reponse->bindParam(':dateModif', $dateModif, PDO::PARAM_STR);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->bindParam(':source', $source, PDO::PARAM_STR);
        $reponse->bindParam(':formeSociale', $formeSociale, PDO::PARAM_STR,50);
        $reponse->bindParam(':secteurActivite', $secteurActivite, PDO::PARAM_STR,50);
        $reponse->bindParam(':technologieUtilisees', $technologieUtilisees, PDO::PARAM_STR,250);
        $reponse->execute();       
        
    }
    
    public function changerEtatEntreprise($nouvelEtat) {
        
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "UPDATE `T_ENTREPRISE` SET `ETAT_ID` = :idEtat WHERE `ENTITE_ID` = :idEntite "; 
        
        $idEntite=$this->getId();
        $reponse = $bdd->prepare($requete);
        
        $reponse->bindParam(':idEtat',$nouvelEtat, PDO::PARAM_INT,1);
        $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
        
        $reponse->execute();
        $this->changerEtat($nouvelEtat);
    }
}