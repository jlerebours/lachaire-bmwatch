<?php
require_once dirname(__FILE__).'/AccesBDClass.php';
require_once dirname(__FILE__).'/EntiteClass.php';

class Recherche{
    public static function rechercheAvecNom($nom){
        $tabEntite = array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_ENTITE_CARACTERISEE` E, `R_ENTITE_POSSEDE_CRITERES` R  WHERE `ENTITE_NOM` LIKE :nom AND E.`ENTITE_ID` = R.`ENTITE_ID` AND R.`CV_ID` = :idMotClef AND E.`ETAT_ID` = '1'";
        $reponse = $bdd->prepare($requete);
        $nom="%".$nom."%";
        $idMotClef = "2";
        $reponse->bindParam(':idMotClef',$idMotClef, PDO::PARAM_INT);
        $reponse->bindParam(':nom',$nom, PDO::PARAM_STR);
        $reponse->execute(); 
        $rows = $reponse->fetchAll();
        $i=0;
        foreach($rows as $row){
            $tabEntite[$i][1] = $row['ENTITE_ID'];
            $tabEntite[$i][2] = $row['CRITERE_VALEUR'];
            $tabEntite[$i][3] = $row['ENTITE_NOM'];
            $tabEntite[$i][4] = $row['ENTITE_DATE_DERNIERE_MODIF'];
            $i++;
        }
        
        return $tabEntite;
    }
    
    public static function rechercheMultiCritere($tabCritere,$type){
        //Pour cette recherche, on créer un tableau par de resultat par criteres et a chaque fois on compare les resultats pour creer le tableau de resultat final
        $resultat = null;
        for($i=0;$i<count($tabCritere);$i++){
            $tabEntite=array();
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            
            switch ($tabCritere[$i][2]){
                case "NUMERIQUE":
                    if($tabCritere[$i][3] == "entre"){
                        $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` BETWEEN :nb1 AND :nb2";
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindParam(':idCv',$tabCritere[$i][1], PDO::PARAM_INT);
                        $reponse->bindParam(':nb1',$tabCritere[$i][4], PDO::PARAM_INT);
                        $reponse->bindParam(':nb2',$tabCritere[$i][5], PDO::PARAM_INT);
                    }
                    else{
                        switch ($tabCritere[$i][3]){
                            case "egale":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` = :nb ";
                                break;
                            case "inferieure":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` < :nb ";
                                break;
                            case "inferieureEgale":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` <=:nb ";
                                break;
                            case "superieure":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` > :nb ";
                                break;
                            case "superieureEgale":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` >= :nb ";
                                break;
                            case "different":
                                $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` != :nb ";
                                break;
                        }
                        $reponse = $bdd->prepare($requete);
                        $reponse->bindParam(':idCv',$tabCritere[$i][1], PDO::PARAM_INT);
                        $reponse->bindParam(':nb',$tabCritere[$i][4], PDO::PARAM_INT);
                    }
                    break;
                case "CLEF":
                    $requete = "SELECT * FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv AND `CRITERE_VALEUR` LIKE :motClef";
                    $reponse = $bdd->prepare($requete);
                    $motClef = "%".$tabCritere[$i][3]."%";
                    $reponse->bindParam(':idCv',$tabCritere[$i][1], PDO::PARAM_INT);
                    $reponse->bindParam(':motClef',$motClef, PDO::PARAM_STR, 50);
                    break;
                case "CHOIX":
                    $requete = "SELECT `ENTITE_ID` FROM `R_ENTITE_POSSEDE_CRITERES` WHERE `CV_ID` = :idCv ";
                    $reponse = $bdd->prepare($requete);
                    $reponse->bindParam(':idCv',$tabCritere[$i][1], PDO::PARAM_INT);
                    break;                  
                
            }
            $reponse->execute(); 
            $rows = $reponse->fetchAll();
            $j=0;
            foreach($rows as $row){
                $tabEntite[$j] = $row['ENTITE_ID'];
                $j++;
            }
            if($resultat != null ){
                $resultat=array_intersect($tabEntite, $resultat);
            }
            else{
                $resultat=$tabEntite;
            }
        }
        
        
        $resultatFinal = null;
        if($resultat != null){
            $i=0;
            foreach($resultat as $idEntite){
                $bdd = new AccesBD();
                $bdd = $bdd->getBdd();
                $requete = "SELECT * FROM `T_ENTITE_CARACTERISEE` E, `R_ENTITE_POSSEDE_CRITERES` R  WHERE E.`ENTITE_ID` = :idEntite AND E.`ENTITE_ID` = R.`ENTITE_ID` AND R.`CV_ID` = :idMotClef AND E.`ETAT_ID` = '1'";
                $reponse = $bdd->prepare($requete);
                $idMotClef = "2";
                $reponse->bindParam(':idEntite',$idEntite, PDO::PARAM_INT);
                $reponse->bindParam(':idMotClef',$idMotClef, PDO::PARAM_INT);
                $reponse->execute();
                $donneesEntite = $reponse->fetch();
                if($donneesEntite!=null){
                    if($type !=""){
                        $entite = new Entite( $donneesEntite['ENTITE_ID']);
                        if($entite->getType()==$type){
                           $resultatFinal[$i][1] = $donneesEntite['ENTITE_ID'];
                            $resultatFinal[$i][2] = $donneesEntite['CRITERE_VALEUR'];
                            $resultatFinal[$i][3] = $donneesEntite['ENTITE_NOM'];
                            $resultatFinal[$i][4] = $donneesEntite['ENTITE_DATE_DERNIERE_MODIF']; 
                        }
                    }else{
                        $resultatFinal[$i][1] = $donneesEntite['ENTITE_ID'];
                        $resultatFinal[$i][2] = $donneesEntite['CRITERE_VALEUR'];
                        $resultatFinal[$i][3] = $donneesEntite['ENTITE_NOM'];
                        $resultatFinal[$i][4] = $donneesEntite['ENTITE_DATE_DERNIERE_MODIF'];
                    }
                
                    $i++;
                }

            }
        }
        return $resultatFinal;
    }
}