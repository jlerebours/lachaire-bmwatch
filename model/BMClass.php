<?php

require_once dirname(__FILE__).'/AccesBDClass.php';

class BM{
    private $id;
    private $nom;
    private $profil;
    private $principe ;
    private $declinaison ;
    private $variantes ;   
    
    public function __construct($id, $nom='', $profil='', $principe='', $declinaison='', $variantes=''){      
        $this->id = $id;
        $this->nom = $nom;
        $this->profil = $profil;
        $this->principe = $principe;
        $this->declinaison = $declinaison;
        $this->variantes = $variantes;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getProfil() {
        return $this->profil;
    }

    public function getPrincipe() {
        return $this->principe;
    }
    
    public function getDeclinaison() {
        return $this->declinaison;
    }
    
    public function getVariantes(){
        return $this->variantes;
    }
 
    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setProfil($profil) {
        $this->profil = $profil;
    }

    public function setPrincipe($principe) {
        $this->principe = $principe;
    }
    public function setDeclinaison($declinaison) {
        $this->declinaison= $declinaison;
    }
    
    public function setVariantes($variantes){
        $this->variantes = $variantes;
    }
    
    public function idExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * "
            . "FROM `T_BM` "
            . "WHERE `BM_ID` = :id " ;
        
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }

        return $resultat;
    }
    
     // Récupère les infos du BM si celui-ci possède un id qui existe
    public function getInfos() {
        if($this->idExiste()){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = " SELECT * "
                    . " FROM `T_BM` "
                    . " WHERE `BM_ID` = :id ";

            $reponse = $bdd->prepare($requete);
            $id = $this->getId();
            $reponse->bindParam(':id', $id, PDO::PARAM_INT);

            $reponse->execute();
            
            $row = $reponse->fetch();
            $this->setNom($row['BM_NOM']);
            $this->setProfil($row['BM_PROFIL']);
            $this->setPrincipe($row['BM_PRINCIPE']);
            $this->setDeclinaison($row['BM_DECLINAISONS']);
            $this->setVariantes($row['BM_VARIANTES']);
        }
    }

    //teste si bm existe avec le nom  
    public function bmExiste(){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `BM_ID` FROM `T_BM` WHERE `BM_NOM` = :nom";
        $reponse = $bdd->prepare($requete);
        $nom = $this->getNom();
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
            $donneesBM = $reponse->fetch();
            $this->setId($donneesBM['BM_ID']);
        }
        return $resultat;
    }
    
    
    
    public function ajouterALaBD() {
        $ajoutEffectue=FALSE;
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();

            $requete = "INSERT INTO `T_BM` VALUES (NULL, :nom , :profil, :principe , :declinaison, :variante)";
            
            $nom = $this->getNom();
            $profil = $this->getProfil();
            $principe = $this->getPrincipe();
            $declinaison = $this->getDeclinaison();
            $variante = $this->getVariantes();
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':nom',$nom, PDO::PARAM_STR,50);
            $reponse->bindParam(':profil',$profil, PDO::PARAM_INT,1);
            $reponse->bindParam(':principe', $principe, PDO::PARAM_STR,250);
            $reponse->bindParam(':declinaison', $declinaison, PDO::PARAM_STR,250);
            $reponse->bindParam(':variante', $variante, PDO::PARAM_STR,250);
            $reponse->execute();
            $this->setId($bdd->lastInsertId());
            $ajoutEffectue=TRUE;
        
        return $ajoutEffectue;
    }
    
    public function updateBM(){
        
        $modifEffectue=FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "UPDATE `T_BM` SET `BM_NOM` = :nom, `BM_PROFIL` = :profil, `BM_PRINCIPE` = :principe, `BM_DECLINAISONS` = :declinaisons, `BM_VARIANTES` = :variantes WHERE `BM_ID` = :id";
        $id = $this->getId();
        $nom = $this->getNom();
        $profil = $this->getProfil();
        $principe = $this->getPrincipe();
        $declinaisons = $this->getDeclinaison();
        $variantes = $this->getVariantes();
            
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT,1);
        $reponse->bindParam(':profil', $profil, PDO::PARAM_INT,1);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,50);
        $reponse->bindParam(':profil', $profil, PDO::PARAM_INT,1);
        $reponse->bindParam(':principe', $principe, PDO::PARAM_STR,250);
        $reponse->bindParam(':declinaisons', $declinaisons, PDO::PARAM_STR,250);
        $reponse->bindParam(':variantes', $variantes, PDO::PARAM_STR,250);
            
        $modifEffectue = $reponse->execute();
        
        return $modifEffectue;
    }
    
    public function supBM(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $supEffectue = FALSE;
        $requete = "DELETE FROM `T_BM` WHERE `BM_ID` = :id";
        $id = $this->getId();
        $reponse =$bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT,1);       
        $supEffectue = $reponse->execute();
        return $supEffectue;
    }
    
    //regarde si bm existe avec id
    public static function bmExisteId($id){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_BM` WHERE `BM_ID` = :id";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT,1);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        return $resultat;
        
    }
    
    //recupere les dimensions
    public static function obtenirDimension(){
        $tabDimension=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_DIMENSION` ";
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();        
        $i=1;
        
        foreach($rows as $row){
            $tabDimension[$i][1]=$row['DIM_ID'];
            $tabDimension[$i][2]=$row['DIM_NOM'];
            $tabDimension[$i][3]=$row['DIM_DESCRIPTION'];
            $i+=1;
        }
        return $tabDimension;        
    }
    
    //recupere les occurences 
    public static function obtenirOccurence(){
        $tabOccurence=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_OCCURENCE` ";
        $reponse = $bdd->prepare($requete);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();        
        $i=0;
        
        foreach($rows as $row){
            $tabOccurence[$i][1]=$row['OC_ID'];
            $tabOccurence[$i][2]=$row['DIM_ID'];
            $tabOccurence[$i][3]=$row['OC_NOM'];
            $tabOccurence[$i][4]=$row['OC_DESCRIPTION'];
            $i+=1;
        }
        return $tabOccurence;        
    }
    
    //Recupere les profils
    public static function obtenirProfil(){
        $tabProfil=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_BM` WHERE `BM_PROFIL` = :profil";
        $profil = 1;
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':profil', $profil, PDO::PARAM_INT,1);
        $reponse->execute();
          
        $rows = $reponse->fetchAll();        
        $i=0;
        
        foreach($rows as $row){
            $tabProfil[$i][1]=htmlentities($row['BM_ID']);
            $tabProfil[$i][2]=htmlentities($row['BM_NOM']);
            $tabProfil[$i][3]=htmlentities($row['BM_PROFIL']);
            $tabProfil[$i][4]=htmlentities($row['BM_PRINCIPE']);
            $tabProfil[$i][5]=htmlentities($row['BM_DECLINAISONS']);
            $tabProfil[$i][6]=htmlentities($row['BM_VARIANTES']);
            $i+=1;
        }
        return $tabProfil;    
    }
    
    //Recupere les occurence d une dimension
    public static function obtenirOccurenceAvecDim($dim){
        $tabOccurence=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_OCCURENCE` WHERE `DIM_ID` = :dim AND `OC_STATUT` = 1";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':dim', $dim, PDO::PARAM_INT,1);
        $reponse->execute();
        
        $rows = $reponse->fetchAll();        
        $i=0;
        
        foreach($rows as $row){
            $tabOccurence[$i][1]=htmlentities($row['OC_ID']);
            $tabOccurence[$i][2]=htmlentities($row['DIM_ID']);
            $tabOccurence[$i][3]=htmlentities($row['OC_NOM']);
            $tabOccurence[$i][4]=htmlentities($row['OC_DESCRIPTION']);
            $i+=1;
        }
        return $tabOccurence;        
    }
    
    //Permet de savoir si une bm possede une occurence particuliere
    public function bmOccExiste($idOc){
        $resultat = FALSE;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT `BM_ID` FROM `T_OC_BM` WHERE `BM_ID` = :idBm AND `OC_ID` = :idOc";
        $reponse = $bdd->prepare($requete);
        $idBm = $this->getId();
        $reponse->bindParam(':idBm', $idBm, PDO::PARAM_INT,1);
        $reponse->bindParam(':idOc', $idOc, PDO::PARAM_INT,1);
        $reponse->execute(); 
        if($reponse->rowCount()> 0){
            $resultat = TRUE;
        }
        return $resultat;
    }
    
    //Ajoute une occurence et un commentaire a une bm
    public function fixerOccurence($idOcc,$commentaire){
        if(!$this->bmOccExiste($idOcc)){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "INSERT INTO `T_OC_BM` VALUES (:idBm,:idOcc,:commentaire)";
            
            $idBm = $this->getId();
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':idBm', $idBm, PDO::PARAM_INT);
            $reponse->bindParam(':idOcc', $idOcc, PDO::PARAM_INT);
            $reponse->bindParam(':commentaire', $commentaire, PDO::PARAM_STR,250);
            $reponse->execute();
            
        }       
    }
    
    public static function occurenceExiste($nom){
        $resultat = 0;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_OCCURENCE` WHERE `OC_NOM` LIKE :nom ";
        $reponse = $bdd->prepare($requete);
        $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,30);
        $reponse->execute();  
        if($reponse->rowCount()> 0){
            $donneesOcc = $reponse->fetch();
            $resultat = $donneesOcc['OC_ID'];
            
        }
        return $resultat;
    }
    
    //Ajoute une occurence
    public static function ajouterOccurence($dimId, $nom, $description){
        $resultat = BM::occurenceExiste($nom);
        if($resultat ==0){
            $bdd = new AccesBD();
            $bdd = $bdd->getBdd();
            $requete = "INSERT INTO `T_OCCURENCE` VALUES (NULL, :dimId, :nom, 0, :description)";
            $reponse = $bdd->prepare($requete);
            $reponse->bindParam(':dimId', $dimId, PDO::PARAM_INT);
            $reponse->bindParam(':nom', $nom, PDO::PARAM_STR,30);
            $reponse->bindParam(':description', $description, PDO::PARAM_STR,250);
            $reponse->execute();
            $resultat = BM::occurenceExiste($nom);
        }
        return $resultat;        
    }
    
    public function estProfil(){
        $resultat = false;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        
        $requete = "SELECT * FROM `T_BM` WHERE `BM_PROFIL` = :profil AND `BM_ID` = :id ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $profil = 1;
        $reponse->bindParam(':profil', $profil, PDO::PARAM_INT,1);
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);       
        $reponse->execute();  
        if($reponse->rowCount()> 0){
            $resultat = true;            
        }
        return $resultat;
        
    }
    
    public function obtenirOccurenceDUneBM($dim){
        $occ=null;
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_OC_BM` T, `T_OCCURENCE` OCC  WHERE T.`BM_ID` = :id AND T.`OC_ID` = OCC.`OC_ID` AND OCC.`DIM_ID`= :dim ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->bindParam(':dim', $dim, PDO::PARAM_STR);
        
        $reponse->execute();  
        if($reponse->rowCount()> 0){
            $donneesOcc = $reponse->fetch();
            $occ = $donneesOcc['OC_ID'];
        }
        return $occ;
    }
    
    public function obtenirOccurenceDUneBMNom($dim){
        $occ=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_OC_BM` T, `T_OCCURENCE` OCC  WHERE T.`BM_ID` = :id AND T.`OC_ID` = OCC.`OC_ID` AND OCC.`DIM_ID`= :dim ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->bindParam(':dim', $dim, PDO::PARAM_STR);
        
        $reponse->execute();  
        if($reponse->rowCount()> 0){
            $donneesOcc = $reponse->fetch();
            $occ[0] = $donneesOcc['OC_NOM'];
            $occ[1] = $donneesOcc['OC_COMMENTAIRE'];
        }
        return $occ;
    }
    
    public function obtenirOccurencesDUneBM(){
        $tabOcc=array();
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "SELECT * FROM `T_OC_BM` T, `T_OCCURENCE` OCC  WHERE T.`BM_ID` = :id AND T.`OC_ID` = OCC.`OC_ID` ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        
        $reponse->execute();
        
        $rows = $reponse->fetchAll();        
        $i=0;
        foreach($rows as $row){
            $tabOcc[$i][1]=htmlentities($row['OC_ID']);
            $tabOcc[$i][2]=htmlentities($row['DIM_ID']);
            $tabOcc[$i][3]=htmlentities($row['OC_COMMENTAIRE']);
            $i+=1;
        }
    
        return $tabOcc;
    }
    
    public function obtenirInfoBM(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();   
        $requete = "SELECT * FROM `T_BM`  WHERE `BM_ID` = :id ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        if($reponse->rowCount()> 0){
            $donneBM = $reponse->fetch();
            $this->setNom($donneBM['BM_NOM']);
            $this->setPrincipe($donneBM['BM_PRINCIPE']);
            $this->setDeclinaison($donneBM['BM_DECLINAISONS']);
            $this->setVariantes($donneBM['BM_VARIANTES']);
        }
    }
    
    public function supRelation(){
        $bdd = new AccesBD();
        $bdd = $bdd->getBdd();
        $requete = "DELETE FROM `T_OC_BM` WHERE `BM_ID` = :id ";
        $reponse = $bdd->prepare($requete);
        $id = $this->getId();
        $reponse->bindParam(':id', $id, PDO::PARAM_INT);
        $reponse->execute();
        
    }
}