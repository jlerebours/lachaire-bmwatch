<?php
include dirname(__FILE__)."/../controller/headerController.php";
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';
require_once dirname(__FILE__).'/../model/EntiteClass.php';
require_once dirname(__FILE__).'/../model/CritereClass.php';
require_once dirname(__FILE__).'/../model/FaqClass.php';
require_once dirname(__FILE__).'/../model/CategorieClass.php';
require_once dirname(__FILE__).'/../model/ProduitClass.php';

//Initialisations
$affichageResulatsBD=false;


// On vérifie avant tout que l'utilisateur est connecté et modo ou admin
if($nonConnecte || !$droitsAdmin || !$droitsModo){
    ToolBox::redirige('erreur', 0);
}

// Partie formulaires
if(isset($_POST['ValiderCritere']) && isset($_POST['typeCritere']) && isset($_POST['nomCritere'])&& isset($_POST['type'])){
    $nomCritere = filter_input(INPUT_POST, 'nomCritere', FILTER_SANITIZE_STRING);
    $typeCritere = filter_input(INPUT_POST, 'typeCritere', FILTER_SANITIZE_STRING);
    $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
    $tabElements= $_POST['element'];
    
    $listeElements = array();
    if($typeCritere === "LISTE"){
        foreach ($tabElements as $element) {
            $elementOk = filter_var($element,FILTER_SANITIZE_STRING);
            if(!empty($elementOk)){
                array_push($listeElements,$elementOk);
            }
        }
    }
    else {
        array_push($listeElements, $typeCritere);
    }
    $critere = new Critere(1, $nomCritere, $listeElements,$type);
    $critere->ajouterALaBD(); 
}

// Formulaire ajout question FAQ
else if(isset($_POST['questionFaq']) && isset($_POST['reponseFaq'])){
    $idFaq = filter_input(INPUT_POST, 'idFaq', FILTER_VALIDATE_INT);
    $question = filter_input(INPUT_POST, 'questionFaq');
    $reponse = filter_input(INPUT_POST, 'reponseFaq');
    $choixCategorie = filter_input(INPUT_POST, 'categorieFaq', FILTER_SANITIZE_STRING);
    $nouvelleCategorie = filter_input(INPUT_POST, 'nouvelleCategorie', FILTER_SANITIZE_STRING);
    
    if(!empty($nouvelleCategorie)){
        $choixCategorie = $nouvelleCategorie;
    }
    $faq = new Faq($idFaq,$choixCategorie,$question,$reponse);
    if($faq->idExiste()){
       $faq->update(); 
    }
    else {
        $faq->ajouterALaBD(); 
    }
    ToolBox::redirige('admin-site', 0);
}

elseif(isset($_POST['btnValiderCategorie']) && isset($_POST['nomCategorie']) && isset($_POST['descriptionCategorie']) && isset($_POST['selectCategorieParente'])&& isset($_POST['range'])){
    $nomCritere = filter_input(INPUT_POST, 'nomCategorie', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'descriptionCategorie', FILTER_SANITIZE_STRING);
    $idCatParente = filter_input(INPUT_POST, 'selectCategorieParente', FILTER_VALIDATE_INT);
    $range = filter_input(INPUT_POST, 'range', FILTER_VALIDATE_INT);
    
    
    $categorie = new Categorie(1,$idCatParente,$nomCritere,$description,$range);
    $categorie->ajouterALaBD();
}

// Partie formulaires
elseif(isset($_POST['tableApps']) ){
    $affichageResulatsBD=true;
    $table = filter_input(INPUT_POST, 'tableApps', FILTER_SANITIZE_STRING);
    $rank = filter_input(INPUT_POST, 'hiddenRank', FILTER_VALIDATE_FLOAT);
    
}

// Partie affichage
if(isset($_GET['users'])){
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
    $droitsAdminOk=(ToolBox::verifierDroits(1));
    $droitsModoOk=(ToolBox::verifierDroits(1) || ToolBox::verifierDroits(2));
    
    $listeUtilisateurs = $utilisateurCourant->getTousLesUtilisateurs();
    
    $listeTypes=array();
    if($droitsAdminOk){
        array_push($listeTypes,$voc_moderateur);
    }
    if($droitsModoOk){
        array_push($listeTypes,$voc_porteur);
        array_push($listeTypes,$voc_nonPorteur);
        array_push($listeTypes,$voc_chercheur);
    }
    
    $listeGroupes = Utilisateur::getGroupes();
  
    include "../view/gestionUtilisateurView.php"; 
}

else if(isset($_GET['assignation'])){
    $droitsAdmin = ToolBox::verifierDroits(1);
    $droitsModo = ToolBox::verifierDroits(2);
    if($droitsAdmin || $droitsModo){
        $moderateur = unserialize($_SESSION['donneesUtilisateur']);
        $listeProduitsDispo = Entite::rechercherSelonEtat(6);
        // Partie suivi
        $listeEntitesAssignees=$moderateur->getEntitesAssigneesParModo();
        $listeSuiviEntite=$listeEntitesAssignees[0];
        $listeSuiviNP=$listeEntitesAssignees[1];
        
        include "../view/assignationView.php";
    }
    else{
        ToolBox::redirige("../view/erreur404View.php", 0);
    }  
}

else if(isset($_GET['gestionEntites'])){
    // On vérifie que l'utilisateur est connecté ou que c'est au moins un porteur de projet
    $type = filter_input(INPUT_GET, 'gestionEntites', FILTER_VALIDATE_INT);
    if(ToolBox::verifierDroits(1)||ToolBox::verifierDroits(2)||ToolBox::verifierDroits(3)||ToolBox::verifierDroits(4)){
        switch ($type){
            case 1: $listeEntiteNew = Entreprise::rechercherEntrepriseSelonEtat(2);
                $listeEntiteModif = Entreprise::getEntrepriseModifie();
                break;
            case 2: $listeEntiteNew = Produit::rechercherProduitSelonEtat(2);
                $listeEntiteModif = Produit::getProduitModifie();
                break;
        }
        include dirname(__FILE__).'/../view/gestionValidationView.php';
    }
}

else if(isset($_GET['viewEntites'])){
    // On vérifie que l'utilisateur est connecté ou que c'est au moins un porteur de projet
    $type = filter_input(INPUT_GET, 'viewEntites', FILTER_VALIDATE_INT);
    if(ToolBox::verifierDroits(1)||ToolBox::verifierDroits(2)||ToolBox::verifierDroits(3)||ToolBox::verifierDroits(4)){
        switch ($type){
            case 1: $listeEntiteNew = Entreprise::rechercherEntrepriseSelonEtat(2);
                $listeEntiteModif = Entreprise::getEntrepriseModifie();
                break;
            case 2: $listeEntiteNew = Produit::rechercherProduitSelonEtat(2);
                $listeEntiteModif = Produit::getProduitModifie();
                break;
        }
        include dirname(__FILE__).'/../view/produitView.php';
    }
}

else if(isset($_GET['gestionBD']) && $droitsAdmin){
    include dirname(__FILE__).'/../view/gestionBDView.php';
}
else if(isset($_GET['gestionSite']) && $droitsAdmin){
    include dirname(__FILE__).'/../view/gestionSiteView.php';
}

else{
    ToolBox::redirige('erreur', 0);
}








