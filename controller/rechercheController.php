<?php
include_once dirname(__FILE__)."/headerController.php";
require_once '../model/EntiteClass.php';
require_once '../model/RechercheClass.php';
require_once '../model/SecuriteClass.php';
require_once '../model/ToolBoxClass.php';

$recherche = null;
$default_tabCriteres = null;
$default_nom = null;
if(isset($_POST['rechercheSimple'])){
    $recherche = "simple";
    if(isset($_POST['nomEntite'])){
        //On sécurise les données
        $message = null;
        $resultat = null;
        
        $nomEntite  =  filter_input(INPUT_POST,"nomEntite", FILTER_SANITIZE_STRING);
        $message = SecuriteClass::securite_string($nomEntite, 0, 30);
        $default_nom = $nomEntite;
        if($message ==null){
            $resultat = Recherche::rechercheAvecNom($nomEntite);
            include dirname(__FILE__)."/../view/pageResultatView.php";
        }
    }
}
elseif(isset($_POST['rechercheMultiCritere'])){
    $recherche ="avance";
    $message = null;
    $resultat = null;
    $type = filter_input(INPUT_POST,"type", FILTER_SANITIZE_STRING);
    //tabCritere[$i][1] : id du valeur critere pour numerique et clef ou l id du critere pour un choix
    //tabCritere[$i][2] : type critere
    //tabCritere[$i][3] : type comparaison ou mot clef
    //tabCritere[$i][4] : chiffre 1
    //tabCritere[$i][5] : chiffre 2
    $tabCritere = array();
    $default_tabCriteres = array();
    $nbCritere = filter_input(INPUT_POST,"nbCriteres", FILTER_VALIDATE_INT);
    $nb=0;
    for ($i=1;$i<=$nbCritere;$i++){
        $k=$i-1;
        $default_tabCriteres[$k][1] = "";
        $default_tabCriteres[$k][2] = "";
        $default_tabCriteres[$k][3] = "";
        $default_tabCriteres[$k][4] = "";
        $typeCritere = filter_input(INPUT_POST,"typeCritere".$i, FILTER_SANITIZE_STRING);
        $message = SecuriteClass::securite_string($typeCritere, 0, 50);
        if($typeCritere == "NUMERIQUE"){
            $valeurCritere = filter_input(INPUT_POST,"valeurCritere".$i, FILTER_VALIDATE_INT);
            $choixComparaison  = filter_input(INPUT_POST,"choixComparaison".$i, FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($typeCritere, 0, 30);
            if($message == null && $choixComparaison!=""){
                $tabCritere[$nb][1] = $valeurCritere;
                $tabCritere[$nb][2] = $typeCritere;
                $tabCritere[$nb][3] = $choixComparaison;
                $default_tabCriteres[$k][1] = $tabCritere[$nb][3];
                $default_tabCriteres[$k][2] = "";
                $default_tabCriteres[$k][3] = "";
                $default_tabCriteres[$k][4] = "";
                if($choixComparaison == "entre"){
                    $tabCritere[$nb][4] = filter_input(INPUT_POST,"nb1critere".$i, FILTER_VALIDATE_INT);
                    $tabCritere[$nb][5] = filter_input(INPUT_POST,"nb2critere".$i, FILTER_VALIDATE_INT);
                    $default_tabCriteres[$k][2] = "entre";                    
                    $default_tabCriteres[$k][3] = $tabCritere[$nb][4];
                    $default_tabCriteres[$k][4] = $tabCritere[$nb][5];
                    $nb++;
                }
                else{
                    $tabCritere[$nb][4] = filter_input(INPUT_POST,"chiffre".$i, FILTER_VALIDATE_INT);
                    $default_tabCriteres[$k][2] = "autre";
                    $default_tabCriteres[$k][3] = $tabCritere[$nb][4];
                    $nb++;
                    
                }
            }
        }
        elseif($typeCritere == "CLEF"){
            $motClefs = filter_input(INPUT_POST,"critere".$i, FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($motClefs, 0, 50);
            if($message == null && $motClefs!=""){
                $default_tabCriteres[$k][1] = $motClefs;
                $tabMotClefs = explode(",", $motClefs);
                foreach($tabMotClefs as $unMotClef){
                    $tabCritere[$nb][1] =  filter_input(INPUT_POST,"valeurCritere".$i, FILTER_VALIDATE_INT);
                    $tabCritere[$nb][2] = $typeCritere;
                    $tabCritere[$nb][3] = strtoupper($unMotClef);
                    $nb++;
                }
            }
        }
        else{
            $choix = filter_input(INPUT_POST,"critere".$i, FILTER_VALIDATE_INT);
            if($choix != ""){                
                $tabCritere[$nb][1] = $choix;
                $default_tabCriteres[$k][1] = $tabCritere[$nb][1];
                $tabCritere[$nb][2] = "CHOIX";
                $nb++;
            }
        }        
    }
    $resultat = Recherche::rechercheMultiCritere($tabCritere,$type);
    if($message ==null){
        include dirname(__FILE__)."/../view/pageResultatView.php";
    }
}
else{
    ToolBox::redirige('erreurController.php', 0);
}