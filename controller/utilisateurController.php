<?php

include dirname(__FILE__)."/../controller/headerController.php";
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';

if($nonConnecte){
    ToolBox::redirige('erreur', 0);
}

if(isset($_GET['modificationProfil'])){
    // Si c'etait la premiere connexion on supprime la variable de session
    if(isset($_SESSION['premiereConnexion'])){
        unset($_SESSION['premiereConnexion']);
    }
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);

    $nomDefault = $utilisateurCourant->getNom();
    $prenomDefault = $utilisateurCourant->getPrenom();
    $emailDefault = $utilisateurCourant->getEmail();
    $mdp = $utilisateurCourant->getMdp();
    include dirname(__FILE__)."/../view/modificationProfilView.php";
}

else{
    ToolBox::redirige('erreurController.php', 0);
}

