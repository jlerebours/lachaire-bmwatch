<?php
include dirname(__FILE__)."/headerController.php";
require_once '../model/FaqClass.php';

if(isset($_GET['accueil'])){
    include dirname(__FILE__)."/../view/accueilView.php";
}

elseif(isset($_GET['faq'])){
    include dirname(__FILE__)."/../view/faqView.php";
}

else{
    ToolBox::redirige('erreur', 0);
}