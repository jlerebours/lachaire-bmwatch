<?php
require_once dirname(__FILE__).'/../model/ToolBoxClass.php';

if(!isset($_COOKIE['langue'])){
    setcookie("langue", "EN", time()+60*60*24*365,"/");
    
} 

$langue = filter_input(INPUT_COOKIE, "langue", FILTER_SANITIZE_STRING);
    switch ($langue){
        case "FR": include dirname(__FILE__)."/langues/lang_fr.php";
            break;
        case "EN": include dirname(__FILE__)."/langues/lang_en.php";
            break;
        // Si autre, possiblement un cookie modifié, on charge le fichier en et on modifie le cookie !
        default : 
            include dirname(__FILE__)."/langues/lang_en.php";
            $langue="EN";
            setcookie("langue", $langue, time()+60*60*24*365,"/");
    }
    // Liste à completer si on ajoute des langues
    $listeLangues = array("EN" => $voc_angleterre,"FR" => $voc_france);