<?php
require_once '../model/ToolBoxClass.php';
session_start();

if(isset($_GET['langue'])){
    $langue = filter_input(INPUT_GET, "langue", FILTER_SANITIZE_STRING);
    switch ($langue){
        case "FR": include "../view/langues/lang_fr.php";
            break;
        case "EN": include "../view/langues/lang_en.php";
            break;
        default : 
            include "../view/langues/lang_en.php";
            $langue="EN";
    } 
    setcookie("langue", $langue, time()+60*60*24*365,"/");
    $url = $_SERVER['HTTP_REFERER'];
    ToolBox::redirige($url, 0);
    
}
