<?php
session_start();
require_once dirname(__FILE__).'/../model/ToolBoxClass.php';
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';

if(isset($_GET['langue'])){
    $langue = filter_input(INPUT_GET, "langue", FILTER_SANITIZE_STRING);
    switch ($langue){
        case "FR": 
            include dirname(__FILE__)."/../controller/langues/lang_fr.php";
            break;
        case "EN": 
            include dirname(__FILE__)."/../controller/langues/lang_en.php";
            break;
        default : 
            include dirname(__FILE__)."/../controller/langues/lang_en.php";
            $langue="EN";
    } 
    setcookie("langue", $langue, time()+60*60*24*365,"/");
    $url = $_SERVER['HTTP_REFERER'];
    ToolBox::redirige($url, 0); 
}
else if(isset($_POST['modifierCompte'])) {
    //On sécurise les données
    $nom     = htmlentities($_POST['nom']);
    $prenom  = htmlentities($_POST['prenom']);
    $email    = htmlentities($_POST['email']);
    $password = htmlentities($_POST['nouveauMdp']);

    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);

    //On dit que les données sont bonnes, puis on va le remettre en question
    $ok = true;
    //Vérification du nom
    if(empty($nom) && $ok) {
        $message = "Champ nom manquant.";
        $ok = false;
    }
    //Le nom doit contenir que des lettres
    if(!preg_match('/^[a-zA-Zé-é\s\'-]{1,29}$/', $nom) && $ok) {
        $message = "Caractére incorrect dans le champ nom.";
        $ok = false;
    }
    //Vérification du prenom
    if(empty($prenom) && $ok) {
        $message = "Champ prénom manquant.";
        $ok = false;
    }
    //Le prénom doit contenir que des lettres
    if(!preg_match('/^[a-zA-Zé-é\s\'-]{1,29}$/', $prenom) && $ok) {
        $message = "Caractére incorrect dans le champ prenom.";
        $ok = false;
    }
    //Vérification de l email
    if(empty($email) && $ok) {
        $message = "Champ email manquant.";
        $ok = false;
    }
    //L email doit etre valide
    if(!filter_var($email, FILTER_VALIDATE_EMAIL) && $ok) {
        $message = "Adresse mail invalide.";
        $ok = false;
    }

    if(!empty($password) && (!$uppercase || !$lowercase || !$number || strlen($password) < 8)) {
        $message = "Mot de passe invalide.";
        $ok = false;
    }
    //Si tout est bon on peut l ajouter a la basse de donnée
    if($ok) {
        $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
        $ancienEmail = $utilisateurCourant->getEmail();
        if(!empty($password)){
            $utilisateur = new Utilisateur('', $nom, $prenom, $email, $password);        
            $utilisateur->modifierLaBD($ancienEmail);
        }
        else{
            $utilisateur = new Utilisateur('', $nom, $prenom, $email, '');        
            $utilisateur->modifierLaBDSansMDP($ancienEmail);                
        }
        $utilisateur->emailExiste();
        $_SESSION['donneesUtilisateur'] = serialize($utilisateur); 
        //Si c'est bon on peut changer de page
        ToolBox::redirige("accueil", 0);

    }
    else {
        echo $message;

    }
}

else if(isset($_GET['logout'])){
    unset($_SESSION['donneesUtilisateur']);
    ToolBox::redirige("accueil", 0);
}

else{
    include dirname(__FILE__)."/erreurController.php";
}
