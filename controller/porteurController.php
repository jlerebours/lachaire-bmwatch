<?php
include dirname(__FILE__) . "/headerController.php";
require_once dirname(__FILE__) . '/../model/UtilisateurClass.php';
require_once dirname(__FILE__) . '/../model/EntiteClass.php';
require_once dirname(__FILE__) . '/../model/ProduitClass.php';
require_once dirname(__FILE__) . '/../model/EntrepriseClass.php';
require_once dirname(__FILE__) . '/../model/CritereClass.php';
require_once dirname(__FILE__) . '/../model/CategorieClass.php';
require_once dirname(__FILE__) . '/../model/SecuriteClass.php';
require_once dirname(__FILE__) . '/../model/ToolBoxClass.php';
$id = null;
$produit=false;

if($nonConnecte || !$droitsPorteur){
    ToolBox::redirige('erreur', 0);
}

if (isset($_GET['ajouterEntreprise'])) {
    $type=1;
    $titre= "Add a company";
    $action="ajouterEntreprise";
    include dirname(__FILE__) . '/../view/ajouterEntiteView.php';
}elseif (isset($_GET['ajouterProduit'])) {
    $type=2;
    $titre= "Add a product";
    $action="ajouterProduit";
    $produit = true;
    $id = $_GET['idEntreprise'];
    include dirname(__FILE__) . '/../view/ajouterEntiteView.php';
}

elseif (isset($_POST)) {

    $utilisateur = unserialize($_SESSION['donneesUtilisateur']);
    $categorie = filter_input(INPUT_POST, "categorie", FILTER_VALIDATE_INT);

    $description = filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($description, 0, 250);

    $source = filter_input(INPUT_POST, "lien", FILTER_VALIDATE_URL);

    $nom = filter_input(INPUT_POST, "nom", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($nom, 0, 30);
    $message = SecuriteClass::securite_vide($nom);

    $formeSociale = filter_input(INPUT_POST, "formeSociale", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($formeSociale, 0, 50);

    $secteurActivite = filter_input(INPUT_POST, "secteurActivite", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($secteurActivite, 0, 50);

    $technologiesUtilisees = filter_input(INPUT_POST, "technologiesUtilisees", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($technologiesUtilisees, 0, 50);

    $nbCriteres = filter_input(INPUT_POST, "nbCriteres", FILTER_VALIDATE_INT);
    $tabCriteres = array();
    for ($i = 0; $i < $nbCriteres; $i++) {
        $j=$i+1;
        $typeCritere = filter_input(INPUT_POST, "typeCritere" . $j, FILTER_SANITIZE_STRING);

        if ($typeCritere == "NUMERIQUE" || $typeCritere == "CLEF") {
            $tabCriteres[$i][0] = filter_input(INPUT_POST, "valeurCritere" . $j, FILTER_VALIDATE_INT);
            
            $tabCriteres[$i][1] = filter_input(INPUT_POST, "critere" . $j, FILTER_SANITIZE_STRING);
            
        } else {
            $tabCriteres[$i][0] = filter_input(INPUT_POST, "critere" . $j, FILTER_VALIDATE_INT);
            $tabCriteres[$i][1] = Entite::obtenirValeurCritere($tabCriteres[$i][0]);
        }
        $message = SecuriteClass::securite_string($tabCriteres[$i][1], 0, 50);
    }

    if ($message == null) {

        $utilisateurId = $utilisateur->getId();
        $etat = 2;
        if(isset($_POST['ajouterEntreprise'])){
            
            $entreprise = new Entreprise('', $utilisateurId, $categorie, $etat, $description, null, null, $nom, $formeSociale, $secteurActivite, $technologiesUtilisees, $source);
            $entreprise->ajouterALaBDEntreprise();
            $entreprise->fixerCriteres($tabCriteres);
            $entite=$entreprise;
            $_SESSION['donneesEntreprise'] = serialize($entreprise);
        }elseif(isset($_POST['ajouterProduit'])){
            $idEntreprise=filter_input(INPUT_POST, "idEntreprise", FILTER_VALIDATE_INT);
            $produit = new Produit('',$idEntreprise, $categorie, $etat, $description, null, null, $nom, null , $source );
            $produit->ajouterALaBDProduit();
            $produit->fixerCriteres($tabCriteres);
            $entite=$produit;
        
        }
    
        ////////////////////////////BM
        $profil = filter_input(INPUT_POST, "profils", FILTER_SANITIZE_STRING);
        if ($profil != "autre") {
            $entite->ajouterBM($profil, "premier BM");
        } else {

            $nomBM = filter_input(INPUT_POST, "nomBm", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($nomBM, 0, 50);

            $principeBM = filter_input(INPUT_POST, "principeBm", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($principeBM, 0, 50);

            $declinaisonBM = filter_input(INPUT_POST, "declinaisonsBm", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($declinaisonBM, 0, 50);

            $variantesBM = filter_input(INPUT_POST, "variantesBm", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($variantesBM, 0, 50);

            if ($message == null) {
                $profil = 0;
                $bm = new BM('', $nomBM, $profil, $principeBM, $declinaisonBM, $variantesBM);
                $bm->ajouterALaBD();
                $idBm = $bm->getId();
                $entite->ajouterBM($idBm, "premier BM");
            } else {
                echo $message;
                exit();
            }

                $nbDimensions = filter_input(INPUT_POST, "nbDimension", FILTER_VALIDATE_INT);
                $tabDimensions = array();
                for ($i = 1; $i <= $nbDimensions; $i++) {
                    $tabDimensions[$i][1] = $i;
                    $tabDimensions[$i][2] = filter_input(INPUT_POST, "boite" . $i, FILTER_SANITIZE_STRING);
                    $tabDimensions[$i][3]= filter_input(INPUT_POST, "commentaireboite" . $i, FILTER_SANITIZE_STRING);
                    if ($tabDimensions[$i][2] == "autre") {
                        $occurence = filter_input(INPUT_POST, "Selectedboite" . $i, FILTER_SANITIZE_STRING);
                        $description = filter_input(INPUT_POST, "descriptionSelectedboite" . $i, FILTER_SANITIZE_STRING);
                        
                        $message = SecuriteClass::securite_string($declinaisonBM, 0, 50);
                        $message = SecuriteClass::securite_string($description, 0, 250);
                        if ($message == null) {
                            $tabDimensions[$i][2] = BM::ajouterOccurence($i, $occurence, $description);
                        } else {
                            echo $message;
                            exit();
                        }
                    }
                }

                if ($message == null) {
                    for ($i = 1; $i <= $nbDimensions; $i++) {
                        if ($tabDimensions[$i][2] != null) {
                            $bm->fixerOccurence($tabDimensions[$i][2],$tabDimensions[$i][3]);
                        }
                    }
                } else {
                    echo $message;
                    exit();
                }

        }
    }
    ToolBox::redirige("accueil", 0);
}
