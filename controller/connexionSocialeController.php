<?php
session_start();
require_once dirname(__FILE__).'/../model/AccesBDClass.php';
require_once dirname(__FILE__).'/../controller/verificationLangueController.php';
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';
$message ="blop";
//Pour que cela fonctionne il faut activer le module CURL


// Ici, un script basique pour recevoir les données des fournisseurs (requiert PHP 5 ou plus)
// Vous devez avoir installé CURL HTTP fetching library

$JanRainApiKey = 'de573f2e3586258a720650bad75c4e561285e60a';  

if(isset($_POST['token'])) { // Si l'utilisateur tente de se connecter avec JanRain
        
    /* ÉTAPE 1 : récupérer le paramètre token */
    $token = $_POST['token'];
  
    /* ÉTAPE 2 : utiliser le token pour envoyer une requête vers le serveur de JanRain, qui interrogera à son tour le fournisseur */
    $post_data = array('token' => $_POST['token'],
                       'apiKey' => $JanRainApiKey,
                       'format' => 'json'); 
  
    $curl = curl_init(); // Initialisation
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Retourne le résultat du transfert au lieu de l'afficher
    curl_setopt($curl, CURLOPT_URL, 'https://rpxnow.com/api/v2/auth_info'); // On définit l'URL cible à récupérer
    curl_setopt($curl, CURLOPT_POST, true); // On dit à PHP de faire un HTTP POST (comme pour les formulaires)
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data); // Données à fournir par HTTP POST
    curl_setopt($curl, CURLOPT_HEADER, false); // On dit de ne pas renvoyer l'en-tête dans la valeur de retour
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // On ne vérifie pas le certificat SSL
    $raw_json = curl_exec($curl); // On exécute la requête
    curl_close($curl); // On ferme la connexion
  
  
    /* ÉTAPE 3 : décoder la réponse Json */
    $auth_info = json_decode($raw_json, true);
  
    if ($auth_info['stat'] == 'ok') { // Si tout est OK
        /* ÉTAPE 4 : récupérer les infos à partir de la réponse */
        $profile = $auth_info['profile']; // Les infos sur le membre
        $default_identifier = $profile['identifier']; // Le lien
        if (isset($profile['photo']))  { // Avatar
          $photo_url = $profile['photo'];
        }
    
        if (isset($profile['name']))  { // Nom à afficher
            $display = $profile['name'];
            $default_prenom = $display['givenName'];
            $default_nom = $display['familyName'];
           
        }
        
        if (isset($profile['verifiedEmail']))  { //Emial a afficher
            $default_email =$profile['verifiedEmail'];           
        }
        
        if($_GET["action"]){
            if($_GET["action"]=="inscription"){
                if(!Utilisateur::utilisateurSocialeExiste($default_identifier)){
                    $sociale = true;
                    include 'inscriptionController.php';
                }
                else{
                    $default_identifier = null;
                    $sociale = false;
                    include 'inscriptionController.php';
                }
            }
            elseif($_GET["action"]=="connexion"){
                $utilisateur = new Utilisateur(null);
                if($utilisateur->obtenirUtilisateurSocial($default_identifier)){
                    $_SESSION['donneesUtilisateur'] = serialize($utilisateur);
                    if($utilisateur->estInactif() != 0){
                        ToolBox::redirige("compte_desactive", 0);
                    }
                    else{
                        ToolBox::redirige("accueil", 0);
                    }
                }
                else{
                    ToolBox::redirige("connexionsoc-fail", 0);
                }
                
            }elseif($_GET["action"]=="add"){
                $utilisateur = unserialize($_SESSION['donneesUtilisateur']);
                $utilisateur->ajouterReseauSociale($default_identifier);
                ToolBox::redirige("profil", 0);
                
            }
            
        }
    }else { /* Une erreur est survenue */
    // On affiche l'erreur
    echo 'Une erreur est survenue : ' . $auth_info['err']['msg'];
    }

}

