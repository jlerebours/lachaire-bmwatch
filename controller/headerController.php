<?php
session_start();
include dirname(__FILE__)."/../view/libView.php";
include dirname(__FILE__)."/verificationLangueController.php";
//include_once dirname(__FILE__).'/analyticstracking.php';
require_once dirname(__FILE__).'/../model/ToolBoxClass.php';
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';

if(!isset($_SESSION['donneesUtilisateur'])){
    $nonConnecte=TRUE;
    
    $droitsAdmin = false;
    $droitsModo = false;
    $droitsPorteur = false;
    $droitsNonPorteur = false;
    $droitsChercheur = false;
    
    include dirname(__FILE__)."/../view/headerNonConnecteView.php";
    
}
else{
    $nonConnecte=FALSE;
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
    $nom = $utilisateurCourant->getNom();
    $prenom = $utilisateurCourant->getPrenom();
    // Cas premiere connexion
    if(empty($nom) && empty($prenom) && !isset($_SESSION['premiereConnexion'])){
        $_SESSION['premiereConnexion']='ok';
        ToolBox::redirige("profil", 0);
    }

    $droitsAdmin = ToolBox::verifierDroits(1);
    $droitsModo = ToolBox::verifierDroits(2);
    $droitsPorteur = ToolBox::verifierDroits(3);
    $droitsNonPorteur = ToolBox::verifierDroits(4);
    $droitsChercheur = ToolBox::verifierDroits(5);
    

    include dirname(__FILE__)."/../view/headerView.php";
}




