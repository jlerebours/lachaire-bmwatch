<?php

include_once dirname(__FILE__)."/../controller/headerController.php";
require_once dirname(__FILE__).'/../model/UtilisateurClass.php';
require_once dirname(__FILE__).'/../model/EntiteClass.php';
require_once dirname(__FILE__).'/../model/CritereClass.php';
require_once dirname(__FILE__).'/../model/CategorieClass.php';

if($nonConnecte || (!$droitsNonPorteur)){
    ToolBox::redirige('erreur', 0);
}

if(isset($_GET['assignation'])){
    include dirname(__FILE__).'/../view/assignationNonPorteurView.php';
}
else{
    ToolBox::redirige('erreurController.php', 0);
}


