<?php
/* Ce fichier a pour objectif de spécifier la valeur des variables utilisées pour l'interface
dans le cas où la langue est en anglais */

// Voc projet
$voc_nomProjet = "BMWatch";

// Voc liste langues
$voc_angleterre = "English";
$voc_france = "Français";

// Voc général
$voc_statut="Status";
$voc_options = "Options";
$voc_derniereModif="Last modification";
$voc_supprimer = "Delete";
$voc_fermer = 'Close';

// Vocabulaire Menu
$voc_administrationUtilisateur = "Users administration";
$voc_validation = "Validation";
$voc_modifierProfil = "Edit profile";
$voc_seDeconnecter = "Sign out";
$voc_ajouterEntreprise = "Add a company";
$voc_moderation = "Moderation";
$voc_site = "Website";


// Formulaire inscription
$voc_pasInscrit = "Not subscribed yet ?";
$voc_nom = "Last name";
$voc_prenom = "First name";
$voc_email = "Email";
$voc_repeterEmail = "Please repeat email here";
$voc_motDePasse = "Password";
$voc_repeterMotDePasse = "Please repeat password here";
$voc_inscriptionBasic = "Basic registration";
$voc_inscriptionReseausociaux = "Registration with social network";
$voc_choixInscription = "Registration's type";
$voc_ajouterProduit = "Add a product";
$voc_compteNonLie = "Your account is not linked to a social network account. Please link it before by subscripbing or editing your profile.";
$voc_chooseOne=" -- Choose one -- ";

$voc_obligatoire = "Required";
$voc_inscription = "Registration";
$voc_informationPersonnelle = "Private information";
$voc_informationEntreprise = "Company's information";
$voc_informationProduit = "Product's information";

//Voc Entite
$voc_categorie = "Category";
$voc_description ="Description";
$voc_Nom = "Name";
$voc_motClef = "Key word";
//Voc entreprise
$voc_entreprises = "Companies";
$voc_entreprise = "Company";
$voc_formeSociale ="Social form";
$voc_secteurActivite = "Business sector";
$voc_technologiesUtilisees = "Used tecnologies";

//Voc Produits
$voc_produit = "Product";
$voc_produits = "Products";
$voc_rank = "Rank";

//Voc BM
$voc_BMs = "Business Models";
$voc_autre = "Other";
$voc_BMNom = "BM's name";
$voc_BMPrincipe = "BM's aim";
$voc_BMDeclinaison = "BM's range";
$voc_BMVariante = "BM's variation";

// Voc connexion
$voc_connexion = "SIGN IN";
$voc_basic = "Basic";
$voc_reseauSocial = "Social network";
$voc_seConnecter ="Connect";
$voc_valider ="Validate";
$voc_annuler ="Cancel";
$voc_mdpErrone = "Bad password";
$voc_erreurChamp = "One of the field contains an error !";
$voc_vousEtesInactif = "Sorry, your account is disabled. </br> It has been either not activated or deactivated by administration.";
$voc_verificationFailed = "Verification failed";
$voc_suppressionFailed = "Suppression failed";

$voc_mdpOublie = "Password forgotten";
$voc_envoyerNewMdp = "Send a new password";

//Popup
$voc_newMdp = "Send a new password";
$voc_resetMdp = "Reset your password";
$voc_warning = "Warning !";
$voc_emailInexistantPhrase ="This email adress is unknown.";

// Partie sur les entités
$voc_nomEntite = "Entity name";
$voc_entite = "Entity";
$voc_entites = "Entities";
$voc_liens = "Link";


// Partie modérateur
$voc_administration = "Administration";
$voc_moderateur = "Moderator";
$voc_moderateurInterface = "Moderator interface";
$voc_adminInterface = "Administrator interface";
$voc_porteurInterface = "Project holder interface";
$voc_nonPorteurInterface = "Non project holder interface";


// Partie utilisateur
$voc_utilisateur = "User";
$voc_renseignezEmails="Write email adresses here, separated by the specified separating character below";
$voc_ajoutEmails = "Email adresses : ";
$voc_caractereSeparation = "Separating character";

// Partie porteur de projet
$voc_nonPorteur = "Non project holder";

// Partie porteur de projet
$voc_porteur = "Project holder";

// Partie chercheurs
$voc_chercheur = "Researcher";

$voc_pageAccueil = "Welcome page";

//Voc des erreurs
$voc_erreur404 = "Error 404 !";
$voc_pageNonTrouvee = "Oops! Sorry, that page could'nt be found.";
$voc_retourAccueil = "Back to home page";

//Voc accueilView.php
$voc_accueil = "Home";

//Voc modificationProfilView.php
$voc_profilUtilisateur = "User profile";
$voc_mdpRequisPourModif ="Your current password is required to allow modifications";
$voc_mdpActuel = "Current password";
$voc_nouveauMdp = "New Password";
$voc_repeterNouveauMdp = "Repeat new password";
$voc_ajouterReseauSocial = "Add social network";
$voc_lierReseauSocial = "Link to a social network account";
$voc_entrerMdp = "Please enter password to enable modification";

//Voc Assignation
$voc_assignation = "Assignation";
$voc_assignations = "Assignations";
$voc_assignePar = "Assigned by";
$voc_mesAssignations = "My assignations";

$voc_nonPorteursDispo = "Available non project holder";
$voc_gestionAssignation="Assignation administration";
$voc_nonPorteursAssignes ="Assigned non project holder";
$voc_entitesAssignees ="Assigned entities to ";
$voc_assigne = "Assigned";
$voc_traite = "Treated";
$voc_dispo = "Available";
$voc_choixNonPorteur = "Choose the non project holder to assign him entities";
$voc_suiviAvt="Progress monitoring";
$voc_supprimerAssignation="Delete assignment";
$voc_voirAvt = "See progress";
$voc_nonPorteurAssigne ="Assigned non project holder";
$voc_changerStatut="Change Status";

//Voc Validation d'une entité
$voc_newEntites = "New entities";
$voc_newPorteurs = "New project holders";
$voc_validationPorteurPhrase = "You need to validate the company (and product) of the project holder to be able to validate the project holder himself.";
$voc_modifEntites = "Modified entities";
$voc_popup = "Are you sure you want to delete this entity?";
$voc_nouvellesOccurences = "New occurences";
$voc_dimensionAssociee = "Associated dimension";
$voc_oui = "Yes";
$voc_non = "No";
$voc_nouveaux = "New";
$voc_modifications = "Modifications";
$voc_visualiser = "Visualize";
$voc_modifier = "Modify";
$voc_affichageOccurence = "Display of the occurence";
$voc_supprimerOcc = "Delete occurence";
$voc_nouveauxCriteres = "New criteria";
$voc_valeurCritere = "Value of the criterion";
$voc_aucunProduit="No product";


// Voc gestion utilisateurs
$voc_utilisateurs = "Users";
$voc_typeUtilisateur = "Type of user";
$voc_groupe="Select group";
$voc_ajouterGroupe = "Add a new group";
$voc_newGroupe = "Name of the new group";
$voc_retourListeGroupes = "Get back to group list";
$voc_administrateur = "Administrator";
$voc_modifierDroits = "Change rights";
$voc_desactiver="Desactivate";
$voc_reactiver="Reactivate";
$voc_nouveau="New";
$voc_liste= "List";
$voc_selectTypeAjout = "Please select the right(s) to give to the new users";
$voc_textareaAjout = "Please type here the email adresses of users you want to add";
$voc_choisissezGroupe = "Please select the group for new user(s)";

//Voc gestion BD
$voc_interfaceAjouts = "Addition Interface";
$voc_BD = "DataBase ";
$voc_affichageCritere = "Display of the criterion"; 
$voc_criteres = "Criteria";
$voc_listeCriteres = "List of criteria in database";
$voc_critere = "Criterion";
$voc_ajouterCritere = "Add a new criterion";
$voc_type = "Type";
$voc_typeNumerique = "Numeric";
$voc_typeListe = "List";
$voc_typeMotsCles = "Keywords";
$voc_ajouterElement= "Add element";
$voc_listeElements = "Element list";
$voc_categories = "Categories";
$voc_listeCategories = "List of categories in database";
$voc_ajouterCategorie = "Add a new category";
$voc_categorieParente = "Parent category";
$voc_transfertEntites = "Entity transfer";
$voc_choisirBD ="Choose DataBase";
// Voc fiche 
$voc_dateCreation = "Creation date";
$voc_dateModification = "Modification date";
$voc_ficheProduit = "Product form";
$voc_ficheEntreprise = "Company form";
$voc_infosValides = "Valid informations";
$voc_infosModifiees = "Modifies informations";

//Onglet
$voc_info_general = "General information";

// Voc menu porteur 
$voc_entreprisesEtProduits= " My Companies & Products";

//Recherche
$voc_recherche = "Search";
$voc_rechercheSimple = "Simple search";
$voc_rechercheMultiCritere = "Multicriteria search";
$voc_entre = "Between";
$voc_aucuneEntrepriseOuProduit = "No company or product found";

$voc_resultat = "Result";
$voc_restriction = "Restriction";
$voc_connexionRequise = "You need to be connected to access this functionnality";
$voc_limiteResultatsNonConnecte = "You are not connected. The number of results will be limited to 5.";
$voc_resultats = "Results";
$voc_action = "Action";
$voc_enregistrer  = "Save";

// Voc templates email
$voc_mail_phraseIntroInscrit = "You are receiving this email because you are registered on the site ";
$voc_mail_phraseIntroInscription = "You are receiving this email because you just registered on the site ";
$voc_mail_phraseIntroMdp = " You are receiving this email because you asked for a new password on the site";

$voc_mail_verifCompte = "Account verification";
$voc_mail_paragrapheVerifCompte = "Just click on the link below to verify your account :";
$voc_mail_lienVerifCompte = "Click here to verify your account";

$voc_mail_jamaisInscrit = "If you never subscribed on this site ...";
$voc_mail_paragrapheSupprCompte = "Just click on the link below to delete account :";
$voc_mail_lienSupprCompte = "Click here to delete your account";

$voc_mail_compteDesactive = "Account disabled";
$voc_mail_compteDesactivePhrase = "This email informs you of the deactivation of your account on the site due to either inactivity or bad activities.";

$voc_mail_compteReactive = "Account reactivated";
$voc_mail_compteReactivePhrase = "This email informs you of the reactivation of your account by a moderator. You can now log in again on the site.";


$voc_mail_compteValide = "Account validated";
$voc_mail_compteValidePhrase = "This email informs you of the validation of your account on the site. You can now log in and use the website.";

$voc_mail_modifDroits = "Modification of your rights";
$voc_mail_modifDroitsPhrase = "This email informs you of a modification of your rights on this site which are now : "; 

$voc_mail_newAssignation = "New assignation";
$voc_mail_newAssignationPhrase = "This email informs you of a new assignation by a moderator. To see it please go on the site, in \"My Assignations\" menu";

$voc_mail_modifAssignation ="Modification of assignation ";
$voc_mail_modifAssignationPhrase = "This email informs you of a modification by a moderator concerning one of your assignation. You will need to connect and work on it again according to the above commentary from your moderator."; 
$voc_mail_modifAssignationCom = "Comment of the moderator";

$voc_mail_newMdp="New password";
$voc_mail_newMdpPhrase="A new password request has been made with your email adress. A new random password has been generated. If you never requested it just change it next time you log in. Your new password is : ";
$voc_mail_newMdpChgt="For safety's sake please change it next time you log in. ";


$voc_mail_inscriptionNP="Validation by moderator required";
$voc_mail_inscriptionNPPhrase="A moderator will validate your informations as soon as possible in ordre to validate your registration. You won't be able to log on the site until then."
        . " You will receive an email when a moderator will have validated your account.";

// Voc FAQ
$voc_faq = "FAQ";
$voc_questions = "Questions";
$voc_question = "Question";
$voc_navigation = "Navigation";
$voc_compte = "Account";
$voc_modifEntite = "Where can I change an entity ?";
$voc_answerModifEntite = "You need to click on the button 'My assignation' and check what entity you need to change.";
$voc_modifMonEntite = "Where can I change informations about my conpanie or my product?";
$voc_answerModifMonEntite = "You need to go to 'My companies & Porducts'.";
$voc_ajoutCritere = "Where can I add a new criteria?";
$voc_answerAjoutCritere = "You need to click on 'Administration' then 'DataBase Administration' and select 'Add a new criterion'.";
$voc_faireAssignation = "How can I make an assignment?";
$voc_answerFaireAssignation = "You need to click on 'Administration' then 'Assignation'. After you choose non project holder and you assign them to an entity.";
$voc_modifBm = "How can I change the BM of an entity?";
$voc_answerModifBm = "You need to go to the record of the entity, then you click on 'Modify' and you change all informations you want.";
$voc_watchEntite = "How can I look the record of an entity?";
$voc_answerWatchEntite = "You need to go to your assignation and you must to click on the little button magnifying glass";
$voc_createAccount = "How to create an account?";
$voc_answerAccount = "You must go to the login page and click the button 'Not subscribe yet'.";
$voc_modifProfil = "How can I change my personal information?";
$voc_answerModifProfil = "You must click on your name at the top right of the page and click on 'Edit profile'.";
$voc_connectSocial = "How can I connect my account to a social network?";
$voc_answerConnectSocial = "You need to go on the page 'Edit profile' and click on 'Link to a social network account'.";
$voc_mdpO = "Where can I reset my forgotten password?";
$voc_answerMdpO = "You can find the button 'Password forgotten' on the login page under 'Not subscribe yet'.";
$voc_listeQuestionsFaq = "List of questions in the FAQ";
$voc_ajouterFaq = "Add a question to the FAQ";
$voc_apercuQuestionFaq = "Visualization and modification of the question ";

//Bouton des fiches
$voc_buttonSubmitARefaire = "To do again";
$voc_buttonSubmitModifierValider = "Modify and validate";
$voc_buttonSubmitModifierEnvoyer = "Modify and send";
$voc_buttonSubmitValider = "Validate";
$voc_buttonSubmitEnvoyer = "Send";
$voc_buttonSubmit = "Modify";
$voc_buttonSubmitAjouterProduit = "Add a product";

// Partie gestion
$voc_vocabulaire = "Vocabulary";
$voc_ajouterQuestionFaq ="Add a question to the FAQ";
$voc_reponse = "Answer";
$voc_nouvelleCategorie = "New category";
$voc_retourListe ="Go back to list";
$voc_variable = "Variable";
$voc_valeur="Value";
$voc_choisirFichierLangue = "Choose the langage file to see or modify";
$voc_modificationVariable = "Modification of a variable of vocabulary";
$voc_fichierActuel = "Current file";
$voc_rechercheApps = "Search for apps to transfer for treatment";

//Partie Chercheur
$voc_analyse = "Analysis";
$voc_choisirEntite = "Choose an entity";
$voc_minRank = "Minimum rank";
$voc_resultatsApps = "Applications found in DataBase";
$voc_avec = "with";
$voc_transfererApps = "Transfer selected applications";
