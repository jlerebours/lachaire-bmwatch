<?php
/* Ce fichier a pour objectif de spécifier la valeur des variables utilisées pour l'interface
dans le cas où la langue est en francais */

// Voc projet
$voc_nomProjet = "BMWatch";

// Voc liste langues
$voc_angleterre = "English";
$voc_france = "Français";

// Voc général
$voc_statut="Status";
$voc_options = "Options";
$voc_derniereModif="Dernière modification";
$voc_supprimer = "supprimer";
$voc_fermer = 'Fermer';

// Vocabulaire Menu
$voc_administrationUtilisateur = "Administration des utilisateurs";
$voc_validation = "Validation";
$voc_modifierProfil = "Modifier profil";
$voc_seDeconnecter = "Se déconnecter";
$voc_ajouterEntreprise = "Ajouter une entreprise";
$voc_moderation = "Modération";
$voc_site = "Site web";


// Formulaire inscription
$voc_pasInscrit = "Pas encore inscrit ?";
$voc_nom = "Nom";
$voc_prenom = "Prénom";
$voc_email = "Email";
$voc_repeterEmail = "Répéter votre email svp";
$voc_motDePasse = "Mot de passe";
$voc_repeterMotDePasse = "Répéter votre mot de passe svp";
$voc_inscriptionBasic = "Inscription normale";
$voc_inscriptionReseausociaux = "Inscription à l'aide des réseaux sociaux";
$voc_choixInscription = "Type d'inscription";
$voc_ajouterProduit = "Ajouter un produit";
$voc_compteNonLie = "Vous n'avez pas encore lié votre compte à un réseau social.Vous pouvez le lier en vous inscrivant ou en modifiant votre compte.";
$voc_chooseOne=" -- Choix -- ";

$voc_obligatoire = "Obligatoire";
$voc_inscription = "Inscription";
$voc_informationPersonnelle = "Informations personnelles";
$voc_informationEntreprise = "Informations de l'entreprise";
$voc_informationProduit = "Informations du poduit";

//Voc Entite
$voc_categorie = "Categorie";
$voc_description ="Description";
$voc_Nom = "Nom";
$voc_motClef = "Mot clef";
//Voc entreprise
$voc_entreprises = "Entreprises";
$voc_entreprise = "Entreprise";
$voc_formeSociale ="Forme sociale";
$voc_secteurActivite = "Secteur d'activité";
$voc_technologiesUtilisees = "Technologies utilisées";

//Voc Produits
$voc_produit = "Produit";
$voc_produits = "Produits";
$voc_rank = "Note";

//Voc BM
$voc_BMs = "Business Models";
$voc_autre = "Autre";
$voc_BMNom = "Nom du BM";
$voc_BMPrincipe = "Pricnipe du BM";
$voc_BMDeclinaison = "Déclinaison du BM";
$voc_BMVariante = "Variante du BM";

// Voc connexion
$voc_connexion = "Connexion";
$voc_basic = "NormalE";
$voc_reseauSocial = "Réseaux sociaux";
$voc_seConnecter ="Se connecter";
$voc_valider ="Valider";
$voc_annuler ="Annuler";
$voc_mdpErrone = "Mauvais mot de passe";
$voc_erreurChamp = "Un des champs contient une erreur !";
$voc_vousEtesInactif = "Désolé, votre compte n'est pas activé. </br> Il n'a soit pas été activé , soit il a été désactivé par l'administration.";
$voc_verificationFailed = "Vérification raté";
$voc_suppressionFailed = "Suppression raté";

$voc_mdpOublie = "Mot de passe oublié";
$voc_envoyerNewMdp = "Envoyer un nouveau mot de passe";

//Popup
$voc_newMdp = "Envoyer un nouveau mot de passe";
$voc_resetMdp = "Réinitialiser le mot de passe ";
$voc_warning = "Attention !";
$voc_emailInexistantPhrase ="Cette adresse email n'existe pas.";

// Partie sur les entités
$voc_nomEntite = "Nom de l'entité";
$voc_entite = "Entité";
$voc_entites = "Entités";
$voc_liens = "Liens";


// Partie modérateur
$voc_administration = "Administration";
$voc_moderateur = "Moderateur";
$voc_moderateurInterface = "Interface du modérateur";
$voc_adminInterface = "Interface de l'administrateur";
$voc_porteurInterface = "Interface du porteur de projet";
$voc_nonPorteurInterface = "Interface du non porteur de projet";


// Partie utilisateur
$voc_utilisateur = "Utilisateur";
$voc_renseignezEmails="Entrez les adresses emails ici, en spécifiant le caractère séparateur en dessous.";
$voc_ajoutEmails = "Adresses email : : ";
$voc_caractereSeparation = "Caractère séparateur";

// Partie porteur de projet
$voc_nonPorteur = "Non porteur de projet";

// Partie porteur de projet
$voc_porteur = "Prorteur de projet";

// Partie chercheurs
$voc_chercheur = "Chercheur";

$voc_pageAccueil = "Page d'accueil";

//Voc des erreurs
$voc_erreur404 = "Error 404 !";
$voc_pageNonTrouvee = "Oops! Sorry, that page could'nt be found.";
$voc_retourAccueil = "Back to home page";

//Voc accueilView.php
$voc_accueil = "Accueil";

//Voc modificationProfilView.php
$voc_profilUtilisateur = "Profil d'utilisateur";
$voc_mdpRequisPourModif ="Votre mot de passe actuel est requis pour apporter des changements à votre profil";
$voc_mdpActuel = "Mot de passe actuel";
$voc_nouveauMdp = "Nouveau mot de passe";
$voc_repeterNouveauMdp = "Répéter le nouveau mot de passe";
$voc_ajouterReseauSocial = "Ajouter un réseau social";
$voc_lierReseauSocial = "Lier à un compte d'un réseau social";
$voc_entrerMdp = "Entrer votre mot de passe pour faire des modifications";

//Voc Assignation
$voc_assignation = "Assignation";
$voc_assignations = "Assignations";
$voc_assignePar = "Assigné par";
$voc_mesAssignations = "Mes assignations";

$voc_nonPorteursDispo = "Non porteur de projet disponible";
$voc_gestionAssignation="Administration des assignations";
$voc_nonPorteursAssignes ="Non porteur de projet assigné";
$voc_entitesAssignees ="Entité assigné à ";
$voc_assigne = "Assigné";
$voc_traite = "Traité";
$voc_dispo = "disponible";
$voc_choixNonPorteur = "Choisissez un non porteur de projet pour lui assigner des entités";
$voc_suiviAvt="Suivi de l'avancement";
$voc_supprimerAssignation="Supprimer une assignation";
$voc_voirAvt = "Voir l'avancement";
$voc_nonPorteurAssigne ="Non porteur de projet assigné";
$voc_changerStatut="Changer statut";

//Voc Validation d'une entité
$voc_newEntites = "Nouvelles entitiés";
$voc_newPorteurs = "Nouveaux porteurs de projet";
$voc_validationPorteurPhrase = "Vous devez valider l'entreprise (et produit) du porteur de projet afin de pouvoir le valider.";
$voc_modifEntites = "Entités modifiées";
$voc_popup = "Etes vous sur de vouloir supprimer cette entité?";
$voc_nouvellesOccurences = "Nouvelles occurences";
$voc_dimensionAssociee = "Dimension associées";
$voc_oui = "Oui";
$voc_non = "Non";
$voc_nouveaux = "Nouveau";
$voc_modifications = "Modifications";
$voc_visualiser = "Visualiser";
$voc_modifier = "Modifier";
$voc_affichageOccurence = "Affichage des occurences";
$voc_supprimerOcc = "Supprimer l'occurence";
$voc_nouveauxCriteres = "Nouveaux criteres";
$voc_valeurCritere = "Valeur du critère";
$voc_aucunProduit="Aucun produit";


// Voc gestion utilisateurs
$voc_utilisateurs = "Utilisateurs";
$voc_typeUtilisateur = "Type d'utilisateur";
$voc_groupe="Selectionner un groupe";
$voc_ajouterGroupe = "Ajouter un nouveau groupe";
$voc_newGroupe = "Nom du nouveau groupe";
$voc_retourListeGroupes = "Retour à la liste des groupes";
$voc_administrateur = "Administrateur";
$voc_modifierDroits = "Modifier les droits";
$voc_desactiver="Désactiver";
$voc_reactiver="Réactiver";
$voc_nouveau="Nouveau";
$voc_liste= "Liste";
$voc_selectTypeAjout = "Sélectionner le(s) droit(s) à donner au nouveau utilisateur";
$voc_textareaAjout = "Entrer les adresse emails des utilisateurs que vous voulez ajouter";
$voc_choisissezGroupe = "Sélectionner le groupe du(des) nouveau(x) utilisateur(s)";

//Voc gestion BD
$voc_interfaceAjouts = "Interface d'ajouts";
$voc_BD = "Bade de données ";
$voc_affichageCritere = "Affichage du critère"; 
$voc_criteres = "Critères";
$voc_listeCriteres = "Liste des critères de la base de données";
$voc_critere = "Critère";
$voc_ajouterCritere = "Ajouter un nouveau critère";
$voc_type = "Type";
$voc_typeNumerique = "Numerique";
$voc_typeListe = "Liste";
$voc_typeMotsCles = "Mot clefs";
$voc_ajouterElement= "Ajouter un élément";
$voc_listeElements = "Liste d'éléments";
$voc_categories = "Categories";
$voc_listeCategories = "Liste des catégories de la base de données";
$voc_ajouterCategorie = "Ajouter une nouvelle catégorie";
$voc_categorieParente = "Catégorie parent";
$voc_transfertEntites = "Transfert d'entités";
$voc_choisirBD ="Choix de la base de données";
// Voc fiche 
$voc_dateCreation = "Date de création";
$voc_dateModification = "Date de modification";
$voc_ficheProduit = "Fiche de produit";
$voc_ficheEntreprise = "Fiche d'entreprise";
$voc_infosValides = "Informations valides";
$voc_infosModifiees = "Informations modifiées";

//Onglet
$voc_info_general = "Informations générales";

// Voc menu porteur 
$voc_entreprisesEtProduits= " Mes entreprises et produits";

//Recherche
$voc_recherche = "Recherche";
$voc_rechercheSimple = "Recherche simple";
$voc_rechercheMultiCritere = "Recherche multi-critères";
$voc_entre = "Entre";
$voc_aucuneEntrepriseOuProduit = "Aucune entreprises ou produits trouvés";

$voc_resultat = "Résultat";
$voc_restriction = "Restriction";
$voc_connexionRequise = "Vous devez être connecté pour avoir acces à cette fonctionnalité";
$voc_limiteResultatsNonConnecte = "Vous n'êtres pas connecté. Le nombre de résultat est limité à 5.";
$voc_resultats = "Résultas";
$voc_action = "Action";
$voc_enregistrer  = "Sauver";

// Voc templates email
$voc_mail_phraseIntroInscrit = "You are receiving this email because you are registered on the site ";
$voc_mail_phraseIntroInscription = "You are receiving this email because you just registered on the site ";
$voc_mail_phraseIntroMdp = " You are receiving this email because you asked for a new password on the site";

$voc_mail_verifCompte = "Account verification";
$voc_mail_paragrapheVerifCompte = "Just click on the link below to verify your account :";
$voc_mail_lienVerifCompte = "Click here to verify your account";

$voc_mail_jamaisInscrit = "If you never subscribed on this site ...";
$voc_mail_paragrapheSupprCompte = "Just click on the link below to delete account :";
$voc_mail_lienSupprCompte = "Click here to delete your account";

$voc_mail_compteDesactive = "Account disabled";
$voc_mail_compteDesactivePhrase = "This email informs you of the deactivation of your account on the site due to either inactivity or bad activities.";

$voc_mail_compteReactive = "Account reactivated";
$voc_mail_compteReactivePhrase = "This email informs you of the reactivation of your account by a moderator. You can now log in again on the site.";


$voc_mail_compteValide = "Account validated";
$voc_mail_compteValidePhrase = "This email informs you of the validation of your account on the site. You can now log in and use the website.";

$voc_mail_modifDroits = "Modification of your rights";
$voc_mail_modifDroitsPhrase = "This email informs you of a modification of your rights on this site which are now : "; 

$voc_mail_newAssignation = "New assignation";
$voc_mail_newAssignationPhrase = "This email informs you of a new assignation by a moderator. To see it please go on the site, in \"My Assignations\" menu";

$voc_mail_modifAssignation ="Modification of assignation ";
$voc_mail_modifAssignationPhrase = "This email informs you of a modification by a moderator concerning one of your assignation. You will need to connect and work on it again according to the above commentary from your moderator."; 
$voc_mail_modifAssignationCom = "Comment of the moderator";

$voc_mail_newMdp="New password";
$voc_mail_newMdpPhrase="A new password request has been made with your email adress. A new random password has been generated. If you never requested it just change it next time you log in. Your new password is : ";
$voc_mail_newMdpChgt="For safety's sake please change it next time you log in. ";


$voc_mail_inscriptionNP="Validation by moderator required";
$voc_mail_inscriptionNPPhrase="A moderator will validate your informations as soon as possible in ordre to validate your registration. You won't be able to log on the site until then."
        . " You will receive an email when a moderator will have validated your account.";

// Voc FAQ
$voc_faq = "FAQ";
$voc_questions = "Questions";
$voc_question = "Question";
$voc_navigation = "Navigation";
$voc_compte = "Account";
$voc_modifEntite = "Where can I change an entity ?";
$voc_answerModifEntite = "You need to click on the button 'My assignation' and check what entity you need to change.";
$voc_modifMonEntite = "Where can I change informations about my conpanie or my product?";
$voc_answerModifMonEntite = "You need to go to 'My companies & Porducts'.";
$voc_ajoutCritere = "Where can I add a new criteria?";
$voc_answerAjoutCritere = "You need to click on 'Administration' then 'DataBase Administration' and select 'Add a new criterion'.";
$voc_faireAssignation = "How can I make an assignment?";
$voc_answerFaireAssignation = "You need to click on 'Administration' then 'Assignation'. After you choose non project holder and you assign them to an entity.";
$voc_modifBm = "How can I change the BM of an entity?";
$voc_answerModifBm = "You need to go to the record of the entity, then you click on 'Modify' and you change all informations you want.";
$voc_watchEntite = "How can I look the record of an entity?";
$voc_answerWatchEntite = "You need to go to your assignation and you must to click on the little button magnifying glass";
$voc_createAccount = "How to create an account?";
$voc_answerAccount = "You must go to the login page and click the button 'Not subscribe yet'.";
$voc_modifProfil = "How can I change my personal information?";
$voc_answerModifProfil = "You must click on your name at the top right of the page and click on 'Edit profile'.";
$voc_connectSocial = "How can I connect my account to a social network?";
$voc_answerConnectSocial = "You need to go on the page 'Edit profile' and click on 'Link to a social network account'.";
$voc_mdpO = "Where can I reset my forgotten password?";
$voc_answerMdpO = "You can find the button 'Password forgotten' on the login page under 'Not subscribe yet'.";
$voc_listeQuestionsFaq = "List of questions in the FAQ";
$voc_ajouterFaq = "Add a question to the FAQ";
$voc_apercuQuestionFaq = "Visualization and modification of the question ";

//Bouton des fiches
$voc_buttonSubmitARefaire = "A refaire";
$voc_buttonSubmitModifierValider = "Modifier et valider";
$voc_buttonSubmitModifierEnvoyer = "Modifier et envoyer";
$voc_buttonSubmitValider = "Valider";
$voc_buttonSubmitEnvoyer = "Envoyer";
$voc_buttonSubmit = "Modifier";
$voc_buttonSubmitAjouterProduit = "Ajouter un produit";

// Partie gestion
$voc_vocabulaire = "Vocabulaire";
$voc_ajouterQuestionFaq ="Ajouter une question à la FAQ";
$voc_reponse = "Réponse";
$voc_nouvelleCategorie = "Nouvelle catégorie";
$voc_retourListe ="Retour à la liste";
$voc_variable = "Variable";
$voc_valeur="Valeur";
$voc_choisirFichierLangue = "Choisissez le ficher de langue pour le voir et le modifier";
$voc_modificationVariable = "Modification de la variable du vocabulaire";
$voc_fichierActuel = "fichier actuel";
$voc_rechercheApps = "Recherche d'application à transférer pour le traiter";

//Partie Chercheur
$voc_analyse = "Analyses";
$voc_choisirEntite = "Choisir une entité";
$voc_minRank = "Note minimale";
$voc_resultatsApps = "Applications trouvés dans la base de données";
$voc_avec = "Avec";
$voc_transfererApps = "Transfer des applications selectionnés";
