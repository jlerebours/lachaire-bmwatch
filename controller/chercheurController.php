<?php
include_once dirname(__FILE__)."/headerController.php";
require_once '../model/ToolBoxClass.php';

if($nonConnecte){
    ToolBox::redirige('erreur', 0);
}
if(isset($_GET['analyse'])){
//    include dirname(__FILE__)."/../view/analyticsCharts.html";
    $listeEntites = ToolBox::obtenirListeEntiteAvecCommentaire();
    $id=null;
    if(isset($_POST['btnChoix'])){
        $id=filter_input(INPUT_POST, "choix", FILTER_VALIDATE_INT);
        $listeCommentaires = ToolBox::obtenirListeCommentaires($id);
        $somme = 0;
        foreach($listeCommentaires as $ranking){
            $somme = $somme + $ranking[2];
        }
        $resultat = $somme/count($listeCommentaires);
    }
    include "../view/analyseChercheurView.php";
}else{
    ToolBox::redirige('erreur', 0);
}