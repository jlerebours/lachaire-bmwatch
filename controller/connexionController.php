<?php
session_start();
include (__DIR__."/../view/libView.php");
include (__DIR__."/verificationLangueController.php");
require_once (__DIR__.'/../model/UtilisateurClass.php');
require_once (__DIR__.'/../model/SecuriteClass.php');
require_once (__DIR__.'/../model/EntiteClass.php');
require_once (__DIR__.'/../model/BMClass.php');

if (isset($_POST["envoiConnexion"])) {
    $connexionOk = FALSE;
    $email = filter_input(INPUT_POST,"email", FILTER_VALIDATE_EMAIL);
    $mdp = filter_input(INPUT_POST,"pass", FILTER_SANITIZE_STRING);

    // On regarde si tout les champs sont remplis. Sinon on lui affiche un message d'erreur. 
    if ($email == NULL OR $mdp == NULL) {
        $errorMSG = $voc_erreurChamp;
    }
    // Sinon si tout les champs sont remplis alors on regarde si le nom de compte rentré existe bien dans la base de données. 
    else {
        $utilisateur = new Utilisateur('', '', '', $email);
        
        // Si oui et si la requête SQL s'est bien passé...  
        if ($utilisateur->emailExiste()) {
            // Si le mot de passe entré à la même valeur que celui de la base de données, on l'autorise a se connecter... 
            if ($utilisateur->mdpBon($mdp)) {
                if($utilisateur->estInactif() == 0){
                    $connexionOk = TRUE;
                    // On sauvegarde en session les données de l'utilisateur
                    $_SESSION['donneesUtilisateur'] = serialize($utilisateur);
                }
                else{
                    $errorMSG=$voc_vousEtesInactif;
                }
            }
            // Sinon on lui affiche un message d'erreur. 
            else {
                $errorMSG = $voc_mdpErrone;
            }
        }
        // Si aucun email ne correspond à celui rentré
        else {
            $errorMSG = $voc_emailInexistantPhrase;
            
        }
    }
    if ($connexionOk) {
        ToolBox::redirige("accueil", 0); 
    }
    else{
        // On réaffiche la vue connexion avec en plus une div d'erreur car connexionOk = false
        include (__DIR__."/../view/connexionView.php");
    }

}
else if(isset($_POST['mdpOublie']) && isset ($_POST['emailPopup'])){
    $email = filter_input(INPUT_POST, 'mdpOublie', FILTER_VALIDATE_EMAIL);
    
    $errorMSG = $voc_emailInexistantPhrase;
}

else if(isset($_GET['connexion'])){
    include (__DIR__."/../view/connexionView.php");
}

else if(isset($_GET['reconnexion'])){
    $reconnexion ='oui';
    include (__DIR__."/../view/connexionView.php");
}
else if(isset($_GET['connexionSocFail'])){
    $connexionOk = FALSE;
    $errorMSG=$voc_compteNonLie;
    include (__DIR__."/../view/connexionView.php");
}
else if(isset($_GET['desactive'])){
    $connexionOk = FALSE;
    $errorMSG=$voc_vousEtesInactif;;
    include (__DIR__."/../view/connexionView.php");
}

else if(isset($_GET['idUser']) && isset($_GET['codeVerification'])){
    $idUser = filter_input(INPUT_GET, 'idUser',FILTER_VALIDATE_INT);
    $codeRecu = filter_input(INPUT_GET, 'codeVerification',FILTER_SANITIZE_STRING);
    
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    $codeATester = sha1(sha1($utilisateur->getEmail())."chaine de verification inscription");
    if($codeRecu == $codeATester){
        $utilisateur->changerInactif(0);
        $_SESSION['donneesUtilisateur'] = serialize($utilisateur);
        ToolBox::redirige('../controller/accueilController.php?accueil', 0);
    }
    else{
        $errorMSG=$voc_verificationFailed;
        include (__DIR__."/../view/connexionView.php");
    } 
}

else if(isset($_GET['idUser']) && isset($_GET['codeSuppression'])){
    $idUser = filter_input(INPUT_GET, 'idUser',FILTER_VALIDATE_INT);
    $codeRecu = filter_input(INPUT_GET, 'codeSuppression',FILTER_SANITIZE_STRING);
    
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    $codeATester = sha1(sha1($utilisateur->getEmail())."chaine de verification inscription");
    if($codeRecu == $codeATester){
        $utilisateur->supprimerDeLaBDSansContraintes();
    }
    else{
        $errorMSG=$voc_suppressionFailed;
    } 
    include (__DIR__."/../view/connexionView.php");
}


else if(isset($_GET['premiereConnexionNP']) && isset($_GET['codeModification']) ){
    $idUser = filter_input(INPUT_GET, 'premiereConnexionNP', FILTER_VALIDATE_INT);
    $codeRecu = filter_input(INPUT_GET, 'codeModification',FILTER_SANITIZE_STRING);
    
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    $codeATester = sha1(sha1($utilisateur->getEmail())."chaine de verification modification NP");
    if($codeRecu == $codeATester){
        $_SESSION['donneesUtilisateur'] = serialize($utilisateur);
        $_SESSION['premiereConnexion']='ok';
        ToolBox::redirige("../controller/utilisateurController.php?modificationProfil", 0);
    }
    else{
        $errorMSG=$voc_suppressionFailed;
    } 
    include (__DIR__."/../view/connexionView.php");
}

else{
    ToolBox::redirige('erreur', 0);
}



