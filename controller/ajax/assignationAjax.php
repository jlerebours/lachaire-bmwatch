<?php
session_start();
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/UtilisateurClass.php';
require_once dirname(__FILE__).'/../../model/SecuriteClass.php';
require_once dirname(__FILE__).'/../../model/EntiteClass.php';

// Initialisations utiles 
$listeEtatsNom=  Entite::getListeEtats();
$listeNomsCouleurs= ToolBox::getListeCouleurs();


// Assignation d'un non porteur au modérateur connecté
if(isset($_GET['assignerNonPorteur'])){
    foreach ($_GET['assignerNonPorteur'] as $idNonPorteur){
        $idNonPorteur = intval($idNonPorteur);
        $moderateur = unserialize($_SESSION['donneesUtilisateur']);
        $moderateur->assigneNonPorteur($idNonPorteur);
    }
    
}

//Permet d assigner un non porteur a une entite
if(isset($_GET['assignerEntite']) && isset($_GET['idNonPorteur'])){
    $idNonPorteur = filter_input(INPUT_GET, 'idNonPorteur',FILTER_VALIDATE_INT);
    foreach ($_GET['assignerEntite'] as $idEntite){
        $idEntite = intval($idEntite);
        $entite = new Entite($idEntite);
        $entite->getInfos();
        
        $moderateur = unserialize($_SESSION['donneesUtilisateur']);
        //Changer true en false pour desactiver la notification du non porteur lors de l'assignation d'une nouvelle entité
        $moderateur->assigneEntite($idNonPorteur,$entite,true);
        switch ($entite->getType()) {
            case 1:
                $entreprise = new Entreprise($entite->getId());
                $entreprise->changerEtatEntreprise(3);
                break;
            case 2 :                
                $produit = new Produit($entite->getId());
                $produit->changerEtatProduit(3);
                break;
        }
        
    }
    echo $moderateur->getId();
}
//Permet de rendre inactive une entite
if(isset($_GET['supprimerEntite'])){
    foreach ($_GET['supprimerEntite'] as $idEntite){
        $idEntite = intval($idEntite);
        $entite = new Entite($idEntite);
        $entite->supprimerAssignation();
        switch ($entite->getType()) {
            case 1:
                $entreprise = new Entreprise($entite->getId());
                $entreprise->changerEtatEntreprise(6);
                break;
            case 2 :                
                $produit = new Produit($entite->getId());
                $produit->changerEtatProduit(6);
                break;
        }
    }
}

//Permet de creer dynamiquement la liste des entités assignées
if(isset($_GET['majEntitesNonPorteur'])){
    $idNonPorteur = intval($_GET['majEntitesNonPorteur']);
    $nonPorteur = new Utilisateur($idNonPorteur,'' ,'' , '', '', '');
    $nonPorteur->idExiste();
    
    $moderateur = unserialize($_SESSION['donneesUtilisateur']);
    $emailNP=$nonPorteur->getEmail();
    ?>
    <div>
        <h4><?=$voc_entitesAssignees?><strong><?=$emailNP?></strong> : </h4>
    </div>
    <div>
        <select multiple="multiple" id="multiselectListeEntites" style="width:400px;" name="my-select[]" data-selectableheader="Available" data-selectionheader="Assigned">
        <?php
        $listeEntitesDispo = Entite::rechercherSelonEtat(6);
        if(!empty($listeEntitesDispo)){
            foreach($listeEntitesDispo as $entiteDispo){
                echo "<option value=\"".$entiteDispo->getId()."\">".$entiteDispo->getNom()."</option>"; 
            }
        }
        $listeEntitesAssignees = $nonPorteur->getEntitesAssignesNPParModo($moderateur->getId());
        if(!empty($listeEntitesAssignees)){
            foreach($listeEntitesAssignees as $entiteAssignee){
                echo "<option selected value=\"".$entiteAssignee->getId()."\">".$entiteAssignee->getNom()."</option>"; 
            }
        }
        ?> 
       </select>
    </div>
   <?php
}

//Permet d enlever un non porteur
if(isset($_GET['supprimerNonPorteur'])){
    foreach ($_GET['supprimerNonPorteur'] as $idNonPorteur){
        $idNonPorteur = intval($idNonPorteur);
        $nonPorteur = new Utilisateur($idNonPorteur);
        $moderateur = unserialize($_SESSION['donneesUtilisateur']);

        $nonPorteur->supprimerAssignations($moderateur->getId(),6);
    }
}

//Permet de mettre a jour la liste des non porteurs
if(isset($_GET['majNonPorteursAssignes'])){
    $moderateur = unserialize($_SESSION['donneesUtilisateur']);
    
    $listeNonPorteursAssignes = $moderateur->getNonPorteurs();
    if(!empty($listeNonPorteursAssignes)){ 
        //Affichage des utilisateurs associés au moderateur connecté 
        if(!empty($listeNonPorteursAssignes)){
            foreach($listeNonPorteursAssignes as $nonPorteurAssigne){
                echo "<option value=\"".$nonPorteurAssigne->getId()."\">".$nonPorteurAssigne->getEmail()."</option>"; 
            }
        }
    }
}

//Permet de mettre a jour le tableau de suivi
if(isset($_GET['majTableauSuivi'])){
    $moderateur = unserialize($_SESSION['donneesUtilisateur']);
    // typeMaj = 1 pour les entreprises et 2 pour les produits
    $typeMaj = filter_input(INPUT_GET, 'majTableauSuivi', FILTER_VALIDATE_INT);
    
    $listeEntitesAssignees=$moderateur->getEntitesAssigneesParModo();
    $listeSuiviEntite=$listeEntitesAssignees[0];
    $listeSuiviNP=$listeEntitesAssignees[1];
    
    $i=0;
    if(!empty($listeSuiviEntite)){
        foreach($listeSuiviEntite as $entite){
            $NP=$listeSuiviNP[$i];
            if(Entreprise::estUneEntreprise($entite->getId()) && $typeMaj == 1 || Produit::estUnProduit($entite->getId()) && $typeMaj == 2){
                $nomEtat = $listeEtatsNom[$entite->getEtat()];
                $couleur=$listeNomsCouleurs[$entite->getEtat()];
                
                echo "<tr>";
                echo "<td class='with-checkbox'><input type='checkbox' name='check' value='1'></td>";
                echo "<td align='center'>".$entite->getNom()."</td>";
                echo "<td align='center'>".$NP->getEmail()."</td>";
                
                echo "<td align='center' style=\"vertical-align:middle\" class='hidden-350'>";
                    echo "<div class=\"btn ".$couleur." taille-parent-100\">".$nomEtat."</div>";
                echo "</td>";
                echo "<td align='center' class='hidden-1024'>".$entite->getDateDerniereModif()."</td>";
                echo "<td class='hidden-480'>";
                    echo "<a class='btn btn-pad' rel='tooltip' title='".$voc_voirAvt."' href=\"visualisation-".$entite->getId()."m\"><i class='fa fa-search'></i></a>";
                    if($entite->getEtat() == 4){
                        echo "<a class='btn btn-pad' rel='tooltip' title=".$voc_valider." onclick=\"validerEntite(".$entite->getId().");\"><i class='fa fa-check'></i></a>";       
                    }
                   echo "<div class='btn-group btn-pad'>"
                            . "<a href='#' data-toggle='dropdown' class='btn dropdown-toggle' title='".$voc_changerStatut."'>"
                                . "<i class='fa fa-cog'><span class='caret'></span></i>"
                            . "</a><"
                            . "ul class='dropdown-menu'>"
                                . "<li><a href='javascript:changerEtatEntite(".$entite->getId().",3);'>".$voc_assigne."</a></li>"
                                . "<li><a href='javascript:changerEtatEntite(".$entite->getId().",4);'>".$voc_traite."</a></li>"
                            . "</ul>"
                        . "</div>";
                    echo "<a class='btn btn-pad' rel='tooltip' title='".$voc_supprimerAssignation."' onclick=\"supprimerAssignation(".$entite->getId().");\"><i class='fa fa-times'></i></a>";       
                echo "</td>";
                echo "</tr>";
            }
            $i=$i+1;
        }
    }
}

//Permet de suppirmer une assignation
if(isset($_GET['delAssign'])){
    $idEntite = filter_input(INPUT_GET, 'delAssign', FILTER_VALIDATE_INT);
    $entite = new Entite($idEntite);
    
    $entite->supprimerAssignation();
    switch ($entite->getType()) {
        case 1:
            $entreprise = new Entreprise($entite->getId());
            $entreprise->changerEtatEntreprise(6);
            break;
        case 2 :                
            $produit = new Produit($entite->getId());
            $produit->changerEtatProduit(6);
            break;
    }
}

//Permet de valider une entite
if(isset($_GET['validerEntite'])){
    $idEntite = filter_input(INPUT_GET, 'validerEntite',FILTER_VALIDATE_INT);
    $entite = new Entite($idEntite);
     switch ($entite->getType()) {
        case 1:
            $entreprise = new Entreprise($entite->getId());
            $entreprise->changerEtatEntreprise(1);
            break;
        case 2 :                
            $produit = new Produit($entite->getId());
            $produit->changerEtatProduit(1);
            break;
    }
}

//Permet de changer l etat d une entite
if(isset($_GET['idEntite']) && isset($_GET['etat'])){
    $etat = filter_input(INPUT_GET, 'etat',FILTER_VALIDATE_INT);
    $idEntite = filter_input(INPUT_GET, 'idEntite',FILTER_VALIDATE_INT);
    $entite = new Entite($idEntite);
    $entite->changerEtat($etat);
}

//Permet de charger la table d assignation
if(isset($_GET['loadTabAssignationsNonPorteur'])){
    //Initialisations 
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
    $listeModos = $utilisateurCourant->getModerateursAssocies();
    
    $tabOutput=array();
    
    foreach ($listeModos as $modo) {
        $listeEntitesAssigneesParModo = $utilisateurCourant->getEntitesAssignesNPParModo($modo->getId());
        $modo->getInfos();
        foreach ($listeEntitesAssigneesParModo as $entiteAssignee) {
            $ligne = array();
            
            $nomEtat = $listeEtatsNom[$entiteAssignee->getEtat()];
            $couleur=$listeNomsCouleurs[$entiteAssignee->getEtat()];
            
            // Ajout des éléments au tableau
            array_push($ligne,$entiteAssignee->getNom());
            array_push($ligne,$modo->getEmail());
            array_push($ligne,"<div class=\"btn ".$couleur." taille-parent-100\">".$nomEtat."</div>");
            array_push($ligne,$entiteAssignee->getDateDerniereModif());
            array_push($ligne,"<a class='btn btn-pad' rel='tooltip' title='".$voc_visualiser."' href=\"visualisation-".$entiteAssignee->getId()."np\"><i class='fa fa-search'></i></a>"
                    . "<a class='btn btn-pad' rel='tooltip' title='".$voc_modifier."' href=\"modification-".$entiteAssignee->getId()."npd\"><i class='fa fa-edit'></i></a>");
            
            array_push($tabOutput,$ligne);
        }
        
    }
    
    $tab = array(
            "sEcho" => intval(1),
            "iTotalRecords" => '$iTotalRecords',
            "iTotalDisplayRecords" => '1',
            "aaData" => $tabOutput
    );
    echo json_encode($tab);
}