<?php
session_start();
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/EntrepriseClass.php';
require_once dirname(__FILE__).'/../../model/ProduitClass.php';
require_once dirname(__FILE__).'/../../model/OccurenceClass.php';
require_once dirname(__FILE__).'/../../model/EntiteClass.php';

if(isset($_POST['validerEntiteModif'])){
    $id = filter_input(INPUT_POST, 'validerEntiteModif', FILTER_VALIDATE_INT);
    
    if(Entreprise::estUneEntreprise($id)){
        $entreprise = new Entreprise($id);
        $entreprise->updateEntreprise();
    }
    else if(Produit::estUnProduit($id)){
        $produit = new Produit($id);
        $produit->updateProduit();
    }
}
//Suppression des entreprises et des produits modifiés préalablement
if(isset($_POST['suppEntiteModif'])){
    $id = filter_input(INPUT_POST, 'suppEntiteModif', FILTER_VALIDATE_INT);
    
    if(Entreprise::estUneEntreprise($id)){
        $entreprise = new Entreprise($id);   
        $entreprise->supprimerEntrepriseModif();
    }
    else if(Produit::estUnProduit($id)){
        $produit = new Produit($id);
        $produit->supprimerProduitModif();
    }
}

//Ajout d'un nouveau produit ou entreprise
if(isset($_POST['validerEntite'])){
    $id = filter_input(INPUT_POST, 'validerEntite', FILTER_VALIDATE_INT);
    
    if(Entreprise::estUneEntreprise($id)){
        $entreprise = new Entreprise($id);   
        $entreprise->validEntreprise();
    }
    else if(Produit::estUnProduit($id)){
        $produit = new Produit($id);
        $produit->validProduit();
    }
}

//Suppression d'un nouveau produit ou entreprise
if(isset($_POST['suppEntite'])){
    $id = filter_input(INPUT_POST, 'suppEntite', FILTER_VALIDATE_INT);
    
    if(Entreprise::estUneEntreprise($id)){
        $entreprise = new Entreprise($id);   
        $entreprise->supprimerEntreprise();
    }
    else if(Produit::estUnProduit($id)){
        $produit = new Produit($id);
        $produit->supprimerProduit();
    }
}

if(isset($_GET['majTableau'])){
    $type = filter_input(INPUT_GET, 'majTableau',FILTER_VALIDATE_INT);
    if($type == 5){
        $listeEntreprisesModifies = Entreprise::getEntrepriseModifie();
        $listeProduitsModifies = Produit::getProduitModifie();
        $listeEntitesNew = array_merge($listeEntreprisesModifies,$listeProduitsModifies);
    }
    else{
        $listeEntitesNew = Entite::rechercherSelonEtat($type);
    }
    
    $tabTypeNom = Entite::getListeTypes();
    $tabTypeCouleur = ToolBox::getListeCouleurs();
    
    $tabOutput = array();
    foreach($listeEntitesNew as $entiteNew){
        // Initialisations
        $ligne = array();
        $cellBoutons="";
        
        $nomType = $tabTypeNom[$entiteNew->getType()];
        $couleur=$tabTypeCouleur[$entiteNew->getType()+2];
        
        array_push($ligne,"<input type='checkbox' name='check' value='1'>");
        array_push($ligne,$entiteNew->getNom());
        array_push($ligne,"<div class=\"btn ".$couleur." taille-parent-100\">".$nomType."</span>");
        $cellBoutons="<a class='btn btn-validate' rel='tooltip' title='".$voc_valider."' id=\"".$entiteNew->getId()."\">
                <i class='fa fa-check'></i>
            </a>";
        if($type == 5){
            $cellBoutons=$cellBoutons."<a class='btn' rel='tooltip' title='Look' href='visualisation-".$entiteNew->getId()."vm'>
                <i class='fa fa-search'></i>
            </a>";
        }
        else{
            $cellBoutons=$cellBoutons."<a class='btn' rel='tooltip' title='Look' href='visualisation-".$entiteNew->getId()."v'>
                <i class='fa fa-search'></i>
            </a>";
        }
       
        $cellBoutons=$cellBoutons."<a class='btn btn-del' rel='tooltip' title='Delete' role='button' class='btn' id=\"".$entiteNew->getId()."\">
                <i class='fa fa-times'>
            </a>";
       
        array_push($ligne,$cellBoutons);
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
    
}

if(isset($_GET['majTableauPorteurs'])){
    
    $listePorteursNonValides = Utilisateur::getNonPorteursNonValides();
   
    $tabOutput = array();
    foreach($listePorteursNonValides as $nonPorteur){
        // A l'inscription le porteur ne peut avoir qu'une entreprise et qu'un produit
        $entreprises = $nonPorteur->getEntreprises();
        (!empty($entreprises))?$entreprise = $entreprises[0]:$entreprise=array();
        $produits = $nonPorteur->getProduits($entreprise->getId());
        (!empty($produits))?$produit = $produits[0]:$produit=array(); 
        
        // Initialisations
        $ligne = array();
        $cellBoutons="";
        
        array_push($ligne,$nonPorteur->getEmail());
        // L'entreprise ne peut pas etre vide
        array_push($ligne,$entreprise->getNom());
        // Le produit lui peut etre vide
        empty($produit)?array_push($ligne,""):array_push($ligne,$produit->getNom());
        
        $cellBoutons ="<div class='btn-group'>
                    <a href='#' data-toggle='dropdown' class='btn dropdown-toggle' title='".$voc_modifierDroits."'>
                        <i class='fa fa-cog'></i>
                        <span class='caret'></span> 
                    </a>
                <ul class='dropdown-menu'>";
        
        if($entreprise->getEtat() == 2){
            $cellBoutons.="<li><a href='visualisation-".$entreprise->getId()."v'><i class='fa fa-check'></i> ".$voc_valider." ".$voc_entreprise."</a></li>";
        }
        if(!empty($produit) && $produit->getEtat() == 2){
            $cellBoutons.="<li><a href='visualisation-".$produit->getId()."v'><i class='fa fa-check'></i> ".$voc_valider." ".$voc_produit."</a></li>";
        }
        if(($entreprise->getEtat() == 1 || empty($entreprise)) && (empty($produit) || $produit->getEtat() == 1 )){
            $cellBoutons="<a class='btn btn-validate' rel='tooltip' title='".$voc_valider." ".$voc_nonPorteur."' id=\"".$nonPorteur->getId()."\">
                            <i class='fa fa-check'></i></a>";
        }
       
        array_push($ligne,$cellBoutons);
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
    
}

if(isset($_POST['validerPorteur'])){
    $idPorteur = filter_input(INPUT_POST, 'validerPorteur',FILTER_VALIDATE_INT);
    $utilisateur = new Utilisateur($idPorteur);
    $utilisateur->getInfos();
    $utilisateur->changerInactif(0);
    ToolBox::envoiMailValidationPorteur($utilisateur);
}

if(isset($_GET['majOccurences'])){
    $listeNouvellesOccurences= Occurence::getNouvellesOccurences();

    $tabOutput = array();
    foreach($listeNouvellesOccurences as $nouvelleOccurence){
        // Initialisations
        $ligne = array();
        
        array_push($ligne,$nouvelleOccurence[1]);
        array_push($ligne,$nouvelleOccurence[2]);
        array_push($ligne,  "<a class='btn btn-viewOcc' rel='tooltip' title='".$voc_visualiser."' id=\"".$nouvelleOccurence[0]."\">
                                <i class='fa fa-search'></i>
                            </a>");
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
   
}

// Partie validation des occurences 
if(isset($_GET['viewOcc'])){
    $idOcc = filter_input(INPUT_GET, 'viewOcc',FILTER_VALIDATE_INT);
    
    $occ = new Occurence($idOcc);
    $occ->getInfos();
    
    $tabOutput=array('id' => $occ->getId(), 'nom' => $occ->getNom(),'description' => $occ->getDescription());
    
    echo json_encode($tabOutput);
}

if(isset($_POST['idOcc']) && isset($_POST['etatOcc']) && isset($_POST['nomOcc']) && isset($_POST['descriptionOcc'])){
    $idOcc = filter_input(INPUT_POST, 'idOcc',FILTER_VALIDATE_INT);
    $nomOcc = filter_input(INPUT_POST, 'nomOcc',FILTER_SANITIZE_STRING);
    $etatOcc = filter_input(INPUT_POST, 'etatOcc',FILTER_VALIDATE_INT);
    $descriptionOcc = filter_input(INPUT_POST, 'descriptionOcc',FILTER_SANITIZE_STRING);
    
    $occ = new Occurence($idOcc);
    $occ->getInfos();
    $occ->setNom($nomOcc);
    $occ->setStatut($etatOcc);
    $occ->setDescription($descriptionOcc);
    $occ->update();
}

if(isset($_POST['suppOcc'])){
    $idOcc = filter_input(INPUT_POST, 'suppOcc',FILTER_VALIDATE_INT);
    
    $occ = new Occurence($idOcc);
    $occ->supprimer();
}

?>