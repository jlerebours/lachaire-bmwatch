<?php
session_start();
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/CritereClass.php';
require_once dirname(__FILE__).'/../../model/FaqClass.php';

if(isset($_GET['chargementFaq'])){
    $listeFaq= Faq::obtenirListeFaq();
    
    $tabOutput = array();
    foreach($listeFaq as $faq){
        // Initialisations
        $ligne = array();
        array_push($ligne,$faq->getCategorie());
        array_push($ligne,  html_entity_decode($faq->getQuestion()));
//      Rajouter la classe  btn-delCritere ci-dessous pour remettre la suppression
        array_push($ligne,  "<a class='btn btn-viewFaq' rel='tooltip' title='".$voc_visualiser."' id=\"".$faq->getId()."\">
                                <i class='fa fa-search'></i>
                            </a>"
                            ."<a class='btn btn-delFaq' rel='tooltip' title='".$voc_supprimer."' id=\"".$faq->getId()."\">
                                <i class='fa fa-times'></i>
                            </a>");
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
   
}


if(isset($_POST['suppFaq'])){
    $id =  filter_input(INPUT_POST, 'suppFaq', FILTER_VALIDATE_INT);
    $faq = new Faq($id);
    $faq->supprimer();
}



if(isset($_GET['viewFaq'])){
   $idFaq = filter_input(INPUT_GET, 'viewFaq',FILTER_VALIDATE_INT);
    
    $faq = new Faq($idFaq);
    $faq->getInfos();
    $tabOutput=array('id' => $faq->getId(), 'categorie' => $faq->getCategorie(), 'question' => $faq->getQuestion(),'reponse' => $faq->getReponse());
    
    echo json_encode($tabOutput);
}


elseif(isset($_POST['modifierVoc'])){
    $fichier = filter_input(INPUT_POST, 'modifierVoc'); 
    $ligne = filter_input(INPUT_POST, 'idLigne'); 
    $variableModif = filter_input(INPUT_POST, 'variable');
    $valeurModif = filter_input(INPUT_POST, 'valeur');
    
    $myFile = getcwd().'/../langues/'.$fichier;
    $aFile = file($myFile);
//    echo $myFile;
    $aFile[$ligne] = $variableModif."= \"".$valeurModif."\"; \n";

    file_put_contents($myFile, implode('', $aFile)); 
}
