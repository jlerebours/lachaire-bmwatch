<?php
session_start();
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/CritereClass.php';

//Permet de créer dynamiquement le tableau de critere
if(isset($_GET['majCriteres'])){
    $listeCriteres= Critere::getTousLesCriteres();
    $tabTypeCouleur = ToolBox::getListeCouleurs();
    
    
    
    $tabOutput = array();
    foreach($listeCriteres as $critere){
        // Initialisations
        $ligne = array();
        $couleur=$tabTypeCouleur[$critere['CRITERE_TYPE']+2];
        $nomType = null;
        switch($critere['CRITERE_TYPE']){
            case 1:
                $nomType = "Company";
                break;
            case 2:
                $nomType = "Product";
                break;
            default :
                $nomType = "Both";
                break;
        }
            
        array_push($ligne,$critere['CRITERE_NOM']);
        array_push($ligne,"<div class=\"btn ".$couleur." taille-parent-100\">".$nomType."</span>");
//      Rajouter la classe  btn-delCritere ci-dessous pour remettre la suppression
        array_push($ligne,  "<a class='btn ' rel='tooltip' title='".$voc_supprimer."' id=\"".$critere['CRITERE_ID']."\">
                                <i class='fa fa-times'></i>
                            </a>");
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
   
}

//Permet de supprimer un critere
if(isset($_POST['suppCritere'])){
    $id =  filter_input(INPUT_POST, 'suppCritere', FILTER_VALIDATE_INT);
    $critere = new Critere($id);
    $critere->supprimer();
    
}

//Permet creer dynamiquement le tableau de categories
if(isset($_GET['majCategories'])){
    $listeCriteres= Critere::getTousLesCriteres();

    $tabOutput = array();
    foreach($listeCriteres as $critere){
        // Initialisations
        $ligne = array();
        
        array_push($ligne,$critere['CRITERE_NOM']);
        array_push($ligne,$critere['CRITERE_NOM']);
//      Rajouter la classe  btn-delCritere ci-dessous pour remettre la suppression
        array_push($ligne,  "<a class='btn ' rel='tooltip' title='".$voc_supprimer."' id=\"".$critere['CRITERE_ID']."\">
                                <i class='fa fa-times'></i>
                            </a>");
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
   
}

//Permet d obtenir le nombre d entite de ce type qui ne seront plus valables apres l ajout du critere
if(isset($_GET['obtenirNbChangement'])){
    $bdd = new AccesBD();
    $bdd = $bdd->getBdd();
    
    $type = $_GET['obtenirNbChangement'];
    
    $requete = "SELECT * FROM `R_ENTITE_POSSEDE_CRITERES` WHERE 1" ;
    $reponse = $bdd->prepare($requete);
    $reponse->execute(); 

    $rows = $reponse->fetchAll();
    $tabListeEntite = array();

    foreach ($rows as $row) {
        array_push($tabListeEntite,$row['ENTITE_ID']);
    }

    $tabListeEntite = array_unique($tabListeEntite);  
    $tabFinal = array();
    if($type !=""){
        foreach ($tabListeEntite as $entite){
                $lentite = new Entite($entite);
                if($lentite->getType()==$type)
                    array_push($tabFinal,$entite);
        }
    }else
        $tabFinal = $tabListeEntite;
                
            
    echo count($tabFinal);
   
}

if(isset($_GET['loadResulatsApps']) && isset($_GET['rank'])){
    $table = filter_input(INPUT_GET, 'loadResulatsApps', FILTER_SANITIZE_STRING);
    $rank = filter_input(INPUT_GET, 'rank');
    $listeApps = Produit::getApps($table, $rank);

    $tabOutput = array();
    foreach($listeCriteres as $critere){
        // Initialisations
        $ligne = array();
        
        array_push($ligne,$listeApps[0]);
        array_push($ligne,$listeApps[1]);
        array_push($ligne,$listeApps[2]);
        array_push($ligne,$listeApps[3]);
      
//      Rajouter la classe  btn-delCritere ci-dessous pour remettre la suppression
        array_push($ligne,  "<a class='btn ' rel='tooltip' title='".$voc_supprimer."' id=\"".$critere['CRITERE_ID']."\">
                                <i class='fa fa-times'></i>
                            </a>");
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
        "sEcho" => intval(1),
        "iTotalRecords" => '$iTotalRecords',
        "iTotalDisplayRecords" => '3',
        "aaData" => $tabOutput
    );
    
    echo json_encode($tab);
}

//Permet de mettre a jour le min et le max
if(isset($_GET['getMinMaxTable'])){
    $table = filter_input(INPUT_GET, 'getMinMaxTable');
    $resultats = Produit::getMinMaxRankApps($table);
    $min = intval($resultats[0]);
    $max = $resultats[1];
    
    $tabOutput=array('min' => $min,'max' => $max);
    
    echo json_encode($tabOutput);
}

if(isset($_POST['ajoutAppsATraiter']) && isset($_POST['table'])){
    $listeApps = $_POST['ajoutAppsATraiter'];
    $table = filter_input(INPUT_POST, 'table',FILTER_SANITIZE_STRING);
    Produit::transfererApps($table,$listeApps);
}