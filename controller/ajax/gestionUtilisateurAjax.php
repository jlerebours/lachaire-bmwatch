<?php
session_start();
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/UtilisateurClass.php';
//require_once dirname(__FILE__).'/../../model/EntiteClass.php';

if(isset($_POST['del'])){
    $id = filter_input(INPUT_POST, 'del', FILTER_VALIDATE_INT);
    $utilisateur = new Utilisateur($id);
    $utilisateur->getInfos();
    
    $utilisateur->supprimerDeLaBD();
}

if (isset($_GET['maj'])){
    // Vérification des droits
    $droitsAdminOk=(ToolBox::verifierDroits(1));
    $droitsModoOk=(ToolBox::verifierDroits(1) || ToolBox::verifierDroits(2));
    
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
    $listeUtilisateurs = $utilisateurCourant->getTousLesUtilisateurs();
    
    $tabOutput=array();
    $translationDroits=  Utilisateur::getListeDroits();
    $listeDroitCouleur=array("Administrator" => "btn-purple","Moderator" => "btn-red", "Project holder" =>"btn-orange", "Non project holder" => "btn-blue", 
        "Researcher" => "btn-satgreen");
    
    // On boucle sur tous les utilisateurs
    foreach($listeUtilisateurs as $utilisateur){
        //Initialisations 
        $listeDroits = $utilisateur->getDroits();
        $listeTotaleDroits=array(1,2,3,4,5);
        $ligne=array();
        $cellDroits="";
        $ajoutDeb="";
        $ajoutFin="";
        if($utilisateur->estInactif()){
            $ajoutDeb="<font color=\"grey\">";
            $ajoutFin="</font>";
        }
        // Ajout des éléments au tableau 
        array_push($ligne,"<input type='checkbox' name='check' value='1'>");
        array_push($ligne,$ajoutDeb.$utilisateur->getNom().$ajoutFin);
        array_push($ligne,$ajoutDeb.$utilisateur->getPrenom().$ajoutFin);
        array_push($ligne,$ajoutDeb.$utilisateur->getEmail().$ajoutFin);
        
        foreach ($listeDroits as $droit){
            $droitString = $translationDroits[$droit];
            $cellDroits = $cellDroits."<div class=\" btn ".$listeDroitCouleur[$droitString]." taille-parent-100\" >".$droitString."</div></br>";
        }
        array_push($ligne,$cellDroits);
        if($utilisateur->estInactif()){
            $ajoutDeb="<font color=\"grey\">";
            $ajoutFin="</font>";
            $cellBoutons="<a class='btn btn-activate' href=\"#\" rel='tooltip' title='".$voc_reactiver."' id=\"".$utilisateur->getId()."\">
                            <i class='fa fa-check'></i></a>";
        }
        else{
            $cellBoutons ="<div class='btn-group'>
                    <a href='#' data-toggle='dropdown' class='btn dropdown-toggle' title='".$voc_modifierDroits."'>
                        <i class='fa fa-cog'></i>
                        <span class='caret'></span> 
                    </a>
                <ul class='dropdown-menu'>";

            foreach ($listeTotaleDroits as $droitCourant) {
                $droitOk=true;
                if(!$droitsAdminOk && (($droitCourant == 1) || ($droitCourant == 2))){
                    $droitOk=false;
                }
                if($droitOk){
                    if($utilisateur->possedeDroit($droitCourant))
                        $cellBoutons=$cellBoutons."<li><a class=\"reload\" href='javascript:supprimerStatut(".$utilisateur->getId().",".$droitCourant.");'><i class='fa fa-minus'></i> ".$translationDroits[$droitCourant]."</a></li>";
                    else
                        $cellBoutons=$cellBoutons."<li><a class=\"reload\" href='javascript:ajouterStatut(".$utilisateur->getId().",".$droitCourant.");'><i class='fa fa-plus'></i> ".$translationDroits[$droitCourant]."</a></li>";    
                }

            }
            $cellBoutons=$cellBoutons."</ul>
                            </div>
                            <a class='btn btn-desactivate' rel='tooltip' title='".$voc_desactiver."' id=\"".$utilisateur->getId()."\">
                            <i class='fa fa-times'></i></a>";
        }
        array_push($ligne,$cellBoutons);
        
        array_push($tabOutput, $ligne);
    }
    
    $tab = array(
            "sEcho" => intval(1),
            "iTotalRecords" => '$iTotalRecords',
            "iTotalDisplayRecords" => '3',
            "aaData" => $tabOutput
        );
    echo json_encode($tab);

}

if(isset($_POST['idUser']) && isset($_POST['ajoutDroit'])){
    $droitAjoute = filter_input(INPUT_POST,'ajoutDroit', FILTER_VALIDATE_INT,1);
    $idUser = filter_input(INPUT_POST,'idUser', FILTER_VALIDATE_INT);
    
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->ajouterStatut($droitAjoute,true);
}

if(isset($_POST['idUser']) && isset($_POST['suppressionDroit'])){
    $droitSuppr = filter_input(INPUT_POST,'suppressionDroit', FILTER_VALIDATE_INT,1);
    $idUser = filter_input(INPUT_POST,'idUser', FILTER_VALIDATE_INT);
    
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    // Si l'utilisateur n'a plus qu'un droit on le desactive
    if(count($utilisateur->getDroits()) == 1){
        $utilisateur->changerInactif(1);
    }
    else{
        $utilisateur->supprimerStatut($droitSuppr,true);
    }
}

if(isset($_POST['desactiver'])){
    $idUser = filter_input(INPUT_POST,'desactiver', FILTER_VALIDATE_INT);
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    $utilisateur->changerInactif(1);
    ToolBox::envoiMailCompteDesactive($utilisateur);
}

if(isset($_POST['activer'])){
    $idUser = filter_input(INPUT_POST,'activer', FILTER_VALIDATE_INT);
    $utilisateur = new Utilisateur($idUser);
    $utilisateur->getInfos();
    $utilisateur->changerInactif(0);
    ToolBox::envoiMailCompteReactive($utilisateur);
}

// Formulaire d'ajout d'utilisateurs
if(isset($_POST['textarea_emails']) && isset($_POST['caractere_separation']) && isset($_POST['selectGroupeNP']) && isset($_POST['selectTypeAjout']) && isset($_POST['nouveauGroupe'])){
    
    $stringEmails = filter_input(INPUT_POST,'textarea_emails', FILTER_SANITIZE_STRING);
    $delimiter = filter_input(INPUT_POST,'caractere_separation', FILTER_SANITIZE_STRING,1);
    $postDroits = $_POST['selectTypeAjout'];
    $groupe = filter_input(INPUT_POST,'selectGroupeNP', FILTER_SANITIZE_STRING);
    $nouveauGroupe = filter_input(INPUT_POST,'nouveauGroupe', FILTER_SANITIZE_STRING);
    
    //Gestion des droits pour l'ajout de l'utilisateur
    $listeDroits =array();
    foreach($postDroits as $droit) {
        array_push($listeDroits,$droit);
    }
    //Gestion du groupe de l'utilisateur
    if(!empty($nouveauGroupe)){
        $groupe=$nouveauGroupe;
    }
    
    $tableauEmails = explode($delimiter, $stringEmails);
    $tabUtilisateursAjoutes=array();
    $tabUtilisateursNonAjoutes=array();
    // On parcourt chaque email et on l'ajoute à la BD
    foreach ($tableauEmails as $value) {
        $email = trim($value);
        $mdpNonChiffre = ToolBox::chaine_aleatoire(8);
        $utilisateurAAjouter=new Utilisateur('', '', '', $email,$mdpNonChiffre,$listeDroits,$groupe,0);
        
        // Si l'email est valide
        if(filter_var($utilisateurAAjouter->getEmail(), FILTER_VALIDATE_EMAIL)){
            // DECOMMENTER A LA FIN DES TESTS
            if($utilisateurAAjouter->ajouterALaBD()){
                array_push($tabUtilisateursAjoutes,$utilisateurAAjouter->getEmail());
                ToolBox::envoiMailInscription($utilisateurAAjouter,$mdpNonChiffre);
            }
            else{
                array_push($tabUtilisateursNonAjoutes,$utilisateurAAjouter->getEmail());
            }
        }
        else{
            array_push($tabUtilisateursNonAjoutes,$utilisateurAAjouter->getEmail());
        }
    }
    
    echo json_encode(array('tabAjoutes' => $tabUtilisateursAjoutes, 'tabNonAjoutes' => $tabUtilisateursNonAjoutes));
}