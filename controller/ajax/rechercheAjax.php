<?php
require_once dirname(__FILE__).'/../../model/AccesBDClass.php';
require_once dirname(__FILE__).'/../../model/RechercheClass.php';
$tabCritereT = array();
if(isset($_POST['tab']))
    $tabCritereT = $_POST['tab'];
$tabCritere = array();
foreach($tabCritereT as $ligne){
    $ligneT = explode(",",$ligne);
    
    switch($ligneT[1]){
        case "NUMERIQUE":
            
            switch($ligneT[2]){
                case "entre":
                    $critere[1]=$ligneT[0];
                    $critere[2]=$ligneT[1];
                    $critere[3]=$ligneT[2];
                    $critere[4]=$ligneT[3];
                    $critere[5]=$ligneT[4];
                    array_push($tabCritere,$critere);
                    break;
                default :
                    $critere[1]=$ligneT[0];
                    $critere[2]=$ligneT[1];
                    $critere[3]=$ligneT[2];                    
                    $critere[4]=$ligneT[3];
                    array_push($tabCritere,$critere);
                    break;                
            }
            break;
        case "CLEF":
            for($i=2;$i<count($ligneT);$i++){
                $critere[1]=$ligneT[0];
                $critere[2]=$ligneT[1];
                $critere[3]=$ligneT[$i];
                array_push($tabCritere,$critere);
            }
            break; 
        default: 
            $critere[1]=$ligneT[0];
            $critere[2]=$ligneT[1];
            array_push($tabCritere,$critere);   
    }
    
    
    
    
}
$type = $_POST['type'];
$tabResultat = Recherche::rechercheMultiCritere($tabCritere,$type);

$nbResultat = count($tabResultat);
echo $nbResultat;