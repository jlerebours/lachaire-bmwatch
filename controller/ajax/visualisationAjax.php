<?php
include dirname(__FILE__).'/../verificationLangueController.php';
require_once dirname(__FILE__).'/../../model/BMClass.php';

    if(isset($_POST['viewBM'])){
        $idBM = filter_input(INPUT_POST, 'viewBM',FILTER_VALIDATE_INT);
        $BM = new BM($idBM);
        $BM->getInfos();
        $default_nomBm = $BM->getNom();
        $default_principe = $BM->getPrincipe();
        $default_declinaison = $BM->getDeclinaison();
        $default_variantes = $BM->getVariantes();
        
        $tabDimension = BM::obtenirDimension();
        $tabInfosBM = array();
        
        array_push($tabInfosBM,array($voc_BMNom,$default_nomBm));
        array_push($tabInfosBM,array($voc_BMPrincipe,$default_principe));
        array_push($tabInfosBM,array($voc_BMDeclinaison,$default_declinaison));
        array_push($tabInfosBM,array($voc_BMVariante,$default_variantes));
        
            foreach ($tabInfosBM as $bm) {
                echo "<div class=\"form-group\">";
                    echo "<label for=\"".$bm[0]."\" class=\"control-label col-sm-2\">".$bm[0]." :</label>";
                    
                    if(!empty($bm[1])){
                        echo "<div class=\"col-sm-10\">";
                            echo $bm[1];
                        echo "</div>";  
                    }
                echo "</div>";
            }
            ?>
            <div class="form-group">
               <?php foreach ($tabDimension as $dimension){?>
                    <div class='form-group'>
                        <label class='control-label col-sm-2 right'><?=$dimension[2]?>:</label>
                            <?php
                            $occ = $BM->obtenirOccurenceDUneBMNom($dimension[1]);
                            $nomOccurence = $occ[0];
                            $commentaireOcc = $occ[1];
                            if(empty($nomOccurence) || ($nomOccurence == null)){
                                $nomOccurence = '<i> Undefined </i>';
                            }
                                echo "<div class=\"col-sm-10\">";
                                    echo $nomOccurence;
                                echo "</div>";  
                            
                            ?>
                    </div>
                   <?php if($commentaireOcc!=""){?>
                    <div class='form-group'>
                        <label class='control-label col-sm-2 right'>Commentary:</label>
                        <div class="col-sm-10">
                            <?=$commentaireOcc ?>
                        </div>    
                    </div>    
                   <?php }} ?>
            </div>

<?php    
    }
    
    if(isset($_POST['viewBmEntreprise'])){
        $idBMEntreprise = filter_input(INPUT_POST, 'viewBmEntreprise',FILTER_VALIDATE_INT);
        $BM = new BM($idBMEntreprise);
        $BM->getInfos();
        $default_nomBm = $BM->getNom();
        $default_principe = $BM->getPrincipe();
        $default_declinaison = $BM->getDeclinaison();
        $default_variantes = $BM->getVariantes();
        
        $tabDimension = BM::obtenirDimension();
        $tabInfosBM = array();
        
        array_push($tabInfosBM,array($voc_BMNom,$default_nomBm));
        array_push($tabInfosBM,array($voc_BMPrincipe,$default_principe));
        array_push($tabInfosBM,array($voc_BMDeclinaison,$default_declinaison));
        array_push($tabInfosBM,array($voc_BMVariante,$default_variantes));
        
            foreach ($tabInfosBM as $bm) {
                echo "<div class=\"form-group\">";
                    echo "<label for=\"".$bm[0]."\" class=\"control-label col-sm-2\">".$bm[0]." :</label>";
                    
                    if(!empty($bm[1])){
                        echo "<div class=\"col-sm-10\">";
                            echo $bm[1];
                        echo "</div>";  
                    }
                echo "</div>";
            }
            ?>
            <div class="form-group">
               <?php foreach ($tabDimension as $dimension){?>
                    <div class='form-group'>
                        <label class='control-label col-sm-2 right'><?=$dimension[2]?>:</label>
                            <?php
                            $nomOccurence = $BM->obtenirOccurenceDUneBMNom($dimension[1]);
                            if(empty($nomOccurence) || ($nomOccurence == null)){
                                $nomOccurence = '<i> Undefined </i>';
                            }
                                echo "<div class=\"col-sm-10\">";
                                    echo $nomOccurence;
                                echo "</div>";
                            ?>
                    </div>
                <?php } ?>
            </div>
<?php    
    }
?>