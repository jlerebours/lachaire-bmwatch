<?php
session_start();
include_once (__DIR__."/../verificationLangueController.php");
require_once dirname(__FILE__).'/../../model/UtilisateurClass.php';
require_once dirname(__FILE__).'/../../model/SecuriteClass.php';
require_once dirname(__FILE__).'/../../model/EntiteClass.php';
require_once dirname(__FILE__).'/../../model/BMClass.php';

if(isset($_GET['verifierEmail'])){ 
    $email = htmlentities($_GET['verifierEmail']);  
    $utilisateur = new Utilisateur('', '', '', $email);
    if($utilisateur->emailExiste()){
        echo "1";  
    }
    else{
        echo "0";
    }
}

else if(isset($_GET['verifierMdp'])){ 
    $mdp = htmlentities($_GET['verifierMdp']);
    $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
    $ancienMdp = $utilisateurCourant->getMdp();
    $utilisateur = new Utilisateur('', '', '', '', $ancienMdp ); 
    if($utilisateur->mdpBon($mdp)){
        echo "1";  
    }
    else{
        echo "0";
    }
}
else if(isset($_GET['verifierNomEntreprise'])){ 
    $nom = htmlentities($_GET['verifierNomEntreprise']);  
    $entreprise = new Entreprise('', '', '', '', '', '','', $nom, '', '', ''); 
    if($entreprise->entrepriseExiste()){
        echo "1";
    }
    else{
        echo "0";
    }
}
else if(isset($_GET['verifierNomProduit'])){ 
    $nom = htmlentities($_GET['verifierNomProduit']);
    $produit = new Produit('', '', '', '', '', '','',  $nom,  ''); 
    if($produit->produitExiste()){
        echo "1";  
    }
    else{
        echo "0";
    }
}

else if(isset($_POST['mdpOublie'])) {
    $email = filter_input(INPUT_POST,'emailPopup',FILTER_VALIDATE_EMAIL);
    $utilisateur = new Utilisateur('', '', '', $email);
    if($utilisateur->emailExiste()){
        $mdp = ToolBox::chaine_aleatoire(8);  
        $utilisateur->setMdp($mdp);
        $utilisateur->modifierLaBD($email);
        $utilisateur->getInfos();
        ToolBox::envoiNouveauMdp($utilisateur,$mdp);
        echo '1';
    }
    else {
        echo '0';
    }
}


