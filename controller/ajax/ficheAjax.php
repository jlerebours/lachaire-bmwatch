<?php
session_start();
require_once dirname(__FILE__).'/../../model/UtilisateurClass.php';
require_once dirname(__FILE__).'/../../model/SecuriteClass.php';
require_once dirname(__FILE__).'/../../model/EntiteClass.php';
require_once dirname(__FILE__).'/../../model/BMClass.php';

//Permet de savoir si un BM est un profil
if(isset($_GET['estProfil']) && isset($_GET['idBM'])){
    $idBM = htmlentities($_GET['idBM']);
    $bm = new BM($idBM,'','','','','');
    echo $bm->estProfil();    
}

//Permet d obtenir les occurences d un bm
if(isset($_GET['obtenirOcc']) && isset($_GET['idBM']) ){
    $idBM = htmlentities($_GET['idBM']);  
    $bm = new BM($idBM,'','','','','');
    echo json_encode($bm->obtenirOccurencesDUneBM());
    
}

//Permet d obtenr les infos d'une bm
if(isset($_GET['obtenirInfoBM']) && isset($_GET['idBM']) ){
    $idBM = htmlentities($_GET['idBM']);  
    $bm = new BM($idBM,'','','','','');
    $bm->obtenirInfoBM();
    $infoBM = array('nom'=>$bm->getNom(), 'principe'=>$bm->getPrincipe(), 'declinaison'=>$bm->getDeclinaison(), 'variantes'=>$bm->getVariantes());
    echo json_encode($infoBM);
    
}
