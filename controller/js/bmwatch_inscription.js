$(document).ready(function() {
    griserEntreprise = false;
    griserProduit = false;
    estProfilEntreprise = false;
    estProfilProduit = false;

    function griserInfoBM() {
        $(".infoBM").prop('disabled', true);
    }

    function degriserInfoBM() {
        $(".infoBM").prop('disabled', false);
    }
    function griserInfoBMProduit() {
        $(".infoBMProduit").prop('disabled', true);
    }

    function degriserInfoBMProduit() {
        $(".infoBMProduit").prop('disabled', false);
    }


    $(".choixDim").change(function() {

        var name = ($(this).attr("name"));
        var value = $(this).val();
        if (value == "autre") {
            $("#occurnenceAutre" + name).show();
            $("#occurnenceAutre" + name + "Produit").show();
            $("#Selected" + name + "Produit").show();
        }
        else {
            $("#Selected" + name).val('');
            $("#descriptionSelected" + name).val('');
            $("#occurnenceAutre" + name).hide();
            $("#occurnenceAutre" + name + "Produit").hide();
            $("#Selected" + name + "Produit").val('');
            $("#descriptionSelected" + name + "Produit").val('');
        }
    });


    function majInfoBM(idBm) {
        $.getJSON("controller/ajax/ficheAjax.php?obtenirInfoBM=true&idBM=" + idBm, function(bm) {

            $("#nomBm").val(bm.nom);
            $("#principeBm").val(bm.principe);
            $("#declinaisonsBm").val(bm.declinaison);
            $("#variantesBm").val(bm.variantes);
        });
    }

    function majInfoBMProduit(idBm) {
        $.getJSON("controller/ajax/ficheAjax.php?obtenirInfoBM=true&idBM=" + idBm, function(bm) {

            $("#nomBmProduit").val(bm.nom);
            $("#principeBmProduit").val(bm.principe);
            $("#declinaisonsBmProduit").val(bm.declinaison);
            $("#variantesBmProduit").val(bm.variantes);
        });
    }

    function enleverSelected(dim) {
        $("#boite" + dim).find('option:selected').removeAttr('selected');
        $("#Selectedboite" + dim).val('');
        $("#descriptionSelectedboite" + dim).val('');
        $("#occurnenceAutreboite" + dim).hide();
        $("#occurnenceAutreboite" + dim + "Produit").hide();
        $("#Selectedboite" + dim + "Produit").val('');
        $("#descriptionSelectedboite" + dim + "Produit").val('');
    }
    function enleverSelectedProduit(dim) {
        $("#boiteProduit" + dim).find('option:selected').removeAttr('selected');
        $("#occurnenceAutreboite" + dim + "Produit").hide();
        $("#Selectedboite" + dim + "Produit").val('');
        $("#descriptionSelectedboite" + dim + "Produit").val('');
    }

    function gestionOcc() {
        var value = $("#profils").val();
        var nbDim = $("#nbDimension").val();
        majInfoBM(value);
        majInfoBMProduit(value);
        $.ajax({
            type: "GET",
            url: "controller/ajax/ficheAjax.php?estProfil=true&idBM=" + value,
            async: false,
            success: function(data) {
                data = $.trim(data);
                if (data == '1')
                {

                    estProfilEntreprise = true;
                    griserInfoBM();
                    griserInfoBMProduit();
                    griserEntreprise = true;
                    griserProduit = true;
                    estProfilProduit = true;
                }
                else {
                    estProfilEntreprise = false;
                    degriserInfoBM();
                    degriserInfoBMProduit();
                    griserEntreprise = false;
                    griserProduit = false;
                    estProfilProduit = false;
                }
            }
        });

        for (var i = 1; i <= nbDim; i++) {
            enleverSelected(i);
        }
        $.getJSON("controller/ajax/ficheAjax.php?obtenirOcc=true&idBM=" + value, function(tabOcc) {
            $.each(tabOcc, function(i, field) {
                $("#commentaireboite"+field[2]).val("");
                if ($("#boite" + field[2] + "Occ" + field[1]).length) {
                    $("#boite" + field[2] + "Occ" + field[1]).prop('selected', 'selected');
                    $("#boite" + field[2] + "Occ" + field[1] + "Produit").prop('selected', 'selected');
                }
                else {
                    $("#boite" + field[2] + "OccAutre").prop('selected', 'selected');
                    $("#boite" + field[2] + "OccAutreProduit").prop('selected', 'selected');
                }
            });

        });
    }

    function gestionOccProduit() {
        var value = $("#profilsProduit").val();
        var nbDim = $("#nbDimension").val();
        majInfoBMProduit(value);
        $.ajax({
            type: "GET",
            url: "controller/ajax/ficheAjax.php?estProfil=true&idBM=" + value,
            async: false,
            success: function(data) {
                data=$.trim(data);
                if (data == '1')
                {
                    estProfilProduit = true;
                    griserInfoBMProduit();
                    griserProduit = true;
                }
                else {
                    estProfilProduit = false;
                    degriserInfoBMProduit();
                    griserProduit = false;
                }
            }
        });

        for (var i = 1; i <= nbDim; i++) {
            enleverSelectedProduit(i);
        }
        $.getJSON("controller/ajax/ficheAjax.php?obtenirOcc=true&idBM=" + value, function(tabOcc) {
            $.each(tabOcc, function(i, field) {
                $("#commentaireboite"+field[2]+"Produit").val("");
                if ($("#boite" + field[2] + "Occ" + field[1] + "Produit").length) {
                    $("#boite" + field[2] + "Occ" + field[1] + "Produit").prop('selected', 'selected');
                }
                else {
                    $("#boite" + field[2] + "OccAutreProduit").prop('selected', 'selected');
                }
            });

        });
    }

    $(".boiteProduit").change(function() {
        if (estProfilProduit) {
            $("#profilsProduit option[value='autre']").prop('selected', 'selected');
            degriserInfoBMProduit();
            $(".infoBMProduit").val("");
            griserProduit = false;
        }
    });

    $(".boite").change(function() {
        if (estProfilEntreprise) {
            $("#profils option[value='autre']").prop('selected', 'selected');
            degriserInfoBM();
            $(".infoBM").val("");

            $("#profilsProduit option[value='autre']").prop('selected', 'selected');
            degriserInfoBMProduit();
            $(".infoBMProduit").val("");

            griserEntreprise = false;
            griserProduit = false;
        }
    });
    
    $(".commentaire").keyup(function() {
        if (estProfilEntreprise) {
            $("#profils option[value='autre']").prop('selected', 'selected');
            degriserInfoBM();
            $("#profilsProduit option[value='autre']").prop('selected', 'selected');
            degriserInfoBMProduit();
            griserEntreprise = false;
            griserProduit = false;
        }
    });

    $(".commentaireProduit").keyup(function() {
        if (estProfilProduit) {

            $("#profilsProduit option[value='autre']").prop('selected', 'selected');
            degriserInfoBMProduit();
            griserProduit = false;
        }
    });



    $("#profils").change(function() {
        gestionOcc();
    });
    $("#profilsProduit").change(function() {
        gestionOccProduit();
    });

    $(".aTransfererSelect").change(function() {
        var name = ($(this).attr("name"));
        var value = $(this).val();

        $("#" + name + "Produit option[value=" + value + "]").prop('selected', 'selected');


    });

    $(".aTransfererInput").change(function() {
        var name = ($(this).attr("name"));
        var value = $(this).val();

        $("#" + name + "Produit").val(value);

    });

    $("#afficherProduit").change(function() {
        var value = $(this).val();
        if (value == "1") {
            $("#infosProduit").show();
        }
        else {
            $("#infosProduit").hide();
        }
    });

    $("#next").click(function() {

        var value = ($(this).attr("value"));
        if (griserEntreprise) {
            griserInfoBM();
        }
        if (griserProduit) {
            griserInfoBMProduit();
        }
        if (value == "Submit") {
            envoie = true;
            $(".form-wizard").submit();
        }
    });

    $("#back").click(function() {
        if ($("#identifiantSocial").val() != "") {
            $("#nomUtilisateur").prop('disabled', 'true');
            $("#prenom").prop('disabled', 'true');
            $(".choixNormal").hide();
        }
        if (griserEntreprise) {
            griserInfoBM();
        }
        if (griserProduit) {
            griserInfoBMProduit();
        }
    });

    $("#choixInscription").change(function() {
        var value = $(this).val();
        if (value == "sociale") {
            $("#boiteTemporaire").hide();
            $("#janrainEngageEmbed").show();
            $("#next").hide();
        }
        else if (value == "normale") {
            $("#boiteTemporaire").show();
            $("#janrainEngageEmbed").hide();
            $("#next").show();
        }
    });



});