$(document).ready(function(){
    construireTableauNewPorteurs();
    construireTableauNew();
    construireTableauModif();
    construireTableauxAjouts();

    initialisationBoutons();      

});
var idOccSelectionnee;

function initialisationBoutons(){
    initialisationBoutonsTabsEntites();
    initialisationBoutonsTabsOccurences();
    initialisationBoutonsTabsCriteres();
    initialisationBoutonsTabsPorteurs();
}

function initialisationBoutonsTabsEntites(){
    $('#tabNew tbody').on('click', 'a.btn-validate', function () {
       if(confirm('Confirm validation of the entity ?')){
            idEntite = $(this).attr('id');
            $.post('controller/ajax/validationAjax.php', {validerEntite: idEntite});
            
            var oTable = $('#tabNew').DataTable();
            var target_row = $(this).closest("tr").get(0);
            var aPos = oTable.fnGetPosition(target_row);

            oTable.fnDeleteRow(aPos);  
        }
    } ); 
    
    $('#tabNew tbody').on('click', 'a.btn-del', function () {
//        $('#modal-2').modal('show');
       if(confirm('Confirm deletion of the entity ?')){
            idEntite = $(this).attr('id');
            $.post('controller/ajax/validationAjax.php', {suppEntite:idEntite});
            
            var oTable = $('#tabNew').DataTable();
            var target_row = $(this).closest("tr").get(0);
            var aPos = oTable.fnGetPosition(target_row);

            oTable.fnDeleteRow(aPos);  
        }
    } ); 
    
    $('#tabModif tbody').on('click', 'a.btn-validate', function () {
       if(confirm('Confirm validation of the modifivations made on the entity ?')){
            idEntite = $(this).attr('id');
            $.post('controller/ajax/validationAjax.php', {validerEntiteModif:idEntite});
            
            var oTable = $('#tabModif').DataTable();
            var target_row = $(this).closest("tr").get(0);
            var aPos = oTable.fnGetPosition(target_row);

            oTable.fnDeleteRow(aPos);
        }
    } ); 
    
    $('#tabModif tbody').on('click', 'a.btn-del', function () {
       if(confirm('Confirm deletion of the modifications made on the entity ?')){
            idEntite = $(this).attr('id');
            $.post('controller/ajax/validationAjax.php', {suppEntiteModif:idEntite});
            
            var oTable = $('#tabModif').DataTable();
            var target_row = $(this).closest("tr").get(0);
            var aPos = oTable.fnGetPosition(target_row);

            oTable.fnDeleteRow(aPos);  
        }
    } ); 
}

function initialisationBoutonsTabsOccurences(){
    $('#tabOccurences tbody').on('click', 'a.btn-viewOcc', function () {
        idOcc = $(this).attr('id');
        $('#btnValiderOcc').attr('idOcc',idOcc);
        
        $.getJSON('controller/ajax/validationAjax.php', {viewOcc: idOcc}, function(data){
            $('#nomOccurence').val(data.nom);
            $('#descriptionOccurence').text(data.description);
            $('#btnValiderOcc').attr('idOcc',data.id);
            $('#boxOccurence').attr('style','display:block');
            
        });
        idOccSelectionnee = $(this).closest("tr").get(0);
    } ); 
}

function initialisationBoutonsTabsPorteurs(){
    $('#tabNewPorteurs tbody').on('click', 'a.btn-validate', function () {
        idPorteur = $(this).attr('id');
        $.post("controller/ajax/validationAjax.php",{validerPorteur:idPorteur});
        var oTable = $('#tabNewPorteurs').DataTable();
        var target_row = $(this).closest("tr").get(0);
        var aPos = oTable.fnGetPosition(target_row);

        oTable.fnDeleteRow(aPos);  
    }); 
}


function initialisationBoutonsTabsCriteres(){
    $('#tabCriteres tbody').on('click', 'a.btn-viewOcc', function () {
        idOcc = $(this).attr('id');
        $('#btnValiderOcc').attr('idOcc',idOcc);
        
        idOccSelectionnee = $(this).closest("tr").get(0);
    } ); 
}

function construireTableauNewPorteurs(){
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [3]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [0,1,2]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": 'controller/ajax/validationAjax.php?majTableauPorteurs',
    };
    
    //Tableau 1
    var oTable = $('#tabNewPorteurs').dataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'aoColumns': [
            {
                type: "text",
            },{
                type: "text",
            },{     
                 type: "text",
             },
            null,
        ]
    });
    $("#tabNewPorteurs").css("width", '100%');
    
}

function construireTableauNew(){
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [0, 3]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [1,2]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": 'controller/ajax/validationAjax.php?majTableau=2',
    };
    
    //Tableau 1
    var oTable = $('#tabNew').dataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'aoColumns': [
            null, {
                type: "text",
            },{     
                 type: "select",
                 bCaseSensitive: true,
                 values: ['Company', 'Product'],
             },
            null,
        ]
    });
    $("#tabNew").css("width", '100%');
    
}

function construireTableauModif(){
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [0, 3]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [1,2]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": 'controller/ajax/validationAjax.php?majTableau=5',
    };
    
    //Tableau 1
    var oTable = $('#tabModif').dataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'aoColumns': [
            null, {
                type: "text",
            },{     
                 type: "select",
                 bCaseSensitive: true,
                 values: ['Company', 'Product'],
             },
            null,
        ]
    });
    $("#tabModif").css("width", '100%');
    
}

function construireTableauxAjouts(){
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [2]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [0,1]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource" : 'controller/ajax/validationAjax.php?majOccurences'
    };
    
    $('.tabs-3col').each(function(){
        
        //Si tableau occurences
        var oTable = $(this).dataTable(opt);
        $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
            disable_search_threshold: 9999999
        });
        $("#check_all").click(function(e) {
            $('input', oTable.fnGetNodes()).prop('checked', this.checked);
        });
        $.datepicker.setDefaults({
            dateFormat: "dd-mm-yy"
        });
        oTable.columnFilter({
            "sPlaceHolder": "head:after",
            'aoColumns': [
                {
                    type: "text",
                },
                {
                    type: "text",
                },
                null,
            ]
        });
        $(this).css("width", '100%');
    });
    

    
}

function validerOccurence(){
    idOcc = $('#btnValiderOcc').attr('idOcc');
    nomOcc = $('#nomOccurence').val();
    descriptionOcc = $('#descriptionOccurence').val();
    
    $.post('controller/ajax/validationAjax.php', {idOcc: idOcc, nomOcc:nomOcc, descriptionOcc:descriptionOcc,etatOcc:1});
    $('#boxOccurence').attr('style','display:none');
    
    var oTable = $('#tabOccurences').DataTable();
    var aPos = oTable.fnGetPosition(idOccSelectionnee);

    oTable.fnDeleteRow(aPos);  
    
}

function supprimerOccurence(){
    $.post('controller/ajax/validationAjax.php', {suppOcc: idOcc});
    $('#boxOccurence').attr('style','display:none');
    
    var oTable = $('#tabOccurences').DataTable();
    var aPos = oTable.fnGetPosition(idOccSelectionnee);

    oTable.fnDeleteRow(aPos);  
}

function annulerOccurence(){
    $('#boxOccurence').attr('style','display:none');
}


// Partie sur les critères 
function validerCritere(){
    idOcc = $('#btnValiderOcc').attr('idOcc');
    nomOcc = $('#nomOccurence').val();
    descriptionOcc = $('#descriptionOccurence').val();
    
    $.post('controller/ajax/validationAjax.php', {idOcc: idOcc, nomOcc:nomOcc, descriptionOcc:descriptionOcc,etatOcc:1});
    $('#boxOccurence').attr('style','display:none');
    
    var oTable = $('#tabOccurences').DataTable();
    var aPos = oTable.fnGetPosition(idOccSelectionnee);

    oTable.fnDeleteRow(aPos);  
    
}

function supprimerCritere(){
    $.post('controller/ajax/validationAjax.php', {suppOcc: idOcc});
    $('#boxOccurence').attr('style','display:none');
    
    var oTable = $('#tabOccurences').DataTable();
    var aPos = oTable.fnGetPosition(idOccSelectionnee);

    oTable.fnDeleteRow(aPos);  
}