$(document).ready(function() {
    $(".notifSeConnecter").click(function() {
        var $el = $(this);
        var textGritter = $el.attr('gritter-text'),
            titleGritter = $el.attr('gritter-title');
        
        $.gritter.add({
            title: titleGritter,
            text: textGritter,
            image: null,
            sticky : false,
            time : 1500,
        });
    });
    

    $(".choixComparaison").each(function(){
        if ($(this).val() == "") {
            var name = $(this).attr("name");
            $("#" + name + "Comparaison").hide();
            $("#" + name + "Entre").hide();
    }
        
    });
    $(".choixComparaison").change(function() {
        var value = $(this).val();
        var name = $(this).attr("name");
        var nbCritere = $(this).attr("nbCritere");
        if (value == "entre") {
            $("#" + name + "Entre").show();
            $("#" + name + "Comparaison").hide();
            $("#chiffre" + nbCritere).val("");
            $("#nb1critere" + nbCritere).val("");
            $("#nb2critere" + nbCritere).val("");

        }
        else if (value == "") {
            $("#" + name + "Entre").hide();
            $("#" + name + "Comparaison").hide();
            $("#chiffre" + nbCritere).val("");
            $("#n1critere" + nbCritere).val("");
            $("#nb2critere" + nbCritere).val("");
        }
        else {
            $("#" + name + "Entre").hide();
            $("#" + name + "Comparaison").show();
            $("#nb1critere" + nbCritere).val("");
            $("#nb2critere" + nbCritere).val("");
        }
    });



    function calculerNombreResultat() {
        var nbCritere = $("#nbCriteres").val();
        var tabCritere = [];
        for (var i = 1; i <= nbCritere; i++) {
            var critere = [];
            var type = $("#typeCritere" + i).val();


            switch (type) {
                case "NUMERIQUE":
                    var choix = $("#choixComparaison" + i).val();

                    switch (choix) {
                        case "entre":
                            var chiffre1 = $("#nb1critere" + i).val();
                            var chiffre2 = $("#nb2critere" + i).val();
                            if (chiffre1 != "" & chiffre2 != "") {
                                critere.push(i);
                                critere.push(type);
                                critere.push(choix);
                                critere.push(chiffre1);
                                critere.push(chiffre2);
                                tabCritere.push(critere);
                            }
                            break;
                        default:
                            var chiffre = $("#chiffre" + i).val();
                            if (chiffre != "") {
                                critere.push(i);
                                critere.push(type);
                                critere.push(choix);
                                critere.push(chiffre);
                                tabCritere.push(critere);
                            }
                            break;

                    }
                    break;

                case "CLEF":
                    var motClef = $("#critere" + i).val();
                    if (motClef != "") {
                        critere.push(i);
                        critere.push(type);
                        critere.push(motClef);
                        tabCritere.push(critere);
                    }
                    break;

                default:
                    var selection = $("#critere" + i).val();
                    if (selection != "") {
                        critere.push(selection);
                        critere.push("CHOIX");
                        tabCritere.push(critere);
                    }
                    break;
            }
        }
        var leType = $("#type").val();
        var req = {'tab[]': tabCritere,'type':leType};
        
        jQuery.ajax({
            type: "POST",
            url: "controller/ajax/rechercheAjax.php",
            data: req,
            success: function(msg) {
                document.getElementById('resultat').innerHTML = msg;

            }
        });


    }
    function calculerNombreResultatParSelect() {
        var nbCritere = $("#nbCriteres").val();
        $(".selection").each(function() {
            var id = $(this).attr("id");
            var nbOption = $("#" + id + ">option").length;
            var leCritere = $(this).attr("name");
            for (var j = 1; j < nbOption; j++) {
                var valeur = $("#" + id + " option:eq(" + j + ")").val();
                var defaut = $("#" + id + " option:eq(" + j + ")").attr("defaut");

                var tabCritere = [];
                for (var i = 1; i <= nbCritere; i++) {
                    var critere = [];
                    var type = $("#typeCritere" + i).val();

                    switch (type) {
                        case "NUMERIQUE":
                            var choix = $("#choixComparaison" + i).val();

                            switch (choix) {
                                case "entre":
                                    var chiffre1 = $("#nb1critere" + i).val();
                                    var chiffre2 = $("#nb2critere" + i).val();
                                    if (chiffre1 != "" & chiffre2 != "") {
                                        critere.push(i);
                                        critere.push(type);
                                        critere.push(choix);
                                        critere.push(chiffre1);
                                        critere.push(chiffre2);
                                        tabCritere.push(critere);
                                    }
                                    break;
                                default:
                                    var chiffre = $("#chiffre" + i).val();
                                    if (chiffre != "") {
                                        critere.push(i);
                                        critere.push(type);
                                        critere.push(choix);
                                        critere.push(chiffre);
                                        tabCritere.push(critere);
                                    }
                                    break;
                            }
                            break;

                        case "CLEF":
                            var motClef = $("#critere" + i).val();
                            if (motClef != "") {
                                critere.push(i);
                                critere.push(type);
                                critere.push(motClef);
                                tabCritere.push(critere);
                            }
                            break;

                        default:
                            if (leCritere != "critere" + i) {
                                var selection = $("#critere" + i).val();
                                if (selection != "") {
                                    critere.push(selection);
                                    critere.push("CHOIX");
                                    tabCritere.push(critere);
                                }
                            } else {
                                critere.push(valeur);
                                critere.push("CHOIX");
                                tabCritere.push(critere);
                            }

                            break;
                    }
                }
                 var leType = $("#type").val();
                var req = {'tab[]': tabCritere,'type':leType}; 
                jQuery.ajax({
                    type: "POST",
                    url: "controller/ajax/rechercheAjax.php",
                    async: false,
                    data: req,
                    success: function(msg) {
                        msg = msg.trim();
                        $("#" + id + " option:eq(" + j + ")").text(defaut + " ("+msg+")");
                    }
                });
            }
        });
    }
    $(".resultatEnBasSelection").change(function() {
        calculerNombreResultat();
        calculerNombreResultatParSelect();
    });     
       
    $(".resultatEnBasNumerique").keyup(function() {
        calculerNombreResultat();
        calculerNombreResultatParSelect();
    });
    $(document).keydown(function(){
        var objet = self.document.activeElement.id;
        if(objet.search("_tag")!=-1){
           calculerNombreResultat();    
           calculerNombreResultatParSelect();
        }        
    });
    
    $("#type").change(function(){
        var value = $(this).val();
            $(".critereType1").each(function(){$(this).hide()});
            $(".critereType2").each(function(){$(this).hide()});
            $(".critereType").each(function(){$(this).hide()});        
        switch (value){
            case "1":
                $(".critereType").each(function(){$(this).show()}); 
                $(".critereType1").each(function(){$(this).show()});
                break;
            case "2":
                $(".critereType").each(function(){$(this).show()});
                $(".critereType2").each(function(){$(this).show()});
                break;
            default : 
                $(".critereType").each(function(){$(this).show()});
                break;
        }
        calculerNombreResultat();
        calculerNombreResultatParSelect();
        
    });
    
    var valueType = $("#type").val();
    $(".critereType1").each(function(){$(this).hide()});
    $(".critereType2").each(function(){$(this).hide()});
    $(".critereType").each(function(){$(this).hide()});        
    switch (valueType){
        case "1":
            $(".critereType").each(function(){$(this).show()}); 
            $(".critereType1").each(function(){$(this).show()});
            break;
        case "2":
            $(".critereType").each(function(){$(this).show()});
            $(".critereType2").each(function(){$(this).show()});
            break;
        default : 
            $(".critereType").each(function(){$(this).show()});
            break;
    }    
    calculerNombreResultat();
    calculerNombreResultatParSelect();
});