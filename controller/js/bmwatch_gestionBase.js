$(document).ready(function() {
    construireTabCriteres();
    construireTabApps();

    initialisationBoutonsCriteres();
    setMinMax();
    $('#idRank').slider("option","change",function (event, ui){
        $('#hiddenRank').val(ui.value);
    });
    $('#tableApps').change(function(){
        setMinMax();
    });
});


function initialisationBoutonsCriteres() {
    $('#tabCriteres tbody').on('click', 'a.btn-delCritere', function() {
        idCritere = $(this).attr('id');
        
        $.post("./ajax/gestionBDAjax.php",{suppCritere:idCritere});
        
        var oTable = $('#tabCriteres').DataTable();
        idOccSelectionnee = $(this).closest("tr").get(0);
        var aPos = oTable.fnGetPosition(idOccSelectionnee);
        
        oTable.fnDeleteRow(aPos);  
    });
    $('#typeCritere').change(function() {
        if ($(this).val() === 'LISTE') {
            $('#liste').attr('style', 'display:block');
        }
        else {
            $('#liste').attr('style', 'display:none');
        }
    })
}


function construireTabCriteres() {
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [1]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [0]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource" :"controller/ajax/gestionBDAjax.php?majCriteres"
    };

    $('.tabs-2col').each(function() {
        
        //Si tableau occurences
        var oTable = $(this).dataTable(opt);
        $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
            disable_search_threshold: 9999999
        });
        $("#check_all").click(function(e) {
            $('input', oTable.fnGetNodes()).prop('checked', this.checked);
        });
        $.datepicker.setDefaults({
            dateFormat: "dd-mm-yy"
        });
        oTable.columnFilter({
            "sPlaceHolder": "head:after",
            'aoColumns': [
                {
                    type: "text",
                },
                null,
            ]
        });
        $(this).css("width", '100%');
    });
    

}


function construireTabApps() {
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [0]
            },
//            {
//                'sClass': 'taille-parent-35',
//                'aTargets': [0,1]
//            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
    };
        
    //Si tableau occurences
    var oTable = $('#tabBDApp').dataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'aoColumns': [
            null,{
                type: "text",
            },{
                type: "text",
            },{
                type: "text",
            },{
                type: "text",
            },
            null,
        ]
    });
    $('#tabBDApp').css("width", '100%');
    

}

$('#btnValiderCritere').on('click', function() {
    var data = $("#type").val();
    $.ajax({
        type: "GET",
        url: "controller/ajax/gestionBDAjax.php?obtenirNbChangement="+data,
        async: false,
        success: function(data) {
            if (confirm('Warning! It will broke '+$.trim(data)+' data')) {
                $("#formCritere").submit();
            }
        }
    });


}); 



function ajouterElement(i){
    dernierElement = i-1;
    // On ajoute l'élément au bon endroit
//    $('#element'+dernierElement).after("<input type='text' id='element"+i+"' name='nomCritere' class='form-control' >");
    $('#element'+dernierElement).after("<div id='element"+i+"' class='input-group'></span><input type='text' name='element[]' class='form-control'><span onclick='supprimerElement("+i+")' class='input-group-addon'><i class='fa fa-minus'></i></span></div>");
    // On modifie le paramètre de la fonction d'ajout
    prochainElement=i+1;
    $('#lienAjout').attr('href',"javascript:ajouterElement("+prochainElement+")");
}
function supprimerElement(i){
    // On ajoute l'élément au bon endroit
    $('#element'+i).remove();
    // On modifie le paramètre de la fonction d'ajout
    $('#lienAjout').attr('href',"javascript:ajouterElement("+i+")");
}

function setMinMax(){
    var table = $('#tableApps option:selected').val();
    $.getJSON('controller/ajax/gestionBDAjax.php',{getMinMaxTable:table}, function(data){
        $('#idRank').slider("option","min",data.min);
        $('#idRank').slider("option","max",data.max);
        $('#idRank').slider("option","step",0.1);
    });
}

function transfererApps(table){
    var listeAppsSelectionnees = new Array();
    $('input:checked').each(function(){
        listeAppsSelectionnees.push($(this).val());
    })
    $.post('controller/ajax/gestionBDAjax.php',{table:table, ajoutAppsATraiter:listeAppsSelectionnees});
}