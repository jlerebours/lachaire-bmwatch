$(document).ready(function() {
    construireTabFaq();
    construireTabsVoc();
    
    initialisationBoutonsFaq();
    
    numeroInit = $('#idSelectLangue option:selected').val();
    $('#tabVoc'+numeroInit).attr('style','display:block');
    
    $('#idSelectLangue').change(function(){
        //On cache toutes les box de voc
        $('.box-voc').each(function(){
            $(this).attr('style','display:none');
        });
        // Puis on affiche celle qui a été selectionnée
        numeroTab = $(this).val();
        $('#tabVoc'+numeroTab).attr('style','display:block');
    });
    

});

function initialisationBoutonsFaq() {
    $('#tabFaq tbody').on('click', 'a.btn-delFaq', function() {
        idFaq = $(this).attr('id');
        
        $.post("controller/ajax/gestionSiteAjax.php",{suppFaq:idFaq});
        
        var oTable = $('#tabFaq').DataTable();
        var id = $(this).closest("tr").get(0);
        var aPos = oTable.fnGetPosition(id);
        
        oTable.fnDeleteRow(aPos);  
    });
    
    $('#tabFaq tbody').on('click', 'a.btn-viewFaq', function () {
        idFaq = $(this).attr('id');
        $('#btnValiderFaq').val(idFaq);
        
        $.getJSON('controller/ajax/gestionSiteAjax.php', {viewFaq: idFaq}, function(data){
            $('#questionFaq').val(data.question);
            $('#categorieFaq').val(data.categorie);
            $('#reponseFaq').val(data.reponse);
            $('#btnValiderFaq').attr('idFaq',data.id);
            modifierTitre('visu');
            $(location).attr('href',"#boxFaq");
            
        });
        idFaqSelectionnee = $(this).closest("tr").get(0);
    } ); 
}

function construireTabFaq() {
    // Options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [2]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [0,1]
            },
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource" :"controller/ajax/gestionSiteAjax.php?chargementFaq"
    };
        
    //Si tableau occurences
    var oTable = $('#tabFaq').dataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'aoColumns': [
            {
                type: "text",
            },{
                type: "text",
            },
            null,
        ]
    });
    $('#tabFaq').css("width", '100%');

}

function construireTabsVoc() {
    // Options avant instanciation
    $('.table-voc').each(function() {
        var opt = {
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
                "sLengthMenu": "_MENU_ <span>entries per page</span>"
            },
            'sDom': "lrtip",
            'aoColumnDefs': [
                {
                    'bSortable': false,
                    'aTargets': [2]
                },
            ],
            'oColVis': {
                "buttonText": "Change columns <i class='icon-angle-down'></i>"
            },
            'oTableTools': {
                "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
            },
            'iDisplayLength':50,
        };

        //Si tableau occurences
        oTable = $(this).dataTable(opt);
        $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
            disable_search_threshold: 9999999
        });
        $("#check_all").click(function(e) {
            $('input', oTable.fnGetNodes()).prop('checked', this.checked);
        });
        $.datepicker.setDefaults({
            dateFormat: "dd-mm-yy"
        });
        oTable.columnFilter({
            "sPlaceHolder": "head:after",
            'aoColumns': [
                {
                    type: "text",
                },{
                    type: "text",
                },
                null,
            ]
        });
        $(this).css("width", '100%');
    });
}


function modifierTitre(type){
    if(type === 'ajout')
        $('#titreBox2Faq').text($('#titreBox2Faq').attr('titreAjout'));
    else
        $('#titreBox2Faq').text($('#titreBox2Faq').attr('titreVisu'));
}

function ajouterCategorie(){
    $('#divNouvelleCategorie').attr('style','display:block');
    $('#selectGroupe').attr('style','display:none');
}
function afficherChoixCategorie(){
    $('#divNouvelleCategorie').attr('style','display:none');
    $('#selectGroupe').attr('style','display:block');
    $('#nouvelleCategorie').val('');
    
}

function annulerVisuFaq(){
    modifierTitre('ajout');
    $('#questionFaq').val('');
    $('#categorieFaq').val('');
    $('#nouvelleCategorie').val('');
    $('#reponseFaq').val('');
    $(location).attr('href',"#");
}

function supprimerFaq(){
//    Suppression en BD
    idFaq = $('#btnValiderFaq').val();
    $.post('controller/ajax/gestionSiteAjax.php', {suppFaq: idFaq});

//    Suppression de la table en dynamique    
    var oTable = $('#tabFaq').DataTable();
    var aPos = oTable.fnGetPosition(idFaqSelectionnee);

    oTable.fnDeleteRow(aPos); 
    
    $('#questionFaq').val('');
    $('#categorieFaq').val('');
    $('#nouvelleCategorie').val('');
    $('#reponseFaq').val('');
    $(location).attr('href',"#");
    modifierTitre('ajout');
}

function modifierVoc(id,variable, valeur){
    $('#idVariable').val(variable);
    $('#idValeur').val(valeur);
    $('#idLigne').val(id);
    $('#modale-voc').modal('show');
}

function validerVoc(){
    fichier = $('#idSelectLangue option:selected').text();
    variable = $('#idVariable').val();
    valeur = $('#idValeur').val();
    ligne = $('#idLigne').val();
    
    $('#modale-voc').modal('hide');
    $.post('controller/ajax/gestionSiteAjax.php', {idLigne:ligne, modifierVoc:fichier, variable:variable,valeur:valeur}, function(data){
       
    });
    
//    location.reload();
}

