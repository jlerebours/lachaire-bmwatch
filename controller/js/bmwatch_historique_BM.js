$(document).ready(function(){
    initialisationBoutons();      
});

function initialisationBoutons(){
    initialisationBoutonsTabsHistorique();
    initialisationBoutonsTabsHistorique2();
}

function initialisationBoutonsTabsHistorique(){
    $('#tabHistorique tbody').on('click', 'a.btn-viewBm', function () {
        idBM = $(this).attr('id');
        
        $.post('controller/ajax/visualisationAjax.php', {viewBM: idBM}, function(data){
            $('#boxHistorique').attr('style','display:block');
             $('#formBM').html(data);
        });
    } ); 
}

function initialisationBoutonsTabsHistorique2(){
    $('#tabHistoriqueEntreprise tbody').on('click', 'a.btn-viewBmEntreprise', function () {
        idBMEntreprise = $(this).attr('id');
        $.post('controller/ajax/visualisationAjax.php', {viewBmEntreprise: idBMEntreprise}, function(data){
            $('#boxHistoriqueEntreprise').attr('style','display:block');
             $('#formBMEntreprise').html(data);
        });
    } ); 
}