(function() {
    if (typeof window.janrain !== 'object') window.janrain = {};
    if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
    janrain.settings.tokenUrl = 'http://incubateurstartingbloc.com/BMwatch/inscription';
    janrain.settings.type = 'embed';
    janrain.settings.appId = 'ppamdodnlhendkkcenda';
    janrain.settings.appUrl = 'https://bmwatch.rpxnow.com';
    janrain.settings.providers = ["googleplus","facebook","linkedin","yahoo"];
    janrain.settings.providersPerPage = '6';
    janrain.settings.format = 'two column';
    janrain.settings.actionText = 'Sign in using your account with';
    janrain.settings.fontColor = '#666666';
    janrain.settings.fontFamily = 'Helvetica, lucida grande, Verdana, sans-serif';
    janrain.settings.backgroundColor = '#ffffff';
    janrain.settings.width = '1500';
    janrain.settings.heigth= '1500';
    janrain.settings.borderColor = '#C0C0C0';
    janrain.settings.borderRadius = '30';    janrain.settings.buttonBorderColor = '#CCCCCC';
    janrain.settings.buttonBorderRadius = '40';
    janrain.settings.buttonBackgroundStyle = 'gradient';
    janrain.settings.language = '';
    janrain.settings.linkClass = 'janrainEngage';
    function isReady() { janrain.ready = true; };
    if (document.addEventListener) {
      document.addEventListener("DOMContentLoaded", isReady, false);
    } else {
      window.attachEvent('onload', isReady);
    }

    var e = document.createElement('script');
    e.type = 'text/javascript';
    e.id = 'janrainAuthWidget';

    if (document.location.protocol === 'https:') {
      e.src = 'https://rpxnow.com/js/lib/bmwatch/engage.js';
    } else {
      e.src = 'http://widget-cdn.rpxnow.com/js/lib/bmwatch/engage.js';
    }

    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(e, s);
})();

