var oTable;

$(document).ready(function(){
    $('#divAjout').attr('style','display:none');
    
    construireTableau1();
//    oTable = $('#tabUtilisateurs').dataTable();
//    $('#tabUtilisateurs tbody').on('click', 'a.reload', function () {
//        oTable.fnReloadAjax();
//    } );
    initialisationBoutons();
       
});
initialisationAjoutUser();

/*********************** Fonctions utilisées : ************************/

function afficherPane2(){
    $('#divAjout').attr('style','display:block');
    $('#selectTypeAjout').trigger("chosen:updated");
}

function initialisationAjoutUser(){
    $("#formAjout").validate({
        errorElement: 'span',
        errorClass: 'help-block has-error',
        errorPlacement: function(error, element) {
            if (element.parents("label").length > 0) {
                element.parents("label").after(error);
            } else {
                element.after(error);
            }
        },
        highlight: function(label) {
            $(label).closest('.form-group').removeClass('has-error has-success').addClass('has-error');
        },
        success: function(label) {
            label.addClass('valid').closest('.form-group').removeClass('has-error has-success').addClass('has-success');
        },
        onkeyup: function(element) {
            $(element).valid();
        },
        onfocusout: function(element) {
            $(element).valid();
        },
        submitHandler:function(form){
            // CODE A AJOUTER LA !
             $.post('controller/ajax/gestionUtilisateurAjax.php', $("#formAjout").serialize(), function(data){
                var obj = jQuery.parseJSON(data);
                var textNotifAjouts = "";
                var textNotifNonAjouts = "";
                $.each(obj['tabAjoutes'], function (i,item){
                    textNotifAjouts += "<p>"+obj['tabAjoutes'][i]+"</p>";
                });
                $.each(obj['tabNonAjoutes'], function (i,item){
                    textNotifNonAjouts += "<p>"+obj['tabNonAjoutes'][i]+"</p>";
                });
                
                if(textNotifAjouts != ""){
                    // Ajout de notif mails ajoutés 
                    $.gritter.add({
                        title: 'Successfully added users',
                        text: textNotifAjouts,
                        image: null,
                        sticky : true,
                        class_name: 'gritter-green',
                    });
                }
                if(textNotifNonAjouts != ""){
                    // Ajout de notif mails ajoutés 
                    $.gritter.add({
                        title: 'Unsuccessfully added users ',
                        text: textNotifNonAjouts,
                        image: null,
                        sticky : true,
                        class_name: 'gritter-red',
                    });
                }
                 
                 // Ajout de notif mails non ajoutés
             });
        }
    });

}

function construireTableau1(){
    // options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [
            {
                'bSortable': false,
                'aTargets': [0,5]
            },
            {
                'sClass': 'colonne-center',
                'aTargets': [0,1,2,3,4]
            }
        ],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": 'controller/ajax/gestionUtilisateurAjax.php?maj=1',
    };
    
    //Tableau 1
    oTable = $('#tabUtilisateurs').DataTable(opt);
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "dd-mm-yy"
    });
    oTable.columnFilter({
       "sPlaceHolder": "head:after",
       'aoColumns': [
           null, {
               type: "text",
            },{
               type: "text",
            },{
               type: "text",
            },{     
                type: "select",
                bCaseSensitive: true,
                values: ['Moderator', 'Project holder','Non project holder', 'Researcher'],
            },
            null,
       ]
    });
    $("#tabUtilisateurs").css("width", '100%');
    
}

function initialisationBoutons(){
//    $('#tabUtilisateurs tbody').on('click', 'a.btn-del', function () {
//       if(confirm('Voulez-vous vraiment supprimer cet utilisateur ?')){
//            idUser = $(this).attr('identifiant');
////            $.post('controller/ajax/gestionUtilisateurAjax.php',{desactiver:idUser});
//            
//            var oTable = $('#tabUtilisateurs').DataTable();
//            var target_row = $(this).closest("tr").get(0);
//            var aPos = oTable.fnGetPosition(target_row);
//
//            oTable.fnDeleteRow(aPos);  
//        }
//    } ); 
    $('#tabUtilisateurs tbody').on('click', 'a.btn-desactivate', function () {
        idUser = $(this).attr('id');
        $.ajax({
           type: "POST",
           url: 'controller/ajax/gestionUtilisateurAjax.php',
           data:{desactiver:idUser},
           async: false,   
           success: function(){
               location.reload();
           }
        });
    });
    $('#tabUtilisateurs tbody').on('click', 'a.btn-activate', function () {
        idUser = $(this).attr('id');
        $.ajax({
           type: "POST",
           url: 'controller/ajax/gestionUtilisateurAjax.php',
           data:{activer:idUser},
           async: false,   
           success: function(){
               location.reload();
           }
        });
        
    } );
//    $('#tabUtilisateurs tbody').on('click', 'a.reload', function () {
//            supprimerStatut()
//            var oTable = $('#tabUtilisateurs').DataTable();
//            oTable.fnReloadAjax();
//    } ); 
}

function afficherAjoutGroupe(){
    $('#divNewGroupe').attr('style','display:block');
    $('#divGroupe').attr('style','display:none');
}

function afficherChoixGroupe(){
    $('#divNewGroupe').attr('style','display:none');
    $('#divGroupe').attr('style','display:block');
    $('#nouveauGroupe').val('');
}

function ajouterStatut(idUser,type){
    $.ajax({
        type: "POST",
        url: 'controller/ajax/gestionUtilisateurAjax.php',
        data:{idUser:idUser,ajoutDroit: type},
        async: false,   
        success: function(){
            location.reload();
        }
     });
}

function supprimerStatut(idUser,type){
    $.ajax({
        type: "POST",
        url: 'controller/ajax/gestionUtilisateurAjax.php',
        data:{idUser:idUser,suppressionDroit: type},
        async: false,   
        success: function(){
            location.reload();
        }
    });
}