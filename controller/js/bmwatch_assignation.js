$(document).ready(function() {
    
    // Construction de l'onglet non porteurs
    construireMultiSelect();
    
    // Construction de l'onglet entités
    $('#listeNonPorteursEntitePane').attr('style','display:none');
    construireMultiSelectEntites();
    $('#selectListeNonPorteursAssignes').change(function(){
        affichageEntitesAFaire();
    });
    
    // Construction de l'onglet suivi
    $('#suiviPaneDiv').attr('style','display:none');
//    affichageTableauxAssignations();
    
    construireTabAssignationsNonPorteur();
});


function affichageEntitePane(){ 
    $('#listeNonPorteursEntitePane').attr('style', 'display:block');
}

function affichageSuiviPane(){ 
    $('#suiviPaneDiv').attr('style', 'display:block');
}

function contruireSelectNonPorteursAssignes(){
    $.ajax({
        type: "GET",
        url: "controller/ajax/assignationAjax.php",
        data:{ majNonPorteursAssignes: "1"},
        async: false,                
        success:function(data){
            $('#selectListeNonPorteursAssignes').html(data);
        }   
    });
    $('#selectListeNonPorteursAssignes').trigger("chosen:updated");
}

function affichageEntitesAFaire(){
    idNonPorteur = $("#selectListeNonPorteursAssignes option:selected").val();
    $.ajax({
        type: "GET",
        url: "controller/ajax/assignationAjax.php",
        data:{ majEntitesNonPorteur: idNonPorteur},
        async: false,                
        success:function(data){
            $('#conteneurMultiSelectEntites').html(data);
        }   
    });
    construireMultiSelectEntites();
}

function affichageTableauxAssignations(){
    $.ajax({
        type: "GET",
        url: "controller/ajax/assignationAjax.php",
        data:{ majTableauSuivi: 1},
        async: false,                
        success:function(data){
            $('#conteneurAssignationsEntreprises').html(data);
        }   
    });
    $.ajax({
        type: "GET",
        url: "controller/ajax/assignationAjax.php",
        data:{ majTableauSuivi: 2},
        async: false,                
        success:function(data){
            $('#conteneurAssignationsProduits').html(data);
        }   
    });
    construireTableaux();
}

// Multiselect des non porteurs (premier onglet)
function construireMultiSelect(){
    $("#multiselectListeNonPorteurs").each(function() { 
        var $el = $(this);
        var selectableHeader = $el.attr('data-selectableheader'),
            selectionHeader = $el.attr('data-selectionheader');
        if (selectableHeader !== undefined) {
            selectableHeader = "<div class='multi-custom-header'>" + selectableHeader + "</div>";
        }
        if (selectionHeader !== undefined) {
            selectionHeader = "<div class='multi-custom-header'>" + selectionHeader + "</div>";
        }
        
        $el.multiSelect({
            dblClick : true,
            cssClass:"multiselect-conteneur",
            afterSelect: function(value){
                $.ajax({
                    type: "GET",
                    url: "controller/ajax/assignationAjax.php",
                    data:{ assignerNonPorteur: value},
                    async: false,   
                });
            },
            afterDeselect: function(value){
                $.ajax({
                    type: "GET",
                    url: "controller/ajax/assignationAjax.php",
                    data:{ supprimerNonPorteur: value},
                    async: false,   
                });
            },
            selectionHeader: selectionHeader,
            selectableHeader: selectableHeader,
            selectableOptgroup: true
        });
//        $el.attr('style',"width:300px;");
    });
}

function construireMultiSelectEntites(){
    $("#multiselectListeEntites").each(function() { 
        var $el = $(this);
        var selectableHeader = $el.attr('data-selectableheader'),
            selectionHeader = $el.attr('data-selectionheader');
        if (selectableHeader !== undefined) {
            selectableHeader = "<div class='multi-custom-header'>" + selectableHeader + "</div>";
        }
        if (selectionHeader !== undefined) {
            selectionHeader = "<div class='multi-custom-header'>" + selectionHeader + "</div>";
        }
        $el.multiSelect({
            dblClick : true,
            afterSelect: function(value){
                idNonPorteur = $("#selectListeNonPorteursAssignes option:selected").val();     
                $.ajax({
                    type: "GET",
                    url: "controller/ajax/assignationAjax.php",
                    data:{ assignerEntite: value, idNonPorteur: idNonPorteur},
                    async: false,   
                });
                affichageEntitesAFaire();
                
            },
            afterDeselect: function(value){
                $.ajax({
                    type: "GET",
                    url: "controller/ajax/assignationAjax.php",
                    data:{ supprimerEntite: value},
                    async: false,   
                });
               affichageEntitesAFaire();
            },
            selectionHeader: selectionHeader,
            selectableHeader: selectableHeader,
            selectableOptgroup: true,
        });
    });
}

function construireTableaux(){
    
    // Définition des options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [0,5]
        }],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        }
        
    }; 
    
    
    if(!$('#table1').attr('initialisee')){
        //Tableau 1
        var oTable = $('#table1').dataTable(opt);

        $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
            disable_search_threshold: 9999999
        });
        $("#check_all").click(function(e) {
            $('input', oTable.fnGetNodes()).prop('checked', this.checked);
        });
        $.datepicker.setDefaults({
            dateFormat: "yy-mm-dd"
        });
        
        oTable.columnFilter({
            "sPlaceHolder": "head:after",
            'sRangeFormat': "{from}{to}",
            'aoColumns': [
                null, {
                    type: "text",
                }, {
                    type: "text",
                },{
                    type: "select",
                    bCaseSensitive: true,
                    values: ['Assigned', 'Treated']
                } ,{
                    type: "date-range"
                },null
            ],
            'bDestroy': true,
        });
        $("#table1").css("width", '100%');
        $("#table1").attr('initialisee',true);
    }
    
    if(!$('#table2').attr('initialisee')){
        //Tableau 1
        var oTable = $('#table2').dataTable(opt);

        $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
            disable_search_threshold: 9999999
        });
        $("#check_all").click(function(e) {
            $('input', oTable.fnGetNodes()).prop('checked', this.checked);
        });
        $.datepicker.setDefaults({
            dateFormat: "yy-mm-dd"
        });
        oTable.columnFilter({
            "sPlaceHolder": "head:after",
            'sRangeFormat': "{from}{to}",
            'aoColumns': [
                null, {
                    type: "text",
                }, {
                    type: "text",
                }, {
                    type: "select",
                    bCaseSensitive: true,
                    values:  ['Assigned', 'Treated']
                },{
                    type: "date-range"
                },
                null
            ]
        });
        $("#table2").css("width", '100%');
        $("#table2").attr('initialisee',true);
    }
    
}

function construireTabAssignationsNonPorteur(){
    // Définition des options avant instanciation
    var opt = {
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> entries",
            "sLengthMenu": "_MENU_ <span>entries per page</span>"
        },
        'sDom': "lrtip",
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [4]
        }],
        'oColVis': {
            "buttonText": "Change columns <i class='icon-angle-down'></i>"
        },
        'oTableTools': {
            "sSwfPath": "js/plugins/datatable/swf/copy_csv_xls_pdf.swf"
        },
        "bProcessing": true,
        "iDisplayLength": 10,
        "sAjaxSource": 'controller/ajax/assignationAjax.php?loadTabAssignationsNonPorteur=1'
        
    }; 
    
    //Tableau 1
    var oTable = $('#tabAssignationsNonPorteur').dataTable(opt);

    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $("#check_all").click(function(e) {
        $('input', oTable.fnGetNodes()).prop('checked', this.checked);
    });
    $.datepicker.setDefaults({
        dateFormat: "yy-mm-dd"
    });

    oTable.columnFilter({
        "sPlaceHolder": "head:after",
        'sRangeFormat': "{from}{to}",
        'aoColumns': [
            {
                type: "text",
            }, {
                type: "text",
            },{
                type: "select",
                bCaseSensitive: true,
                values: ['Assigned', 'Treated']
            } ,{
                type: "date-range"
            },null
        ],
        'bDestroy': true,
    });
    $("#tabAssignationsNonPorteur").css("width", '100%');
    
}

function supprimerAssignation (idEntite){
    $.ajax({
        type: "GET",
        url: "controller/ajax/assignationAjax.php",
        data:{ delAssign: idEntite},
        async: false,                
        success:function(){
            affichageTableauxAssignations();
        }   
    });
}
function validerEntite(idEntite){
    if(confirm("Voulez-vous vraiment valider l'entité ?")){        
        $.get('controller/ajax/assignationAjax.php',{delAssign: idEntite});
        $.get('controller/ajax/assignationAjax.php',{validerEntite:idEntite});
        affichageTableauxAssignations();
    }
}
function changerEtatEntite(idEntite,nouvelEtat){
    $.get('controller/ajax/assignationAjax.php',{idEntite:idEntite, etat: nouvelEtat});
    affichageTableauxAssignations();
}


