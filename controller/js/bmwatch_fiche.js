$(document).ready(function() {
    function enleverSelected(dim){
        $("#boite"+dim).find('option:selected').removeAttr('selected');
    }
    function griser(dim){
        $("#boite"+dim).each(function(){$(this).prop('disabled',true);});
    }
    function degriser(dim){
        $("#boite"+dim).each(function(){$(this).prop('disabled',false);});
    }
    function majInfoBM(idBm){
        $.getJSON("controller/ajax/ficheAjax.php?obtenirInfoBM=true&idBM="+idBm, function (bm) {
            $("#nomBM").val(bm.nom);
            $("#principeBM").val(bm.principe);
            $("#declinaisonBM").val(bm.declinaison);
            $("#variantesBM").val(bm.variantes);
        });
    }
    
    function griserInfoBM(){
        $("#nomBM").prop('disabled',true);
        $("#principeBM").prop('disabled',true);
        $("#declinaisonBM").prop('disabled',true);
        $("#variantesBM").prop('disabled',true);
    }
    
    function degriserInfoBM(){
        $("#nomBM").prop('disabled',false);
        $("#principeBM").prop('disabled',false);
        $("#declinaisonBM").prop('disabled',false);
        $("#variantesBM").prop('disabled',false);
    }
    
    function gestionOcc(){
        var value = $("#profils").val();
        var nbDim = $("#nbDim").val();
        var estProfil = false;
        majInfoBM(value);
        $.ajax({
            type: "GET",
            url: "controller/ajax/ficheAjax.php?estProfil=true&idBM=" + value,
            async: false,
            success: function(data){
                if (data == '1')
                {
                
                    estProfil = true;
                    griserInfoBM();                    
                }
                else{
                    degriserInfoBM();
                }
            }
        });
        
        for(var i =1; i<=nbDim; i++){
            enleverSelected(i);
            if (estProfil) {
                griser(i);
            }
            else{
                degriser(i);
            }
        }
        $.getJSON("controller/ajax/ficheAjax.php?obtenirOcc=true&idBM="+value, function (tabOcc) {
            $.each(tabOcc, function(i, field){
                if ($("#boite"+field[2]+"Occ"+field[1]).length) {
                    $("#boite"+field[2]+"Occ"+field[1]).prop('selected','selected');
                }
                else{
                    $("#boite"+field[2]+"OccAutre").prop('selected','selected');                         
                }
                $("#commentaireboite"+field[2]).val(field[3]);
            });
              
        });
    }
    
    gestionOcc();
    $("#profils").change(function(){
        gestionOcc();
    });
});

