$(document).ready(function() {
    envoie = false;
    jQuery.validator.addMethod("mdpCorrect", function(value, element) {
        return /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,20})/.test(value);
    }, "Password has to contain an upper case, a lower case, a  capital, a numeral and 8 characters.");
   
    jQuery.validator.addMethod("stringCorrect", function(value, element) {
        return /^[,;.:!?)(-_"0-9a-zA-ZÀ-ÿ\s\'-]*$/.test(value);
    }, "String only.");
    
    jQuery.validator.addMethod("intCorrect", function(value, element) {
        return /^[0-9]*$/.test(value);
    }, "Integer only.");
   
    jQuery.validator.addMethod("emailExistePas", function(value, element) {
        var emailBon = false;

        $.ajax({
            type: "GET",
            url: "controller/ajax/verificationsAjax.php?verifierEmail=" + value,
            async: false,
            success: function(data) {
                if (data.match("[1]"))                
                {
                    emailBon = false;
                }
                else
                {
                    emailBon = true;
                }
            }
        });

        return emailBon;
    }, "This email is already used");
   
    jQuery.validator.addMethod("mdpValide", function(value, element) {
        var mdpBon = false;

        $.ajax({
            type: "GET",
            url: "controller/ajax/verificationsAjax.php?verifierMdp=" + value,
            async: false,
            success: function(data) {

                if (data.match("[0]"))
                {
                    mdpBon = false;
                    $(".inputGrise").prop('disabled', true);
                }
                else
                {
                    mdpBon = true;
                    $(".inputGrise").prop('disabled', false);
                }
            }
        });
        return mdpBon;
    }, "Wrong password");

    jQuery.validator.addMethod("nomEntrepriseValide", function(value, element) {
        var nomEntrepriseBon = false;

        $.ajax({
            type: "GET",
            url: "controller/ajax/verificationsAjax.php?verifierNomEntreprise=" + value,
            async: false,
            success: function(data) {

                if (data.match("[1]"))
                {
                    nomEntrepriseBon = false;
                }
                else
                {
                    nomEntrepriseBon = true;
                }
            }
        });
        return nomEntrepriseBon;
    }, "This company already exists");

    jQuery.validator.addMethod("nomProduitValide", function(value, element) {
        var nomProduitBon = false;
        if (envoie == false) {


            $.ajax({
                type: "GET",
                url: "controller/ajax/verificationsAjax.php?verifierNomProduit=" + value,
                async: false,
                success: function(data) {
                    if (data.match("[1]"))
                    {
                        nomProduitBon = false;
                    }
                    else
                    {
                        nomProduitBon = true;
                    }
                }
            });
        }
        else {
            nomProduitBon = true;
        }
        return nomProduitBon;
    }, "This product already exists");

});

   