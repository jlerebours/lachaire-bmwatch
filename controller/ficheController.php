<?php

include dirname(__FILE__) . "/headerController.php";
require_once '../model/EntiteClass.php';
require_once '../model/EntrepriseClass.php';
require_once '../model/ProduitClass.php';
require_once '../model/BMClass.php';
require_once '../model/SecuriteClass.php';
require_once '../model/ToolBoxClass.php';

//regarder si on parle d un produit ou d une entreprise
if($nonConnecte){
    ToolBox::redirige('erreur', 0);
}
if (isset($_GET['idEntite'])) {
    $statut = null;
    $modification = true;
// Initialisations 
    $tabInfosEntite = array();
    $tabInfosEntreprise = array();
    $tabInfosProduits = array();
    $tabInfosBM = array();
    $tabInfosEntiteModif = array();
    $tabInfosModif = array();
    $tabInfosBMModif = array();
    $tabHistoriqueBm = array();

    $id = filter_input(INPUT_GET, "idEntite", FILTER_VALIDATE_INT);
    $cacheEntreprise = "";
    $cacheProduit = "";
//Entreprise
    if (Entite::estUneEntite($id)) {
        $default_formeSociale = null;
        $default_secteurActivite = null;
        $default_technologieUtilisees = null;
        $default_porteur = null;
        $default_ranking = null;
        $default_entreprise = null;
        $default_nomEntreprise = null;
        $default_nomBm = null;
        $default_principe = null;
        $default_declinaison = null;
        $default_variantes = null;
        $default_entiteId = null;
        $BM = new BM('', '', '', '', '', '');
        $entite = new Entite($id);

        if (Entreprise::estUneEntreprise($id)) {
            $entreprise = Entreprise::obtenirEntreprise($id);
            $entite = $entreprise;
            $type = 1;
//Récupération des infos entreprise:
            $default_formeSociale = $entreprise->getFormesSociale();
            $default_secteurActivite = $entreprise->getSecteurActivite();
            $default_technologieUtilisees = $entreprise->getTechnologieUtilisees();
            $porteur = new Utilisateur($entreprise->getPorteur());
            $porteur->getInfos();
            $default_porteur = $porteur->getEmail();
            $default_product = $entreprise->getAllProductEntreprise();

//affiche les info de entreprise
            $cacheProduit = "hidden = true";

            array_push($tabInfosEntreprise, array($voc_formeSociale, $default_formeSociale));
            array_push($tabInfosEntreprise, array($voc_secteurActivite, $default_secteurActivite));
            array_push($tabInfosEntreprise, array($voc_technologiesUtilisees, $default_technologieUtilisees));
            array_push($tabInfosEntreprise, array($voc_porteur, $default_porteur));
        }

//Produit
        else if (Produit::estUnProduit($id)) {
            $produit = Produit::obtenirProduit($id);
            $entite = $produit;
            $type = 2;

//Récupération des info produits
            $default_ranking = $produit->getRanking();
            $default_entreprise = $produit->getEntreprise();
            $default_nomEntreprise = Entreprise::obtenirNomEntreprise($default_entreprise);

//affiche les info de produit
            $cacheEntreprise = "hidden = true";

            array_push($tabInfosProduits, array($voc_rank, $default_ranking));
        } else {
            ToolBox::redirige('erreurController.php', 0);
        }

//Entite
        $default_categorie = $entite->getCategorie();
        $default_nomCategorie = Entite::obtenirCategorie($default_categorie);
        $default_description = $entite->getDescription();
        $default_dateCreation = $entite->getDateCreation();
        $default_dateDerniereModif = $entite->getDateDerniereModif();
        $default_nom = $entite->getNom();
        $default_source = $entite->getSourceBM();


        array_push($tabInfosEntite, array($voc_Nom, $default_nom));
        array_push($tabInfosEntite, array($voc_description, $default_description));
        array_push($tabInfosEntite, array($voc_categorie, $default_nomCategorie));
        array_push($tabInfosEntite, array($voc_dateCreation, $default_dateCreation));
        array_push($tabInfosEntite, array($voc_dateModification, $default_dateDerniereModif));

//Critere
        $tabCriteres = $entite->obtenirCriteresFixees();
        if ($tabCriteres == null) {
            $tabCriteres = $entite->obtenirCriteres();
            foreach ($tabCriteres as $critere) {
                array_push( $critere,array("",""));
            }
        }

//Bm
        if ($entite->possedeBM()) {
            $BM = $entite->obtenirBM();
            $default_nomBm = $BM->getNom();
            $default_principe = $BM->getPrincipe();
            $default_declinaison = $BM->getDeclinaison();
            $default_variantes = $BM->getVariantes();
            if ($BM->estProfil()) {
                $default_BMAutre = null;
            } else {
                $default_BMAutre = $BM->getId();
            }
        } else {
            $default_BMAutre = null;
        }
        $tabProfil = BM::obtenirProfil();
        $tabDimension = BM::obtenirDimension();

// AJOUTER LES DIMENSIONS AU TABLEAU ICI !
        array_push($tabInfosBM, array($voc_BMNom, $default_nomBm));
        array_push($tabInfosBM, array($voc_BMPrincipe, $default_principe));
        array_push($tabInfosBM, array($voc_BMDeclinaison, $default_declinaison));
        array_push($tabInfosBM, array($voc_BMVariante, $default_variantes));

// Pour le modifier
        if ((isset($_GET['visualisation']) && $_GET['visualisation'] == "validationModification" ) || ( isset($_GET['modification']) && $_GET['modification'] == "validationModification")) {
            $default_formeSocialeModifier = null;
            $default_secteurActiviteModifier = null;
            $default_technologieUtiliseesModifier = null;
            $default_rankingModifier = null;
            if (Entreprise::estUneEntreprise($id)) {
                $entrepriseModifier = Entreprise::obtenirEntrepriseModifier($id);
                $entiteModifier = $entrepriseModifier;

//Récupération des infos entreprise:
                $default_formeSocialeModifier = $entrepriseModifier->getFormesSociale();
                $default_secteurActiviteModifier = $entrepriseModifier->getSecteurActivite();
                $default_technologieUtiliseesModifier = $entrepriseModifier->getTechnologieUtilisees();
            }

//Produit
            else if (Produit::estUnProduit($id)) {
                $produitModifier = Produit::obtenirProduitModifier($id);
                $entiteModifier = $produitModifier;

//Récupération des info produits
            }
            
            $utilisateurModif = new Utilisateur($entiteModifier->getIdModif());
            $utilisateurModif->getInfos();
            $default_emailModif = $utilisateurModif->getEmail();
            $default_categorieModifier = $entiteModifier->getCategorie();
            $default_nomCategorieModifier = Entite::obtenirCategorie($default_categorieModifier);
            $default_descriptionModifier = $entiteModifier->getDescription();
            $default_dateDerniereModifier = $entiteModifier->getDateDerniereModif();
            $default_nomModifier = $entiteModifier->getNom();
            array_push($tabInfosModif, array($voc_Nom, $default_nom, $default_nomModifier, $default_nomModifier));
            array_push($tabInfosModif, array($voc_description, $default_description, $default_descriptionModifier, $default_descriptionModifier));
            array_push($tabInfosModif, array($voc_categorie, $default_nomCategorie, $default_nomCategorieModifier, $default_categorieModifier));
            array_push($tabInfosModif, array($voc_formeSociale, $default_formeSociale, $default_formeSocialeModifier, $default_formeSocialeModifier));
            array_push($tabInfosModif, array($voc_secteurActivite, $default_secteurActivite, $default_secteurActiviteModifier, $default_secteurActiviteModifier));
            array_push($tabInfosModif, array($voc_technologiesUtilisees, $default_technologieUtilisees, $default_technologieUtiliseesModifier, $default_technologieUtiliseesModifier));
        }

//Si action de visualisation : 
        if (isset($_GET['visualisation'])) {
            $submit = null;
            $submitModifierValider = null;
            $submitARefaire = null;
            $statut = null;
            $griser = false;
            
            $buttonSubmitARefaire = $voc_buttonSubmitARefaire;
            $buttonSubmitModifierValider = $voc_buttonSubmitModifierValider;
            $buttonSubmitModifierEnvoyer = $voc_buttonSubmitModifierEnvoyer;
            $buttonSubmitValider = $voc_buttonSubmitValider;
            $buttonSubmitEnvoyer = $voc_buttonSubmitEnvoyer;
            $buttonSubmit = $voc_buttonSubmit;
            $buttonSubmitAjouterProduit = $voc_buttonSubmitAjouterProduit;

            $entite = new Entite($id);
            $entite->getInfos();
            $type = $entite->getType();
            
// Gestion des actions des boutons
            if ($_GET['visualisation'] == "normale") {
                $submit = "n";
            } elseif ($_GET['visualisation'] == "moderateur" && ($droitsAdmin || $droitsModo)) {
                $submit = "m";
                $submitARefaire = "arfm";
                $statut = "modo";
            } elseif ($_GET['visualisation'] == "nonPorteur" && $droitsPorteur) {
                $submit = "np";
                $submitModifierValider = "enp";
                $statut = "nonPorteur";
            } elseif ($_GET['visualisation'] == "validationModification" && ($droitsAdmin || $droitsModo)) {
                $submit = "vm";
                $submitModifierValider = "vm";
                $statut = "validation";
            } elseif ($_GET['visualisation'] == "validation" && ($droitsAdmin || $droitsModo)) {
                $submit = "v";
                $submitModifierValider = "vv";
                $statut = "validation";
            }elseif ($_GET['visualisation'] == "porteur" && $droitsPorteur) {
                $submit = "p";                
                $statut = "porteur";
                if($entite->getEtat()!=1)
                    $griser=true;
            }
            


// Inclusion de la bonne vue
            if ($_GET['visualisation'] != "validationModification") {
                include dirname(__FILE__) . '/../view/visualisationEntiteView.php';
            } else {
                include dirname(__FILE__) . '/../view/visualisationEntiteModifieeView.php';
            }
            exit();
        }

//Si action de modification : 
        elseif (isset($_GET['modification'])) {
            $direct = false;
            $buttonSubmit = $voc_buttonSubmit;
            $statut = null;
            $commentaire = false;
            
// Gestion des actions des boutons
            if ($_GET['modification'] == "normale") {
                $submit = "n";
            }elseif ($_GET['modification'] == "moderateur" && ($droitsAdmin || $droitsModo)) {
                $statut = "modo";
                $submit = "m";
                $submitValider = "vmr";
                $buttonSubmitValider = $voc_buttonSubmitModifierValider;
            }elseif ($_GET['modification'] == "validationModerateur" && ($droitsAdmin || $droitsModo)) {
                $buttonSubmit = $voc_buttonSubmitModifierValider;
                $submit = "vmr";
            }elseif ($_GET['modification'] == "nonPorteur" && $droitsNonPorteur) {
                $submit = "np";
            }elseif ($_GET['modification'] == "envoieNonPorteur" && $droitsNonPorteur) {
                $buttonSubmit = $voc_buttonSubmitModifierEnvoyer;
                $submit = "enp";
            }elseif ($_GET['modification'] == "nonPorteurDirect" && $droitsNonPorteur) {
                $submit = "np";
                $buttonSubmitEnvoyer = $voc_buttonSubmitModifierEnvoyer;
                $submitEnvoyer = "envoieNonPorteur";
                $direct = true;
            }elseif ($_GET['modification'] == "validation" && ($droitsAdmin || $droitsModo)) {
                $submit = "v";
            }elseif ($_GET['modification'] == "validationValidation" && ($droitsAdmin || $droitsModo)) {
                $submit = "vv";
                $buttonSubmit = $voc_buttonSubmitModifierValider;
            }elseif ($_GET['modification'] == "validationModification" && ($droitsAdmin || $droitsModo)) {
                $submit = "vm";
                $buttonSubmit = $voc_buttonSubmitModifierValider;
            }elseif ($_GET['modification'] == "porteur" && $droitsPorteur) {
                $submit = "p";
                $commentaire = true;
            } else
            ToolBox::redirige('erreur', 0);
            
// Inclusion de la bonne vue
            if ($_GET['modification'] != "validationModification") {

                include '../view/modificationEntiteView.php';
            } else {
                include '../view/modificationEntiteModifieeView.php';
            }
        }
//Si action de modification : 
        else if (isset($_GET['validation']) && ($droitsAdmin || $droitsModo)) {

            if ($_GET['validation'] == "moderateur" || $_GET['validation'] == "validation") {
                $entite->setEtat(1);
            } elseif ($_GET['validation'] == "nonPorteur") {
                $entite->setEtat(4);
            }
            $type = $entite->getType();
            switch ($type) {
                case 1 :
                    $entite->modifierEntreprise();
                    break;
                case 2 :
                    $entite->modifierProduit();
                    break;
                default :
                    ToolBox::redirige('erreur', 0);
            }
            if ($_GET['validation'] == "moderateur") {
                $entite->supprimerAssignation();
                ToolBox::redirige("admin-assignations", 0);
            } elseif ($_GET['validation'] == "nonPorteur") {
                ToolBox::redirige("mes-assignations", 0);
            } elseif ($_GET['validation'] == "validation") {


                ToolBox::redirige("admin-gestion-entites", 0);
            }
        } else
            ToolBox::redirige('erreur', 0);
    }else
        ToolBox::redirige('erreur', 0);
}else if (isset($_POST) ) {
    $idEntite = filter_input(INPUT_POST, "idEntite", FILTER_VALIDATE_INT);
    $message = SecuriteClass::securite_vide($idEntite);
    $nom = filter_input(INPUT_POST, "nomEntite", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($nom, 0, 30);
    $message = SecuriteClass::securite_vide($nom);

    $categorie = filter_input(INPUT_POST, "categorie", FILTER_VALIDATE_INT);
    $description = filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($description, 0, 250);

    $formeSociale = filter_input(INPUT_POST, "formeSociale", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($formeSociale, 0, 50);
    $secteurActivite = filter_input(INPUT_POST, "secteurActivite", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($secteurActivite, 0, 50);
    $technologieUtilisees = filter_input(INPUT_POST, "technologieUtilisees", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($technologieUtilisees, 0, 50);

    $ranking = filter_input(INPUT_POST, "ranking", FILTER_VALIDATE_INT);
    $nomEntreprise = filter_input(INPUT_POST, "nomEntreprise", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($nomEntreprise, 0, 30);

    $nbCriteres = filter_input(INPUT_POST, "nbCriteres", FILTER_VALIDATE_INT);
    $tabCriteres = array();
    for ($i = 1; $i <= $nbCriteres; $i++) {

        $typeCritere = filter_input(INPUT_POST, "typeCritere" . $i, FILTER_SANITIZE_STRING);

        if ($typeCritere == "NUMERIQUE" || $typeCritere == "CLEF") {
            $tabCriteres[$i][1] = filter_input(INPUT_POST, "valeurCritere" . $i, FILTER_VALIDATE_INT);
            $tabCriteres[$i][2] = filter_input(INPUT_POST, "critere" . $i, FILTER_SANITIZE_STRING);
        } else {
            $tabCriteres[$i][1] = filter_input(INPUT_POST, "critere" . $i, FILTER_VALIDATE_INT);
            $tabCriteres[$i][2] = Entite::obtenirValeurCritere($tabCriteres[$i][1]);
        }
        $message = SecuriteClass::securite_string($tabCriteres[$i][2], 0, 50);
    }

    $profil = filter_input(INPUT_POST, "profils", FILTER_VALIDATE_INT);
    $idAncienBM = filter_input(INPUT_POST, "idAncienBm", FILTER_VALIDATE_INT);

    $nomBM = filter_input(INPUT_POST, "nomBM", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($nomBM, 0, 30);
    $principeBM = filter_input(INPUT_POST, "principeBM", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($principeBM, 0, 50);
    $declinaisonBM = filter_input(INPUT_POST, "declinaisonBM", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($declinaisonBM, 0, 50);
    $vairantesBM = filter_input(INPUT_POST, "variantesBM", FILTER_SANITIZE_STRING);
    $message = SecuriteClass::securite_string($vairantesBM, 0, 50);

    $nbDim = filter_input(INPUT_POST, "nbDim", FILTER_VALIDATE_INT);
    $tabDimensions = array();
    for ($i = 1; $i <= $nbDim; $i++) {
        $tabDimensions[$i][1] = $i;
        $tabDimensions[$i][2] = filter_input(INPUT_POST, "boite" . $i, FILTER_SANITIZE_STRING);
        $tabDimensions[$i][3] = filter_input(INPUT_POST, "commentaireboite" . $i, FILTER_SANITIZE_STRING);
        if ($tabDimensions[$i][2] == "autre") {
            $tabDimensions[$i][2] = null;
        }
    }
    $tabInfo = array();
//pour entreprise modifier
    $tabInfo[0] = filter_input(INPUT_POST, "nomEntiteModifier", FILTER_SANITIZE_STRING);
    $tabInfo[1] = filter_input(INPUT_POST, "descriptionModifier", FILTER_SANITIZE_STRING);
    $tabInfo[2] = filter_input(INPUT_POST, "categorieModifier", FILTER_VALIDATE_INT);
    $tabInfo[3] = filter_input(INPUT_POST, "formeSocialeModifier", FILTER_SANITIZE_STRING);
    $tabInfo[4] = filter_input(INPUT_POST, "secteurActiviteModifier", FILTER_SANITIZE_STRING);
    $tabInfo[5] = filter_input(INPUT_POST, "technologieUtiliseesModifier", FILTER_SANITIZE_STRING);


    $nbInfo = filter_input(INPUT_POST, "nbInfo", FILTER_VALIDATE_INT);
    for ($i = 0; $i < $nbInfo; $i++) {
        $tabInfo[$i] = filter_input(INPUT_POST, "info" . $i, FILTER_SANITIZE_STRING);
    }

    $commentaire = filter_input(INPUT_POST, "commentaire", FILTER_SANITIZE_STRING);


    if ($message == null) {
        $entite = null;
        if (isset($_POST['n'])) {
            $etat = 5;
            $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
            $idUtilisateurQuiAModifier = $utilisateurCourant->getId();
            if (Entreprise::estUneEntreprise($idEntite)) {
                $entreprise = new Entreprise($idEntite, "", $categorie, $etat, $description, "", "", $nom, $formeSociale, $secteurActivite, $technologieUtilisees,"",$idUtilisateurQuiAModifier);
                $entreprise->ajouterEntrepriseModifier();
            } elseif (Produit::estUnProduit($idEntite)) {
                $produit = new Produit($idEntite, "", $categorie, $etat, $description, "", "", $nom, $ranking,"",$idUtilisateurQuiAModifier);
                $produit->ajouterProduitModifier();
            } else {
                ToolBox::redirige('erreur', 0);
                exit();
            }
            ToolBox::redirige("accueil", 0);
        } elseif (isset($_POST['arfm'])&& ($droitsAdmin || $droitsModo)) {
            $etat = 3;

            if (Entreprise::estUneEntreprise($idEntite)) {
                $entreprise = new Entreprise($idEntite);
                $entreprise->changerEtatEntreprise($etat);
                $entite = $entreprise;
            } elseif (Produit::estUnProduit($idEntite)) {
                $produit = new Produit($idEntite);
                $produit->changerEtatProduit($etat);
                $entite = $produit;
            } else {
                ToolBox::redirige('erreur', 0);
                exit();
            }

            $nonPorteur = new Utilisateur($entite->getNonPorteurAssigne());
            $nonPorteur->getInfos();
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
            ToolBox::envoiModifAssignation($nonPorteur,$entite, $commentaire);
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////
            ToolBox::redirige("admin-assignations", 0);
        } elseif (isset($_POST['vm'])&& ($droitsAdmin || $droitsModo)) {
            $etat = 1;

            if (Entreprise::estUneEntreprise($idEntite)) {
                $entreprise = new Entreprise($idEntite, "", $tabInfo[2], $etat, $tabInfo[1], "", "", $tabInfo[0], $tabInfo[3], $tabInfo[4], $tabInfo[5]);
                $entreprise->modifierEntreprise();
                $entreprise->supprimerEntrepriseModif();
            } elseif (Produit::estUnProduit($idEntite)) {

                $produit = new Produit($idEntite, "", $tabInfo[2], $etat, $tabInfo[1], "", "", $tabInfo[0]);
                $produit->modifierProduit();
                $produit->supprimerProduitModif();
            } else {
                ToolBox::redirige('erreur', 0);
                exit();
            }
            ToolBox::redirige("admin-gestion-entites", 0);
        } elseif (((isset($_POST['vmr']) || isset($_POST['m']) || isset($_POST['v']) || isset($_POST['vv']) ) && ($droitsAdmin || $droitsModo) ) || ((isset($_POST['np']) || isset($_POST['enp']) ) && $droitsNonPorteur) || (isset($_POST['p'])&& $droitsPorteur)) {

            if (isset($_POST['vmr']) || isset($_POST['vv'])) {
                $etat = 1;
            } elseif (isset($_POST['m']) || isset($_POST['np'])) {
                $etat = 3;
            } elseif (isset($_POST['enp'])) {
                $etat = 4;
            } elseif (isset($_POST['v'])||isset($_POST['p'])) {
                $etat = 2;
            }

            if (Entreprise::estUneEntreprise($idEntite)) {
                $entreprise = new Entreprise($idEntite, "", $categorie, $etat, $description, "", "", $nom, $formeSociale, $secteurActivite, $technologieUtilisees);
                $entreprise->modifierEntreprise();
                $entite = $entreprise;
                $type = 1;
            } elseif (Produit::estUnProduit($idEntite)) {
                $produit = new Produit($idEntite, "", $categorie, $etat, $description, "", "", $nom, $ranking);
                $produit->modifierProduit();
                $entite = $produit;
                $type = 2;
            } else {
                ToolBox::redirige('erreur', 0);
                exit();
            }
            $entite->modifierCritere($tabCriteres);
            if (isset($_POST['vmr'])) {
                $entite->supprimerAssignation();
            }
            $bm = new BM($profil, $nomBM, "", $principeBM, $declinaisonBM, $vairantesBM);
            if(!isset($_POST['p'])){
                if ($idAncienBM != "") {
                    $ancienBm = new BM($idAncienBM);
                    if ($ancienBm->estProfil()) {
                        if ($bm->estProfil()) {
                            $entite->modifierBM($profil);
                        } else {
                            $bm->setProfil(0);
                            $bm->ajouterALaBD();

                            $entite->modifierBM($bm->getId());

                            for ($i = 1; $i <= $nbDim; $i++) {
                                $bm->fixerOccurence($tabDimensions[$i][2],$tabDimensions[$i][3]);
                            }
                        }
                    } else {
                        if ($bm->estProfil()) {
                            $entite->modifierBM($profil);

                            $ancienBm->supRelation();
                            $ancienBm->supBM();
                        } else {
                            $bm->setProfil(0);
                            $bm->updateBM();
                            $bm->supRelation();

                            $entite->modifierBM($bm->getId());

                            for ($i = 1; $i <= $nbDim; $i++) {
                                $bm->fixerOccurence($tabDimensions[$i][2],$tabDimensions[$i][3]);
                            }
                        }
                    }
                }
            }else{
                if ($bm->estProfil()) {
                    $entite->ajouterBM($profil,$commentaire);
                } else {
                    $bm->setProfil(0);
                    $bm->ajouterALaBD();

                    $entite->ajouterBM($bm->getId(),$commentaire);

                    for ($i = 1; $i <= $nbDim; $i++) {
                        $bm->fixerOccurence($tabDimensions[$i][2],$tabDimensions[$i][3]);
                    }
                }
            }
            if (isset($_POST['m']) || isset($_POST['vmr'])) {
                ToolBox::redirige("admin-assignations", 0);
            } elseif (isset($_POST['np']) || isset($_POST['enp'])) {
                ToolBox::redirige("mes-assignations", 0);
            } elseif (isset($_POST['v']) || isset($_POST['vv'])) {
                ToolBox::redirige("admin-gestion-entites", 0);
            }elseif (isset($_POST['p'])) {
                ToolBox::redirige("accueil", 0);
            }
        }else
            ToolBox::redirige('erreur', 0);
    } else {
        ToolBox::redirige('erreur', 0);
    }
}
