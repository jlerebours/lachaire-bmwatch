<?php

session_start();
include_once 'verificationLangueController.php';
include_once '../view/libView.php';
require_once '../model/EntiteClass.php';
require_once '../model/EntrepriseClass.php';
require_once '../model/ProduitClass.php';
require_once '../model/BMClass.php';
require_once '../model/SecuriteClass.php';
require_once '../model/ToolBoxClass.php';

if(!$nonConnecte){
    if (isset($_POST['nomFormulaire'])) {
        if ($_POST['nomFormulaire'] == "inscriptionPorteur") {
            //On sécurise les données
            $message = null;

            ////////////////////////////Utilisateur

            $identifiantSocial = filter_input(INPUT_POST, "identifiantSocial", FILTER_SANITIZE_STRING);
            // $message = SecuriteClass::securite_identifiantSociale($identifiantSocial, 0, 30);

            $passwordUtilisateur = filter_input(INPUT_POST, "mdp", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_mdp($passwordUtilisateur, 8);
            $message = SecuriteClass::securite_vide($passwordUtilisateur);


            $nomUtilisateur = filter_input(INPUT_POST, "nomUtilisateur", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($nomUtilisateur, 0, 30);

            $emailUtilisateur = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
            $message = SecuriteClass::securite_email($emailUtilisateur);
            $message = SecuriteClass::securite_vide($emailUtilisateur);

            $prenomUtilisateur = filter_input(INPUT_POST, "prenom", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($prenomUtilisateur, 0, 30);

            $typeUtilisateur = array(3);

            //Si tout est bon on peut l ajouter a la basse de donnée
            if ($message == null) {
                $utilisateur = new Utilisateur('', $nomUtilisateur, $prenomUtilisateur, $emailUtilisateur, $passwordUtilisateur, $typeUtilisateur, null, 2);
                $utilisateur->ajouterALaBD();
                ToolBox::envoiMailAttenteNonPorteur($utilisateur);
                $utilisateur->emailExiste();

                if($identifiantSocial != null){
                   $utilisateur->ajouterReseauSociale($identifiantSocial);
                   $_SESSION['identifiantSocial'] = serialize($identifiantSocial);
                }

            } else {
                echo $message;
                exit();
            }

            ////////////////////////////Entreprise

            $categorieEntreprise = filter_input(INPUT_POST, "categorieEntreprise", FILTER_VALIDATE_INT);

            $descriptionEntreprise = filter_input(INPUT_POST, "descriptionEntreprise", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($descriptionEntreprise, 0, 250);

            $sourceEntreprise = filter_input(INPUT_POST, "lienEntreprise", FILTER_VALIDATE_URL);     

            $nomEntreprise = filter_input(INPUT_POST, "nomEntreprise", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($nomEntreprise, 0, 30);
            $message = SecuriteClass::securite_vide($nomEntreprise);

            $formeSociale = filter_input(INPUT_POST, "formeSociale", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($formeSociale, 0, 50);

            $secteurActivite = filter_input(INPUT_POST, "secteurActivite", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($secteurActivite, 0, 50);

            $technologiesUtilisees = filter_input(INPUT_POST, "technologiesUtilisees", FILTER_SANITIZE_STRING);
            $message = SecuriteClass::securite_string($technologiesUtilisees, 0, 50);

            $nbCriteres = filter_input(INPUT_POST, "nbCriteres", FILTER_VALIDATE_INT);
            $tabCriteres = array();
            for ($i = 1; $i <= $nbCriteres; $i++) {

                $typeCritere = filter_input(INPUT_POST, "typeCritere" . $i, FILTER_SANITIZE_STRING);
                if($typeCritere!=""){
                    if ($typeCritere == "NUMERIQUE" || $typeCritere == "CLEF") {
                        $valeurCritere = filter_input(INPUT_POST, "valeurCritere" . $i, FILTER_VALIDATE_INT);
                        $choixCritere = filter_input(INPUT_POST, "critere" . $i, FILTER_SANITIZE_STRING);
                    } else {
                        $valeurCritere = filter_input(INPUT_POST, "critere" . $i, FILTER_VALIDATE_INT);
                        $choixCritere = Entite::obtenirValeurCritere($valeurCritere);
                    }
                    $ligneCritere = [$valeurCritere, $choixCritere];
                    array_push($tabCriteres, $ligneCritere);
                    $message = SecuriteClass::securite_string($choixCritere, 0, 50);
                }
            }


            if ($message == null) {

                $utilisateurId = $utilisateur->getId();
                $etat = 2;
                $entreprise = new Entreprise('', $utilisateurId, $categorieEntreprise, $etat, $descriptionEntreprise, null, null, $nomEntreprise, $formeSociale, $secteurActivite, $technologiesUtilisees,$sourceEntreprise);
                $entreprise->ajouterALaBDEntreprise();
                $entreprise->fixerCriteres($tabCriteres);
                $_SESSION['donneesEntreprise'] = serialize($entreprise);
            } else {
                echo $message;
                exit();
            }

            ////////////////////////////BM Entreprise
            $profilEntreprise = filter_input(INPUT_POST, "profils", FILTER_SANITIZE_STRING);

            if ($profilEntreprise != "autre") {
                $entreprise->ajouterBM($profilEntreprise, "premier BM");
            } else {

                $nomBMEntreprise = filter_input(INPUT_POST, "nomBm", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($nomBMEntreprise, 0, 50);

                $principeBMEntreprise = filter_input(INPUT_POST, "principeBm", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($principeBMEntreprise, 0, 50);

                $declinaisonBMEntreprise = filter_input(INPUT_POST, "declinaisonsBm", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($declinaisonBMEntreprise, 0, 50);

                $variantesBMEntreprise = filter_input(INPUT_POST, "variantesBm", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($variantesBMEntreprise, 0, 50);

                if ($message == null) {
                    $profil = 0;
                    $bmEntreprise = new BM('', $nomBMEntreprise, $profil, $principeBMEntreprise, $declinaisonBMEntreprise, $variantesBMEntreprise);
                    $bmEntreprise->ajouterALaBD();
                    $idBm = $bmEntreprise->getId();
                    $entreprise->ajouterBM($idBm, "premier BM");
                } else {
                    echo $message;
                    exit();
                }


                $nbDimensionsEntreprise = filter_input(INPUT_POST, "nbDimension", FILTER_VALIDATE_INT);
                $tabDimensionsEntreprise = array();
                for ($i = 1; $i <= $nbDimensionsEntreprise; $i++) {
                    $tabDimensionsEntreprise[$i][1] = $i;
                    $tabDimensionsEntreprise[$i][2] = filter_input(INPUT_POST, "boite" . $i, FILTER_SANITIZE_STRING);
                    $commentaire = filter_input(INPUT_POST, "commentaireboite" . $i, FILTER_SANITIZE_STRING);
                    $message = SecuriteClass::securite_string($commentaire, 0, 250);
                    if ($message == null) {
                        $tabDimensionsEntreprise[$i][3] = $commentaire;

                    } else {
                        echo $message;
                        exit();
                    }
                    if ($tabDimensionsEntreprise[$i][2] == "autre") {
                        $occurence = filter_input(INPUT_POST, "Selectedboite" . $i, FILTER_SANITIZE_STRING);
                        $description = filter_input(INPUT_POST, "descriptionSelectedboite" . $i, FILTER_SANITIZE_STRING);
                        $message = SecuriteClass::securite_string($declinaisonBMEntreprise, 0, 50);
                        $message = SecuriteClass::securite_string($description, 0, 250);
                        if ($message == null) {
                            $tabDimensionsEntreprise[$i][2] = BM::ajouterOccurence($i, $occurence, $description);
                        } else {
                            echo $message;
                            exit();
                        }
                    }
                }
                if ($message == null) {
                    for ($i = 1; $i <= $nbDimensionsEntreprise; $i++) {
                        if ($tabDimensionsEntreprise[$i][2] != null) {
                            $bmEntreprise->fixerOccurence($tabDimensionsEntreprise[$i][2],$tabDimensionsEntreprise[$i][3]);
                        }
                    }
                } else {
                    echo $message;
                    exit();
                }
            }

            ////////////////////////////Produit
            $estAfficherProduit = filter_input(INPUT_POST, "afficherProduit", FILTER_VALIDATE_INT);
            if ($estAfficherProduit) {
                $categorieProduit = filter_input(INPUT_POST, "categorieProduit", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($categorieProduit, 0, 50);

                $descriptionProduit = filter_input(INPUT_POST, "descriptionProduit", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($descriptionProduit, 0, 250);

                $nomProduit = filter_input(INPUT_POST, "nomProduit", FILTER_SANITIZE_STRING);
                $message = SecuriteClass::securite_string($nomProduit, 0, 30);

                $sourceProduit = filter_input(INPUT_POST, "lienProduit", FILTER_VALIDATE_URL);

                $ligneCritereProduit=array();
                $tabCriteresProduit = array();
                for ($i = 1; $i <= $nbCriteres; $i++) {

                    $typeCritereProduit = filter_input(INPUT_POST, "typeCritereProduit" . $i, FILTER_SANITIZE_STRING);
                    if ($typeCritereProduit != "") {
                        if ($typeCritereProduit == "NUMERIQUE" || $typeCritereProduit == "CLEF") {
                            $valeurCritereProduit = filter_input(INPUT_POST, "valeurCritereProduit" . $i, FILTER_VALIDATE_INT);
                            $choixCritereProduit = filter_input(INPUT_POST, "critereProduit" . $i, FILTER_SANITIZE_STRING);
                        } else {
                            $valeurCritereProduit = filter_input(INPUT_POST, "critereProduit" . $i, FILTER_VALIDATE_INT);
                            $choixCritereProduit = Entite::obtenirValeurCritere($valeurCritereProduit);
                        }

                        $ligneCritereProduit = [$valeurCritereProduit,$choixCritereProduit];
                        array_push($tabCriteresProduit, $ligneCritereProduit);
                        $message = SecuriteClass::securite_string($choixCritereProduit, 0, 50);
                    }
                }

                if ($message == null) {
                    $entrepriseId = $entreprise->getId();
                    $produit = new Produit(null, $entrepriseId, $categorieProduit, $etat, $descriptionProduit, null, null, $nomProduit, null, $sourceProduit);
                    $produit->ajouterALaBDProduit();
                    $produit->fixerCriteres($tabCriteresProduit);
                    $_SESSION['donneesProduit'] = serialize($produit);
                } else {
                    echo $message;
                    exit();
                }


                ////////////////////////////BM Produit

                $profilProduit = filter_input(INPUT_POST, "profilsProduit", FILTER_SANITIZE_STRING);
                if ($profilProduit != "autre") {
                    $produit->ajouterBM($profilProduit, "premier BM");
                } else {

                    $nomBMProduit = filter_input(INPUT_POST, "nomBmProduit", FILTER_SANITIZE_STRING);
                    $message = SecuriteClass::securite_string($nomBMProduit, 0, 50);

                    $principeBMProduit = filter_input(INPUT_POST, "principeBmProduit", FILTER_SANITIZE_STRING);
                    $message = SecuriteClass::securite_string($principeBMProduit, 0, 50);

                    $declinaisonBMProduit = filter_input(INPUT_POST, "declinaisonsBmProduit", FILTER_SANITIZE_STRING);
                    $message = SecuriteClass::securite_string($declinaisonBMProduit, 0, 50);

                    $variantesBMProduit = filter_input(INPUT_POST, "variantesBmProduit", FILTER_SANITIZE_STRING);
                    $message = SecuriteClass::securite_string($variantesBMProduit, 0, 50);

                    if ($message == null) {
                        $profil = 0;
                        $bmProduit = new BM('', $nomBMProduit, $profilProduit, $principeBMProduit, $declinaisonBMProduit, $variantesBMProduit);
                        $bmProduit->ajouterALaBD();
                        $idBm = $bmProduit->getId();
                        $produit->ajouterBM($idBm, "premier BM");
                    } else {
                        echo $message;
                        exit();
                    }


                    $nbDimensionsProduit = filter_input(INPUT_POST, "nbDimensionProduit", FILTER_VALIDATE_INT);
                    $tabDimensionsProduit = array();
                    for ($i = 1; $i <= $nbDimensionsProduit; $i++) {
                        $tabDimensionsProduit[$i][1] = $i;
                        $tabDimensionsProduit[$i][2] = filter_input(INPUT_POST, "boite" . $i . "Produit", FILTER_SANITIZE_STRING);
                        $tabDimensionsProduit[$i][3] = filter_input(INPUT_POST, "commentaireboite" . $i . "Produit", FILTER_SANITIZE_STRING);
                        if ($tabDimensionsProduit[$i][2] == "autre") {
                            $occurence = filter_input(INPUT_POST, "SelectedboiteProduit" . $i, FILTER_SANITIZE_STRING);
                            $message = SecuriteClass::securite_string($declinaisonBMProduit, 0, 50);
                            if ($message == null) {
                                $tabDimensionsProduit[$i][2] = BM::ajouterOccurence($i, $occurence);
                            } else {
                                echo $message;
                                exit();
                            }
                        }
                    }

                    if ($message == null) {
                        for ($i = 1; $i <= $nbDimensionsProduit; $i++) {
                            if ($tabDimensionsProduit[$i][2] != null) {
                                $bmProduit->fixerOccurence($tabDimensionsProduit[$i][2],$tabDimensionsProduit[$i][3]);
                            }
                        }
                    } else {
                        echo $message;
                        exit();
                    }
                }
            }
            ToolBox::redirige("accueil", 0);
        } else
            ToolBox::redirige("erreur", 0);
    }
    else {
        if (!isset($sociale))
            $sociale = null;
        include '../view/inscriptionView.php';
    }
}else{
    ToolBox::redirige("erreur", 0);
}