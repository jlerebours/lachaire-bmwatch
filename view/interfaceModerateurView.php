<?php
    session_start();
    require_once '../model/UtilisateurClass.php';
    ToolBox::verifierDroitsEtRedirigerSiPasOk(2,"../view/accueilView.php",0);
?>
 <html>
    <head>
        <title><?=$moderateurInterface?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
            echo "<a href=\"./gestionUtilisateurView.php?type=3\"> Gestion porteurs</a><br/>";
            echo "<a href=\"./gestionUtilisateurView.php?type=4\"> Gestion non porteurs</a><br/>";
            echo "<a href=\"./assignationNonPorteurView.php\"> Assignation non porteurs</a><br/>";
        ?>
</body>
</html>
