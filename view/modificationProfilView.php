<html>
<head>
</head>
<body>
    <script src="controller/js/bmwatch_addSociale.js"></script>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1><?=$voc_profilUtilisateur?></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="fa fa-user"></i>
                                    <?=$voc_modifierProfil?>
                                </h3>
                            </div>
                            <div class="box-content">
                                <form action="formulaire-profil" method="POST" id="formModificationProfil" class='form-horizontal form-striped form-validate'>
                                    <div class="form-group text_style">
                                        <label><?=$voc_entrerMdp?></label>
                                    </div>
                                    <div class="form-group">
                                        <label for="motDePasseActuel" class="control-label col-sm-2 right"><?=$voc_mdpActuel?> :</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="motDePasseActuel" id="motDePasseActuel" class='form-control' data-rule-mdpValide="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nom" class="control-label col-sm-2 right"><?=$voc_nom?> : </label>
                                        <div class="col-sm-10">
                                            <input type="text" id="nom" disabled=true name="nom" class="form-control inputGrise" value="<?=$nomDefault?>" data-rule-required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="prenom" class="control-label col-sm-2 right"><?=$voc_prenom?> : </label>
                                        <div class="col-sm-10">
                                            <input type="text" id="prenom" disabled=true name="prenom" class='form-control inputGrise' data-rule-required="true" value=<?=$prenomDefault?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="control-label col-sm-2 right "><?=$voc_email?> : </label>
                                        <div class="col-sm-10">
                                            <input type="text" id="email" disabled=true  name="email" class='form-control inputGrise' data-rule-required="true" data-rule-email="true" value=<?=$emailDefault?>>
                                        </div>
                                        <label for="email" class="control-label col-sm-2 right "><?=$voc_repeterEmail?> : </label>
                                        <div class="col-sm-10">
                                            <input type="text" id="email2" disabled=true  name="email2" class='form-control inputGrise' data-rule-equalTo="#email" value=<?=$emailDefault?>>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="pw" class="control-label col-sm-2 right"><?=$voc_nouveauMdp?> :</label>
                                        <div class="col-sm-10">
                                            <input type="password" id="nouveauMdp" name="nouveauMdp" disabled=true class='form-control inputGrise'>
                                        </div>
                                        <label for="pw" class="control-label col-sm-2 right"><?=$voc_repeterNouveauMdp?> :</label>
                                        <div class="col-sm-10">
                                            <input type="password" id="nouveauMdp2" name="nouveauMdp2" disabled=true data-rule-equalTo="#nouveauMdp" class='form-control inputGrise'>
                                        </div>
                                    </div>
                                    
                                    <div class="form-actions">
                                        <input type="submit" disabled="true" name="modifierCompte" class='btn btn-green inputGrise' value="<?=$voc_enregistrer?>">                                        
                                        <input type="reset" disabled="true" class='btn inputGrise' value="Discard changes">
                                    </div>                                        
                                </form> 
                                <button href="#modal" disabled="true" class=" btn btn-blue inputGrise " data-toggle="modal" style="width : 230px" ><?=$voc_lierReseauSocial?></button>
                            </div> 
                        </div>
                    </div>
                    <!--Fenetre modale pour connexion sociale-->            
                    <div id="modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span><span class="sr-only"><?=$voc_fermer?></span></button>
                                    <h4 class="modal-title"><?=$voc_reseauSocial?></h4>
                                </div>
                                <form action="" method="POST" id="formSocialPopup" class='form-horizontal form-striped form-validate'>
                                    <div class="form-group">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <center>
                                                    <div id="janrainEngageEmbed"></div>                                   
                                                </center>   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?=$voc_fermer?>">
                                    </div>
                                </form>
                                <!-- /.modal-footer -->
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    
</body>

</html>
    




