<html>
<head>
</head>
<body>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1><?=$voc_assignations?> <?=$voc_moderation?></h1>
                    </div>
                </div>
                <div style="margin:15px">
                    <div id="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#nonPorteurPane" role="tab" data-toggle="tab"><?=$voc_nonPorteur?></a></li>
                            <li ><a href="#entitePane" role="tab" data-toggle="tab" onClick="affichageEntitePane();contruireSelectNonPorteursAssignes();affichageEntitesAFaire();"><?=$voc_entite?></a></li>
                            <li><a href="#suiviPane" role="tab" data-toggle="tab" onClick="affichageSuiviPane();affichageTableauxAssignations();"><?=$voc_suiviAvt?></a></li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content" style="margin:15px">
                        <!--Onglet assignation non porteurs-->
                        <div class="tab-pane active" id="nonPorteurPane">
                            <div>
                                <h4><?=$voc_nonPorteur?></h4>
                            </div>
                            <select multiple="multiple" id="multiselectListeNonPorteurs" name="my-select[]" data-selectableheader="<?=$voc_dispo?>" data-selectionheader="<?=$voc_assigne?>">
                                
                                    <?php
                                       
                                    $listeNonPorteurs = Utilisateur::rechercherUtilisateurParEtat(4);
                                    $groupePrecedent="";
                                    foreach($listeNonPorteurs as $nonPorteur){
                                        $selected="";
                                        $groupeCourant = $nonPorteur->getGroupe();
                                        if($groupeCourant != $groupePrecedent){
                                            echo "</optgroup>";
                                            echo "<optgroup label=\"".$groupeCourant."\">";
                                            $groupePrecedent=$groupeCourant;
                                        }
                                        if($moderateur->possedeNonPorteur($nonPorteur->getId())){
                                            $selected="selected";
                                        }
                                        echo "<option $selected value=\"".$nonPorteur->getId()."\">".$nonPorteur->getEmail()."</option>"; 
                                    }
                                    ?>
                            </select>
                        </div>
                        <!--Onglet assignation entités-->
                        <div class="tab-pane active"  id="entitePane">
                            <div id="listeNonPorteursEntitePane">
                                    <div>
                                        <h4><?=$voc_choixNonPorteur?></h4>
                                    </div>
                                    <select name="selectListeNonPorteursAssignes" id="selectListeNonPorteursAssignes" class='chosen-select form-control' >
                                        <?php
                                        //Affichage des utilisateurs associés au moderateur connecté 
                                            if(!empty($listeNonPorteursAssignes)){
                                                foreach($listeNonPorteursAssignes as $nonPorteurAssigne){
                                                    echo "<option value=\"".$nonPorteurAssigne->getId()."\">".$nonPorteurAssigne->getEmail()."</option>"; 
                                                }
                                            }
                                        ?>
                                    </select>
                                <div id="conteneurMultiSelectEntites" style="margin-top:30px"></div>
                            </div>
                        </div>
                         <!--Onglet suivi avancement-->
                        <div class="tab-pane active" id="suiviPane">
                            <div id="suiviPaneDiv">
                                <div class="box-title">
                                    <h3 align="center"><?=$voc_suiviAvt?></h3>
                                    </div>
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h4><?=$voc_entreprises?></h4>
                                    </div>
                                    <!--Tableau des entreprises-->
                                    <div class="box-content">
                                        <table id="table1" class="table table-hover table-nomargin table-bordered usertable display">
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th class='with-checkbox'></th>
                                                    <th><?=$voc_produit?></th>
                                                    <th><?=$voc_nonPorteurAssigne?></th>
                                                    <th class='hidden-350'><?=$voc_statut?></th>
                                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                                <tr>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?=$voc_produit?></th>
                                                    <th><?=$voc_nonPorteurAssigne?></th>
                                                    <th class='hidden-350'><?=$voc_statut?></th>
                                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                            </thead>
                                            <tbody id="conteneurAssignationsEntreprises"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <br/>
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h4><?=$voc_produits?></h4>
                                    </div>
                                    <!--Tableau des produits-->
                                    <div class="box-content">
                                        <table id="table2" class="table table-hover table-nomargin table-bordered usertable display">
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th class='with-checkbox'></th>
                                                    <th><?=$voc_produit?></th>
                                                    <th><?=$voc_nonPorteurAssigne?></th>
                                                    <th class='hidden-350'><?=$voc_statut?></th>
                                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                                <tr>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?=$voc_produit?></th>
                                                    <th><?=$voc_nonPorteurAssigne?></th>
                                                    <th class='hidden-350'><?=$voc_statut?></th>
                                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                            </thead>
                                            <tbody id="conteneurAssignationsProduits"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="controller/js/bmwatch_assignation.js"></script>
</html>