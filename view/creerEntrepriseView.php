<?php
    session_start();
?>
<!DOCTYPE html>
    <?php
    require_once '../model/EntiteClass.php';
    require_once '../model/BMClass.php';
    ?>
<html>
    <head>
        <title>création des entreprises</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <p>Page permettant de créer les entreprises</p>
        <form method="POST" action="../controller/entiteController.php">
    <label>Categorie : </label> <select  name="categorie" />
    <?php
    $tabCategories = Entite::obtenirCategories();
        for($i=0;$i<count($tabCategories);$i++){
            $chaineEspace = "";
            $niveau = $tabCategories[$i][5];
            if($niveau != 0){
            //str_pad($chaineEspace,$niveau*2,".");
            $chaineEspace.=str_repeat("&nbsp;",$niveau*3);
            $categories = $chaineEspace . "|_". $tabCategories[$i][3];
            }
            else{
               $categories = $tabCategories[$i][3];
            }
        echo "<option value='".$tabCategories[$i][1]."'>".$categories."</option>";
        
    }
    ?>
    </select>
    <span class="erreur" ></span><br/>
    <label>Description : </label> <input type="text" name="description" class="texte"/><span class="erreur" ></span><br/>
    <label>Nom : </label> <input type="text" name="nom" id="nom" class="texte"/><span class="erreur" ></span><br/>
    <label>Forme sociale : </label> <input type="text" name="formeSociale" class="texte"/><span class="erreur" ></span><br/>
    <label>Secteur d'activité : </label> <input type="text" name="secteurActivite" class="texte"/><span class="erreur" ></span><br/>
    <label>Technologies utilisées : </label> <input type="text" name="technologieUtilisees" class="texte"/><span class="erreur" ></span><br/>
    
    <?php
    $tabCriteres = Entite::obtenirCriteres();
    echo "<input name = 'nbCriteres' id='nbCriteres' value='".count($tabCriteres)."' hidden=true />";
    for($i=0;$i<count($tabCriteres);$i++){
        echo "<label>".$tabCriteres[$i][2]."</label> <input type='text' id='critere".$tabCriteres[$i][1]."' name = 'critere".$tabCriteres[$i][1]."' class='critere' /> <span class='erreur' ></span> <br/>";
    }
    
    
    $tabDimension = BM::obtenirDimension();
    echo "<input name = 'nbDimension' id='nbDimension' value='".count($tabDimension)."' hidden=true />";
    for($i=0;$i<count($tabDimension);$i++){
        $tabOcc = BM::obtenirOccurenceAvecDim($tabDimension[$i][1]);
        echo "<label>".$tabDimension[$i][2]."</label>";
        echo "<select  name='boite".$i."' class ='choixDim'/>";
        echo "<option value='null'>Non connu</option>";
        for($j=0;$j<count($tabOcc);$j++){
            echo "<option value='".$tabOcc[$j][1]."'>".$tabOcc[$j][3]."</option>";   
        }
        echo "<option value='autre'>Autre</option>";
        echo "</select><input type='text' id='boite".$i."' name='boite".$i."' class='boiteDim' hidden = true valide = true/><span class='erreur' ></span></br>";
        
    }
    ?>
    
    
    <input type="submit" id="submit" value="Création" name="creerEntreprise" disabled = true /><br/>
    <a href="./ajoutBMView.php" target=_blank>Ajout de la BM</a> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="./dynamique/verification_creerEntrepriseView.js"></script>
</form>
    </body>
</html>
