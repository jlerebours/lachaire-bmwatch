<!DOCTYPE html>
<html>
    <head>
        <title>Interface de Visualisation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
           <div id="main">
               <div class="container-fluid ">
                   <div class="page-header">
                       <div class="pull-left">
                           <h1><?=$default_nom?></h1>
                       </div>
                   </div>
                    <div style="margin:15px">
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                            <form action="formulaire-fiche" method="POST" class='form-horizontal form-bordered' enctype='multipart/form-data'>
                                <div class="tab-pane active" id="general">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="box box-color box-bordered">
                                                <div class="box-title">
                                                    <h3><i class="fa fa-th-list"></i>
                                                    <?php
                                                    switch ($entite->getType()){
                                                        case 1:
                                                            echo $voc_ficheEntreprise;
                                                            break;
                                                        case 2:
                                                            echo $voc_ficheProduit;
                                                            break;
                                                        default :
                                                            echo 'erreur';
                                                    }?>
                                                    </h3>
                                                </div>
                                                <div class="box-content nopadding">
                                                
                                                    <div class='form-group'>
                                                        <label class='control-label col-sm-2 right'></label>
                                                        <div class="col-sm-5" align="center" style="background-color:rgb(51,204,0);font-size: 16px;"><?=$voc_infosValides?></div>
                                                        <div class="col-sm-5" align="center" style="background-color:rgb(255,40,0);font-size: 16px;"><?=$voc_infosModifiees?></div>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='control-label col-sm-2'><?=$voc_dateModification?>:</label>
                                                        <div class="col-sm-5"><?=$default_dateDerniereModif?></div>                                                        
                                                    </div>
                                                     <div class='form-group'>
                                                        <label class='control-label col-sm-2'><?=$voc_email?>:</label>
                                                        <div class="col-sm-5"><?=$default_emailModif?></div>                                                        
                                                    </div>
                                                    <input hidden="true" id="nbInfo" name="nbInfo" value="<?= count($tabInfosModif)?>" />
                                                    <input hidden="true" id="idEntite" name="idEntite" value="<?= $id?>" />
                                                    <?php $i=0;                                                       
                                                        foreach ($tabInfosModif as $company) {
                                                            if( !empty($company[2])){ ?>
                                                                <div class="form-group">
                                                                    <label for="info<?=$i?>" class="control-label col-sm-2" > <?=$company[0]?>:</label>
                                                                    <div class="col-sm-5"> <?=$company[1]?> </div>
                                                                    <div class="col-sm-5" > <?=$company[2]?></div>
                                                                    <input hidden="true" id="info<?=$i?>" name ="info<?=$i?>" value="<?=$company[3]?>" />
                                                                </div>
                                                            <?php $i++;}
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <p>
                                        <a href="modification-<?=$id?><?=$submitModifierValider?>"><input type="button" class="btn btn-primary" value="<?=$buttonSubmitModifierValider?>"></a>
                                        <button type="submit" class="btn btn-primary" name="<?=$submit?>" value="<?=$submit?>"><?=$buttonSubmitValider?></button>
                                    </p>
                                    <p>
                                        <a href="javascript:history.go(-1)"><input type="button" class="btn btn-red" value="Back"></a>
                                    </p>    
                                </div>
                            </form>

                        </div>
                   </div>
               </div>
            </div>
        </body>