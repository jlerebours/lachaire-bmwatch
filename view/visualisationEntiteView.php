<!DOCTYPE html>
<html>
    <head>
        <title>Interface de Visualisation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1><?= $default_nom ?>
                            </h1>
                        </div>
                    </div>
                    <div style="margin:15px">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                <a href="#general" data-toggle='tab'>
                                    <i class="glyphicon-notes" style="margin-right:5px"></i><?= $voc_info_general ?></a>
                            </li>
                            <li>
                                <a href="#bm" data-toggle='tab'>
                                    <i class="glyphicon-tie" style="margin-right:5px"></i>BM</a>
                            </li>
                            <li>
                                <a href="#source" data-toggle='tab'>
                                    <i class="glyphicon-pen" style="margin-right:5px"></i>Sources</a>
                            </li>
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                            <div class="tab-pane active" id="general">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box box-color box-bordered">
                                            <div class="box-title">
                                                <h3><i class="fa fa-th-list"></i>
                                                    <?php
                                                    switch ($entite->getType()) {
                                                        case 1:
                                                            echo $voc_ficheEntreprise;
                                                            break;
                                                        case 2:
                                                            echo $voc_ficheProduit;
                                                            break;
                                                        default :
                                                            echo 'erreur';
                                                    }
                                                    ?>
                                                </h3>
                                            </div>
                                            <div class="box-content nopadding">
                                                <form action="#" method="POST" class='form-horizontal form-bordered' enctype='multipart/form-data'>

                                                    <?php
                                                    foreach ($tabInfosEntite as $info) {
                                                        echo "<div class=\"form-group\">";
                                                        echo "<label for=\"" . $info[0] . "\" class=\"control-label col-sm-2\">" . $info[0] . " :</label>";

                                                        if (!empty($info[1])) {
                                                            echo "<div class=\"col-sm-10\">";
                                                            echo $info[1];
                                                            echo "</div>";
                                                        }
                                                        echo "</div>";
                                                    }
                                                    if (Entreprise::estUneEntreprise($id)) {
                                                        // div des produits de l'entreprise
                                                        echo "<div class=\"form-group\">";
                                                        echo "<label class=\"control-label col-sm-2\">" . $voc_produits . " :</label>";

                                                        if (!empty($default_product)) {
                                                            echo "<div class=\"col-sm-10\">";
                                                            foreach ($default_product as $product) {
                                                                echo "<a href=\"visualisation-" . $product->getId() . "m\">" . $product->getNom() . "</a></br>";
                                                            }
                                                            echo "</div>";
                                                        }
                                                        echo "</div>";
                                                        // div du reste des infos 
                                                        foreach ($tabInfosEntreprise as $company) {
                                                            echo "<div class=\"form-group\">";
                                                            echo "<label for=\"" . $company[0] . "\" class=\"control-label col-sm-2\">" . $company[0] . " :</label>";

                                                            if (!empty($company[1])) {
                                                                echo "<div class=\"col-sm-10\">";
                                                                echo $company[1];
                                                                echo "</div>";
                                                            }
                                                            echo "</div>";
                                                        }
                                                    }
                                                    if (Produit::estUnProduit($id)) {
                                                        ?>
                                                        <div class="form-group">
                                                            <label for="textfield" class="control-label col-sm-2"><?= $voc_entreprise ?> :</label>
                                                            <div class="col-sm-10">
                                                                <?php
                                                                echo "<a href=\"visualisation-" . $default_entreprise . "n\">" . $default_nomEntreprise . "</a></br>";
                                                                ?>
                                                            </div>
                                                        </div> <?php
                                                        foreach ($tabInfosProduits as $prod) {
                                                            echo "<div class=\"form-group\">";
                                                            echo "<label for=\"" . $prod[0] . "\" class=\"control-label col-sm-2\">" . $prod[0] . " :</label>";

                                                            if (!empty($prod[1])) {
                                                                echo "<div class=\"col-sm-10\">";
                                                                echo $prod[1];
                                                                echo "</div>";
                                                            }
                                                            echo "</div>";
                                                        }
                                                    }
                                                    foreach ($tabCriteres as $critere) {
                                                        if (!empty($critere[4])) {
                                                            echo "<div class=\"form-group\">";
                                                            echo "<label for=\"" . $critere[2] . "\" class=\"control-label col-sm-2\">" . $critere[2] . " :</label>";


                                                            echo "<div class=\"col-sm-10\">";
                                                            echo $critere[5];
                                                            echo "</div>";

                                                            echo "</div>";
                                                        }
                                                    }
                                                    ?>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <p>
                                                <?php if ($modification) {
                                                    if ($griser) {
                                                        ?>
                                                        <input type="button" class="btn btn-primary" disabled="true" value="Validation in progress"  >
                                                    <?php } else { ?>
                                                        <a  href="modification-<?= $id ?><?= $submit ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmit ?>"  ></a>
                                                    <?php }
                                                } if ($statut == "modo" || $statut == "validation") {
                                                     ?>
                                                    <a href="validation-<?= $id ?><?= $submit ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmitValider ?>"></a>
                                                <?php } elseif ($statut == "nonPorteur") { ?>
                                                    <a href="modification-<?= $id ?><?= $submitModifierValider ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmitModifierEnvoyer ?>"></a>
                                                    <a href="validation-<?= $id ?><?= $submit ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmitEnvoyer ?>"></a>                                                    
                                                <?php }if ($statut == "modo") { ?>
                                                    <a href="#modal" data-toggle="modal" ><input type="button" class="btn btn-primary" value="<?= $buttonSubmitARefaire ?>"></a>
                                        <?php }if ($statut == "validation") { ?>
                                                    <a href="modification-<?= $id ?><?= $submitModifierValider ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmitModifierValider ?>"></a>
                                        <?php } $entiteT = new Entite($id);
                                        if ($statut == "porteur" && $entiteT->getType() == 1) { ?>
                                                    <a href="newProduct-<?= $id ?>"><input type="button" class="btn btn-primary" value="<?= $buttonSubmitAjouterProduit ?>"></a>
                                        <?php } ?>
                                            </p>
                                            <p>
                                                <a href="javascript:history.go(-1)"><input type="button" class="btn btn-red" value="Back"></a>
                                            </p>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="bm">
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3>Historique</h3>
                                    </div>
                                    <table id="tabHistorique" class="table table-hover table-nomargin table-bordered usertable">
                                        <thead>
                                            <tr>
                                                <th><?= $voc_BMNom ?></th>
                                                <th>Date</th>
                                                <th class='hidden-350'><?= $voc_statut ?></th>
                                                <th class='hidden-320'>Commentaires</th>
                                                <th class='hidden-480'><?= $voc_options ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $tabHistoriqueBm = $entite->getHistoriqueBm();
                                            foreach ($tabHistoriqueBm as $Hist) {
                                                ?>
                                                <tr>
                                                    <td><?= $Hist[1]; ?></td>
                                                    <td><?= $Hist[2]; ?></td>
                                                    <td><?= $Hist[3]; ?></td>
                                                    <td><?= $Hist[4]; ?></td>
                                                    <td><a class="btn btn-viewBm" id="<?= $Hist[5] ?>" rel="tooltip" title="Look">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="box box-color box-bordered" id='boxHistorique' style='display:none'>
                                    <div class="box-title">
                                        <h3><i class="fa fa-th-list"></i><?= $voc_produit ?></h3>
                                    </div>
                                    <div class="box-content nopadding">
                                        <form action="#" method="POST" id="formBM" class='form-horizontal form-bordered' enctype='multipart/form-data'>
<!--                                            Rempli avec l ajax-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="source">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box box-color box-bordered">
                                            <div class="box-title">
                                                <h3><i class="fa fa-th-list"></i><?= $voc_produit ?></h3>
                                            </div>
                                            <div class="box-content nopadding">
                                                <form action="#" method="POST" class='form-horizontal form-bordered' enctype='multipart/form-data'>
                                                    <div class="form-group">
                                                        <label for="textfield" class="control-label col-sm-2">Sources :</label>
                                                                    <div class="col-sm-10">
                                                                    <?php
                                                                    echo "<a href=" . $default_source . ">" . $default_source . "</a>";
                                                                    ?>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= $voc_fermer ?></span></button>
                                <h4 class="modal-title">Send a commentary</h4>
                            </div>
                            <form class='form-validate' action="formulaire-fiche" method="POST" id="formCommentaire" > 
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="commentaire" class="control-label col-sm-3 right">Commentary : </label>
                                            <div class="col-sm-9">
                                                <textarea type="text" name="commentaire" id="commentaire" class='form-control' style="resize:none" data-rule-stringCorrect=true ></textarea>
                                            </div>
                                        </div>
                                        <input hidden="true" id="idEntite" name="idEntite" value="<?= $id ?>" />
                                    </div>    
                                </div>
                                <div class="modal-footer">
                                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?= $voc_fermer ?>">
                                    <input type="submit" name="<?= $submitARefaire ?>"  class='btn btn-primary' value="<?= $buttonSubmitEnvoyer ?>">
                                </div>
                            </form>
                            <!-- /.modal-footer -->
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
        </div>
    </body>
    <script src='controller/js/bmwatch_historique_BM.js'></script>