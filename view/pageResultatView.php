<html>
<head>
    
</head>
<body>
    <script src="controller/js/bmwatch_recherche.js"></script>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1><?= $voc_resultats ?></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div <?php if($recherche == "avance"){echo"hidden=true";} ?>>
                            <div class="box box-color box-bordered">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-search"></i>
                                        <?= $voc_rechercheSimple ?>
                                    </h3>
                                </div>
                                <div class="box-content">

                                    <form action="recherche" method="POST" id="formRechercheSimple" class='form-horizontal form-striped form-validate'>
                                        <div class="form-group">
                                            <label for="nomEntite" class="control-label col-sm-2 right"><?= $voc_recherche ?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nomEntite" id="nomEntite" value="<?=$default_nom?>" class='form-control'  >
                                            </div>
                                        </div>
                                        <?php $tabCriteres = Entite::obtenirCriteres(); ?>
                                        <input name = 'nbCriteres' id="nbCriteres" value="<?=count($tabCriteres)?>" hidden=true />
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary" name="rechercheSimple"><?= $voc_rechercheSimple ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div <?php if($recherche == "simple"){echo"hidden=true";} ?>>
                            <form action="recherche" method="POST" id="formRechercheMultiCritere" class='form-horizontal form-striped form-validate'>

                                <div class="box box-color box-bordered">
                                    <div class="box-title">
                                        <h3>
                                            <i class="fa fa-search"></i>
                                            <?= $voc_rechercheMultiCritere ?>
                                        </h3>
                                        <div class="actions">
                                            <a href="#" class="btn btn-mini content-slideUp">
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="form-group">
                                            <label for="type" class="control-label col-sm-2">Range</label>
                                            <div class="col-sm-10">
                                                <select type="text" id="type" name="type" class="form-control">
                                                    <option value="">Both</option>
                                                    <option value="1"><?= $voc_entreprise ?></option>
                                                    <option value="2"><?= $voc_produit ?></option>
                                                </select>
                                            </div>
                                        </div>                   
                                        <?php
                                        $tabCriteres = Entite::obtenirCriteres(); ?>
                                        <input name = 'nbCriteres' id="nbCriteres" value="<?=count($tabCriteres)?>" hidden=true />
                                        <?php for($i=0;$i<count($tabCriteres);$i++){
                                            $tabValeurs = Entite::obtenirValeursCritere($tabCriteres[$i][1]); ?>
                                            <div class='form-group critereType<?=$tabCriteres[$i][3]?>' >
                                                <input name = "typeCritere<?=$tabCriteres[$i][1]?>" id="typeCritere<?=$tabCriteres[$i][1]?>" value="<?=$tabValeurs[0][2]?>" hidden=true />
                                                <input name = "valeurCritere<?=$tabCriteres[$i][1]?>" id="valeurCritere<?=$tabCriteres[$i][1]?>" value="<?=$tabValeurs[0][1]?>" hidden=true />
                                                <label for='text' class='control-label col-sm-2'><?=$tabCriteres[$i][2]?></label>
                                                <?php switch($tabValeurs[0][2]){
                                                    case "NUMERIQUE":?>
                                                        <div class='col-sm-3'>
                                                            <select name="choixComparaison<?=$tabCriteres[$i][1]?>" id="choixComparaison<?=$tabCriteres[$i][1]?>" nbCritere="<?=$tabCriteres[$i][1]?>" class='form-control choixComparaison resultatEnBasSelection'/>
                                                                <option value=''>-- Chose one --</option>
                                                                <option value="egale"
                                                                <?php if($default_tabCriteres[$i][1] == "egale"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >=</option>
                                                                <option value="inferieure"
                                                                <?php if($default_tabCriteres[$i][1] == "inferieure"){
                                                                echo "selected";
                                                                }  ?>     
                                                                ><</option>
                                                                <option value="inferieureEgale"
                                                                <?php if($default_tabCriteres[$i][1] == "inferieureEgale"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >≤</option>
                                                                <option value="superieure"
                                                                <?php if($default_tabCriteres[$i][1] == "superieure"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >></option>
                                                                <option value="superieureEgale"
                                                                <?php if($default_tabCriteres[$i][1] == "superieureEgale"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >≥</option>
                                                                <option value="different"
                                                                <?php if($default_tabCriteres[$i][1] == "different"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >≠</option>
                                                                <option value="entre"
                                                                <?php if($default_tabCriteres[$i][1] == "entre"){
                                                                echo "selected";
                                                                }  ?>     
                                                                >between</option>

                                                            </select>

                                                        </div>
                                                        <div id="choixComparaison<?=$tabCriteres[$i][1]?>Comparaison" <?php if ($default_tabCriteres[$i][2] =="entre") {echo "hidden = true";} ?> >
                                                            <div class='col-sm-7'>
                                                                <input type='text' id="chiffre<?=$tabCriteres[$i][1]?>" value ="<?=$default_tabCriteres[$i][3]?>" name ="chiffre<?=$tabCriteres[$i][1]?>" class='critere form-control resultatEnBasNumerique' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true'/>
                                                            </div>
                                                        </div>
                                                        <div id="choixComparaison<?=$tabCriteres[$i][1]?>Entre" <?php if ($default_tabCriteres[$i][2] =="autre") {echo "hidden = true";} ?> >
                                                            <div class='col-sm-3'>
                                                                <input type='text' id="nb1critere<?=$tabCriteres[$i][1]?>" name ="nb1critere<?=$tabCriteres[$i][1]?>" value ="<?=$default_tabCriteres[$i][3]?>" class='critere form-control resultatEnBasNumerique' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true'/>
                                                            </div>
                                                            <div class='col-sm-3'>
                                                                <input type='text' id="nb2critere<?=$tabCriteres[$i][1]?>" name ="nb2critere<?=$tabCriteres[$i][1]?>" value ="<?=$default_tabCriteres[$i][4]?>" class='critere form-control resultatEnBasNumerique' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true'/>
                                                            </div>                                                        
                                                        </div>
                                                    <?php break;

                                                    case "CLEF": ?>
                                                        <div class='col-sm-8' >
                                                            <input type='text' id="critere<?=$tabCriteres[$i][1]?>" name ="critere<?=$tabCriteres[$i][1]?>" clef ="true" value ="<?=$default_tabCriteres[$i][1]?>" class=' resultatEnBasClef tagsinput critere form-control'  />
                                                        </div>
                                                    <?php break;

                                                    default: ?>
                                                        <div class='col-sm-8' > 
                                                            <select  name="critere<?=$tabCriteres[$i][1]?>" id="critere<?=$tabCriteres[$i][1]?>" class ='critere resultatEnBasSelection selection form-control' />
                                                                <option value=''>-- Chose one --</option>
                                                                <?php for($j=0;$j<count($tabValeurs);$j++){ ?>
                                                                    <option value="<?=$tabValeurs[$j][1]?>"  defaut="<?=$tabValeurs[$j][2]?>"
                                                                    <?php if($default_tabCriteres[$i][1] == $tabValeurs[$j][1]){
                                                                    echo "selected";
                                                                    }  ?>   
                                                                    ><?=$tabValeurs[$j][2]?></option>
                                                                <?php } ?>
                                                            </select>

                                                        </div> 
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary" name="rechercheMultiCritere"><?= $voc_recherche ?></button>
                                            <span>Nombre de resultat : <span id="resultat" ></span>
                                             </span>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div> 
                        <?php if($nonConnecte){ ?>
                        <hr>
                            <div><p align="center" style="font-size:18px;font-weight:500;" ><?=$voc_limiteResultatsNonConnecte?></p></div>
                        <hr>
                        <?php }?>
                        <!--Partie résultats-->
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="fa fa-table"></i>
                                    <?= $voc_resultats?>
                                </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered">
                                    <thead>
                                        <tr>
                                            <th><?= $voc_Nom ?></th>
                                            <th><?= $voc_motClef ?></th>
                                            <th class='hidden-350'><?= $voc_dateModification ?></th>
                                            <th class='hidden-1024'><?= $voc_type ?></th>
                                            <th class='hidden-480'><?= $voc_action ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($resultat != null){
                                            if($nonConnecte){
                                                $resultat = array_chunk($resultat, 5);
                                                $resultat= $resultat[0];
                                            }
                                            foreach($resultat as $entite){?>
                                                <tr>
                                                    <td><?=$entite[3]?></td>
                                                    <td>
                                                        <?=$entite[2]?>
                                                    </td>
                                                    <td class='hidden-350'><?=$entite[4]?></td>
                                                    <td class='hidden-1024'>
                                                    <?php
                                                        if(Entreprise::estUneEntreprise($entite[1])){
                                                            echo "<div class='btn btn-blue taille-parent-100'>".$voc_entreprise."</div>";
                                                        }
                                                        if(Produit::estUnProduit($entite[1])){
                                                             echo "<div class='btn btn-orange taille-parent-100'>".$voc_produit."</div>";
                                                        }
                                                    ?>                        
                                                    </td>
                                                    <td class='hidden-480'>
                                                        <?php 
                                                        if($nonConnecte){ 
                                                            echo "<a class='btn notifSeConnecter' rel='tooltip' title='Look' gritter-title='".$voc_restriction."' gritter-text='".$voc_connexionRequise."'>";
                                                        }
                                                        else{
                                                            echo "<a class='btn' rel='tooltip' title='Look' href='visualisation-".$entite[1]."n '>";
                                                        }
                                                        ?>  
                                                        <i class='fa fa-search'></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                        <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </br>
                        <form action="accueil" method="POST" class='form-striped '>
                            <button type="submit" class="btn btn-primary"><?=$voc_retourAccueil?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>
