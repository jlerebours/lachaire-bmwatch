<?php
    include_once '../controller/verificationLangueController.php';
    include_once '../view/libView.php';
?>
<html>
    <head>
    
    </head>
    <body class='error'>
	<div class="wrapper">
		<div class="code">
			<span>404</span>
			<i class="fa fa-warning"></i>
		</div>
		<div class="desc"><?=$voc_pageNonTrouvee?></div>
        
		<div class="buttons">
			<div class="pull-left">
                <a href="accueil" class="btn">
					<i class="fa fa-arrow-left"></i> <?=$voc_retourAccueil?></a>
			</div>
		</div>
	</div>
</body>
    
</html>
    

