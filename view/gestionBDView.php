<!DOCTYPE html>
<html>
    <head>
        <title><?= $voc_BD ?><?= $voc_administration ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>  
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1><?= $voc_BD ?><?= $voc_administration ?></h1>
                        </div>
                    </div>
                    <div style="margin:15px">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                <a href="#transfertEntites" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?= $voc_transfertEntites ?>
                                </a>
                            </li>
                            <li>
                                <a href="#validationCriteres" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?= $voc_criteres ?>
                                </a>
                            </li>
                            <li>
                                <a href="#validationCategories" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?= $voc_categories ?>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                             <!--Partie transfert d'entités--> 
                            <div class="tab-pane active" id="transfertEntites">
                                <div class="box box-bordered box-color box-voc">
                                    <div class="box-title">
                                        <h3><?=$voc_rechercheApps?> :</h3>
                                    </div>
                                    <div class="box-content">
                                        <form action="formulaire-recherche-apps" method="POST" class='form-horizontal form-bordered form-validate' id="formAjout">
                                            <div class="form-group ">
                                                <label for="tableApps" class="control-label col-sm-2"><?=$voc_choisirBD?></label>
                                                <div class="col-sm-8">
                                                    <select id="tableApps" name="tableApps" class="form-control">
                                                        <option value='Android'>Android</option>";
                                                        <option value='Apple'>Apple</option>";
                                                        <option value='Windows'>Windows</option>";
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="rankApps" class="control-label col-sm-2"><?=$voc_minRank?></label>
                                                <div class="col-sm-8">
                                                    <div id='idRank' class="slider" data-min="0">
                                                        <div name="rankApps" class="amount"></div>
                                                        <div class="slide"></div>
                                                    </div>   
                                                </div>
                                            </div>
                                            <input type="hidden" id="hiddenRank" name="hiddenRank" value="0">
                                            <div class="form-actions">
                                                <input type="submit" id="btnValiderRechercheApps" class="btn btn-primary" value=<?=$voc_valider?>>
                                                <button type="button" class="btn"><?=$voc_annuler?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php if($affichageResulatsBD){ ?>
                                <div class="box box-bordered box-color box-voc">
                                    <div class="box-title">
                                        <h3><?=$voc_resultatsApps?> <strong> <?=$table?> </strong><?=$voc_avec?> <strong><?=$voc_rank?> > <?=$rank?></strong></h3>
                                    </div>
                                    <div class="box-content">
                                        <table id="tabBDApp" class="table table-hover table-nomargin table-bordered usertable " >
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?= $voc_nomEntite ?></th>
                                                    <th><?=$voc_entreprise?> </th>
                                                    <th style="width: 50%;"><?=$voc_description?> </th>
                                                    <th style="width: 10%;"><?=$voc_rank?> </th>

                                                </tr>
                                                <tr>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?= $voc_nomEntite ?></th>
                                                    <th><?= $voc_entreprise?></th>
                                                    <th><?=$voc_description?> </th>
                                                    <th><?=$voc_rank?> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $listeApps = Produit::getApps($table, $rank);
                                                    foreach($listeApps as $app){
                                                        $produit = new Produit("","","","","","","",$app[1]);
                                                        if(!$produit->nomExiste()){
                                                            echo "<tr>";
                                                                echo "<td class='with-checkbox'><input type='checkbox' name='check' value=".$app[0]."></td>";
                                                                echo"<td>".$app[1]."</td>";
                                                                echo"<td>".$app[2]."</td>";
                                                                echo "<td style='max-width:200px;max-height:200px;'><div style='max-height:200px;overflow:auto;overflow-y:scroll;overflow-x:hidden;'>".$app[3]."</div></td>";
                                                                echo"<td>".$app[4]."</td>";
                                                            echo "</tr>";
                                                        }
                                                    } ?>
                                            </tbody>
                                        </table>
                                        <button id="btn-transfert" class="btn btn-red form-control" style="margin-top:30px;" onclick="transfererApps('<?=$table?>');"><?=$voc_transfererApps?></button> 
                                    </div>  
                                </div>
                                <?php } ?>
                            </div>
                            
                            <!--Partie validation critères--> 
                            <div class="tab-pane" id="validationCriteres">
                                <div class="panel-group panel-widget" id="acCritere">
                                    <div class="panel panel-default blue">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#c1Critere" data-toggle="collapse" data-parent="#acCritere"><?= $voc_listeCriteres ?></a>
                                            </h4>
                                        </div>
                                        <div id="c1Critere" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table id="tabCriteres" class=" tabs-2col table table-hover table-nomargin table-bordered usertable " >
                                                    <thead>
                                                        <tr class='thefilter'>
                                                            <th><?= $voc_critere ?></th>
                                                            <th class='hidden-480'><?=$voc_type?> </th>
                                                            <th class='hidden-480'><?= $voc_options ?></th>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <th><?= $voc_critere ?></th>
                                                            <th><?= $voc_type?></th>
                                                            <th class='hidden-480'><?= $voc_options ?></th>
                                                        </tr>
                                                        
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default green">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#c2Critere" data-toggle="collapse" data-parent="#acCritere"><?= $voc_ajouterCritere ?></a>
                                            </h4>
                                        </div>
                                        <div id="c2Critere" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="admin-gestion-bd" method="POST" id="formCritere" class='form-horizontal form-validate'>
                                                    <div class="form-group">
                                                        <label for="type" class="control-label col-sm-2">Range</label>
                                                        <div class="col-sm-10">
                                                            <select type="text" id="type" name="type" class="form-control">
                                                                <option value="">Both</option>
                                                                <option value="1"><?= $voc_entreprise ?></option>
                                                                <option value="2"><?= $voc_produit ?></option>
                                                            </select>
                                                        </div>
                                                    </div>                                                   
                                                    <div class="form-group">
                                                        <label for="nomCritere" class="control-label col-sm-2"><?= $voc_Nom ?></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="nomCritere" name="nomCritere" class="form-control" data-rule-required="true" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="typeCritere" class="control-label col-sm-2"><?= $voc_type ?></label>
                                                        <div class="col-sm-10">
                                                            <select type="text" id="typeCritere" name="typeCritere" class="form-control">
                                                                <option value="NUMERIQUE"><?= $voc_typeNumerique ?></option>
                                                                <option value="CLEF"><?= $voc_typeMotsCles ?></option>
                                                                <option value="LISTE"><?= $voc_typeListe ?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="liste" class="form-group" style="display:none">
                                                        <label class="control-label col-sm-2"><?= $voc_listeElements ?></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="element0" name="element[]" class="form-control" >
                                                            <a href="javascript:ajouterElement(1)" id="lienAjout"><?= $voc_ajouterElement ?></a>
                                                        </div>
                                                    </div>
                                                    
                                                    <input type="hidden" id="ValiderCritere" name="ValiderCritere">
                                                    <div class="form-actions">
                                                        <button type="button" id="btnValiderCritere" name="btnValiderCritere" class="btn btn-success"><?= $voc_valider ?></button>
                                                        <button type="button" class="btn" onclick="clear();"><?= $voc_annuler ?></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Partie validation catégories--> 
                            <div class="tab-pane" id="validationCategories">
                                <div class="panel-group panel-widget" id="acCategorie">
                                    <div class="panel panel-default blue">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#c1Categorie" data-toggle="collapse" data-parent="#acCategorie"><?= $voc_listeCategories ?></a>
                                            </h4>
                                        </div>
                                        <div id="c1Categorie" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php
                                                $tabCategories = Entite::obtenirCategories();
                                                foreach ($tabCategories as $category) {
                                                    $chaineEspace = "";
                                                    $niveau = $category[5];
                                                    if ($niveau != 0) {
                                                        $chaineEspace.=str_repeat("&nbsp;", $niveau * 5);
                                                        $categories = $chaineEspace . "|_" . $category[3];
                                                    } else {
                                                        $categories = $category[3];
                                                    }
                                                    echo $categories."</br>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default green">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a href="#c2Categorie" data-toggle="collapse" data-parent="#acCategorie"><?= $voc_ajouterCategorie ?></a>
                                            </h4>
                                        </div>
                                        <div id="c2Categorie" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form action="admin-gestion-bd" method="POST" id="formCategorie" class='form-validate form-horizontal'>
                                                    <div class="form-group">
                                                        <label for="typeCritere" class="control-label col-sm-2"><?= $voc_categorieParente ?></label>
                                                        <div class="col-sm-10">
                                                            <select type="text" name="selectCategorieParente" class="form-control">
                                                                <?php 
                                                                    foreach ($tabCategories as $category) {
                                                                        $chaineEspace = "";
                                                                        $niveau = $category[5];
                                                                        if ($niveau != 0) {
                                                                            $chaineEspace.=str_repeat("&nbsp;", $niveau * 5);
                                                                            $categories = $chaineEspace . "|_" . $category[3];
                                                                        } else {
                                                                            $categories = $category[3];
                                                                        }
                                                                        echo "<option value=\"".$category[1]."\">".$categories."</option>";
                                                                    }
                                                                    ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="range" class="control-label col-sm-2">Range</label>
                                                        <div class="col-sm-10">
                                                            <select type="text" id="range" name="range" class="form-control" >
                                                                <option value="0" >Both</option>
                                                                <option value="1" >Company</option>
                                                                <option value="2" >Product</option>
                                                            </select>
                                                        </div>
                                                    </div>                                                    
                                                    <div class="form-group">
                                                        <label for="nomCategorie" class="control-label col-sm-2"><?= $voc_Nom ?></label>
                                                        <div class="col-sm-10">
                                                            <input type="text" id="nomCategorie" name="nomCategorie" class="form-control" data-rule-required="true" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="descriptionCategorie" class="control-label col-sm-2"><?= $voc_description ?></label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" id="descriptionCategorie" name="descriptionCategorie" class="form-control" data-rule-required="true" ></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-actions">
                                                        <button type="submit" name="btnValiderCategorie" class="btn btn-success"><?= $voc_valider ?></button>
                                                        <button type="button" class="btn" onclick="clear();"><?= $voc_annuler ?></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script src='controller/js/bmwatch_gestionBase.js'></script>

<div id="modal-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?= $voc_entites ?></h4>
            </div>
            <form action="" method="POST" id="formPopup" class='form-horizontal form-striped form-validate'>
                <div class="form-group">
                    <div class="modal-body">
                        <input type="hidden" id="idSup" value="id">
                        <div class="form-group">
                            <label for="verifPopup" class="control-label"><?= $voc_popup ?></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?= $voc_non ?>">
                    <input type="submit" name="supModif"  class='btn btn-primary inputGrise' id='deleteEntity' value="<?= $voc_oui ?>">
                </div>
            </form>
            <!-- /.modal-footer -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal-2" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?= $voc_entites ?></h4>
            </div>
            <form action="" method="POST" id="formPopup" class='form-horizontal form-striped form-validate'>
                <div class="form-group">
                    <div class="modal-body">
                        <input type="hidden" id="idSupprimer" value="id">
                        <div class="form-group">
                            <label for="verifPopup" class="control-label"><?= $voc_popup ?></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?= $voc_non ?>">
                    <input type="submit" name="suppNew"  class='btn btn-primary inputGrise' id='deleteEntityNew' value="<?= $voc_oui ?>">
                </div>
            </form>
            <!-- /.modal-footer -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

