<?php
    session_start();
?>
<html>
 <head>

<meta charset="utf-8" />
  <title>Page de gestion de son compte</title>
  <?php
  //A changer pour la suite avec la session de l utilisateur
  require_once '../model/UtilisateurClass.php';
  
  $utilisateurCourant = unserialize($_SESSION['donneesUtilisateur']);
  if($utilisateurCourant->emailExiste()){
    $nomDefault = $utilisateurCourant->getNom();
    $prenomDefault = $utilisateurCourant->getPrenom();
    $emailDefault = $utilisateurCourant->getEmail();
    $mdp = $utilisateurCourant->getMdp();
  }
  ?>
 </head>
 <body>

<p>Page permettant de gérer son compte</p>
<form method="POST" action="../controller/accueilController.php">
    <label> Ancien mot de passe : </label><input type="password" id="mdpAncien" name="mdpAncien" class="ancien" /><span class="erreur" ></span><br/>
    <label>Nom : </label> <input type="text" id="nom" name="nom" class="ancien" disabled=true value= <?php echo $nomDefault; ?> /><span class="erreur" ></span><br/>
    <label>Prenom : </label> <input type="text" id="prenom" name="prenom" class="ancien" disabled=true value=<?php echo $prenomDefault; ?> /><span class="erreur" ></span><br/>
    <label>Email : </label> <input type="text" id="email" name="email" class="ancien" disabled=true value=<?php echo $emailDefault; ?> /><span class="erreur" ></span><br/>
    <label>Nouveau mot de passe : </label> <input type="password" id="mdp" name="mdp" disabled=true /><span class="erreur" ></span><br/>
   
    <input type="submit" id="submit" value="Modifier" name="gestionCompte" disabled=true/>
    
</form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="./dynamique/verification_gestionCompteView.js"></script>
 </body>
</html>