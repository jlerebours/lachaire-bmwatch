<?php
    session_start();
    require_once '../model/UtilisateurClass.php';
    include '../controller/verificationController.php';
    ToolBox::verifierDroitsEtRedirigerSiPasOk(3,"../view/accueilView.php",0);
?>


<html> 
    <head>
        <title><?=$voc_porteurInterface?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body> 
        <?php
        $utilisateur = unserialize($_SESSION['donneesUtilisateur']);
        $entreprise = $utilisateur->getEntreprise();
        
        if(empty($entreprise)){
            ToolBox::redirige("../view/creerEntrepriseView.php", 0);
        }
        else{
            $produit = $utilisateur->getProduit($entreprise->getId());
            if(empty($produit)){
                ToolBox::redirige("../view/creerProduitView.php", 0);
            }
            echo $voc_nonPorteurInterface;
        }  
            
        ?>
    </body> 
</html>

