<html>
<head>
</head>
<body>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1><?=$voc_mesAssignations?></h1>
                    </div>
                </div>
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h4><?=$voc_entreprises?></h4>
                    </div>
                    <!--Tableau des entreprises-->
                    <div class="box-content">
                        <table id="tabAssignationsNonPorteur" class="table table-hover table-nomargin table-bordered usertable">
                            <thead>
                                <tr class='thefilter'>
                                    <th><?=$voc_nomEntite?></th>
                                    <th><?=$voc_assignePar?></th>
                                    <th class='hidden-350'><?=$voc_statut?></th>
                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                    <th class='hidden-480'><?=$voc_options?></th>
                                </tr>
                                <tr>
                                    <th><?=$voc_nomEntite?></th>
                                    <th><?=$voc_assignePar?></th>
                                    <th class='hidden-350'><?=$voc_statut?></th>
                                    <th class='hidden-1024'><?=$voc_derniereModif?></th>
                                    <th class='hidden-480'><?=$voc_options?></th>
                                </tr>
                            </thead>
                            <tbody id="conteneurAssignationsEntreprises"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="controller/js/bmwatch_assignation.js"></script>
</html>