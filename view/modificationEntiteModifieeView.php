<html>
<head>
    
</head>
<body>
    <script src="controller/js/bmwatch_fiche.js"></script>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1>Fiche </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <form action="formulaire-fiche" method="POST" id="formModificationProfil" class='form-horizontal form-striped form-validate'>
                            <div class="box box-color box-bordered">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-user"></i>
                                        Info entite
                                    </h3>
                                    <div class="actions">
                                        <a href="#" class="btn btn-mini content-slideUp">
                                        <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="form-group">
                                        <label for="nomEntite" class="control-label col-sm-2 right " ><?=$voc_Nom?> :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nomEntite" id="nomEntite" value="<?=$default_nom?>" class='form-control' disabled = "true" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomEntiteModifier" class="control-label col-sm-2 right " >Nom modifier :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nomEntiteModifier" id="nomEntiteModifier" value="<?=$default_nomModifier?>" class='form-control' data-rule-stringCorrect="true" data-rule-required="true" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text" class="control-label col-sm-2 right"><?=$voc_categorie?> :</label>
                                        <div class="col-sm-10">
                                            <select  name="categorie" id="categorie" disabled = "true" />
                                                
                                                <?php
                                                    $tabCategories = Entite::obtenirCategories($type);
                                                    for($i=0;$i<count($tabCategories);$i++){
                                                        $chaineEspace = "";
                                                        $niveau = $tabCategories[$i][5];
                                                        if($niveau != 0){
                                                            $chaineEspace.=str_repeat("&nbsp;",$niveau*5);
                                                            $categories = $chaineEspace . "|_". $tabCategories[$i][3];
                                                        }
                                                        else{
                                                            $categories = $tabCategories[$i][3];
                                                        }
                                                        echo "<option value='".$tabCategories[$i][1]."'";
                                                        
                                                        if($tabCategories[$i][1] == $default_categorie){
                                                            echo "selected";
                                                        }
                                                        echo ">".$categories."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text" class="control-label col-sm-2 right"><?=$voc_categorie?> modifier :</label>
                                        <div class="col-sm-10">
                                            <select  name="categorieModifier" id="categorieModifier" data-rule-required="true" />
                                                
                                                <?php
                                                    for($i=0;$i<count($tabCategories);$i++){
                                                        $chaineEspace = "";
                                                        $niveau = $tabCategories[$i][5];
                                                        if($niveau != 0){
                                                            $chaineEspace.=str_repeat("&nbsp;",$niveau*5);
                                                            $categories = $chaineEspace . "|_". $tabCategories[$i][3];
                                                        }
                                                        else{
                                                            $categories = $tabCategories[$i][3];
                                                        }
                                                        echo "<option value='".$tabCategories[$i][1]."'";
                                                        
                                                        if($tabCategories[$i][1] == $default_categorieModifier){
                                                            echo "selected";
                                                        }
                                                        echo ">".$categories."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>                                
                                    <div class="form-group">
                                        <label for="dateCreation" class="control-label col-sm-2 right">Date d inscription :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="dateCreation" id="dateCreation" value="<?=$default_dateCreation?>" class='form-control' disabled = 'true' >
                                        </div>
                                    </div>                                  
                                    <div class="form-group">
                                        <label for="dateModif" class="control-label col-sm-2 right">Date dernière modif :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="dateModif" id="dateModif" value="<?=$default_dateDerniereModif?>" class='form-control' disabled = 'true' >
                                        </div>
                                    </div>
                                    <div id="infoEntreprise" <?=$cacheEntreprise?> >
                                        <div class="form-group">
                                            <label for="formeSociale" class="control-label col-sm-2 right"><?=$voc_formeSociale?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="formeSociale" id="formeSociale" value="<?=$default_formeSociale?>" class='form-control' disabled = 'true' >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="formeSocialeModifier" class="control-label col-sm-2 right"><?=$voc_formeSociale?> modifier:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="formeSocialeModifier" id="formeSocialeModifier" value="<?=$default_formeSocialeModifier?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="secteurActivite" class="control-label col-sm-2 right"><?=$voc_secteurActivite?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="secteurActivite" id="secteurActivite" value="<?=$default_secteurActivite?>" class='form-control' disabled = 'true' >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="secteurActiviteModifier" class="control-label col-sm-2 right"><?=$voc_secteurActivite?> modifier:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="secteurActiviteModifier" id="secteurActiviteModifier" value="<?=$default_secteurActiviteModifier?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="technologieUtilisees" class="control-label col-sm-2 right"><?=$voc_technologiesUtilisees?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="technologieUtilisees" id="technologieUtilisees" value="<?=$default_technologieUtilisees?>" class='form-control' disabled = 'true' >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="technologieUtiliseesModifier" class="control-label col-sm-2 right"><?=$voc_technologiesUtilisees?> Modifier:</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="technologieUtiliseesModifier" id="technologieUtiliseesModifier" value="<?=$default_technologieUtiliseesModifier?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div id="infoProduit" <?=$cacheProduit?> >
                                        <div class="form-group">
                                            <label for="nomEntreprise" class="control-label col-sm-2 right"><?=$voc_entreprise?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nomEntreprise" id="nomEntreprise" value="<?=$default_nomEntreprise?>" class='form-control' disabled = 'true'>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="ranking" class="control-label col-sm-2 right">Ranking :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="ranking" id="ranking" value="<?=$default_ranking?>" class='form-control' disabled = 'true' >
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="control-label col-sm-2 right">Description</label>
                                        <div class="col-sm-10">
                                            <textarea style="resize:none" name="description" id="description"  resize=none class='form-control' disabled = "true" ><?=$default_description?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="descriptionModifier" class="control-label col-sm-2 right">Description Modifier</label>
                                        <div class="col-sm-10">
                                            <textarea style="resize:none" name="descriptionModifier" id="descriptionModifier"  resize=none class='form-control' data-rule-maxlength="250"><?=$default_descriptionModifier?></textarea>
                                        </div>
                                    </div>                                    
                                </div> 
                            </div>
                            <input type="hidden" id="idEntite" name="idEntite" value="<?=$id?>" >
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary" name="<?=$submit?>"><?=$buttonSubmit?></button>
                                <a href="javascript:history.go(-1)"><input type="button" class="btn btn-red" value="Back"></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</body>

</html>
    




