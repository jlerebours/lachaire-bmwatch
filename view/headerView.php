<div id="navigation">
    <div class="container-fluid">
        <a href="accueil" id="brand">BMWatch</a>

        <!-- Elements de menu -->
        <ul class='main-nav'>
            <!--Partie menu administrateur -->
             <?php if ($droitsAdmin) { ?>
                <li data-trigger="hover">
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span><?= $voc_administration ?></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="admin-gestion-bd"><?= $voc_BD ?></a>
                        </li>
                         <li>
                            <a href="admin-site"><?= $voc_site ?></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <!--Partie menu modérateur-->
            <?php if ($droitsModo) { ?>
                <li data-trigger="hover">
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span><?= $voc_moderation ?></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="admin-gestion-entites"><?= $voc_validation ?></a>
                        </li>
                        <li>
                            <a href="admin-assignations"><?= $voc_assignation ?></a>
                        </li>
                        <li>
                            <a href="admin-utilisateurs"><?= $voc_utilisateurs ?></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if ($droitsNonPorteur) { ?>
                <li>
                    <a href="mes-assignations"><?= $voc_mesAssignations ?></a>
                </li>
            <?php } ?>
            <!--Partie menu porteur-->
            <?php
            if ($droitsPorteur) {
                $tabEntreprises = $utilisateurCourant->getEntreprises();
                ?>
                <li data-trigger="hover">
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span><?= $voc_entreprisesEtProduits ?></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php
                        foreach ($tabEntreprises as $entreprise) {
                            $tabProduits = $utilisateurCourant->getProduits($entreprise->getId());
                            $classeSubMenu = "";
                            $classeSubMenu = "class=\"dropdown-submenu\"";
                            echo "<li " . $classeSubMenu . ">";
                            echo "<a href=\"visualisation-". $entreprise->getId() ."p\">" . $entreprise->getNom() . "</a>";
                            
                            echo "<ul class=\"dropdown-menu\">";
                            if (!empty($tabProduits)) {
                                foreach ($tabProduits as $produit) {
                                    echo "<li>";
                                    echo "<a href=\"visualisation-". $produit->getId() . "p\">" . $produit->getNom() . "</a>";
                                    echo "</li>";
                                }
                            }
                            echo "<li><a href=\"newProduct-".$entreprise->getId()."\">" . $voc_ajouterProduit . "</a></li>";
                            echo "</ul>";
                           
                            echo "</li>";
                        }
                        echo "<li><a href=\"newCompany\">" . $voc_ajouterEntreprise . "</a></li>";
                        ?>
                    </ul>
                </li>
        <?php } ?>
        <?php if ($droitsChercheur) { ?>
            <li data-trigger="hover">
                <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                    <span><?= $voc_chercheur ?></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="analyse">Analyse</a>
                    </li>
                </ul>
            </li>
        <?php } ?>
                <li>
                    <a href="faq"><?= $voc_faq ?></a>
                </li>
        </ul>

        <div class="user">
            <ul class="icon-nav">
                <li class='dropdown language-select' >
                    <?php
                    $langueSelectionne = $_COOKIE['langue'];
                    echo "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo "<img src=\"controller/img/flags/" . strtolower($langueSelectionne) . ".gif\" alt=\"\">";
                    echo "<span>" . $langueSelectionne . "</span>";
                    echo "</a>";
                    echo "<ul class=\"dropdown-menu pull-right\">";
                    while ($langueCourante = current($listeLangues)) {
                        if (key($listeLangues) != $langue) {
                            echo "<li>";
                            echo "<a href=\"langue-" . key($listeLangues) . "\">";
                            echo "<img src=\"controller/img/demo/flags/" . strtolower(key($listeLangues)) . ".gif\" alt=\"\">";
                            echo "<span>" . $langueCourante . "</span>";
                            echo "</a>";
                            echo "</li>";
                        }
                        next($listeLangues);
                    }
                    echo "</ul>";
                    ?>
                </li>
                <li>
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown" >
                        <?php
                        $nom = $utilisateurCourant->getPrenom();
                        $prenom = $utilisateurCourant->getNom();
                        if (empty($nom)) {
                            $nom = "Unknown";
                        }
                        echo $nom." ".$prenom;
                        ?>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="profil"><?= $voc_modifierProfil ?></a>
                        </li>
                        <li>
                            <a href="logout"><?= $voc_seDeconnecter ?></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

