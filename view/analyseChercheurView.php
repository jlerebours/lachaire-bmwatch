<!DOCTYPE html>
<html>
    <head>
        <title><?= $voc_site ?> <?= $voc_administration ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>  
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1><?= $voc_analyse ?></h1>
                        </div>
                    </div>
                    <div style="margin:15px">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                <a href="#occ" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                   nb d'occurence
                                </a>
                            </li>
                            <li>
                                <a href="#google" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    google
                                </a>
                            </li>
                            <li>
                                <a href="#clicdata" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    clicdata
                                </a>
                            </li>                            
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">

                            <!--Partie validation catégories--> 
                            <div class="tab-pane active" id="occ">
                                <div class="box box-color box-bordered">
                                    <div class="box-title">
                                        <h3>
                                            <i class="fa fa-search"></i>
                                            Commentary
                                        </h3>
                                    </div>
                                    <div class="box-content">
                                        <form action="analyse" method="POST" id="formChoix" class='form-horizontal form-striped form-validate'>
                                            <div class="form-group">
                                                <label for="choix" class="control-label col-sm-2 right"><?= $voc_entite ?> :</label>
                                                <div class="col-sm-10">
                                                    <select type="text" id="choix" name="choix" class="form-control">
                                                        <?php foreach($listeEntites as $entite){?>
                                                            <option value="<?=$entite[0]?>" <?php if($id==$entite[0]) echo "selected"; ?>><?=$entite[1]?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary" name="btnChoix"><?= $voc_recherche ?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php if($id != null){ ?>
                                    <div class="box box-bordered box-color">
                                        <div class="box-title">
                                            <h3>Historique</h3>
                                        </div>
                                        <div class="box-content nopadding">
                                            <table id="tabHistorique" class="table table-hover table-nomargin dataTable table-bordered" style=" table-layout: fixed  ">
                                                <thead>
                                                    <tr>
                                                        <th><?=$voc_nom ?></th>
                                                        <th>Date</th>
                                                        <th class='hidden-350'><?= $voc_rank ?></th>
                                                        <th class='hidden-320'>Commentary</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php
                                                    foreach ($listeCommentaires as $ligne) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $ligne[0]; ?></td>
                                                            <td ><?= $ligne[1]; ?></td>
                                                            <td ><?= $ligne[2]; ?></td>
                                                            <td style='max-width:150px;max-height:100px;'><div style='max-height:100px;overflow:auto;overflow-y:scroll;overflow-x:hidden;'><?= $ligne[3]; ?></div></td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                         </div> 
                                        Average ranking : <?= $resultat?></br>
                                        <button class="btn-primary btn" disabled="true">Calculate</button>
                                     
                                    </div> 
                                <?php } ?>
                            </div>
                            <div class="tab-pane" id="google">

                                <a href="https://www.google.com/analytics/web/?authuser=1#home/a53598991w86352224p89628667/">Google Analytics</a>
                            </div>
                            <div class="tab-pane" id="clicdata">

                                <a href="https://i3.clicdata.com/menu/menu">ClicData</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>