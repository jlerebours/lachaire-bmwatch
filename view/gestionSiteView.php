<!DOCTYPE html>
<html>
    <head>
        <title><?= $voc_site ?> <?= $voc_administration ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>  
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1><?= $voc_site ?> <?= $voc_administration ?></h1>
                        </div>
                    </div>
                    <div style="margin:15px">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                <a href="#faq" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?= $voc_faq ?>
                                </a>
                            </li>
                            <li>
                                <a href="#vocabulaire" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?= $voc_vocabulaire ?>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                            <!--Partie validation critères--> 
                            <div class="tab-pane active" id="faq">
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3><?=$voc_listeQuestionsFaq?></h3>
                                    </div>
                                    <div class="box-content">
                                        <table id="tabFaq" class=" table table-hover table-nomargin table-bordered usertable " >
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th><?= $voc_categorie ?></th>
                                                    <th><?= $voc_question ?></th>
                                                    <th class='hidden-480'><?= $voc_options ?></th>
                                                </tr>
                                                <tr>
                                                    <th><?= $voc_categorie ?></th>
                                                    <th><?= $voc_question ?></th>
                                                    <th class='hidden-480'><?= $voc_options ?></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                    <div id="boxFaq" class="box box-bordered box-color">
                                        <div class="box-title">
                                            <h3 id="titreBox2Faq" titreAjout='<?=$voc_ajouterFaq?>' titreVisu='<?=$voc_apercuQuestionFaq?>'><?=$voc_ajouterFaq?></h3>
                                        </div>
                                        <div class="box-content">
                                            <form action="formulaire-faq" method="POST" id="formAjoutFaq" class='form-horizontal'>
                                                <div class="form-group">
                                                    <div id="selectGroupe">
                                                        <label for="categorieFaq" class="control-label col-sm-2"><?=$voc_categorie?></label>
                                                        <div class="col-sm-4">
                                                            <select style="resize:none" id="categorieFaq" name="categorieFaq" class="form-control" data-rule-required="true" >
                                                            <?php 
                                                                $listeCategories = Faq::getListeCategories();
                                                                foreach ($listeCategories as $categorie) {
                                                                    echo "<option value='".$categorie."'>".$categorie."</option>";
                                                                }
                                                            ?>
                                                            </select>
                                                            <a href="javascript:ajouterCategorie()" ><?= $voc_ajouterCategorie ?></a>
                                                            <input style="display:none"type="text" id="nouvelleCatFaq" name="nouvelleCatFaq" class="form-control" data-rule-required="true" >
                                                        </div>
                                                    </div>
                                                     <div id="divNouvelleCategorie" style="display:none">
                                                        <label for="nouvelleCategorie" class="control-label col-sm-2"><?=$voc_nouvelleCategorie?></label>
                                                        <div class="col-sm-6">
                                                            <input type="text" id="nouvelleCategorie" name="nouvelleCategorie" class="form-control" data-rule-required="true"  data-rule-minlength="5">
                                                            <a href="javascript:afficherChoixCategorie();" ><?=$voc_retourListe?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="questionFaq" class="control-label col-sm-2"><?=$voc_question?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="questionFaq" name="questionFaq" class="form-control" data-rule-required="true" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="reponseFaq" class="control-label col-sm-2"><?=$voc_reponse?></label>
                                                    <div class="col-sm-10">
                                                        <textarea style="resize:none" rows="6" type="text" id="reponseFaq" name="reponseFaq" class="form-control" data-rule-required="true" ></textarea>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="idInputIdFaq" value="">
                                                <div class="form-actions">
                                                    <button type="submit" id="btnValiderFaq" name="idFaq" value="ajout" class="btn btn-success"><?=$voc_valider?></button>
                                                    <button type="button" class="btn btn-danger" onclick="supprimerFaq();"><?=$voc_supprimer?> <?=$voc_question?></button>
                                                    <button type="button" class="btn" onclick="annulerVisuFaq();"><?=$voc_fermer?></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Partie validation catégories--> 
                                <div class="tab-pane" id="vocabulaire">
                                    
                                    <hr>
                                    <p style="font-weight:bold;font-size: 16px"><?=$voc_choisirFichierLangue?> : </p>
                                    <div>
                                        <select id="idSelectLangue" class="form-control">
                                            <?php 
                                            if($dossier = opendir('../controller/langues'))
                                            {
                                                $i=0;
                                                while(($fichier = readdir($dossier)) !== false  ){
                                                    if($fichier != '.' && $fichier != '..' ){
                                                        echo "<option value='".$i."'>".$fichier."</option>";
                                                        $i+=1;
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <hr>
                                    <?php
                                    if($dossier = opendir('../controller/langues'))
                                    {
                                        $i=0;
                                        while(($fichier = readdir($dossier)) !== false  ){
                                            if($fichier != '.' && $fichier != '..' ){ ?>
                                                <div id="tabVoc<?=$i?>" class="box box-bordered box-color box-voc" style="display:none">
                                                    <div class="box-title">
                                                        <h3><?=$voc_fichierActuel?> : <?=$fichier?></h3>
                                                    </div>
                                                    <div class="box-content">
                                                        <table fichier='<?=$fichier?>' class="table-voc table table-hover table-nomargin table-bordered usertable " >
                                                            <thead>
                                                                <tr class='thefilter'>
                                                                    <th><?= $voc_variable ?></th>
                                                                    <th><?= $voc_valeur ?></th>
                                                                    <th class='hidden-480'><?= $voc_options ?></th>
                                                                </tr>
                                                                <tr>
                                                                    <th><?= $voc_variable ?></th>
                                                                    <th><?= $voc_valeur ?></th>
                                                                    <th class='hidden-480'><?= $voc_options ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $myFile = '../controller/langues/'.$fichier;
                                                            $fh = fopen($myFile, "r");
                                                            if ( $fh ) {
                                                                $ligne=0;
                                                                while ( !feof($fh) ) {
                                                                    $line = fgets($fh);
                                                                    $resultat = explode("=", $line);
                                                                    if(count($resultat) == 2){
                                                                        $variable = $resultat[0];
                                                                        $variable = trim($variable);
                                                                        $valeur = trim($resultat[1]);
                                                                        $valeur = substr($valeur,0,strlen($valeur)-1);
                                                                        echo "<tr>";
                                                                            echo "<td>".$variable."</td>";
                                                                            echo "<td>".$valeur."</td>";
                                                                            echo "<td><a class='btn btn-viewFaq' rel='tooltip' title='".$voc_modifier."' onclick='modifierVoc(".$ligne.",\"".$variable."\",".$valeur.");'>
                                                                                        <i class='fa fa-edit'></i>
                                                                                    </a></td>";
                                                                        echo "</tr>";
                                                                    }
                                                                    $ligne+=1;
                                                                }
                                                                fclose($fh);
                                                            }
                                                            ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <?php
                                                $i+=1;
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
</html>
<script src='controller/js/bmwatch_gestionSite.js'></script>


<div id="modale-voc" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?= $voc_modificationVariable ?></h4>
            </div>
            <form class='form-horizontal form-striped form-validate'>
                 <div class="modal-body">
                    <div class="form-group">
                        <label for="variableModale" class="control-label col-sm-2"><?=$voc_variable?></label>
                        <div class="col-sm-8">
                            <input type="text" disabled name="variableModale" id="idVariable" class="form-control" value="id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="valeurVariableModale" class="control-label col-sm-2"><?=$voc_valeur?></label>
                        <div class="col-sm-8">
                            <input type="text" name="valeurVariableModale" id="idValeur" class="form-control" value="id">
                        </div>
                    </div>
                </div>
                <input type="hidden" id="idLigne" name="idLigne" value="id">
                <div class="modal-footer">
                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?= $voc_annuler ?>">
                    <input type="button" name="modifierVoc"  class='btn btn-primary'  value="<?= $voc_enregistrer ?> " onclick="validerVoc();">
                </div>
            </form>
            <!-- /.modal-footer -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




