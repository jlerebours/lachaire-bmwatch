<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>FLAT - Wizard</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">


	
	<script src="js/jquery.min.js"></script>

	
	<script src="js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	
	<script src="js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	
	<script src="js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	
	<script src="js/bootstrap.min.js"></script>
	
	
	
	
	<script src="js/plugins/bootbox/jquery.bootbox.js"></script>
	
	<script src="js/plugins/form/jquery.form.min.js"></script>
	
	<script src="js/plugins/validation/jquery.validate.min.js"></script>
	<script src="js/plugins/validation/additional-methods.min.js"></script>
	
	<script src="js/plugins/form/jquery.form.min.js"></script>
	
	
	
	
	<script src="js/plugins/wizard/jquery.form.wizard.min.js"></script>
	<script src="js/plugins/mockjax/jquery.mockjax.js"></script>

	
	<script src="js/eakroko.min.js"></script>
	
	<script src="js/application.min.js"></script>
	
	<script src="js/demonstration.min.js"></script>

	
		<script src="js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" />
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />

</head>

<body>
	<div class="row">
					<div class="col-sm-12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="fa fa-magic"></i>
									Basic wizard
								</h3>
							</div>
							<div class="box-content">
								<form action="post.php" method="POST" class='form-horizontal form-wizard' id="bb">
									<div class="step" id="firstStep">
										<div class="form-group">
											<label for="firstname" class="control-label col-sm-2">First name</label>
											<div class="col-sm-10">
												<input type="text" name="firstname" id="firstname" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label for="anotherelem" class="control-label col-sm-2">Last name</label>
											<div class="col-sm-10">
												<input type="text" name="anotherelem" id="anotherelem" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label for="additionalfield" class="control-label col-sm-2">Additional information</label>
											<div class="col-sm-10">
												<input type="text" name="additionalfield" id="additionalfield" class="form-control">
											</div>
										</div>
									</div>
									<div class="step" id="secondStep">
										<div class="form-group">
											<label for="text" class="control-label col-sm-2">Gender</label>
											<div class="col-sm-10">
												<select name="gend" id="gend">
													<option value="">-- Chose one --</option>
													<option value="1">Male</option>
													<option value="2">Female</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="text" class="control-label col-sm-2">Accept policy</label>
											<div class="col-sm-10">
												<div class="checkbox">
													<label>
														<input type="checkbox" name="policy" value="agree">I agree the policy.</label>
												</div>
											</div>
										</div>
									</div>
									
										<input type="reset" class="btn" value="Back" id="back">
										<input type="submit" class="btn btn-primary" value="Submit" id="next">
									
								</form>
							</div>
						</div>
					</div>
				</div>
				
</body>

</html>
