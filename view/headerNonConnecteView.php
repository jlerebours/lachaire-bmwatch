<div id="navigation">
    <div class="container-fluid">
        <a href="accueil" id="brand">BMWatch</a>

        <!-- Elements de menu -->
        <ul class='main-nav'>
            <li>
                <a href="accueil"><?= $voc_accueil ?></a>
            </li>
            <li>
                <a href="faq"><?= $voc_faq ?></a>
            </li>
        </ul>

        <div class="user">
            <ul class="icon-nav">
                <li class='dropdown language-select' >
                    <?php
                    $langueSelectionne = $_COOKIE['langue'];
                    echo "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    echo "<img src=\"controller/img/flags/" . strtolower($langueSelectionne) . ".gif\" alt=\"\">";
                    echo "<span>" . $langueSelectionne . "</span>";
                    echo "</a>";
                    echo "<ul class=\"dropdown-menu pull-right\">";
                    while ($langueCourante = current($listeLangues)) {
                        if (key($listeLangues) != $langue) {
                            echo "<li>";
                            echo "<a href=\"langue-" . key($listeLangues) . "\">";
                            echo "<img src=\"controller/img/demo/flags/" . strtolower(key($listeLangues)) . ".gif\" alt=\"\">";
                            echo "<span>" . $langueCourante . "</span>";
                            echo "</a>";
                            echo "</li>";
                        }
                        next($listeLangues);
                    }
                    echo "</ul>";
                    ?>
                </li>
                <li>
                    <a href="connexion" style="width:100%;"><?=$voc_seConnecter?></a>
                </li>
            </ul>
        </div>
    </div>
</div>

