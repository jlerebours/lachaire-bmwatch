
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes" />
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<!--<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />-->

<title>BmWatch</title>
<!-- Bootstrap -->
<link rel="stylesheet" href="controller/css/bootstrap.min.css">
<!-- jQuery UI -->
<link rel="stylesheet" href= "controller/css/plugins/jquery-ui/smoothness/jquery-ui.css" >
<link rel="stylesheet" href= "controller/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css" >
<!-- PageGuide -->
<link rel="stylesheet" href= "controller/css/plugins/pageguide/pageguide.css" >
<!-- Fullcalendar -->
<link rel="stylesheet" href= "controller/css/plugins/fullcalendar/fullcalendar.css" >
<link rel="stylesheet" href= "controller/css/plugins/fullcalendar/fullcalendar.print.css"  media="print">
<!-- Tagsinput -->
<link rel="stylesheet" href= "controller/css/plugins/tagsinput/jquery.tagsinput.css" >
<!-- chosen -->
<link rel="stylesheet" href= "controller/css/plugins/chosen/chosen.css" >
<!-- multi select -->
<link rel="stylesheet" href= "controller/css/plugins/multiselect/multi-select.css" >
<!-- timepicker -->
<link rel="stylesheet" href= "controller/css/plugins/timepicker/bootstrap-timepicker.min.css" >
<!-- colorpicker -->
<link rel="stylesheet" href= "controller/css/plugins/colorpicker/colorpicker.css" >
<!-- Datepicker -->
<link rel="stylesheet" href= "controller/css/plugins/datepicker/datepicker.css" >
<!-- Daterangepicker -->
<link rel="stylesheet" href= "controller/css/plugins/daterangepicker/daterangepicker.css" >
<!-- Plupload -->
<link rel="stylesheet" href= "controller/css/plugins/plupload/jquery.plupload.queue.css" >
<!-- select2 -->
<link rel="stylesheet" href= "controller/css/plugins/select2/select2.css" >
<!-- icheck -->
<link rel="stylesheet" href= "controller/css/plugins/icheck/all.css" >
<!-- dataTables -->
<link rel="stylesheet" href= "controller/css/plugins/datatable/TableTools.css" >
<!-- Theme CSS -->
<link rel="stylesheet" href= "controller/css/style.css" >
<!-- Color CSS -->
<link rel="stylesheet" href= "controller/css/themes.css" >
<!-- Notify -->
<link rel="stylesheet" href= "controller/css/plugins/gritter/jquery.gritter.css" >
<!-- Bootstrap -->
<link rel="stylesheet" href= "controller/css/bootstrap.min.css" >
<!-- PageGuide -->
<link rel="stylesheet" href= "controller/css/plugins/pageguide/pageguide.css" >
<!-- chosen -->
<link rel="stylesheet" href= "controller/css/plugins/chosen/chosen.css" >
<!-- Theme CSS -->
<link rel="stylesheet" href= "controller/css/style.css" >
<!-- Color CSS -->
<link rel="stylesheet" href= "controller/css/themes.css" >
<!--CSS BMWatch-->
<link rel="stylesheet" href= "controller/css/bmwatch.css" >


<!-- jQuery -->
<script src= "controller/js/jquery.min.js" ></script>
<!-- Bootstrap -->
<script src= "controller/js/bootstrap.min.js" ></script>
<!-- Nice Scroll -->
<script src= "controller/js/plugins/nicescroll/jquery.nicescroll.min.js" ></script>
<!-- imagesLoaded -->
<script src= "controller/js/plugins/imagesLoaded/jquery.imagesloaded.min.js" ></script>
<!-- jQuery UI -->
<script src= "controller/js/plugins/jquery-ui/jquery.ui.core.min.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.widget.min.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.mouse.min.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.resizable.min.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.sortable.min.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.spinner.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.slider.js" ></script>
<script src= "controller/js/plugins/jquery-ui/jquery.ui.datepicker.min.js" ></script>
<!-- slimScroll -->
<script src= "controller/js/plugins/slimscroll/jquery.slimscroll.min.js" ></script>

<!-- Bootbox -->
<script src= "controller/js/plugins/form/jquery.form.min.js" ></script>
<script src= "controller/js/plugins/bootbox/jquery.bootbox.js" ></script>
<!-- Masked inputs -->
<script src= "controller/js/plugins/maskedinput/jquery.maskedinput.min.js" ></script>
<!-- TagsInput -->
<script src= "controller/js/plugins/tagsinput/jquery.tagsinput.min.js" ></script>
<!-- Timepicker -->
<script src= "controller/js/plugins/timepicker/bootstrap-timepicker.min.js" ></script>
<!-- Colorpicker -->
<script src= "controller/js/plugins/colorpicker/bootstrap-colorpicker.js" ></script>
<!-- MultiSelect -->
<script src= "controller/js/plugins/multiselect/jquery.multi-select.js" ></script>
<!-- Validation -->
<script src= "controller/js/plugins/validation/jquery.validate.min.js" ></script>
<script src= "controller/js/plugins/validation/additional-methods.min.js" ></script>
<!-- Custom file upload -->
<script src= "controller/js/plugins/fileupload/bootstrap-fileupload.min.js" ></script>
<!-- Notify -->
<script src= "controller/js/plugins/gritter/jquery.gritter.js" ></script>
<!-- dataTables -->
<script src= "controller/js/plugins/datatable/jquery.dataTables.min.js" ></script>
<script src= "controller/js/plugins/datatable/TableTools.min.js" ></script>
<script src= "controller/js/plugins/datatable/ColReorderWithResize.js" ></script>
<script src= "controller/js/plugins/datatable/ColVis.min.js" ></script>
<script src= "controller/js/plugins/datatable/jquery.dataTables.columnFilter.js" ></script>
<script src= "controller/js/plugins/datatable/jquery.dataTables.grouping.js" ></script>
<!-- Chosen -->
<script src= "controller/js/plugins/chosen/chosen.jquery.min.js" ></script>
<!-- Form -->
<script src= "controller/js/plugins/form/jquery.form.min.js" ></script>
<!--Wizard--> 
<script src= "controller/js/plugins/wizard/jquery.form.wizard.min.js" ></script>
<!-- select2 -->
<script src= "controller/js/plugins/select2/select2.min.js" ></script>
<!-- icheck -->
<script src= "controller/js/plugins/icheck/jquery.icheck.min.js" ></script>
<!-- complexify -->
<script src= "controller/js/plugins/complexify/jquery.complexify-banlist.min.js" ></script>
<script src= "controller/js/plugins/complexify/jquery.complexify.min.js" ></script>
<!-- Mockjax -->
<script src= "controller/js/plugins/mockjax/jquery.mockjax.js" ></script>

<!-- Theme framework -->
<script src= "controller/js/eakroko.min.js" ></script>
<!-- Theme scripts -->
<script src= "controller/js/application.min.js" ></script>

<!--<script src= "controller/js/custom.js" ></script>-->


<!--[if lte IE 9]>
    <script src= "controller/js/plugins/placeholder/jquery.placeholder.min.js" ></script>
    <script>
        $(document).ready(function() {
            $('input, textarea').placeholder();
        });
    </script>
<![endif]-->

<!-- Favicon -->
<link rel="shortcut icon" href="controller/img/favicon.ico" />
<!-- Apple devices Homescreen icon -->
<link rel="apple-touch-icon-precomposed" href="controller/img/apple-touch-icon-precomposed.png" />

<script src= "controller/js/bmwatch_validations.js" ></script>

<?php
    if(isset($addLib)){
        echo $addLib;
    }
?>


    
