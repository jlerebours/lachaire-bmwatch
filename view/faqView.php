<!DOCTYPE html>
<html>
    <head>
        <title>FAQ</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1><?=$voc_faq?></h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box box-bordered box-color">
                        <div class="box-title">
                            <h3>
                                <i class="fa fa-bars"></i>
                                <?=$voc_questions?>
                            </h3>
                        </div>
                        <div class="box-content nopadding">
                            <div class="tabs-container">
                                <ul class="tabs tabs-inline tabs-left">
                                    <?php $categories = Faq::getListeCategories();
                                    $i = 0;
                                    foreach ($categories as $categorie) {
                                        if($i == 0){
                                            echo "<li class='active'>";
                                        }
                                        else{
                                            echo "<li>";
                                        }
                                        echo "<a href='#".$i."' data-toggle='tab'>";
                                        echo "<i class='fa fa-lock'></i> ".$categorie."</a>";
                                        echo "</li>";
                                        $i+=1;
                                    } ?>
                                </ul>
                            </div>
                            <div class="tab-content padding tab-content-inline" style="min-height:100px;">
                                <?php
                                $i=0;
                                foreach ($categories as $categorie) {
                                    $classActive='';
                                    if($i == 0){
                                        $classActive='active';
                                    }
                                    echo "<div class='tab-pane ". $classActive."' id='".$i."'>";
                                        echo "<div class='panel-group panel-widget' id='ac".$i."'>";
                                        $j=0;
                                        foreach (Faq::getListeParCategorie($categorie) as $faq) {
                                            echo "<div class='panel panel-default'>"
                                                ."<div class='panel-heading'>"
                                                    ."<h4 class='panel-title'>"
                                                        ."<a href='#c".$i.$j."' data-toggle='collapse' data-parent='#ac".$i."'>".$faq->getQuestion()."</a>"
                                                    ."</h4>"
                                                ."</div>"
                                            ."</div>";
                                            echo "<div id='c".$i.$j."' class='panel-collapse collapse'>"
                                                ."<div class='panel-body'>".$faq->getReponse()."</div>"
                                            ."</div>";
                                            $j+=1;
                                        }
                                        echo "</div>";
                                    echo "</div>";
                                    $i+=1;
                                }
                                ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>