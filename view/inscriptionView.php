<!doctype html>
<html>


    <body>
        <script src="controller/js/bmwatch_inscription.js"></script>
        <script src="controller/js/bmwatch_validations.js"></script>
        <script src="controller/js/bmwatch_inscriptionSociale.js"></script>
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                        <div class="pull-left">
                            <h1><a href="connexion">BM Watch</a></h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span12">
                            <div class="box">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa glyphicon-book_open"></i>
                                        <?= $voc_inscription ?>
                                    </h3>
                                </div>
                                <div class="box-content">
                                    <form action="inscription" method="POST" class='form-horizontal form-wizard form-validation' id="ss">

                                        <div class="step" id="firstStep">
                                            <ul class="wizard-steps steps-3">
                                                <li class='active'>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            1</span>
                                                        <span class="circle">
                                                            <span class="active"></span>
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationPersonnelle ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            2</span>
                                                        <span class="circle">
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationEntreprise ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            3</span>
                                                        <span class="circle">
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationProduit ?>
                                                        </span>
                                                    </div>
                                                </li>

                                            </ul>
                                            <div class="step-forms">
                                                <div id=boiteChoix <?php if ($sociale == true) echo "hidden = true"; ?>>
                                                    <div class="form-group">
                                                        <label for="choixInscription" class="control-label col-sm-2"><?=$voc_choixInscription?></label>
                                                        <div class="col-sm-10">
                                                            <select name="choixInscription" id="choixInscription" class="form-control"   >
                                                                <option value="normale"><?=$voc_inscriptionBasic?></option>
                                                                <option value="sociale"><?=$voc_inscriptionReseausociaux?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <center>
                                                            <div id="janrainEngageEmbed" hidden=true ></div>                                   
                                                        </center>                                       
                                                    </div>
                                                </div>

                                                <div id= "boiteTemporaire">
                                                    <div class="form-group">
                                                        <label for="nomUtilisateur" class="control-label col-sm-2"><?= $voc_nom ?>*</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="nomUtilisateur" id="nomUtilisateur" class="form-control"
                                                            <?php if ($sociale == true) echo "value=" . $default_nom; ?>
                                                                   data-rule-maxlength="30" data-rule-stringCorrect="true" data-rule-required="true" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="prenom" class="control-label col-sm-2"><?= $voc_prenom ?>*</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="prenom" id="prenom" class="form-control"
                                                            <?php if ($sociale == true) echo " value=" . $default_prenom; ?>       
                                                                   data-rule-maxlength="30" data-rule-stringCorrect="true" data-rule-required="true" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="email" class="control-label col-sm-2"><?= $voc_email ?>*</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="email" id="email" class="form-control" <?php if(isset($default_email)) echo "value =".$default_email; ?> data-rule-email="true" data-rule-required="true" data-rule-emailExistePas = "true">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="confirmEmail" class="control-label col-sm-2"><?= $voc_repeterEmail ?>*</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" name="confirmEmail" id="confirmEmail" class="form-control" <?php if(isset($default_email)) echo "value =".$default_email; ?> data-rule-equalTo="#email" data-rule-required="true">
                                                        </div>
                                                    </div>
                                                    
                                                        <div class="form-group">
                                                            <label for="mdp" class="control-label col-sm-2"><?= $voc_motDePasse ?>*</label>
                                                            <div class="col-sm-10">
                                                                <input type="password" name="mdp" id="mdp" class="form-control" data-rule-required="true" data-rule-mdpCorrect="true">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="confirmMdp" class="control-label col-sm-2"><?= $voc_repeterMotDePasse ?>*</label>
                                                            <div class="col-sm-10">
                                                                <input type="password" name="confirmMdp" id="confirmMdp" class="form-control" data-rule-equalTo="#mdp" data-rule-required="true">
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                                <input type="hidden" name="identifiantSocial" id="identifiantSocial" class="form-control" <?php if ($sociale == true) echo"value = " . $default_identifier; ?>>
                                            </div>
                                        </div>
                                        <div class="step" id="secondStep">
                                            <ul class="wizard-steps steps-3">
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            1</span>
                                                        <span class="circle">

                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationPersonnelle ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class='active'>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            2</span>
                                                        <span class="circle">
                                                            <span class="active"></span>
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationEntreprise ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            3</span>
                                                        <span class="circle">
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationProduit ?>
                                                        </span>
                                                    </div>
                                                </li>

                                            </ul>
                                            <div class="form-group">
                                                <label for="text" class="control-label col-sm-2"><?= $voc_categorie ?>*</label>
                                                <div class="col-sm-10">
                                                    <select  name="categorieEntreprise" id="categorieEntreprise" data-rule-required="true" class='form-control'>
                                                    <option value=''><?=$voc_chooseOne?></option>"
                                                        <?php
                                                    $tabCategories = Entite::obtenirCategories(1);
                                                    for ($i = 0; $i < count($tabCategories); $i++) {
                                                        $chaineEspace = "";
                                                        $niveau = $tabCategories[$i][5];
                                                        if ($niveau != 0) {
                                                            $chaineEspace.=str_repeat("&nbsp;", $niveau * 5);
                                                            $categories = $chaineEspace . "|_" . $tabCategories[$i][3];
                                                        } else {
                                                            $categories = $tabCategories[$i][3];
                                                        }
                                                        echo "<option value='" . $tabCategories[$i][1] . "'> " . $categories . " </option>";
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptionEntreprise" class="control-label col-sm-2"><?= $voc_description ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="descriptionEntreprise" id="descriptionEntreprise" class="form-control"  data-rule-maxlength="250" data-rule-stringCorrect="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nomEntreprise" class="control-label col-sm-2"><?= $voc_Nom ?>*</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nomEntreprise" id="nomEntreprise" class="form-control" data-rule-required="true" data-rule-maxlength="50" data-rule-stringCorrect="true" data-rule-nomEntrepriseValide="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="lienEntreprise" class="control-label col-sm-2"><?= $voc_liens ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="lienEntreprise" id="lienEntreprise" class="form-control"  data-rule-maxlength="256" data-rule-url="true">
                                                </div>
                                            </div>                                            
                                            <div class="form-group">
                                                <label for="formeSociale" class="control-label col-sm-2"><?= $voc_formeSociale ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="formeSociale" id="formeSociale" class="form-control"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="secteurActivite" class="control-label col-sm-2"><?= $voc_secteurActivite ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="secteurActivite" id="secteurActivite" class="form-control" data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="technologiesUtilisees" class="control-label col-sm-2"><?= $voc_technologiesUtilisees ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="technologiesUtilisees" id="technologiesUtilisees" class="form-control" data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>

                                            <?php
                                            $tabCriteres = Entite::obtenirCriteres();
                                            echo "<input name = 'nbCriteres' id='nbCriteres' value='" . count($tabCriteres) . "' hidden=true />";
                                            for ($i = 0; $i < count($tabCriteres); $i++) {
                                                if($tabCriteres[$i][3] != 2){
                                                    $tabValeurs = Entite::obtenirValeursCritere($tabCriteres[$i][1]);
                                                    echo "<div class='form-group'>";

                                                    echo "
                                                    <input name = 'typeCritere" . $tabCriteres[$i][1] . "' id='typeCritere" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][2] . "' hidden=true />
                                                    <input name = 'valeurCritere" . $tabCriteres[$i][1] . "' id='valeurCritere" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][1] . "' hidden=true />
                                                    <label for='text' class='control-label col-sm-2'>" . $tabCriteres[$i][2] . "*</label>";
                                                    switch ($tabValeurs[0][2]) {
                                                        case "NUMERIQUE":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critere" . $tabCriteres[$i][1] . "' name = 'critere" . $tabCriteres[$i][1] . "' class='critere form-control' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true' />
                                                                </div>";
                                                            break;

                                                        case "CLEF":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critere" . $tabCriteres[$i][1] . "' name = 'critere" . $tabCriteres[$i][1] . "' class=' tagsinput critere form-control' value='PHP,JQUERY'  />
                                                                </div>";
                                                            break;

                                                        default:
                                                            echo "<div class='col-sm-10'>
                                                                   <select  name='critere" . $tabCriteres[$i][1] . "' id='critere" . $tabCriteres[$i][1] . "' class ='critere form-control' data-rule-required='true'>
                                                                      <option value=''>".$voc_chooseOne."</option>";
                                                            for ($j = 0; $j < count($tabValeurs); $j++) {
                                                                echo "<option value='" . $tabValeurs[$j][1] . "'>" . $tabValeurs[$j][2] . "</option>";
                                                            }
                                                            echo "</select>
                                                                </div> ";
                                                    }
                                                    echo "</div>";
                                                }
                                            }


                                            echo "<div class='form-group'>
                                              <label for='profils' class='control-label col-sm-2'>" . $voc_BMs . "*</label>";
                                            $tabProfil = BM::obtenirProfil();

                                            echo "<div class='col-sm-10'>
                                                      <select  name='profils' id='profils' class ='choixProfil aTransfererSelect form-control' data-rule-required='true'>
                                                      <option value=''>".$voc_chooseOne."</option>";
                                            for ($j = 0; $j < count($tabProfil); $j++) {
                                                echo "<option value='" . $tabProfil[$j][1] . "'>" . $tabProfil[$j][2] . "</option>";
                                            }
                                            echo "<option value='autre'>" . $voc_autre . "</option>
                                                      </select>
                                                    </div>
                                             </div>";
                                            ?>



                                            <div class="form-group">
                                                <label for="nomBm" class="control-label col-sm-2"><?= $voc_BMNom ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nomBm" id="nomBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="principeBm" class="control-label col-sm-2"><?= $voc_BMPrincipe ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="principeBm" id="principeBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="declinaisonsBm" class="control-label col-sm-2"><?= $voc_BMDeclinaison ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="declinaisonsBm" id="declinaisonsBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>                                     
                                            <div class="form-group">
                                                <label for="variantesBm" class="control-label col-sm-2"><?= $voc_BMVariante ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="variantesBm" id="variantesBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                </div>
                                            </div>                                     
                                            <?php
                                            $tabDimension = BM::obtenirDimension();
                                            echo "<input name = 'nbDimension' id='nbDimension' value='" . count($tabDimension) . "' hidden=true />";
                                            for ($i = 1; $i <= count($tabDimension); $i++) {
                                                $tabOcc = BM::obtenirOccurenceAvecDim($tabDimension[$i][1]);

                                                echo "<div class='form-group'>
                                                <label for='boite" . $i . "' class='control-label col-sm-2'>" . $tabDimension[$i][2] . "*</label>
                                                <div class='col-sm-10'>
                                                  <select  name='boite" . $i . "' id='boite" . $i . "' class ='choixDim  aTransfererSelect boiteEntreprise boite form-control' data-rule-required='true' >
                                                    <option value=''>-- Chose one --</option>
                                                    <option value='null'>Non connu</option>";
                                                for ($j = 0; $j < count($tabOcc); $j++) {
                                                    echo "<option value='" . $tabOcc[$j][1] . "' id='boite" . $i . "Occ" . $tabOcc[$j][1] . "'>" . $tabOcc[$j][3] . "</option>";
                                                }
                                                echo "<option value='autre'>" . $voc_autre . "</option>
                                                  </select>
                                                </div>
                                              
                                               </div>
                                               <div id='occurnenceAutreboite" . $i . "' hidden='true'>
                                                  <div class='form-group'>                                                 
                                                    <label for='Selectedboite" . $i . "' class='control-label col-sm-2'>Nom de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <input type='text' id='Selectedboite" . $i . "' name='Selectedboite" . $i . "' class='boiteDim aTransfererInput form-control'  data-rule-required='true' data-rule-maxlength='50' data-rule-stringCorrect='true' />
                                                    </div>
                                                  </div>
                                                   <div class='form-group'>                                                 
                                                    <label for='descriptionSelectedboite" . $i . "' class='control-label col-sm-2'>Description de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <textarea type='text' id='descriptionSelectedboite" . $i . "' style='resize:none' name='descriptionSelectedboite" . $i . "' class='boiteDim aTransfererInput form-control'  data-rule-required='true' data-rule-maxlength='250' data-rule-stringCorrect='true' ></textarea>
                                                    </div>
                                                  </div>                                                 
                                               </div>
                                                <div class='form-group'>                                                 
                                                  <label for='commentaireboite" . $i . "' class='control-label col-sm-2'>Commentaire de l occurence*</label>
                                                  <div class='col-sm-10'>
                                                     <input type='text' id='commentaireboite" . $i . "' name='commentaireboite" . $i . "' class='commentaire boiteDim aTransfererInput form-control'  data-rule-maxlength='250' data-rule-stringCorrect='true' />
                                                  </div>
                                                </div>";
                                            }
                                            ?>



                                        </div>
                                        <div class="step" id="thirdStep">
                                            <ul class="wizard-steps steps-3">
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            1</span>
                                                        <span class="circle">

                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationPersonnelle ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            2</span>
                                                        <span class="circle">

                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationEntreprise ?>
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class='active'>
                                                    <div class="single-step">
                                                        <span class="title">
                                                            3</span>
                                                        <span class="circle">
                                                            <span class="active"></span>
                                                        </span>
                                                        <span class="description">
                                                            <?= $voc_informationProduit ?>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="form-group">
                                                <label for="afficherProduit" class="control-label col-sm-2"><?=$voc_ajouterProduit?></label>
                                                <div class="col-sm-10">
                                                    <select name="afficherProduit" id="afficherProduit" class="form-control">
                                                    <option value="0"><?=$voc_non?></option>
                                                    <option value="1"><?=$voc_oui?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="infosProduit" hidden="true">
                                                <div class="form-group">
                                                    <label for="text" class="control-label col-sm-2"><?= $voc_categorie ?>*</label>
                                                    <div class="col-sm-10">
                                                        <select  name="categorieProduit" id="categorieProduit" data-rule-required="true" class='form-control'>
                                                        <option value=''><?=$voc_chooseOne?></option>
                                                        <?php
                                                        $tabCategories = Entite::obtenirCategories(2);
                                                        for ($i = 0; $i < count($tabCategories); $i++) {
                                                            $chaineEspace = "";
                                                            $niveau = $tabCategories[$i][5];
                                                            if ($niveau != 0) {
                                                                $chaineEspace.=str_repeat("&nbsp;", $niveau * 5);
                                                                $categories = $chaineEspace . "|_" . $tabCategories[$i][3];
                                                            } else {
                                                                $categories = $tabCategories[$i][3];
                                                            }
                                                            echo "<option value='" . $tabCategories[$i][1] . "' >" . $categories . "</option>";
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="descriptionProduit" class="control-label col-sm-2"><?= $voc_description ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="descriptionProduit" id="descriptionProduit" class="form-control" data-rule-maxlength='250' data-rule-stringCorrect='true'>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nomProduit" class="control-label col-sm-2"><?= $voc_Nom ?>*</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="nomProduit" id="nomProduit" class="form-control" data-rule-required="true" data-rule-nomProduitValide="true" data-rule-maxlength='30' data-rule-stringCorrect='true'>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lienProduit" class="control-label col-sm-2"><?= $voc_liens ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="lienProduit" id="lienProduit" class="form-control"  data-rule-maxlength="256" data-rule-url="true">
                                                    </div>
                                                </div> 
                                                
                                                <?php
                                            for ($i = 0; $i < count($tabCriteres); $i++) {
                                                $tabValeurs = Entite::obtenirValeursCritere($tabCriteres[$i][1]);
                                                if($tabCriteres[$i][3] != 1){
                                                    echo "<div class='form-group'>";

                                                    echo "
                                                    <input name = 'typeCritereProduit" . $tabCriteres[$i][1] . "' id='typeCritereProduit" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][2] . "' hidden=true />
                                                    <input name = 'valeurCritereProduit" . $tabCriteres[$i][1] . "' id='valeurCritereProduit" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][1] . "' hidden=true />
                                                    <label for='text' class='control-label col-sm-2'>" . $tabCriteres[$i][2] . "*</label>";
                                                    switch ($tabValeurs[0][2]) {
                                                        case "NUMERIQUE":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critereProduit" . $tabCriteres[$i][1] . "' name = 'critereProduit" . $tabCriteres[$i][1] . "' class='critere form-control' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true' />
                                                                </div>";
                                                            break;

                                                        case "CLEF":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critereProduit" . $tabCriteres[$i][1] . "' name = 'critereProduit" . $tabCriteres[$i][1] . "' class=' tagsinput critere form-control' value='PHP,JQUERY'  />
                                                                </div>";
                                                            break;

                                                        default:
                                                            echo "<div class='col-sm-10'>
                                                                   <select  name='critereProduit" . $tabCriteres[$i][1] . "' id='critereProduit" . $tabCriteres[$i][1] . "' class ='critere form-control' data-rule-required='true'>
                                                                      <option value=''>".$voc_chooseOne."</option>";
                                                            for ($j = 0; $j < count($tabValeurs); $j++) {
                                                                echo "<option value='" . $tabValeurs[$j][1] . "'>" . $tabValeurs[$j][2] . "</option>";
                                                            }
                                                            echo "</select>
                                                                </div> ";
                                                    }
                                                    echo "</div>";
                                                }
                                            }
                                                echo "<div class='form-group'>
                                              <label for='profilsProduit' class='control-label col-sm-2'>" . $voc_BMs . "*</label>";
                                                $tabProfilProduit = BM::obtenirProfil();

                                                echo "<div class='col-sm-10'>
                                                      <select  name='profilsProduit' id='profilsProduit' class ='choixProfil form-control' data-rule-required='true'/>
                                                        <option value=''>-- Chose one --</option>";
                                                for ($j = 0; $j < count($tabProfilProduit); $j++) {
                                                    echo "<option value='" . $tabProfilProduit[$j][1] . "'>" . $tabProfilProduit[$j][2] . "</option>";
                                                }
                                                echo "<option value='autre'>" . $voc_autre . "</option>
                                                      </select>
                                                    </div>
                                             </div>";
                                                ?>



                                                <div class="form-group">
                                                    <label for="nomBmProduit" class="control-label col-sm-2"><?= $voc_BMNom ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="nomBmProduit" id="nomBmProduit" class="form-control infoBMProduit"  data-rule-maxlength="50" data-rule-stringCorrect="true" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="principeBmProduit" class="control-label col-sm-2"><?= $voc_BMPrincipe ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="principeBmProduit" id="principeBmProduit" class="form-control infoBMProduit"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="declinaisonsBmProduit" class="control-label col-sm-2"><?= $voc_BMDeclinaison ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="declinaisonsBmProduit" id="declinaisonsBmProduit" class="form-control infoBMProduit"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                    </div>
                                                </div>                                     
                                                <div class="form-group">
                                                    <label for="variantesBmProduit" class="control-label col-sm-2"><?= $voc_BMVariante ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="variantesBmProduit" id="variantesBmProduit" class="form-control infoBMProduit"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                                    </div>
                                                </div>                                     
                                                <?php
                                                $tabDimensionProduit = BM::obtenirDimension();
                                                echo "<input name = 'nbDimensionProduit' id='nbDimensionProduit' value='" . count($tabDimensionProduit) . "' hidden=true />";
                                                for ($i = 1; $i <= count($tabDimensionProduit); $i++) {
                                                    $tabOccProduit = BM::obtenirOccurenceAvecDim($tabDimensionProduit[$i][1]);

                                                    echo "<div class='form-group'>
                                                <label for='boiteProduit" . $i . "' class='control-label col-sm-2'>" . $tabDimensionProduit[$i][2] . "*</label>
                                                <div class='col-sm-10'>
                                                  <select  name='boite" . $i . "Produit' id='boite" . $i . "Produit' class ='choixDim boiteProduit form-control' data-rule-required='true'/>
                                                    <option value=''>-- Chose one --</option>
                                                    <option value='null'>Non connu</option>";
                                                    for ($j = 0; $j < count($tabOccProduit); $j++) {
                                                        echo "<option value='" . $tabOccProduit[$j][1] . "' id='boite" . $i . "Occ" . $tabOccProduit[$j][1] . "Produit'>" . $tabOccProduit[$j][3] . "</option>";
                                                    }
                                                    echo "<option value='autre' id='boite" . $i . "OccAutreProduit'>" . $voc_autre . "</option>
                                                  </select>
                                                </div>
                                              
                                               </div>
                                               <div id='occurnenceAutreboite" . $i . "Produit' hidden='true'>
                                                  <div class='form-group'>                                                
                                                    <label for='Selectedboite" . $i . "Produit' class='control-label col-sm-2'>Nom de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <input type='text' id='Selectedboite" . $i . "Produit' name='Selectedboite" . $i . "Produit' class='boiteDim form-control'  data-rule-required='true' data-rule-maxlength='50' data-rule-stringCorrect='true' />
                                                    </div>
                                                  </div>
                                                  <div class='form-group'>                                                 
                                                    <label for='descriptionSelectedboite" . $i . "Produit' class='control-label col-sm-2'>Description de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <textarea type='text' id='descriptionSelectedboite" . $i . "Produit' style='resize:none' name='descriptionSelectedboite" . $i . "Produit' class='boiteDim form-control'  data-rule-required='true' data-rule-maxlength='250' data-rule-stringCorrect='true' ></textarea>
                                                    </div>
                                                  </div>
                                               </div>
                                                    
                                                    
                                                <div class='form-group'>                                                 
                                                  <label for='commentaireboite" . $i . "Produit' class='control-label col-sm-2'>Commentaire de l occurence*</label>
                                                  <div class='col-sm-10'>
                                                     <input type='text' id='commentaireboite" . $i . "Produit' name='commentaireboite" . $i . "Produit' class='commentaireProduit choixDim boiteDim aTransfererInput form-control'  data-rule-maxlength='250' data-rule-stringCorrect='true' />
                                                  </div>
                                                </div>";
                                                }
                                                ?>

                                            </div>
                                        </div>
                                        <input name = 'nomFormulaire' id='nomFormaulaire' value='inscriptionPorteur' hidden=true />
                                        <div class="form-actions">
                                            <input type="reset" class="btn" value="Back" id="back">

                                            <input type="submit" class="btn btn-primary" value="Submit" id="next" >

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>