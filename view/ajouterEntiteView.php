<!doctype html>
<html>


    <body>
        <script src="controller/js/bmwatch_inscription.js"></script>
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="row">
                        <div class="span12">
                            <form action="porteur" method="POST" class='form-horizontal form-validate' id="formAjout" >
                                <div class="box">
                                    <div class="box-title">
                                        <h3>
                                            <i class="fa glyphicon-book_open"></i>
                                            <?= $titre ?>
                                        </h3>
                                    </div>
                                    <div class="box-content">


                                        <div class="form-group">
                                            <label for="text" class="control-label col-sm-2"><?= $voc_categorie ?>*</label>
                                            <div class="col-sm-10">
                                                <select  name="categorie" id="categorieEntreprise" data-rule-required="true" class='form-control'>
                                                    <option value="">-- Chose one --</option>
                                                    <?php
                                                    $tabCategories = Entite::obtenirCategories($type);
                                                    for ($i = 0; $i < count($tabCategories); $i++) {
                                                        $chaineEspace = "";
                                                        $niveau = $tabCategories[$i][5];
                                                        if ($niveau != 0) {
                                                            $chaineEspace.=str_repeat("&nbsp;", $niveau * 5);
                                                            $categories = $chaineEspace . "|_" . $tabCategories[$i][3];
                                                        } else {
                                                            $categories = $tabCategories[$i][3];
                                                        }
                                                        echo "<option value='" . $tabCategories[$i][1] . "'> " . $categories . " </option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="description" class="control-label col-sm-2"><?= $voc_description ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="description" id="description" class="form-control"  data-rule-maxlength="250" data-rule-stringCorrect="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nom" class="control-label col-sm-2"><?= $voc_Nom ?>*</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nom" id="nom" class="form-control" data-rule-required="true" data-rule-maxlength="50" data-rule-stringCorrect="true" data-rule-nomEntrepriseValide="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="lien" class="control-label col-sm-2"><?= $voc_liens ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="lien" id="lien" class="form-control"  data-rule-maxlength="256" data-rule-url="true" >
                                            </div>
                                        </div> 

                                        <div class="form-group" <?php if ($produit) echo"hidden=true"; ?>>
                                            <label for="formeSociale" class="control-label col-sm-2"><?= $voc_formeSociale ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="formeSociale" id="formeSociale" class="form-control"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>
                                        <div class="form-group" <?php if ($produit) echo"hidden=true"; ?>>
                                            <label for="secteurActivite" class="control-label col-sm-2"><?= $voc_secteurActivite ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="secteurActivite" id="secteurActivite" class="form-control" data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>
                                        <div class="form-group" <?php if ($produit) echo"hidden=true"; ?>>
                                            <label for="technologiesUtilisees" class="control-label col-sm-2"><?= $voc_technologiesUtilisees ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="technologiesUtilisees" id="technologiesUtilisees" class="form-control" data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>

                                       
                                            <?php
                                            $tabCriteres = Entite::obtenirCriteres();
                                            echo "<input name = 'nbCriteres' id='nbCriteres' value='" . count($tabCriteres) . "' hidden=true />";
                                            for ($i = 0; $i < count($tabCriteres); $i++) {
                                                
                                                if($tabCriteres[$i][3]==$type||$tabCriteres[$i][3]==NULL){
                                                    $tabValeurs = Entite::obtenirValeursCritere($tabCriteres[$i][1]);
                                                    echo "<div class='form-group'>
                                                        <input name = 'typeCritere" . $tabCriteres[$i][1] . "' id='typeCritere" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][2] . "' hidden=true />
                                                        <input name = 'valeurCritere" . $tabCriteres[$i][1] . "' id='valeurCritere" . $tabCriteres[$i][1] . "' value='" . $tabValeurs[0][1] . "' hidden=true />
                                                        <label for='text' class='control-label col-sm-2'>" . $tabCriteres[$i][2] . "*</label>";
                                                    switch ($tabValeurs[0][2]) {
                                                        case "NUMERIQUE":
                                                            echo "<div class='col-sm-10'>
                                                                       <input type='text' id='critere" . $tabCriteres[$i][1] . "' name = 'critere" . $tabCriteres[$i][1] . "' class='critere form-control' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true' />
                                                                    </div>";
                                                            break;

                                                        case "CLEF":
                                                            echo "<div class='col-sm-10'>
                                                                       <input type='text' id='critere" . $tabCriteres[$i][1] . "' name = 'critere" . $tabCriteres[$i][1] . "' class=' tagsinput critere form-control' value='PHP,JQUERY'  />
                                                                    </div>";
                                                            break;

                                                        default:
                                                            echo "<div class='col-sm-10'>
                                                                       <select  name='critere" . $tabCriteres[$i][1] . "' id='critere" . $tabCriteres[$i][1] . "' class ='critere form-control' data-rule-required='true' >
                                                                          <option value=''>-- Chose one --</option>";
                                                            for ($j = 0; $j < count($tabValeurs); $j++) {
                                                                echo "<option value='" . $tabValeurs[$j][1] . "'>" . $tabValeurs[$j][2] . "</option>";
                                                            }
                                                            echo "</select>
                                                                    </div> ";
                                                    }
                                                    echo "</div>";
                                                }
                                            }
                                            ?>
                                        


                                        <?php
                                        echo "<div class='form-group'>
                                              <label for='profils' class='control-label col-sm-2'>" . $voc_BMs . "*</label>";
                                        $tabProfil = BM::obtenirProfil();

                                        echo "<div class='col-sm-10'>
                                                      <select  name='profils' id='profils' class ='choixProfil aTransfererSelect form-control' data-rule-required='true'/>
                                                        <option value=''>-- Chose one --</option>";
                                        for ($j = 0; $j < count($tabProfil); $j++) {
                                            echo "<option value='" . $tabProfil[$j][1] . "'>" . $tabProfil[$j][2] . "</option>";
                                        }
                                        echo "<option value='autre'>" . $voc_autre . "</option>
                                                      </select>
                                                    </div>
                                             </div>";
                                        ?>



                                        <div class="form-group">
                                            <label for="nomBm" class="control-label col-sm-2"><?= $voc_BMNom ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nomBm" id="nomBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="principeBm" class="control-label col-sm-2"><?= $voc_BMPrincipe ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="principeBm" id="principeBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="declinaisonsBm" class="control-label col-sm-2"><?= $voc_BMDeclinaison ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="declinaisonsBm" id="declinaisonsBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>                                     
                                        <div class="form-group">
                                            <label for="variantesBm" class="control-label col-sm-2"><?= $voc_BMVariante ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="variantesBm" id="variantesBm" class="form-control aTransfererInput infoBM"  data-rule-maxlength="50" data-rule-stringCorrect="true">
                                            </div>
                                        </div>                                     
                                        <?php
                                        $tabDimension = BM::obtenirDimension();
                                        echo "<input name = 'nbDimension' id='nbDimension' value='" . count($tabDimension) . "' hidden=true />";
                                        for ($i = 1; $i <= count($tabDimension); $i++) {
                                            $tabOcc = BM::obtenirOccurenceAvecDim($tabDimension[$i][1]);

                                            echo "<div class='form-group'>
                                                <label for='boite" . $i . "' class='control-label col-sm-2'>" . $tabDimension[$i][2] . "*</label>
                                                <div class='col-sm-10'>
                                                  <select  name='boite" . $i . "' id='boite" . $i . "' class ='choixDim  aTransfererSelect boite boite form-control' data-rule-required='true'/>
                                                    <option value=''>-- Chose one --</option>
                                                    <option value='null'>Non connu</option>";
                                            for ($j = 0; $j < count($tabOcc); $j++) {
                                                echo "<option value='" . $tabOcc[$j][1] . "' id='boite" . $i . "Occ" . $tabOcc[$j][1] . "'>" . $tabOcc[$j][3] . "</option>";
                                            }
                                            echo "<option value='autre'>" . $voc_autre . "</option>
                                                  </select>
                                                </div>
                                              
                                               </div>
                                               <div id='occurnenceAutreboite" . $i . "' hidden='true'>
                                                  <div class='form-group'>                                                 
                                                    <label for='Selectedboite" . $i . "' class='control-label col-sm-2'>Nom de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <input type='text' id='Selectedboite" . $i . "' name='Selectedboite" . $i . "' class='boiteDim aTransfererInput form-control'  data-rule-required='true' data-rule-maxlength='50' data-rule-stringCorrect='true' />
                                                    </div>
                                                  </div>
                                                   <div class='form-group'>                                                 
                                                    <label for='descriptionSelectedboite" . $i . "' class='control-label col-sm-2'>Description de l occurence*</label>
                                                    <div class='col-sm-10'>
                                                       <textarea type='text' id='descriptionSelectedboite" . $i . "' style='resize:none' name='descriptionSelectedboite" . $i . "' class='boiteDim aTransfererInput form-control'  data-rule-required='true' data-rule-maxlength='250' data-rule-stringCorrect='true' ></textarea>
                                                    </div>
                                                  </div>                                                 
                                               </div>
                                                <div class='form-group'>                                                 
                                                  <label for='commentaireboite" . $i . "' class='control-label col-sm-2'>Commentaire de l occurence*</label>
                                                  <div class='col-sm-10'>
                                                     <input type='text' id='commentaireboite" . $i . "' name='commentaireboite" . $i . "' class='commentaire boiteDim aTransfererInput form-control'  data-rule-maxlength='250' data-rule-stringCorrect='true' />
                                                  </div>
                                                </div>";
                                        }
                                        ?>



                                    </div>  
                                    <input type="hidden"  name="idEntreprise" value="<?= $id ?>" />
                                    <div class="form-actions">
                                        <input type="reset" class="btn" value="Back" id="back">

                                        <input type="submit" class="btn btn-primary" value="Submit" id="ajouter" name="<?= $action ?>" />

                                    </div>
                                </div>
                            </form>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

