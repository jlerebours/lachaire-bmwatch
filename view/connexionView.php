<!DOCTYPE html>
<html> 
    <head>
    </head>
    <body class='login'>
        <script src="controller/js/bmwatch_connexionSociale.js"></script>
        <div class="wrapper">
            <h1>
                <a href="accueil">
                <img src="controller/img/logo-big.png" alt="" class='retina-ready' width="59" height="49">BMWatch</a>
            </h1>
            <!--Corps de la connexion-->
            <div class="login-body">
                <h2><?=$voc_connexion ?></h2>
                <?php if(isset($connexionOk) && !$connexionOk){ ?>
                <div id="divAlertEmailFaux" class="alert alert-danger alert-dismissable">
                    <strong><?=$voc_warning?> ! </strong><?=$errorMSG?>
                </div>
                <?php } ?>
                <center>
                    <button href="#modal-3" class=" btn btn-green btn-large" data-toggle="modal" style="width : 150px"><?=$voc_basic?></button>
                </center>
                </br>
                <center>
                    <button href="#modal-2" class=" btn btn-blue btn-large" data-toggle="modal" style="width : 150px"><?=$voc_reseauSocial?></button>
                </center>
                <div class="forget">
                    <a href='inscription' role="button" class="btn"><?=$voc_pasInscrit ?></a>
                    <a href="#modal-1" role="button" class="btn" data-toggle="modal"><?=$voc_mdpOublie?></a>
                </div>
            </div>
            
             <!--Fenetre modale pour mot de passe oublié-->
            <div id="modal-1" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=$voc_fermer?></span></button>
                        <h4 class="modal-title"><?=$voc_mdpOublie?></h4>
                    </div>
                    <form id="formPopupMdpOublie" class='form-horizontal form-striped form-validate'>
                        <div class="form-group">
                            <div class="modal-body">
                                <div id="divPopupEmailFaux" style="display:none" class="alert alert-danger alert-dismissable">
                                    <strong><?=$voc_warning?> </strong><?=$voc_emailInexistantPhrase?>
                                </div>
                                <div class="form-group">
                                    <label for="emailPopup" class="control-label col-sm-2 right"><?=$voc_email?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="emailPopup" id="emailPopup" class='form-control' data-rule-email=true > 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="Close">
                            <input type="submit" name="mdpOublie"  class='btn btn-primary inputGrise' value="<?=$voc_envoyerNewMdp?>">
                        </div>
                    </form>
                        <!-- /.modal-footer -->
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
             
            <!--Fenetre modale pour connexion sociale-->            
            <div id="modal-2" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=$voc_fermer?></span></button>
                        <h4 class="modal-title"><?=$voc_reseauSocial?></h4>
                    </div>
                    <form action="" method="POST" id="formSocialPopup" class='form-horizontal form-striped form-validate'>
                        <div class="form-group">
                            <div class="modal-body">
                                <div class="form-group">
                                    <center>
                                        <div id="janrainEngageEmbed"></div>                                   
                                    </center>   
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="Close">
                        </div>
                    </form>
                        <!-- /.modal-footer -->
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            
            <!--Fenetre modale pour la connexion-->            
            <div id="modal-3" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=$voc_fermer?></span></button>
                            <h4 class="modal-title"><?=$voc_connexion?></h4>
                        </div>
                        
                            <form class='form-validate' action="connexion" method="POST" id="formConnexion" > 
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="email" class="control-label col-sm-2 right"><?=$voc_email?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="email" id="email" class='form-control' data-rule-email=true data-rule-required=true> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="pass" class="control-label col-sm-2 right"><?=$voc_motDePasse?></label>
                                            <div class="col-sm-10">
                                                <input type="password" name="pass" id="pass" class='form-control' data-rule-required=true > 
                                            </div>
                                        </div>                                        
                                    </div>    
                                </div>
                            
                                <div class="modal-footer">
                                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?=$voc_fermer?>">
                                    <input type="submit" name="envoiConnexion"  class='btn btn-primary inputGrise' value="<?=$voc_seConnecter?>">
                                </div>
                            </form>
                        
                        <!-- /.modal-footer -->
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
        
 
        <script>
            $(document).ready(function(){
                <?php if(isset($reconnexion)){ ?>
                // Ajout de notif mails ajoutés 
                $.gritter.add({
                    title: 'Reconnection ',
                    text: 'You have been disconnected from the site due to inactivity. </br> Please reconnect to continue.',
                    image: null,
                    sticky : false,
                    time : 2500,
                    class_name: 'gritter-red',
                });
               <?php } ?>
                
            });
            $('#formPopupMdpOublie').validate({
                    errorElement: 'span',
                    errorClass: 'help-block has-error',
                    errorPlacement: function(error, element) {
                        if (element.parents("label").length > 0) {
                            element.parents("label").after(error);
                        } else {
                            element.after(error);
                        }
                    },
                    highlight: function(label) {
                        $(label).closest('.form-group').removeClass('has-error has-success').addClass('has-error');
                    },
                    success: function(label) {
                        label.addClass('valid').closest('.form-group').removeClass('has-error has-success').addClass('has-success');
                    },
                    onkeyup: function(element) {
                        $(element).valid();
                    },
                    onfocusout: function(element) {
                        $(element).valid();
                    },
                    rules:{
                        email:"true"
                    },
                    submitHandler: function(form) {
                        $.post("controller/ajax/verificationsAjax.php",$("#formPopupMdpOublie").serialize(), function(data){
                            if(data != '1'){
                                $('#divPopupEmailFaux').attr('style','display:block');
                            }
                            else{
                                console.log(form);
                                $('#boutonClose').click();
                                $.gritter.add({
                                    title: 'Forgotten password ',
                                    text: 'An email was sent to <strong>'+email+'</strong> with a new password',
                                    image: null,
                                    sticky : false,
                                    time : 2500,
                                    class_name: 'gritter-red',
                                });
                            }
                        });                    
                    }
                });
        </script>
    </body>
</html>