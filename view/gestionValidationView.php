<!DOCTYPE html>
<html>
    <head>
        <title><?=$voc_validation?> <?=$voc_moderation?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>  
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
           <div id="main">
               <div class="container-fluid ">
                   <div class="page-header">
                       <div class="pull-left">
                           <h1><?=$voc_validation?> <?=$voc_moderation?></h1>
                       </div>
                   </div>
                    <div style="margin:15px">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                <a href="#nouveauxPorteurs" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?=$voc_newPorteurs?>
                                </a>
                            </li>
                            <li>
                                <a href="#nouveau" data-toggle='tab'>
                                    <!--<i class="fa fa-user" style="margin-right:5px"></i>-->
                                    <?=$voc_newEntites?>
                                </a>
                            </li>
                            <li>
                                <a href="#modifier" data-toggle='tab'>
                                    <!--<i class="fa fa-bullhorn" style="margin-right:5px"></i>-->
                                    <?=$voc_modifEntites?>
                                </a>
                            </li>
                            <li>
                                <a href="#validationOccurences" data-toggle='tab'>
                                    <!--<i class="fa fa-bullhorn" style="margin-right:5px"></i>-->
                                    <?=$voc_nouvellesOccurences?>
                                </a>
                            </li>
                            
<!--                            <li>
                                <a href="#validationCriteres" data-toggle='tab'>
                                    <i class="fa fa-bullhorn" style="margin-right:5px"></i>
                                    <?=$voc_nouveauxCriteres?>
                                </a>
                            </li>-->
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                            <div class="tab-pane active" id="nouveauxPorteurs">
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3><?=$voc_newPorteurs?></h3>
                                    </div>
                                    <div class="box-content">
                                        <p style="font-size: 16px;font-weight: bold;"><?=$voc_validationPorteurPhrase?></p>
                                        
                                         <table id="tabNewPorteurs" class="table table-hover table-nomargin table-bordered usertable" >
                                             <thead>
                                                 <tr class='thefilter'>
                                                    <th><?=$voc_email?></th>
                                                    <th class='hidden-350'><?=$voc_entreprise?></th>
                                                    <th class='hidden-350'><?=$voc_produit?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                 </tr>
                                                 <tr>
                                                    <th><?=$voc_email?></th>
                                                    <th class='hidden-350'><?=$voc_entreprise?></th>
                                                    <th class='hidden-350'><?=$voc_produit?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                 </tr>
                                             </thead>
                                             <!--On charge ici dynamiquement le contenu--> 
                                             <tbody></tbody>
                                         </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="nouveau">
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3><?=$voc_newEntites?></h3>
                                    </div>
                                    <div class="box-content">
                                         <table id="tabNew" class="table table-hover table-nomargin table-bordered usertable" >
                                             <thead>
                                                 <tr class='thefilter'>
                                                    <th class='with-checkbox'></th>
                                                    <th><?=$voc_nom?></th>
                                                    <th class='hidden-350'><?=$voc_type?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                 </tr>
                                                 <tr>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?=$voc_nom?></th>
                                                    <th class='hidden-350'><?=$voc_type?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                 </tr>
                                             </thead>
                                             <!--On charge ici dynamiquement le contenu--> 
                                             <tbody></tbody>
                                         </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="modifier">
                                <div class="box box-bordered box-color" id='modifTab'>
                                    <div class="box-title">
                                        <h3><?=$voc_modifEntites?></h3>
                                    </div>
                                    <div class="box-content">
                                        <table id="tabModif" class="table table-hover table-nomargin table-bordered usertable" >
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th class='with-checkbox'></th>
                                                    <th><?=$voc_nom?></th>
                                                    <th class='hidden-350'><?=$voc_type?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                                <tr>
                                                    <th class='with-checkbox'>
                                                        <input type="checkbox" name="check_all" id="check_all">
                                                    </th>
                                                    <th><?=$voc_nom?></th>
                                                    <th class='hidden-350'><?=$voc_type?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="validationOccurences">
                                <div class="box box-bordered box-color">
                                    <div class="box-title">
                                        <h3><?=$voc_nouvellesOccurences?></h3>
                                    </div>
                                    <div class="box-content">
                                        <table id="tabOccurences" class="table table-hover table-nomargin table-bordered tabs-3col usertable " >
                                            <thead>
                                                <tr class='thefilter'>
                                                    <th><?=$voc_nom?></th>
                                                    <th><?=$voc_dimensionAssociee?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                                <tr>
                                                    <th><?=$voc_nom?></th>
                                                    <th><?=$voc_dimensionAssociee?></th>
                                                    <th class='hidden-480'><?=$voc_options?></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="boxOccurence" class="box box-bordered box-color" style="display:none">
                                    <div class="box-title">
                                        <h3><?=$voc_affichageOccurence?></h3>
                                    </div>
                                    <div class="box-content">
                                        <form id="formOccurence" class='form-horizontal'>
                                            <div class="form-group">
                                                <label for="nomOccurence" class="control-label col-sm-2"><?=$voc_Nom?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="nomOccurence" name="nomOccurence" class="form-control" data-rule-required="true" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="descriptionOccurence" class="control-label col-sm-2"><?=$voc_description?></label>
                                                <div class="col-sm-10">
                                                    <textarea size="10" type="text" id="descriptionOccurence" name="descriptionOccurence" class="form-control" data-rule-required="true" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button type="button" id="btnValiderOcc" idOcc="" class="btn btn-success" onclick="validerOccurence();"><?=$voc_valider?></button>
                                                <button type="button" class="btn btn-danger" onclick="supprimerOccurence();"><?=$voc_supprimerOcc?></button>
                                                <button type="button" class="btn" onclick="annulerOccurence();"><?=$voc_annuler?></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </body>
</html>
<script src='controller/js/bmwatch_validations_entites.js'></script>

<div id="modal-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><?=$voc_entites?></h4>
            </div>
            <form action="" method="POST" id="formPopup" class='form-horizontal form-striped form-validate'>
                <div class="form-group">
                    <div class="modal-body">
                        <input type="hidden" id="idSup" value="id">
                        <div class="form-group">
                            <label for="verifPopup" class="control-label"><?=$voc_popup?></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?=$voc_non?>">
                    <input type="submit" name="supModif"  class='btn btn-primary inputGrise' id='deleteEntity' value="<?=$voc_oui?>">
                </div>
            </form>
            <!-- /.modal-footer -->
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal-2" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><?=$voc_entites?></h4>
            </div>
            <form action="" method="POST" id="formPopup" class='form-horizontal form-striped form-validate'>
                <div class="form-group">
                    <div class="modal-body">
                        <input type="hidden" id="idSupprimer" value="id">
                        <div class="form-group">
                            <label for="verifPopup" class="control-label"><?=$voc_popup?></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="boutonClose" type="button" class="btn btn-default" data-dismiss="modal" value="<?=$voc_non?>">
                    <input type="submit" name="suppNew"  class='btn btn-primary inputGrise' id='deleteEntityNew' value="<?=$voc_oui?>">
                </div>
            </form>
            <!-- /.modal-footer -->
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

