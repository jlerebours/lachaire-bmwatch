<!DOCTYPE html>
<html>
    <head>
        <title><?=$voc_utilisateurs?> <?=$voc_moderation?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
            <div id="main">
                <div class="container-fluid ">
                    <div class="page-header">
                    <div class="pull-left">
                        <h1><?=$voc_utilisateurs?> <?=$voc_moderation?></h1>
                    </div>
                    </div>
                    <div style="margin:15px">
                        <!--<div id="tabs">-->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class='active'>
                                 <a href="#listePane" data-toggle='tab'>
                                 <i class="fa fa-user"></i><?=$voc_liste?></a>
                            </li>
                            <li>
                                <a href="#ajoutPane" data-toggle='tab' onclick="afficherPane2();">
                                <i class="glyphicon-user_add"></i> <?=$voc_nouveau?></a>
                            </li>
                        </ul>
                        <div class="tab-content padding tab-content-inline tab-content-bottom">
                            <div class="tab-pane active" id="listePane">
                                <div class="row">
                                    <?php // var_dump($listeUtilisateurs); ?>
                                    <table id="tabUtilisateurs" class="table table-hover table-nomargin table-bordered usertable">
                                        <thead>
                                            <tr class='thefilter'>
                                                <th class='with-checkbox'></th>
                                                <th><?=$voc_nom?></th>
                                                <th><?=$voc_prenom?></th>
                                                <th><?=$voc_email?></th>
                                                <th class='hidden-350'><?=$voc_statut?></th>
                                                <th class='hidden-480'><?=$voc_options?></th>
                                            </tr>
                                            <tr>
                                                <th class='with-checkbox'>
                                                    <input type="checkbox" name="check_all" id="check_all">
                                                </th>
                                                <th><?=$voc_nom?></th>
                                                <th><?=$voc_prenom?></th>
                                                <th><?=$voc_email?></th>
                                                <th class='hidden-350'><?=$voc_statut?></th>
                                                <th class='hidden-480'><?=$voc_options?></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane active " id="ajoutPane">
                                <div id="divAjout">
                                    <form class='form-horizontal form-bordered form-validate' id="formAjout">
                                        <div class="form-group">
                                            <label for="selectTypeAjout" class="control-label col-sm-2"><?=$voc_typeUtilisateur?></label>
                                            <div class="col-sm-10">
                                                <select name="selectTypeAjout[]" placeholder='<?=$voc_selectTypeAjout?>' id="selectTypeAjout" class='select2-me' style="width:100%;" multiple="multiple" data-rule-required="true">
                                                    <?php
                                                        //Affichage des utilisateurs associés au moderateur connecté 
                                                        ($droitsAdminOk)?$i=2:$i=3;
                                                        foreach($listeTypes as $type){
                                                            echo "<option value=\"".$i."\">".$type."</option>"; 
                                                            $i += 1;
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div id="divGroupe">
                                                <label for="selectGroupeNP" class="control-label col-sm-2"><?=$voc_groupe?></label>
                                                <div class="col-sm-10">
                                                    <select name="selectGroupeNP" id="selectGroupeNP" class='chosen-select form-control' >
                                                        <option value="Aucun">Aucun</option>"; 
                                                        <?php
                                                            foreach($listeGroupes as $groupe){
                                                                echo "<option value=\"".$groupe."\">".$groupe."</option>"; 
                                                            }
                                                        ?>
                                                    </select>
                                                    <a class="col-sm-10" style="margin:5px;" href="javascript:afficherAjoutGroupe();" ><?=$voc_ajouterGroupe?></a>
                                                </div>
                                            </div>
                                            <div id="divNewGroupe" style="display:none">
                                                <label for="nouveauGroupe" class="control-label col-sm-2"><?=$voc_newGroupe?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" id="nouveauGroupe" name="nouveauGroupe" placeholder='<?=$voc_choisissezGroupe?>' class="form-control" data-rule-required="true"  data-rule-minlength="5">
                                                    <a class="col-sm-10" style="margin:5px;" href="javascript:afficherChoixGroupe();" ><?=$voc_retourListeGroupes?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textarea_emails" class="control-label col-sm-2"><?=$voc_ajoutEmails?></label>
                                            <div class="col-sm-10">
                                                <textarea id="textarea_emails" placeholder='<?=$voc_textareaAjout?>' name="textarea_emails" rows="5" class="form-control" placeholder=""<?=$voc_renseignezEmails?>" data-rule-required="true"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="caractere_separation" class="control-label col-sm-2"><?=$voc_caractereSeparation?></label>
                                            <div class="col-sm-10">
                                                <input type="text" size="1" maxlength="1" id="caractere_separation" name="caractere_separation" value=";" class="form-control" data-rule-required="true">
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <input type="submit" id="btnAjouterMails" class="btn btn-primary" value=<?=$voc_valider?>>
                                            <button type="button" class="btn"><?=$voc_annuler?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </body>
</html>
<script src='controller/js/bmwatch_gestionUtilisateurs.js'></script>