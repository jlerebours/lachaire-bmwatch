<!DOCTYPE html>
    <?php
    require_once '../model/EntiteClass.php';
    ?>
<html>
    <head>
        <title>création des produits produit</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <p>Page permettant de créer les produits</p>
        <form method="POST" action="../controller/entiteController.php">
    <label>Categorie : </label>  </label> <select  name="categorie" />
    <?php
    $tabCategories = Entite::obtenirCategories();
        for($i=0;$i<count($tabCategories);$i++){
            $chaineEspace = "";
            $niveau = $tabCategories[$i][5];
            if($niveau != 0){
            //str_pad($chaineEspace,$niveau*2,".");
            $chaineEspace.=str_repeat("&nbsp;",$niveau*3);
            $categories = $chaineEspace . "|_". $tabCategories[$i][3];
            }
            else{
               $categories = $tabCategories[$i][3];
            }
        echo "<option value='".$tabCategories[$i][1]."'>".$categories."</option>";
        
    }
    ?>
    </select><span class="erreur" ></span><br/>
    <label>Description : </label> <input type="text" name="description"/><span class="erreur" ></span><br/>
    <label>Nom : </label> <input type="text" name="nom" id="nom"/><span class="erreur" ></span><br/>
    <?php
    $tabCriteres = Entite::obtenirCriteres();
    echo "<input name = 'nbCriteres' id='nbCriteres' value='".count($tabCriteres)."' hidden=true />";
    for($i=0;$i<count($tabCriteres);$i++){
        echo "<label>".$tabCriteres[$i][2]."</label> <input type='text' id='critere".$tabCriteres[$i][1]."' name = 'critere".$tabCriteres[$i][1]."' class='critere' /> <span class='erreur' ></span> <br/>";
        
    }
    
    ?>
    <input type="submit" id="submit" value="Création" name="creerProduit" disabled = true/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="./dynamique/verification_creerProduitView.js" ></script>
</form>
    </body>
</html>
