<html>
<head>
    
</head>
<body>
    <script src="controller/js/bmwatch_fiche.js"></script>
    <div class="container-fluid nav-hidden" id="content" style="max-width: 1100px">  
        <div id="main">
            <div class="container-fluid ">
                <div class="page-header">
                    <div class="pull-left">
                        <h1>Fiche </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <form action="formulaire-fiche" method="POST" id="formModificationProfil" class='form-horizontal form-striped form-validate'>
                            <div class="box box-color box-bordered">
                                <div class="box-title">
                                    <h3>
                                        <i class="fa fa-user"></i>
                                        Info entite
                                    </h3>
                                    <div class="actions">
                                        <a href="#" class="btn btn-mini content-slideUp">
                                        <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="form-group">
                                        <label for="nomEntite" class="control-label col-sm-2 right " ><?=$voc_Nom?> :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nomEntite" id="nomEntite" value="<?=$default_nom?>" class='form-control' >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="text" class="control-label col-sm-2 right"><?=$voc_categorie?> :</label>
                                        <div class="col-sm-10">
                                            <select  name="categorie" id="categorie" data-rule-required="true" />
                                                
                                                <?php
                                                    $tabCategories = Entite::obtenirCategories($type);
                                                    for($i=0;$i<count($tabCategories);$i++){
                                                        $chaineEspace = "";
                                                        $niveau = $tabCategories[$i][5];
                                                        if($niveau != 0){
                                                            $chaineEspace.=str_repeat("&nbsp;",$niveau*5);
                                                            $categories = $chaineEspace . "|_". $tabCategories[$i][3];
                                                        }
                                                        else{
                                                            $categories = $tabCategories[$i][3];
                                                        }
                                                        echo "<option value='".$tabCategories[$i][1]."'";
                                                        
                                                        if($tabCategories[$i][1] == $default_categorie){
                                                            echo "selected";
                                                        }
                                                        echo ">".$categories."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="dateCreation" class="control-label col-sm-2 right">Date d inscription :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="dateCreation" id="dateCreation" value="<?=$default_dateCreation?>" class='form-control' disabled = 'true' >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="dateModif" class="control-label col-sm-2 right">Date dernière modif :</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="dateModif" id="dateModif" value="<?=$default_dateDerniereModif?>" class='form-control' disabled = 'true' >
                                        </div>
                                    </div>
                                    <div id="infoEntreprise" <?=$cacheEntreprise?> >
                                        <div class="form-group">
                                            <label for="formeSociale" class="control-label col-sm-2 right"><?=$voc_formeSociale?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="formeSociale" id="formeSociale" value="<?=$default_formeSociale?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="secteurActivite" class="control-label col-sm-2 right"><?=$voc_secteurActivite?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="secteurActivite" id="secteurActivite" value="<?=$default_secteurActivite?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="technologieUtilisees" class="control-label col-sm-2 right"><?=$voc_technologiesUtilisees?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="technologieUtilisees" id="technologieUtilisees" value="<?=$default_technologieUtilisees?>" class='form-control' data-rule-stringCorrect="true" >
                                            </div>
                                        </div>
                                    </div>
                                    <div id="infoProduit" <?=$cacheProduit?> >
                                        <div class="form-group">
                                            <label for="nomEntreprise" class="control-label col-sm-2 right"><?=$voc_entreprise?> :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="nomEntreprise" id="nomEntreprise" value="<?=$default_nomEntreprise?>" class='form-control' disabled = 'true'>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="ranking" class="control-label col-sm-2 right">Ranking :</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="ranking" id="ranking" value="<?=$default_ranking?>" class='form-control' disabled = 'true' >
                                            </div>
                                        </div>          
                                    </div>
                                    <div class="form-group">
                                        <label for="description" class="control-label col-sm-2 right">Description</label>
                                        <div class="col-sm-10">
                                            <textarea style="resize:none" name="description" id="description"  resize=none class='form-control' ><?=$default_description?></textarea>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div id='infoMultiples' <?php if($submit == 'n') echo "hidden=true";?> >
                                <div class="box box-color box-bordered">
                                    <div class="box-title">
                                        <h3>
                                            <i class="fa fa-user"></i>
                                            Criteres
                                        </h3>
                                        <div class="actions">
                                                <a href="#" class="btn btn-mini content-slideUp">
                                                        <i class="fa fa-angle-down"></i>
                                                </a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                            <?php
                                                $tabCriteresEntite = Entite::obtenirCriteres();
//                                                $tabCriteres[$i][1]=$row['CRITERE_ID'];            
//                                                $tabCriteres[$i][2]=$row['CRITERE_NOM'];
//                                                $tabCriteres[$i][3]=$row['CV_TYPE'];
//                                                $tabCriteres[$i][4]=$row['CV_ID'];
//                                                $tabCriteres[$i][5]=$row['CRITERE_VALEUR'];
                                                echo "<input name = 'nbCriteres' id='nbCriteres' value='" . count($tabCriteresEntite) . "' hidden=true />";
                                                for ($i = 0; $i < count($tabCriteresEntite); $i++) {
                                                    if($tabCriteresEntite[$i][3]==$type || $tabCriteresEntite[$i][3]==NULL){
                                                    $tabValeurs = Entite::obtenirValeursCritere($tabCriteresEntite[$i][1]);
                                                    echo "<div class='form-group'>
                                                    <input name = 'typeCritere" . $tabCriteresEntite[$i][1] . "' id='typeCritere" . $tabCriteresEntite[$i][1] . "' value='" . $tabValeurs[0][2] . "' hidden=true />
                                                    <input name = 'valeurCritere" . $tabCriteresEntite[$i][1] . "' id='valeurCritere" . $tabCriteresEntite[$i][1] . "' value='" . $tabValeurs[0][1] . "' hidden=true />
                                                    <label for='text' class='control-label col-sm-2'>" . $tabCriteresEntite[$i][2] . "*</label>";
                                                    switch ($tabValeurs[0][2]) {
                                                        case "NUMERIQUE":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critere" . $tabCriteresEntite[$i][1] . "' name = 'critere" . $tabCriteresEntite[$i][1] . "' value ='";if(isset($tabCriteres[$i][5])) echo$tabCriteres[$i][5]; echo"' class='critere form-control' data-rule-maxlength='50' data-rule-intCorrect='true' data-rule-required='true' />
                                                                </div>";
                                                            break;

                                                        case "CLEF":
                                                            echo "<div class='col-sm-10'>
                                                                   <input type='text' id='critere" . $tabCriteresEntite[$i][1] . "' name = 'critere" . $tabCriteresEntite[$i][1] . "' value ='";if(isset($tabCriteres[$i][5])) echo$tabCriteres[$i][5]; echo"'  class=' tagsinput critere form-control' value='PHP,JQUERY'  />
                                                                </div>";
                                                            break;

                                                        default:
                                                            echo "<div class='col-sm-10'>
                                                                   <select  name='critere" . $tabCriteresEntite[$i][1] . "' id='critere" . $tabCriteresEntite[$i][1] . "' class ='critere' data-rule-required='true'/>
                                                                      <option value=''>-- Chose one --</option>";
                                                            for ($j = 0; $j < count($tabValeurs); $j++) {
                                                                echo "<option value='" . $tabValeurs[$j][1] . "'";
                                                                if(isset($tabCriteres[$i][4])&&$tabCriteres[$i][4]==$tabValeurs[$j][1]){
                                                                    echo "selected";
                                                                }
                                                                echo ">" . $tabValeurs[$j][2] . "</option>";
                                                            }
                                                            echo "</select>
                                                                </div> ";
                                                    }
                                                    echo "</div>";
                                                }}?>
                                    </div>
                                </div>
                                <div class="box box-color box-bordered">
                                    <div class="box-title">
                                        <h3>
                                            <i class="fa fa-user"></i>
                                            BM
                                        </h3>
                                        <div class="actions">
                                                <a href="#" class="btn btn-mini content-slideUp">
                                                        <i class="fa fa-angle-down"></i>
                                                </a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class='form-group'>
                                            <label for='profils' class='control-label col-sm-2 right'><?=$voc_BMs?> :</label>
                                            <div class='col-sm-10'>
                                                <select  id='profils' name='profils' class ='choix' />                                                
                                                    <?php for($j=0;$j<count($tabProfil);$j++){ ?>
                                                        <option value="<?=$tabProfil[$j][1]?>"
                                                        <?php if($BM->estProfil()){
                                                            if($BM->getId() == $tabProfil[$j][1]){
                                                                echo " selected";
                                                            }

                                                        } ?>
                                                        ><?=$tabProfil[$j][2]?></option>
                                                    <?php } ?>

                                                    <option value='<?=$default_BMAutre?>'
                                                    <?php
                                                        if(!$BM->estProfil()){
                                                            echo " selected";    
                                                        }

                                                    ?>
                                                    ><?=$voc_autre?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="panel-group panel-widget" id="ac4">
                                            <div class="panel panel-default blue">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a href="#c11" data-toggle="collapse" data-parent="#ac4">
                                                            Information sur la BM
                                                        </a>
                                                    </h4>
                                                </div>
                                                <!-- /.panel-heading -->
                                                <div id="c11" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label for="nomBM" class="control-label col-sm-2 right"><?=$voc_BMNom?> :</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="nomBM" id="nomBM" value="<?=$default_nomBm?>" class='form-control' data-rule-stringCorrect="true" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="principeBM" class="control-label col-sm-2 right"><?=$voc_BMPrincipe?> :</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="principeBM" id="principeBM" value="<?=$default_principe?>" class='form-control' data-rule-stringCorrect="true"  >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="declinaisonBM" class="control-label col-sm-2 right"><?=$voc_BMDeclinaison?> :</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="declinaisonBM" id="declinaisonBM" value="<?=$default_declinaison?>" class='form-control' data-rule-stringCorrect="true"  >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="variantesBM" class="control-label col-sm-2 right"><?=$voc_BMVariante?> :</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="variantesBM" id="variantesBM" value="<?=$default_variantes?>" class='form-control' data-rule-stringCorrect="true"  >
                                                            </div>
                                                        </div>                                                               
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel  blue">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a href="#c12" data-toggle="collapse" data-parent="#ac4">
                                                            Occurence
                                                        </a>
                                                    </h4>
                                                </div>
                                                <!-- /.panel-heading -->
                                                <div id="c12" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php for($i=1;$i<=count($tabDimension);$i++){
                                                            $tabOcc = BM::obtenirOccurenceAvecDim($tabDimension[$i][1]); ?>
                                                        <div class='form-group'>
                                                            <label for='boite<?=$i?>' class='control-label col-sm-2 right'><?=$tabDimension[$i][2]?>:</label>
                                                            <div class='col-sm-10'>

                                                                <select  id='boite<?=$i?>' name='boite<?=$i?>' class ='choixDim' nb='<?=count($tabOcc)?>'/>
                                                                <option value='null' id="boite<?=$i?>Occ">Non connu</option>
                                                                    <?php for($j=0;$j<count($tabOcc);$j++){ ?>
                                                                        <option id="boite<?=$i?>Occ<?=$tabOcc[$j][1]?>" value="<?=$tabOcc[$j][1]?>" ><?=$tabOcc[$j][3]?></option>  
                                                                    <?php } ?>
                                                                    <option value='autre' id="boite<?=$i?>OccAutre"><?=$voc_autre?></option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class='form-group'>
                                                            <label for='commentaireboite<?=$i?>' class='control-label col-sm-2 right'>Commentary:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="commentaireboite<?=$i?>" id="commentaireboite<?=$i?>" value="" class='form-control' data-rule-stringCorrect="true"  >
                                                            </div>                                                            
                                                        </div>    
                                                        <?php } ?>
                                                    </div>
                                                    <input type="hidden" id="nbDim" name="nbDim" value="<?= count($tabDimension) ?>" >
                                                    <input type="hidden" id="nbCriteres" name="nbCriteres" value="<?= count($tabCriteres) ?>" >
                                                    <input type="hidden" id="idAncienBm" name="idAncienBm" value="<?= $BM->getId() ?>" >
                                                    <input type="hidden" id="idEntite" name="idEntite" value="<?= $id ?>" >

                                                </div>
                                            </div>
                                            <?php if($commentaire){?>
                                            <div class="form-group" >
                                                <label for="commentaire" class="control-label col-sm-2 right">Commentary:</label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="commentaire" id="commentaire" class='form-control' data-rule-stringCorrect="true"  >
                                                </div>
                                            </div>                  
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary" name="<?= $submit ?>"><?= $buttonSubmit ?></button>
                                <?php if($direct){?>
                                    <button type="submit" class="btn btn-primary" name="<?= $submitEnvoyer ?>"><?= $buttonSubmitEnvoyer ?></button>
                                <?php } if($statut=="modo"){?>
                                    <button type="submit" class="btn btn-primary" name="<?= $submitValider ?>"><?= $buttonSubmitValider ?></button>
                                <?php } ?>
                            </div>
                        </form>
                        <p>
                            <a href="javascript:history.go(-1)"><input type="button" class="btn btn-red" value="Back"></a>
                        </p> 
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>

</html>
    




